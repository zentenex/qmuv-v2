export class TimedUpandGo {
  fs = 100; //frecuencia de muestre en Hz
  filter_coef = [0.00044532, 0.00066801, 0.000937,   0.00125597, 0.00162827, 0.00205683,
      0.00254407, 0.00309183, 0.00370129, 0.0043729,  0.00510634, 0.00590045,
      0.00675321, 0.00766171, 0.00862214, 0.00962982, 0.01067917, 0.01176381,
      0.01287658, 0.01400959, 0.01515433, 0.01630178, 0.01744245, 0.01856658,
      0.01966419, 0.02072527, 0.02173987, 0.02269828, 0.02359112, 0.02440952,
      0.02514522, 0.0257907,  0.02633928, 0.02678525, 0.02712392, 0.02735172,
      0.02746623, 0.02746623, 0.02735172, 0.02712392, 0.02678525, 0.02633928,
      0.0257907, 0.02514522, 0.02440952, 0.02359112, 0.02269828, 0.02173987,
      0.02072527, 0.01966419, 0.01856658, 0.01744245, 0.01630178, 0.01515433,
      0.01400959, 0.01287658, 0.01176381, 0.01067917, 0.00962982, 0.00862214,
      0.00766171, 0.00675321, 0.00590045, 0.00510634, 0.0043729, 0.00370129,
      0.00309183, 0.00254407, 0.00205683, 0.00162827, 0.00125597, 0.000937,
      0.00066801, 0.00044532]; //coeficientes del filtro FIR pasa bajo de 0.5 Hz 

  ax = []; //arreglo de aceleraciones en el eje x o laterales
  ay = []; //arreglo de aceleraciones en el eje y o verticales
  az = []; //arreglo de aceleraciones en el eje z o antero-posteriores
  gx = []; //arreglo de velocidades angulares del eje x o sagitales
  gy = []; //arreglo de velocidades angulares del eje y o coronales
  gz = []; //arreglo de velocidades angulares del eje z o frontales
  qw = []; //arreglo con el cuaternion real o rotacional
  qx = []; //arreglo con el cuaternion imaginario x 
  qy = []; //arreglo con el cuaternion imaginario y
  qz = []; //arreglo con el cuaternion imaginario z
  ax_test =[];
  ay_test =[];
  az_test =[];
  gx_test =[];
  gy_test =[];
  gz_test =[];
  inclination_test =[];
  rotation_test =[];
  inclination_ = []; //arreglo con la inclinación del tronco
  rotation_ = []; //arreglo con la rotación del tronco
  max_inclination = []; //arreglo con los puntos de máxima inclinación
  max_rotation = []; //arreglo con los puntos de rotación maxima en ambos sentidos
  inclination_f = []; //arreglo con la inclinación del tronco filtrada (señal a graficar)
  rotation_f = []; //arreglo con la rotación del tronco filtrada (señal a graficar)
  az_f = [];
  gy_f = [];
  az_n = [];
  gy_n = [];
  events = []; //arreglo con los puntos en donde se realiza la división de cada sub-tarea en segundos
  test_number = 0; //medición -1
  analysis_mode = 0; //tipo de analisis 0 = mejor tiempo, 1 = peor tiempo, 2 = promedio
  eval_ok = 0; //estado que indica si la medición fue correcta, si eval_ok = 0 repetir medición y no se puede generar el reporte 

  events_array = [];
  d_stand_array = []; 
  d_sit_array = [];
  d_go_array = [];
  d_turn1_array = [];
  d_back_array = [];
  d_turn2_array = [];
  d_test_array = [];
  max_stand_inclination_array = [];
  max_sit_inclination_array = [];
  cadence_go_array = [];
  cadence_return_array = [];
  stand_acc_max_array = [];
  sit_acc_max_array = [];
  turn1_vel_array = []; 
  turn2_vel_array = []; 


  d_stand = 0; //duración evento de levantarse en la silla en segundos (reporte)
  d_sit = 0; //duración evento de sentarse en la silla en segundos (reporte)
  d_go = 0; //duración evento de ida en segundos (reporte)
  d_turn1 = 0; //duración evento de giro a los 3 metros en segundos (reporte)
  d_back = 0; //duración evento de vuelta en segundos (reporte)
  d_turn2 = 0; //duración evento de giro en la silla en segundos (reporte)
  d_test = 0; //duración de la prueba en total en segundos (reporte)
  rof = 'none'; // riesgo de caída (reporte diagnóstico)
  max_stand_inclination = 0; //inclinación máxima al pararse
  max_sit_inclination = 0; //inclinación máxima al sentarse
  cadence_go = 0; //cantidad pasos ida step/min
  cadence_return = 0; //cantidad pasos vuelta step/min
  stand_acc_max = 0; //aceleración al pararse en m/s2
  sit_acc_max = 0; //aceleración al sentarse en m/s2
  turn1_vel = 0; //velocidad angular en el primer giro °/s
  turn2_vel = 0; //velocidad angular en el segundo giro °/s
  rof_stand = 0; //riesgo de caida durante la fase de pararse
  rof_go = 0; //riesgo de caida durante la fase de marcha de ida
  rof_turn1 = 0; //riesgo de caida en la fase del primer giro
  rof_return = 0; //riesgo de caida durante la fase de marcha de regreso
  rof_turn2 = 0; //riesgo de caida durante la fase del giro antes de sentarse
  rof_sit = 0; //riesgo de caída durante la fase de sentarse


  conditioning() { //acondicionamiento de las señales y generación de las señales de inclinación y rotación
      this.inclination_ = []; //arreglo con la inclinación del tronco
      this.rotation_ = []; //arreglo con la rotación del tronco
    
      let q_inv = [this.qw[0],-this.qx[0],-this.qy[0],-this.qz[0]];
        
      /*for (var i = 0; i < 100; i++) {
        this.qw[i] =  this.qw[100];
        this.qx[i] =  this.qx[100];
        this.qy[i] =  this.qy[100];
        this.qz[i] =  this.qz[100];
      }*/
    
      for (var i = 0; i < this.qw.length; i++) {
        let q_ref = this.q_mult(q_inv,[this.qw[i],this.qx[i],this.qy[i],this.qz[i]]);
        this.rotation_.push(Math.abs(this.quat2YPR(q_ref)[0]));
        this.inclination_.push(Math.abs(this.quat2YPR(q_ref)[2]));
      }

      for(var i = 0; i<this.ax.length; i++){
        this.ax[i] = this.ax[i] - this.ax[0]; 
      }
      for(var i = 0; i<this.ay.length; i++){
        this.ay[i] = this.ay[i] - this.ay[0]; 
      }
      for(var i = 0; i<this.az.length; i++){
        this.az[i] = this.az[i] - this.az[0]; 
      }

  }
  subtask_points(){//filtrado de las señales, obtención de señal derivada y de los puntos de inicio del algoritmo de búsqueda
      this.max_inclination = []; //arreglo con los puntos de máxima inclinación
      this.max_rotation = []; //arreglo con los puntos de rotación maxima en ambos sentidos
      this.az_n = [];
      this.gy_n = [];

      this.inclination_f = this.filter_signal(this.inclination_,this.filter_coef);
      this.rotation_f = this.filter_signal(this.rotation_,this.filter_coef);

      console.log("data");
      console.log(this.rotation_f);
      console.log(this.inclination_f);

      this.az_f = this.filter_signal(this.az,this.filter_coef)
      this.gy_f = this.filter_signal(this.gy.map( s => Math.abs(s)),this.filter_coef)

      var max_az = Math.max(...this.az_f.map(a => Math.abs(a)));
      var max_gy = Math.max(...this.gy_f.map(a => Math.abs(a)));

      var i = 0;
      
      for(i = 0; i<this.az_f.length; i++){
          this.az_n.push(this.az_f[i]/max_az);
          this.gy_n.push(this.gy_f[i]/max_gy);
      }

      let aux_vector = [];
      let maxi_rotation = []

      for(i = 0; i<this.gy_n.length; i = i+100){
          if((i+100)<this.gy_n.length){
              aux_vector = this.gy_n.slice(i,i+100)
          }
          else{
              aux_vector = this.gy_n.slice(i,this.gy_n.length)
          }
          maxi_rotation.push(aux_vector.indexOf(Math.max(...aux_vector))+i);
      }
      
      var pos1 = 0;
      var val1 = 0;
      for(i = 0; i<maxi_rotation.length; i++){
          if(this.gy_n[maxi_rotation[i]]>val1){
              val1 = this.gy_n[maxi_rotation[i]];
              pos1 = maxi_rotation[i];
          }
      }
      var pos2 = 0
      var val2 = 0
      for(i = 0; i<maxi_rotation.length; i++){
          if(this.gy_n[maxi_rotation[i]]>val2 && this.gy_n[maxi_rotation[i]]<val1 && Math.abs(maxi_rotation[i]-pos1)>this.fs){
              val2 = this.gy_n[maxi_rotation[i]];
              pos2 = maxi_rotation[i];
          }
      }
      
      if (pos1 > pos2){
          this.max_rotation = [pos2,pos1];
      }
      else{
          this.max_rotation = [pos1,pos2];
      }

      aux_vector = this.az_n.slice(0,this.max_rotation[0]);
      this.max_inclination.push(aux_vector.indexOf(Math.max(...aux_vector)));
      aux_vector = this.az_n.slice(this.max_rotation[1], this.az_n.length);
      this.max_inclination.push(aux_vector.indexOf(Math.max(...aux_vector))+this.max_rotation[1]);

      console.log(this.max_rotation);
      console.log(this.max_inclination);


    }
    subtask_events(){ //algoritmo de segmentación automática
      this.events = []; //arreglo con los puntos en donde se realiza la división de cada sub-tarea en segundos
      let aux_vector = []
      var aux_cont = 0;
      var i = 0;
      aux_vector = this.az_n.slice(0,this.max_inclination[0]);
      
      for(i = aux_vector.length-1; i>0; i--){
        if (aux_vector[i] <=0.3 || aux_vector[i]-aux_vector[i-1] <= 0){
          this.events.push((this.max_inclination[0]-aux_cont)/this.fs);
          this.events.push((this.max_inclination[0]+aux_cont)/this.fs);
          break;
        } 
        aux_cont++;
      }
  
      aux_cont = 0;
      aux_vector = this.gy_n.slice(0,this.max_rotation[0]);
      for(i = aux_vector.length-1; i>0; i--){
        if (aux_vector[i] <=0.3){
          this.events.push((this.max_rotation[0]-aux_cont)/this.fs);
          break;
        } 
        aux_cont++;
      }
  
      aux_cont = 0;
      aux_vector = this.gy_n.slice(this.max_rotation[0],this.gy_n.length);
      for(i = 0; i<aux_vector.length; i++){
        if (aux_vector[i] <=0.3){
          this.events.push((this.max_rotation[0]+aux_cont)/this.fs);
          break
        } 
        aux_cont++;
      }
  
      aux_cont = 0;
      aux_vector = this.gy_n.slice(0,this.max_rotation[1]);
      for(i = aux_vector.length-1; i>0; i--){
        if (aux_vector[i] <=0.3){
          this.events.push((this.max_rotation[1]-aux_cont)/this.fs);
          break;
        } 
        aux_cont++;
      }
  
      aux_cont = 0;
      aux_vector = this.gy_n.slice(this.max_rotation[1],this.gy_n.length);
      for(i = 0; i<aux_vector.length-1; i++){
        if (aux_vector[i] <=0.3){
          this.events.push((this.max_rotation[1]+aux_cont)/this.fs);
          break;
        } 
        aux_cont++;
      }
  
      aux_cont = 0;
      aux_vector = this.az_n.slice(this.max_inclination[1],this.az_n.length);
      for(i = 0; i<aux_vector.length-1; i++){
        if (aux_vector[i] <=0.3 || aux_vector[i]-aux_vector[i+1] <= 0){
          this.events.push((this.max_inclination[1]-aux_cont)/this.fs);
          this.events.push((this.max_inclination[1]+aux_cont)/this.fs);
          break;
        } 
        aux_cont++;  
      }
  
    }

  filter_signal(signal, filter){
      
      let delay = (0.5*(filter.length-1)/this.fs);
      let filt_data = [];
      let zeros = Array(Math.trunc(delay*100)+1).fill(0);
      let zeros_aux = zeros.concat(signal);
      signal = zeros_aux.concat(zeros);
      let aux_array = signal.slice(0,filter.length);
      
      for (var i = filter.length; i < signal.length; i++){
        filt_data.push(this.DotProduct(filter,aux_array));
        aux_array = aux_array.slice(1,filter.length);
        aux_array.push(signal[i]);
      }
      for (i = 0; i < filt_data.length; i++){
        filt_data[i] = filt_data[i] - filt_data[0]
      }
      return filt_data;
    }
  
  q_rotation(v,q){
    let v3D = [(1-2*(q[2]*q[2])-2*(q[3]*q[3]))*v[0]+(2*(q[1]*q[2]+q[0]*q[3]))*v[1]+(2*(q[1]*q[3]-q[0]*q[2]))*v[2],
           (2*(q[1]*q[2]-q[0]*q[3]))*v[0]+(1-2*(q[1]*q[1])-2*(q[3]*q[3]))*v[1]+(2*(q[2]*q[3]+q[0]*q[1]))*v[2],
           (2*(q[1]*q[3]+q[0]*q[2]))*v[0]+(2*(q[2]*q[3]-q[0]*q[1]))*v[1]+(1-2*(q[1]*q[1])-2*(q[2]*q[2]))*v[2]]
    return v3D
  }

  q_mult(q1,q2){ //multiplicación de cuaterniones
      let w1 = q1[0];
      let x1 = q1[1];
      let y1 = q1[2];
      let z1 = q1[3];
      let w2 = q2[0];
      let x2 = q2[1];
      let y2 = q2[2];
      let z2 = q2[3];
      let w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2;
      let x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2;
      let y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2;
      let z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2;
      return [w, x, y, z];
  }
  quat2YPR(q){
      let w = q[0];
      let x = q[1];
      let y = q[2];
      let z = q[3];
      let yaw  = Math.atan2(2*y*w + 2*x*z, 1 - 2*y*y - 2*z*z);
      let roll = Math.atan2(2*x*w + 2*y*z, 1 - 2*x*x - 2*z*z);
      let pitch = 0;

      if((2*x*y + 2*z*w > -1) && (2*x*y + 2*z*w < 1)){
        pitch   = Math.asin(2*x*y + 2*z*w);
      }
          
      else if(2*x*y + 2*z*w > 1){
        pitch   = Math.asin(1);
      }
          
      else if(2*x*y + 2*z*w < -1){
        pitch   = Math.asin(-1)
    }
  
      return [yaw*180.0/Math.PI, pitch*180.0/Math.PI, roll*180.0/Math.PI];
  
  }

  DotProduct(a, b){
  let sum = 0;
  /* Dot product is the sum of the products of the corresponding entries of two vectors.*/
  for(var i = 0; i < a.length; i = i + 1){
    sum = sum + a[i]*b[i];
  }
  return sum;
  }
  
  CumulativeSum(a){
  let s = 0;

  for(var i = 0; i < a.length; i = i + 1){
    s = s + a[i];
    }
      
      return s;
  }

  test_ok(){
    if(this.events.length == 8 && this.events[0]<this.events[1] && this.events[1]<this.events[2] && this.events[2]<this.events[3] && this.events[3]<this.events[4] && this.events[4]<this.events[5] && this.events[6]<this.events[7]){
      this.eval_ok = 1
      this.ax_test.push(this.ax);
      this.ay_test.push(this.ay);
      this.az_test.push(this.az);
      this.gx_test.push(this.gx);
      this.gy_test.push(this.gy);
      this.gz_test.push(this.gz);
      this.inclination_test.push(this.inclination_f);
      this.rotation_test.push(this.rotation_f);
      this.events_array.push(this.events)

    }
    else{
      this.eval_ok = 0
    }
  }
  
  subtask_duration(){ //obtención de la duración de cada una de las sub-tareas
      
      this.d_stand_array.push(this.events[1]-this.events[0]);
      this.d_go_array.push(this.events[2]-this.events[1]);
      this.d_turn1_array.push(this.events[3]-this.events[2]);
      this.d_back_array.push(this.events[4]-this.events[3]);
      this.d_turn2_array.push(this.events[5]-this.events[4]);
      this.d_sit_array.push(this.events[7]-this.events[6]);
      this.d_test_array.push(this.events[7]-this.events[0]);
  }

  subtask_movility(){
      this.max_stand_inclination_array.push(this.inclination_f[this.max_inclination[0]]); 
      this.max_sit_inclination_array.push(this.inclination_f[this.max_inclination[1]]);
      
      let aux_ax = this.ax.slice(this.events[0]*this.fs,this.events[1]*this.fs);
      let aux_ay = this.ay.slice(this.events[0]*this.fs,this.events[1]*this.fs);
      let aux_az = this.az.slice(this.events[0]*this.fs,this.events[1]*this.fs);
      var aux_max = 0;
      var max = 0;
      var i = 0;
      for(i = 0; i<aux_ax.length; i++){
        aux_max = Math.sqrt(aux_ax[i]*aux_ax[i]+aux_ay[i]*aux_ay[i]+aux_az[i]*aux_az[i]);
        if (aux_max >= max){
          max = aux_max;
        } 
      }
      this.stand_acc_max_array.push(max); 
      aux_ax = this.ax.slice(this.events[6]*this.fs,this.events[7]*this.fs);
      aux_ay = this.ay.slice(this.events[6]*this.fs,this.events[7]*this.fs);
      aux_az = this.az.slice(this.events[6]*this.fs,this.events[7]*this.fs);
      aux_max = 0;
      max = 0;
      for(i = 0; i<aux_ax.length; i++){
        aux_max = Math.sqrt(aux_ax[i]*aux_ax[i]+aux_ay[i]*aux_ay[i]+aux_az[i]*aux_az[i]);
        if (aux_max >= max){
          max = aux_max;
        } 
      }
      this.sit_acc_max_array.push(max); 
      this.turn1_vel_array.push(180.0/(this.events[3]-this.events[2])); 
      this.turn2_vel_array.push(180.0/(this.events[5]-this.events[4])); 
  }
  
  test_results(){
    //tipo de analisis 0 = mejor tiempo, 1 = peor tiempo, 2 = promedio
      var aux_index = 0;
      var min_prev = 1000;
      var max_prev = 0;
      if(this.analysis_mode == 0){
        for(var i = 0; i<this.d_test_array.length; i++){
          if(this.d_test_array[i] <= min_prev){
            aux_index = i;
            min_prev = this.d_test_array[i];
          }
        }
        this.d_stand = this.d_stand_array[aux_index];
        this.d_sit = this.d_sit_array[aux_index];
        this.d_go = this.d_go_array[aux_index];
        this.d_turn1 = this.d_turn1_array[aux_index];
        this.d_back = this.d_back_array[aux_index];
        this.d_turn2 = this.d_turn2_array[aux_index];
        this.d_test = this.d_test_array[aux_index];
        this.max_stand_inclination = this.max_stand_inclination_array[aux_index];
        this.max_sit_inclination = this.max_sit_inclination_array[aux_index];
        this.stand_acc_max = this.stand_acc_max_array[aux_index];
        this.sit_acc_max = this.sit_acc_max_array[aux_index];
        this.turn1_vel = this.turn1_vel_array[aux_index];
        this.turn2_vel = this.turn2_vel_array[aux_index];
      }
      else if(this.analysis_mode == 1){
        for(i = 0; i<this.d_test_array.length; i++){
          if(this.d_test_array[i] >= max_prev){
            aux_index = i;
            max_prev = this.d_test_array[i];
          }
        }
        this.d_stand = this.d_stand_array[aux_index];
        this.d_sit = this.d_sit_array[aux_index];
        this.d_go = this.d_go_array[aux_index];
        this.d_turn1 = this.d_turn1_array[aux_index];
        this.d_back = this.d_back_array[aux_index];
        this.d_turn2 = this.d_turn2_array[aux_index];
        this.d_test = this.d_test_array[aux_index];
        this.max_stand_inclination = this.max_stand_inclination_array[aux_index];
        this.max_sit_inclination = this.max_sit_inclination_array[aux_index];
        this.stand_acc_max = this.stand_acc_max_array[aux_index];
        this.sit_acc_max = this.sit_acc_max_array[aux_index];
        this.turn1_vel = this.turn1_vel_array[aux_index];
        this.turn2_vel = this.turn2_vel_array[aux_index];
      }
      else if(this.analysis_mode == 2){
        var div = this.d_test_array.length;
        this.d_stand = this.CumulativeSum(this.d_stand_array)/div;
        this.d_sit = this.CumulativeSum(this.d_sit_array)/div;
        this.d_go = this.CumulativeSum(this.d_go_array)/div;
        this.d_turn1 = this.CumulativeSum(this.d_turn1_array)/div;
        this.d_back = this.CumulativeSum(this.d_back_array)/div;
        this.d_turn2 = this.CumulativeSum(this.d_turn2_array)/div;
        this.d_test = this.CumulativeSum(this.d_test_array)/div;
        this.max_stand_inclination = this.CumulativeSum(this.max_stand_inclination_array)/div;
        this.max_sit_inclination = this.CumulativeSum(this.max_sit_inclination_array)/div;
        this.stand_acc_max = this.CumulativeSum(this.stand_acc_max_array)/div;
        this.sit_acc_max = this.CumulativeSum(this.sit_acc_max_array)/div;
        this.turn1_vel = this.CumulativeSum(this.turn1_vel_array)/div;
        this.turn2_vel = this.CumulativeSum(this.turn2_vel_array)/div;
      }
      

      //obtención del diagnostico de la prueba del timed up and go
      if(this.d_test <=10){
        this.rof = 'Sin riesgo de caída'; //etiqueta
      }
      else if(this.d_test >10 && this.d_test <=20){
        this.rof = 'Riesgo de caida'; //etiqueta
      }
      else if(this.d_test >20){
        this.rof = 'Gran riesgo de caida'; //etiqueta
      }
      if(this.d_stand > 2.8){
        this.rof_stand = 1;
      }
      if(this.d_go > 3){
        this.rof_go = 1;
      }
      if(this.turn1_vel < 100.0 || this.d_turn1 > 3.5){
        this.rof_turn1 = 1;
      }
      if(this.d_back > 3){
        this.rof_return = 1;
      }
      if(this.d_turn2 > 3.0 || this.turn2_vel < 80.0){
        this.rof_turn2 = 1;
      }
      if(this.d_sit > 3){
        this.rof_sit = 1;
      }
  } 
  other_measurement(){
    this.test_number = this.test_number + 1; //Si se quiere hacer una medición más, se debe aumentar test_number y reiniciar los procesos de medición anteriores
  }
  new_session(){
    this.ax = []; //arreglo de aceleraciones en el eje x o laterales
    this.ay = []; //arreglo de aceleraciones en el eje y o verticales
    this.az = []; //arreglo de aceleraciones en el eje z o antero-posteriores
    this.gx = []; //arreglo de velocidades angulares del eje x o sagitales
    this.gy = []; //arreglo de velocidades angulares del eje y o coronales
    this.gz = []; //arreglo de velocidades angulares del eje z o frontales
    this.qw = []; //arreglo con el cuaternion real o rotacional
    this.qx = []; //arreglo con el cuaternion imaginario x 
    this.qy = []; //arreglo con el cuaternion imaginario y
    this.qz = []; //arreglo con el cuaternion imaginario z
    this.az_f = [];
    this.gy_f = [];
    this.az_n = [];
    this.gy_n = [];
    this.ax_test =[];
    this.ay_test =[];
    this.az_test =[];
    this.gx_test =[];
    this.gy_test =[];
    this.gz_test =[];
    this.inclination_test =[];
    this.rotation_test =[];
    this.inclination_ = []; //arreglo con la inclinación del tronco
    this.rotation_ = []; //arreglo con la rotación del tronco
    this.max_inclination = []; //arreglo con los puntos de máxima inclinación
    this.max_rotation = []; //arreglo con los puntos de rotación maxima en ambos sentidos
    this.inclination_f = []; //arreglo con la inclinación del tronco filtrada (señal a graficar)
    this.rotation_f = []; //arreglo con la rotación del tronco filtrada (señal a graficar)
    this.events = []; //arreglo con los puntos en donde se realiza la división de cada sub-tarea en segundos
    this.test_number = 0; //medición -1
    this.analysis_mode = 0; //tipo de analisis 0 = mejor tiempo, 1 = peor tiempo, 2 = promedio
    this.eval_ok = 0; //estado que indica si la medición fue correcta, si eval_ok = 0 repetir medición y no se puede generar el reporte 
    
    this.events_array = [];
    this.d_stand_array = []; 
    this.d_sit_array = [];
    this.d_go_array = [];
    this.d_turn1_array = [];
    this.d_back_array = [];
    this.d_turn2_array = [];
    this.d_test_array = [];
    this.max_stand_inclination_array = [];
    this.max_sit_inclination_array = [];
    this.cadence_go_array = [];
    this.cadence_return_array = [];
    this.stand_acc_max_array = [];
    this.sit_acc_max_array = [];
    this.turn1_vel_array = []; 
    this.turn2_vel_array = []; 

    this.d_stand = 0; //duración evento de levantarse en la silla en segundos (reporte)
    this.d_sit = 0; //duración evento de sentarse en la silla en segundos (reporte)
    this.d_go = 0; //duración evento de ida en segundos (reporte)
    this.d_turn1 = 0; //duración evento de giro a los 3 metros en segundos (reporte)
    this.d_back = 0; //duración evento de vuelta en segundos (reporte)
    this.d_turn2 = 0; //duración evento de giro en la silla en segundos (reporte)
    this.d_test = 0; //duración de la prueba en total en segundos (reporte)
    this.rof = 'none'; // riesgo de caída (reporte diagnóstico)
    this.max_stand_inclination = 0; //inclinación máxima al pararse
    this.max_sit_inclination = 0; //inclinación máxima al sentarse
    this.cadence_go = 0; //cantidad pasos ida step/min
    this.cadence_return = 0; //cantidad pasos vuelta step/min
    this.stand_acc_max = 0; //aceleración al pararse en m/s2
    this.sit_acc_max = 0; //aceleración al sentarse en m/s2
    this.turn1_vel = 0; //velocidad angular en el primer giro °/s
    this.turn2_vel = 0; //velocidad angular en el segundo giro °/s
    this.rof_stand = 0; //riesgo de caida durante la fase de pararse
    this.rof_go = 0; //riesgo de caida durante la fase de marcha de ida
    this.rof_turn1 = 0; //riesgo de caida en la fase del primer giro
    this.rof_return = 0; //riesgo de caida durante la fase de marcha de regreso
    this.rof_turn2 = 0; //riesgo de caida durante la fase del giro antes de sentarse
    this.rof_sit = 0; //riesgo de caída durante la fase de sentarse
  }
  
}
