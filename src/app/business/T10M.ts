export class GaitAnalysis{
    fs = 100; //frecuencia de muestre en Hz
    filter_coef = [-0.00018286,-0.00027179,-0.00037186,-0.00047797,-0.0005828,-0.00067671,
                    -0.0007477,-0.00078149,-0.00076173,-0.00067036,-0.000488,-0.00019462,
                    0.00022985,0.00080469,0.00154758,0.00247374,0.00359507,0.00491932,
                    0.00644941,0.00818274,0.01011082,0.01221894,0.01448614,0.01688533,
                    0.01938372,0.02194342,0.02452225,0.0270748,0.0295536,0.03191041,
                    0.03409764,0.03606973,0.03778456,0.03920474,0.04029882,0.04104227,
                    0.04141826,0.04141826,0.04104227,0.04029882,0.03920474,0.03778456,
                    0.03606973,0.03409764,0.03191041,0.0295536,0.0270748,0.02452225,
                    0.02194342,0.01938372,0.01688533,0.01448614,0.01221894,0.01011082,
                    0.00818274,0.00644941,0.00491932,0.00359507,0.00247374,0.00154758,
                    0.00080469,0.00022985,-0.00019462,-0.000488,-0.00067036,-0.00076173,
                    -0.00078149,-0.0007477,-0.00067671,-0.0005828,-0.00047797,-0.00037186,
                    -0.00027179,-0.00018286] //coeficientes del filtro FIR pasa bajo de 5 Hz 
    ax = []; //arreglo de aceleraciones en el eje x o laterales
    ay = []; //arreglo de aceleraciones en el eje y o verticales
    az = []; //arreglo de aceleraciones en el eje z o antero-posteriores
    gx = []; //arreglo de velocidades angulares del eje x o sagitales
    gy = []; //arreglo de velocidades angulares del eje y o coronales
    gz = []; //arreglo de velocidades angulares del eje z o frontales
    qw = []; //arreglo con el cuaternion real o rotacional
    qx = []; //arreglo con el cuaternion imaginario x 
    qy = []; //arreglo con el cuaternion imaginario y
    qz = []; //arreglo con el cuaternion imaginario z
    ICpos = [];
    ICval = [];
    ICside = [];
    FCpos = [];
    FCval = [];
    FCside = [];
    cycles_detected = []
    first_step = []
    
    k = 1.07;
    gender = 0;
    height = 0;
    test_number = 0; //medición -1
    analysis_mode = 0; //tipo de analisis 0 = mejor tiempo, 1 = peor tiempo, 2 = promedio
    eval_ok = 0; //estado que indica si la medición fue correcta, si eval_ok = 0 repetir medición y no se puede generar el reporte
    
    ax_array = [];
    ay_array = [];
    az_array = [];
    
    ICpos_array = [];
    ICval_array = [];
    ICside_array = [];
    FCpos_array = [];
    FCval_array = [];
    FCside_array = [];
    
    v_gait_array = [];
    l_cadence_array = [];
    dv_l_cadence_array = [];
    r_cadence_array = [];
    dv_r_cadence_array = [];
    l_stridetime_array = [];
    dv_l_stridetime_array = [];
    r_stridetime_array = [];
    dv_r_stridetime_array = [];
    l_steptime_array = [];
    dv_l_steptime_array = [];
    r_steptime_array = [];
    dv_r_steptime_array = [];
    l_ssupporttime_array = [];
    dv_l_ssupporttime_array = [];
    r_ssupporttime_array = [];
    dv_r_ssupporttime_array = [];
    l_dsupporttime_array = [];
    dv_l_dsupporttime_array = [];
    r_dsupporttime_array = [];
    dv_r_dsupporttime_array = [];
    l_steplength_array = [];
    dv_l_steplength_array = [];
    r_steplength_array = [];
    dv_r_steplength_array = [];
    l_stance_array = [];
    dv_l_stance_array = [];
    r_stance_array = [];
    dv_r_stance_array = [];
    l_swing_array = [];
    dv_l_swing_array = [];
    r_swing_array = [];
    dv_r_swing_array = [];
    stance_simetry_array = [];
    swing_simetry_array = [];
    gait_simetry_array = [];
    
    //Indices temporales
    l_cadence = 0; //cadencia del pie izquierdo en pasos por minuto
    dv_l_cadence = 0; //desviación estándar de la cadencia del pie izquierdo
    r_cadence = 0; //cadencia del pie derecho en pasos por minuto
    dv_r_cadence = 0; //desviación estándar de la cadencia del pie derecho
    l_stridetime = 0; //tiempo de zancada izquierdo en  segundos
    dv_l_stridetime = 0;//desviación estándar de tiempo de zancada izquierdo
    r_stridetime = 0; //tiempo de zancada derecho en  segundos
    dv_r_stridetime = 0;//desviación estándar de tiempo de zancada derecho
    l_steptime = 0; //tiempo de paso izquierdo en  segundos
    dv_l_steptime = 0;//desviación estándar de tiempo de paso izquierdo
    r_steptime = 0; //tiempo de paso derecho en  segundos
    dv_r_steptime = 0;//desviación estándar de tiempo de paso derecho
    l_ssupporttime = 0; //tiempo de soporte simple izquierdo en  segundos
    dv_l_ssupporttime = 0;//desviación estándar de tiempo de soporte simple izquierdo
    r_ssupporttime = 0; //tiempo de soporte simple derecho en  segundos
    dv_r_ssupporttime = 0;//desviación estándar de tiempo de soporte simple derecho
    l_dsupporttime = 0;//tiempo de soporte doble izquierdo en  segundos
    dv_l_dsupporttime = 0;//desviación estándar de tiempo de soporte doble izquierdo
    r_dsupporttime = 0;//tiempo de soporte doble derecho en  segundos
    dv_r_dsupporttime = 0;//desviación estándar de tiempo de soporte doble derecho

    //Indices espaciales
    l_steplength = 0; //largo de paso izquierdo en metros
    dv_l_steplength = 0;//desviación estándar de largo de paso izquierdo
    r_steplength = 0; //largo de paso derecho en metros
    dv_r_steplength = 0;//desviación estándar de largo de paso derecho
    l_stance = 0; // duración fase de apoyo izquierdo en porcentaje
    dv_l_stance = 0;//desviación estándar de duración fase de apoyo izquierdo
    r_stance = 0; // duración fase de apoyo derecho en porcentaje
    dv_r_stance = 0;//desviación estándar de duración fase de apoyo derecho
    l_swing = 0; //duración fase de balanceo izquierdo en porcentaje
    dv_l_swing = 0;//desviación estándar de duración fase de balanceo izquierdo
    r_swing = 0; // duración fase de balanceo derecho en porcentaje
    dv_r_swing = 0;//desviación estándar de duración fase de balanceo derecho
    
    //Indices generales
    v_gait = 0; //velocodad de marcha medido en metros por segundo
    stance_simetry = 0; // simetria de la fase de apoyo
    swing_simetry = 0; // simetría de la fase de balanceo
    gait_simetry = 0; // simetria de marcha
    gait_result = 'none'; // tipo de marcha

    conditioning() { //acondicionamiento de las señales para análisis de marcha

        for(var i = 0; i<this.az.length; i++){
            this.az[i] = (this.az[i]+4)*-1;
        }
        
        var mean = this.CumulativeSum(this.az)/this.az.length;

        for(var i = 0; i<this.az.length; i++){
          this.az[i] = (this.az[i] - mean);
        }
        
        mean = this.CumulativeSum(this.ay)/this.ay.length;
        for(i = 0; i<this.ay.length; i++){
            this.ay[i] = this.ay[i] - mean;
        }
        mean = this.CumulativeSum(this.ax)/this.ax.length;
        for(i = 0; i<this.ax.length; i++){
            this.ax[i] = this.ax[i] - mean;
        }
    }

    filter_signal(signal){
        let delay = (0.5*(this.filter_coef.length-1)/this.fs);
        let filt_data = [];
        let zeros = Array(Math.trunc(delay*100)+1).fill(0);
        let aux_az = zeros.concat(signal);
        let aux_array = aux_az.slice(0,this.filter_coef.length);
        
        for (var i = this.filter_coef.length; i < aux_az.length; i++){
          filt_data.push(this.DotProduct(this.filter_coef,aux_array));
          aux_array = aux_array.slice(1,this.filter_coef.length);
          aux_array.push(aux_az[i]);
        }

        return filt_data;
      }

    DotProduct(a, b){
		let sum = 0;
		/* Dot product is the sum of the products of the corresponding entries of two vectors.*/
		for(var i = 0; i < a.length; i = i + 1){
			sum = sum + a[i]*b[i];
		}
		return sum;
    }
    
    CumulativeSum(a){
		let s = 0;

		for(var i = 0; i < a.length; i = i + 1){
			s = s + a[i];
        }
        
        return s;
    }

    step_search(){
        this.FCpos = [];
        this.FCval = [];
        this.ICpos = [];
        this.ICval = []; 

        let azc = this.filter_signal(this.az);

        let zero_area = [];
        let aux = [0,0];
        for(var i = 0; i<azc.length; i++){
            if((azc[i] > 0) && (azc[i+1] < 0)){
                aux[0] = i;
            }
            else if((azc[i] < 0) && (azc[i+1] > 0)){
                aux[1] = i;
                zero_area.push([aux[0],aux[1]]);
            }
        }

        var umbral = 0;
        var max = 0;

        for(i = 1; i<zero_area.length; i++){
            if(zero_area[i][1]-zero_area[i][0]>= max){
                max = zero_area[i][1]-zero_area[i][0];
            }
        }

        umbral = max/16.0;

        var prev_val = 0;
        var azpos = 0;
        var ICflag = 0;

        for(i = 0; i<zero_area.length; i++){
            if(ICflag){
                aux = this.az.slice(prev_val,zero_area[i][0]);
                max = -100000;
                azpos = 0;
                for(var j = 0; j<aux.length; j++){
                    if(aux[j]>= max){
                        max = aux[j];
                        azpos = j;
                    }
                }
                azpos = azpos + prev_val;
                this.ICpos.push(azpos);
                this.ICval.push(this.az[azpos]);
                ICflag = 0;
            }
            if(zero_area[i][1]-zero_area[i][0]> umbral){
                aux = this.az.slice(zero_area[i][0],zero_area[i][1]);
                var min = 100000;
                azpos = 0;
                for(j = 0; j<aux.length; j++){
                    if(aux[j]<= min){
                        min = aux[j];
                        azpos = j;
                    }
                }
                azpos = azpos + zero_area[i][0];
                if(this.ay[azpos]>=0){
                    this.FCpos.push(azpos);
                    this.FCval.push(this.az[azpos]);
                }
                else{
                    aux = this.az.slice(azpos,zero_area[i][1]);
                    let azposmod = 0;
                    min = 100000;
                    for(j = 0; j<aux.length; j++){
                        if(aux[j]<= min){
                            min = aux[j];
                            azposmod = j;
                        }
                    }
                    azposmod = azposmod + azpos;
                    this.FCpos.push(azposmod);
                    this.FCval.push(this.az[azposmod]);
                }
                ICflag = 1;
                prev_val = zero_area[i][1];
            }
        }

        console.log(this.FCpos)
        console.log(this.ICpos)
    }

    side_leg(){
        this.ICside = []
        this.FCside = []
        this.cycles_detected = []
        this.first_step = []

        if(this.ICpos.length>this.FCpos.length){
            this.ICpos = this.ICpos.slice(0,this.FCpos.length)
            this.ICval = this.ICval.slice(0,this.FCval.length)
        }
            
        else if(this.ICpos.length<this.FCpos.length){
            this.FCpos = this.FCpos.slice(0,this.ICpos.length)
            this.FCval = this.FCval.slice(0,this.ICval.length)
        }

        let aux = this.ax.slice(this.FCpos[0]+2,this.ICpos[1]);

        var maximum = -100000000
        var minimum = 100000000
        for(var j = 0; j<aux.length; j++){
            if(aux[j]>= maximum){
                maximum = j;
            }
            if(aux[j]<= minimum){
                minimum = j;
            }
        }

        if(aux[maximum]>0 && aux[minimum]>0){
            this.ICside.push(1) //left
            this.FCside.push(1) //right
        }
        else if(aux[maximum]<0 && aux[minimum]<0){
            this.ICside.push(0) //right
            this.FCside.push(0) //left
        }
                
        else if(maximum < minimum){
            this.ICside.push(0) //right
            this.FCside.push(0) //left
        }
                
        else if(maximum > minimum){
            this.ICside.push(1) //left
            this.FCside.push(1) //right
        }

            
        for(var i = 0; i<this.ICpos.length; i++){
            if(this.ICside[i]==0){
                this.ICside.push(1) //left
                this.FCside.push(1) //right
            }
            else{
                this.ICside.push(0) //right
                this.FCside.push(0) //left
            }
            
        }

        this.ICpos = this.ICpos.slice(1,this.ICpos.length-1)
        this.ICval = this.ICval.slice(1,this.ICval.length-1)
        this.FCpos = this.FCpos.slice(1,this.FCpos.length-1)
        this.FCval = this.FCval.slice(1,this.FCval.length-1)
        this.ICside = this.ICside.slice(1,this.ICside.length-1)
        this.FCside = this.FCside.slice(1,this.FCside.length-1)


        for(var i = 0; i<this.FCside.length-1; i++){
            let aux = this.FCside.slice(i,this.FCside.length-1)
            if(aux.length>4){
                if(this.FCside[i+1]!=this.FCside[i+2] && this.FCside[i+2]!=this.FCside[i+3] && this.FCside[i+3]!=this.FCside[i+4]){
                    this.cycles_detected.push([this.ICpos[i],this.FCpos[i+1],this.ICpos[i+1],this.FCpos[i+2],this.ICpos[i+2],this.FCpos[i+3],this.ICpos[i+3],this.FCpos[i+4]])
                    this.first_step.push(this.ICside[i])
                }   
            }
        }

        console.log(this.cycles_detected);
        console.log(this.first_step);
            
    }

    test_ok(){
        if(this.cycles_detected.length>0){
          this.eval_ok = 1
          this.ICside_array.push(this.ICside);
          this.FCside_array.push(this.FCside);
          this.ICpos_array.push(this.ICpos);
          this.ICval_array.push(this.ICval);
          this.FCpos_array.push(this.FCpos);
          this.FCval_array.push(this.FCval);
          this.ax_array.push(this.ax);
          this.ay_array.push(this.ay);
          this.az_array.push(this.az);
        }
        else{
          this.eval_ok = 0
        }
    }

    step_length(){
        let foot_size = 0;
        let leg_length = 0;
        if(this.gender){
            foot_size = this.height*0.147;
            leg_length = this.height*0.513;
        }
        else{
            foot_size = this.height*0.152;
            leg_length = this.height*0.521;
        }
        let aux_l = [];
        let aux_r = [];
        var h = 0;
        
        for(var i = 0; i<this.FCpos.length-1; i++){
            
            h = this.trapzoid(this.ay.slice(this.FCpos[i],this.ICpos[i+1]))
            if(this.FCside[i] == 1 && h>0){
                aux_r.push(2*Math.sqrt(2*leg_length*h-h*h)+this.k*foot_size);
                
            }
            if(this.FCside[i] == 0 && h>0){
                aux_l.push(2*Math.sqrt(2*leg_length*h-h*h)+this.k*foot_size);
            }
            
                
        }

        this.l_steplength_array.push(this.CumulativeSum(aux_l)/aux_l.length);
        this.dv_l_steplength_array.push(this.getSD(aux_l));
        this.r_steplength_array.push(this.CumulativeSum(aux_r)/aux_r.length);
        this.dv_r_steplength_array.push(this.getSD(aux_r));

        //this.v_gait_array.push((this.l_steplength_array[this.l_steplength_array.length-1]+this.r_steplength_array[this.r_steplength_array.length-1])/(this.l_steptime_array[this.l_steptime_array.length-1]+this.r_steptime_array[this.r_steptime_array.length-1]));
        this.v_gait_array.push(10/(this.az.length/this.fs));

    }

    trapzoid(v){
        var t = 1/this.fs;
        var hmax = -1000000;
        for(var j = 0; j<v.length-1;j++){
            if((t*((v[j]+v[j+1])/2)) >= hmax){
                hmax = t*((v[j]+v[j+1])/2)
            }
        }
        return hmax;
    }

    getSD(v){
        var sd = 0;
        var aux = 0;
        if(v.length > 1){
            var mean = this.CumulativeSum(v)/v.length;
            for(var i = 0; i<v.length; i++){
                aux = aux+((v[i]-mean)*(v[i]-mean));
            }
            sd = Math.sqrt(aux/(v.length-1));
        }
        return sd;
    }

    gait_index(){
        let aux_l_cadence_array = [];
        let aux_r_cadence_array = [];
        let aux_l_stridetime_array = [];
        let aux_r_stridetime_array = [];
        let aux_l_steptime_array = [];
        let aux_r_steptime_array = [];
        let aux_l_ssupporttime_array = [];
        let aux_r_ssupporttime_array = [];
        let aux_l_dsupporttime_array = [];
        let aux_r_dsupporttime_array = [];
        let aux_l_stance_array = [];
        let aux_r_stance_array = [];
        let aux_l_swing_array = [];
        let aux_r_swing_array = [];
        
        for(var i = 0; i<this.cycles_detected.length;i++){
            if(this.first_step[i] == 0){//first step right
                aux_r_cadence_array.push(60/((this.cycles_detected[i][4]-this.cycles_detected[i][2])/this.fs));
                aux_l_cadence_array.push(60/((this.cycles_detected[i][2]-this.cycles_detected[i][0])/this.fs));
                aux_r_stridetime_array.push((this.cycles_detected[i][4]-this.cycles_detected[i][0])/this.fs);
                aux_l_stridetime_array.push((this.cycles_detected[i][6]-this.cycles_detected[i][2])/this.fs);
                aux_r_steptime_array.push((this.cycles_detected[i][4]-this.cycles_detected[i][2])/this.fs);
                aux_l_steptime_array.push((this.cycles_detected[i][2]-this.cycles_detected[i][0])/this.fs);
                aux_r_ssupporttime_array.push((this.cycles_detected[i][2]-this.cycles_detected[i][1])/this.fs);
                aux_l_ssupporttime_array.push((this.cycles_detected[i][4]-this.cycles_detected[i][3])/this.fs);
                aux_r_dsupporttime_array.push((this.cycles_detected[i][1]-this.cycles_detected[i][0])/this.fs);
                aux_l_dsupporttime_array.push((this.cycles_detected[i][3]-this.cycles_detected[i][2])/this.fs);
                aux_r_stance_array.push((this.cycles_detected[i][3]-this.cycles_detected[i][0])/(this.cycles_detected[i][4]-this.cycles_detected[i][0])*100);
                aux_l_stance_array.push((this.cycles_detected[i][5]-this.cycles_detected[i][2])/(this.cycles_detected[i][6]-this.cycles_detected[i][2])*100);
                aux_r_swing_array.push((this.cycles_detected[i][4]-this.cycles_detected[i][3])/(this.cycles_detected[i][4]-this.cycles_detected[i][0])*100);
                aux_l_swing_array.push((this.cycles_detected[i][6]-this.cycles_detected[i][5])/(this.cycles_detected[i][6]-this.cycles_detected[i][2])*100);
            }
            else if(this.first_step[i] == 1){//first step left
                aux_l_cadence_array.push(60/((this.cycles_detected[i][4]-this.cycles_detected[i][2])/this.fs));
                aux_r_cadence_array.push(60/((this.cycles_detected[i][2]-this.cycles_detected[i][0])/this.fs));
                aux_l_stridetime_array.push((this.cycles_detected[i][4]-this.cycles_detected[i][0])/this.fs);
                aux_r_stridetime_array.push((this.cycles_detected[i][6]-this.cycles_detected[i][2])/this.fs);
                aux_l_steptime_array.push((this.cycles_detected[i][4]-this.cycles_detected[i][2])/this.fs);
                aux_r_steptime_array.push((this.cycles_detected[i][2]-this.cycles_detected[i][0])/this.fs);
                aux_l_ssupporttime_array.push((this.cycles_detected[i][2]-this.cycles_detected[i][1])/this.fs);
                aux_r_ssupporttime_array.push((this.cycles_detected[i][4]-this.cycles_detected[i][3])/this.fs);
                aux_l_dsupporttime_array.push((this.cycles_detected[i][1]-this.cycles_detected[i][0])/this.fs);
                aux_r_dsupporttime_array.push((this.cycles_detected[i][3]-this.cycles_detected[i][2])/this.fs);
                aux_l_stance_array.push((this.cycles_detected[i][3]-this.cycles_detected[i][0])/(this.cycles_detected[i][4]-this.cycles_detected[i][0])*100);
                aux_r_stance_array.push((this.cycles_detected[i][5]-this.cycles_detected[i][2])/(this.cycles_detected[i][6]-this.cycles_detected[i][2])*100);
                aux_l_swing_array.push((this.cycles_detected[i][4]-this.cycles_detected[i][3])/(this.cycles_detected[i][4]-this.cycles_detected[i][0])*100);
                aux_r_swing_array.push((this.cycles_detected[i][6]-this.cycles_detected[i][5])/(this.cycles_detected[i][6]-this.cycles_detected[i][2])*100);
            }
            
        }
        
        this.l_cadence_array.push(this.CumulativeSum(aux_l_cadence_array)/aux_l_cadence_array.length);
        this.dv_l_cadence_array.push(this.getSD(aux_l_cadence_array));
        this.r_cadence_array.push(this.CumulativeSum(aux_r_cadence_array)/aux_r_cadence_array.length);
        this.dv_r_cadence_array.push(this.getSD(aux_r_cadence_array));
        this.l_stridetime_array.push(this.CumulativeSum(aux_l_stridetime_array)/aux_l_stridetime_array.length);
        this.dv_l_stridetime_array.push(this.getSD(aux_l_stridetime_array));
        this.r_stridetime_array.push(this.CumulativeSum(aux_r_stridetime_array)/aux_r_stridetime_array.length);
        this.dv_r_stridetime_array.push(this.getSD(aux_r_stridetime_array));
        this.l_steptime_array.push(this.CumulativeSum(aux_l_steptime_array)/aux_l_steptime_array.length);
        this.dv_l_steptime_array.push(this.getSD(aux_l_steptime_array));
        this.r_steptime_array.push(this.CumulativeSum(aux_r_steptime_array)/aux_r_steptime_array.length);
        this.dv_r_steptime_array.push(this.getSD(aux_r_steptime_array));
        this.l_ssupporttime_array.push(this.CumulativeSum(aux_l_ssupporttime_array)/aux_l_ssupporttime_array.length);
        this.dv_l_ssupporttime_array.push(this.getSD(aux_l_ssupporttime_array));
        this.r_ssupporttime_array.push(this.CumulativeSum(aux_r_ssupporttime_array)/aux_r_ssupporttime_array.length);
        this.dv_r_ssupporttime_array.push(this.getSD(aux_r_ssupporttime_array));
        this.l_dsupporttime_array.push(this.CumulativeSum(aux_l_dsupporttime_array)/aux_l_dsupporttime_array.length);
        this.dv_l_dsupporttime_array.push(this.getSD(aux_l_dsupporttime_array));
        this.r_dsupporttime_array.push(this.CumulativeSum(aux_r_dsupporttime_array)/aux_r_dsupporttime_array.length);
        this.dv_r_dsupporttime_array.push(this.getSD(aux_r_dsupporttime_array));
        this.l_stance_array.push(this.CumulativeSum(aux_l_stance_array)/aux_l_stance_array.length);
        this.dv_l_stance_array.push(this.getSD(aux_l_stance_array));
        this.r_stance_array.push(this.CumulativeSum(aux_r_stance_array)/aux_r_stance_array.length);
        this.dv_r_stance_array.push(this.getSD(aux_r_stance_array));
        this.l_swing_array.push(this.CumulativeSum(aux_l_swing_array)/aux_l_swing_array.length);
        this.dv_l_swing_array.push(this.getSD(aux_l_swing_array));
        this.r_swing_array.push(this.CumulativeSum(aux_r_swing_array)/aux_r_swing_array.length);
        this.dv_r_swing_array.push(this.getSD(aux_r_swing_array));

        if(this.l_stance_array[this.test_number] >= this.r_stance_array[this.test_number]){
            this.stance_simetry_array.push(this.r_stance_array[this.test_number]/this.l_stance_array[this.test_number]);
        }
        else{
            this.stance_simetry_array.push(this.l_stance_array[this.test_number]/this.r_stance_array[this.test_number]);
        }

        if(this.l_swing_array[this.test_number] >= this.r_swing_array[this.test_number]){
            this.swing_simetry_array.push(this.r_swing_array[this.test_number]/this.l_swing_array[this.test_number]);
        }
        else{
            this.swing_simetry_array.push(this.l_swing_array[this.test_number]/this.r_swing_array[this.test_number]);
        }
        this.gait_simetry_array.push((this.swing_simetry_array[this.test_number]+this.stance_simetry_array[this.test_number])/2);
    }

    test_results(){
        //tipo de analisis 0 = mayor velocidad, 1 = menor velocidad, 2 = promedio
        var aux_index = 0;
        var min_prev = 1000;
        var max_prev = 0;
        if(this.analysis_mode == 1){
            for(var i = 0; i<this.v_gait_array.length; i++){
                if(this.v_gait_array[i] <= min_prev){
                    aux_index = i;
                    min_prev = this.v_gait_array[i];
                }
            }

            this.v_gait = this.v_gait_array[aux_index];
            this.l_cadence = this.l_cadence_array[aux_index];
            this.r_cadence = this.r_cadence_array[aux_index];
            this.l_stridetime = this.l_stridetime_array[aux_index];
            this.r_stridetime = this.r_stridetime_array[aux_index];
            this.l_steptime = this.l_steptime_array[aux_index];
            this.r_steptime = this.r_steptime_array[aux_index];
            this.l_ssupporttime = this.l_ssupporttime_array[aux_index];
            this.r_ssupporttime = this.r_ssupporttime_array[aux_index];
            this.l_dsupporttime = this.l_dsupporttime_array[aux_index];
            this.r_dsupporttime = this.r_dsupporttime_array[aux_index];
            this.l_steplength = this.l_steplength_array[aux_index];
            this.dv_l_steplength = this.dv_l_steplength_array[aux_index];
            this.r_steplength = this.r_steplength_array[aux_index];
            this.dv_r_steplength = this.dv_r_steplength_array[aux_index];
            this.l_stance = this.l_stance_array[aux_index];
            this.r_stance = this.r_stance_array[aux_index];
            this.l_swing = this.l_swing_array[aux_index];
            this.r_swing = this.r_swing_array[aux_index];

            this.dv_l_cadence = this.dv_l_cadence_array[aux_index];
            this.dv_r_cadence = this.dv_r_cadence_array[aux_index];
            this.dv_l_stridetime = this.dv_l_stridetime_array[aux_index];
            this.dv_r_stridetime = this.dv_r_stridetime_array[aux_index];
            this.dv_l_steptime = this.dv_l_steptime_array[aux_index];
            this.dv_r_steptime = this.dv_r_steptime_array[aux_index];
            this.dv_l_ssupporttime = this.dv_l_ssupporttime_array[aux_index];
            this.dv_r_ssupporttime = this.dv_r_ssupporttime_array[aux_index];
            this.dv_l_dsupporttime = this.dv_l_dsupporttime_array[aux_index];
            this.dv_r_dsupporttime = this.dv_r_dsupporttime_array[aux_index];
            this.dv_l_stance = this.dv_l_stance_array[aux_index];
            this.dv_r_stance = this.dv_r_stance_array[aux_index];
            this.dv_l_swing = this.dv_l_swing_array[aux_index];
            this.dv_r_swing = this.dv_r_swing_array[aux_index];

            this.stance_simetry = this.stance_simetry_array[aux_index];
            this.swing_simetry = this.swing_simetry_array[aux_index];
            this.gait_simetry = this.gait_simetry_array[aux_index];
        }

        else if(this.analysis_mode == 0){
            for(i = 0; i<this.v_gait_array.length; i++){
                if(this.v_gait_array[i] >= max_prev){
                    aux_index = i;
                    max_prev = this.v_gait_array[i];
                }
            }

            this.v_gait = this.v_gait_array[aux_index];
            this.l_cadence = this.l_cadence_array[aux_index];
            this.r_cadence = this.r_cadence_array[aux_index];
            this.l_stridetime = this.l_stridetime_array[aux_index];
            this.r_stridetime = this.r_stridetime_array[aux_index];
            this.l_steptime = this.l_steptime_array[aux_index];
            this.r_steptime = this.r_steptime_array[aux_index];
            this.l_ssupporttime = this.l_ssupporttime_array[aux_index];
            this.r_ssupporttime = this.r_ssupporttime_array[aux_index];
            this.l_dsupporttime = this.l_dsupporttime_array[aux_index];
            this.r_dsupporttime = this.r_dsupporttime_array[aux_index];
            this.l_steplength = this.l_steplength_array[aux_index];
            this.dv_l_steplength = this.dv_l_steplength_array[aux_index];
            this.r_steplength = this.r_steplength_array[aux_index];
            this.dv_r_steplength = this.dv_r_steplength_array[aux_index];
            this.l_stance = this.l_stance_array[aux_index];
            this.r_stance = this.r_stance_array[aux_index];
            this.l_swing = this.l_swing_array[aux_index];
            this.r_swing = this.r_swing_array[aux_index];

            this.dv_l_cadence = this.dv_l_cadence_array[aux_index];
            this.dv_r_cadence = this.dv_r_cadence_array[aux_index];
            this.dv_l_stridetime = this.dv_l_stridetime_array[aux_index];
            this.dv_r_stridetime = this.dv_r_stridetime_array[aux_index];
            this.dv_l_steptime = this.dv_l_steptime_array[aux_index];
            this.dv_r_steptime = this.dv_r_steptime_array[aux_index];
            this.dv_l_ssupporttime = this.dv_l_ssupporttime_array[aux_index];
            this.dv_r_ssupporttime = this.dv_r_ssupporttime_array[aux_index];
            this.dv_l_dsupporttime = this.dv_l_dsupporttime_array[aux_index];
            this.dv_r_dsupporttime = this.dv_r_dsupporttime_array[aux_index];
            this.dv_l_stance = this.dv_l_stance_array[aux_index];
            this.dv_r_stance = this.dv_r_stance_array[aux_index];
            this.dv_l_swing = this.dv_l_swing_array[aux_index];
            this.dv_r_swing = this.dv_r_swing_array[aux_index];

            this.stance_simetry = this.stance_simetry_array[aux_index];
            this.swing_simetry = this.swing_simetry_array[aux_index];
            this.gait_simetry = this.gait_simetry_array[aux_index];
        }

        else if(this.analysis_mode == 2){
            var div = this.v_gait_array.length;
            this.v_gait = this.CumulativeSum(this.v_gait_array)/div;
            this.l_cadence = this.CumulativeSum(this.l_cadence_array)/div;
            this.r_cadence = this.CumulativeSum(this.r_cadence_array)/div;
            this.l_stridetime = this.CumulativeSum(this.l_stridetime_array)/div;
            this.r_stridetime = this.CumulativeSum(this.r_stridetime_array)/div;
            this.l_steptime = this.CumulativeSum(this.l_steptime_array)/div;
            this.r_steptime = this.CumulativeSum(this.r_steptime_array)/div;
            this.l_ssupporttime = this.CumulativeSum(this.l_ssupporttime_array)/div;
            this.r_ssupporttime = this.CumulativeSum(this.r_ssupporttime_array)/div;
            this.l_dsupporttime = this.CumulativeSum(this.l_dsupporttime_array)/div;
            this.r_dsupporttime = this.CumulativeSum(this.r_dsupporttime_array)/div;
            this.l_steplength = this.CumulativeSum(this.l_steplength_array)/div;
            this.dv_l_steplength = this.CumulativeSum(this.dv_l_steplength_array)/div;
            this.r_steplength = this.CumulativeSum(this.r_steplength_array)/div;
            this.dv_r_steplength = this.CumulativeSum(this.dv_r_steplength_array)/div;
            this.l_stance = this.CumulativeSum(this.l_stance_array)/div;
            this.r_stance = this.CumulativeSum(this.r_stance_array)/div;
            this.l_swing = this.CumulativeSum(this.l_swing_array)/div;
            this.r_swing = this.CumulativeSum(this.r_swing_array)/div;

            this.dv_l_cadence = this.CumulativeSum(this.dv_l_cadence_array)/div;
            this.dv_r_cadence = this.CumulativeSum(this.dv_r_cadence_array)/div;
            this.dv_l_stridetime = this.CumulativeSum(this.dv_l_stridetime_array)/div;
            this.dv_r_stridetime = this.CumulativeSum(this.dv_r_stridetime_array)/div;
            this.dv_l_steptime = this.CumulativeSum(this.dv_l_steptime_array)/div;
            this.dv_r_steptime = this.CumulativeSum(this.dv_r_steptime_array)/div;
            this.dv_l_ssupporttime = this.CumulativeSum(this.dv_l_ssupporttime_array)/div;
            this.dv_r_ssupporttime = this.CumulativeSum(this.dv_r_ssupporttime_array)/div;
            this.dv_l_dsupporttime = this.CumulativeSum(this.dv_l_dsupporttime_array)/div;
            this.dv_r_dsupporttime = this.CumulativeSum(this.dv_r_dsupporttime_array)/div;
            this.dv_l_stance = this.CumulativeSum(this.dv_l_stance_array)/div;
            this.dv_r_stance = this.CumulativeSum(this.dv_r_stance_array)/div;
            this.dv_l_swing = this.CumulativeSum(this.dv_l_swing_array)/div;
            this.dv_r_swing = this.CumulativeSum(this.dv_r_swing_array)/div;


            this.stance_simetry = this.CumulativeSum(this.stance_simetry_array)/div;
            this.swing_simetry = this.CumulativeSum(this.swing_simetry_array)/div;
            this.gait_simetry = this.CumulativeSum(this.gait_simetry_array)/div;
          }
  
          //obtención del diagnostico de la prueba
          if(this.v_gait < 0.4){
            this.gait_result = 'Marcha intra-domiciliaria';
          }
          else if((this.v_gait >= 0.4) && (this.v_gait < 0.8)){
            this.gait_result = 'Marcha comunitaria dependiente';
          }
          else if((this.v_gait >= 0.8) && (this.v_gait < 1.2)){
            this.gait_result = 'Marcha comunitaria';
          }
          else if(this.v_gait >= 1.2){
            this.gait_result = 'Marcha segura';
          }
      } 

    other_measurement(){
        this.test_number = this.test_number + 1; //Si se quiere hacer una medición más, se debe aumentar test_number y reiniciar los procesos de medición anteriores
    }
    new_session(){
        this.gait_result = 'none';
        this.ax = []; //arreglo de aceleraciones en el eje x o laterales
        this.ay = []; //arreglo de aceleraciones en el eje y o verticales
        this.az = []; //arreglo de aceleraciones en el eje z o antero-posteriores
        this.gx = []; //arreglo de velocidades angulares del eje x o sagitales
        this.gy = []; //arreglo de velocidades angulares del eje y o coronales
        this.gz = []; //arreglo de velocidades angulares del eje z o frontales
        this.qw = []; //arreglo con el cuaternion real o rotacional
        this.qx = []; //arreglo con el cuaternion imaginario x 
        this.qy = []; //arreglo con el cuaternion imaginario y
        this.qz = []; //arreglo con el cuaternion imaginario z
        this.ICpos = [];
        this.ICval = [];
        this.FCpos = [];
        this.FCval = [];
        this.ICside = [];
        this.FCside = [];
        this.k = 1.07;
        this.gender = 0;
        this.height = 0;
        this.test_number = 0; //medición -1
        this.analysis_mode = 0; //tipo de analisis 0 = mejor tiempo, 1 = peor tiempo, 2 = promedio
        this.eval_ok = 0; //estado que indica si la medición fue correcta, si eval_ok = 0 repetir medición y no se puede generar el reporte

        this.ax_array = []; // Aceleraciones
        this.ay_array = []; // Aceleración en la que se grafican los valores de FC O CONTACTO FINAL
        this.az_array = []; // Aceleración en la que se grafican los valores de IC O CONTACTO INICIAL
        this.ICside_array = [];
        this.FCside_array = [];
        this.ICpos_array = []; // muestra en la que ocurre el eevento de contacto inicial
        this.ICval_array = []; // valor del evento de contacto inicial
        this.FCpos_array = []; // muestra en la que ocurre el evento de contacto final
        this.FCval_array = []; // valor del evento de contacto final
        
        this.v_gait_array = []; // resultados de velocidad
        this.l_cadence_array = [];
        this.r_cadence_array = [];
        this.l_stridetime_array = [];
        this.r_stridetime_array = [];
        this.l_steptime_array = [];
        this.r_steptime_array = [];
        this.l_ssupporttime_array = [];
        this.r_ssupporttime_array = [];
        this.l_dsupporttime_array = [];
        this.r_dsupporttime_array = [];
        this.l_steplength_array = [];
        this.dv_l_steplength_array = [];
        this.r_steplength_array = [];
        this.dv_r_steplength_array = [];
        this.l_stance_array = [];
        this.r_stance_array = [];
        this.l_swing_array = [];
        this.r_swing_array = [];
        this.stance_simetry_array = [];
        this.swing_simetry_array = [];
        this.gait_simetry_array = [];

        this.v_gait = 0;
        this.l_cadence = 0;
        this.r_cadence = 0;
        this.l_stridetime = 0;
        this.r_stridetime = 0;
        this.l_steptime = 0;
        this.r_steptime = 0;
        this.l_ssupporttime = 0;
        this.r_ssupporttime = 0;
        this.l_dsupporttime = 0;
        this.r_dsupporttime = 0;
        this.l_steplength = 0;
        this.dv_l_steplength = 0;
        this.r_steplength = 0;
        this.dv_r_steplength = 0;
        this.l_stance = 0;
        this.r_stance = 0;
        this.l_swing = 0;
        this.r_swing = 0;
        this.stance_simetry = 0;
        this.swing_simetry = 0;
        this.gait_simetry = 0;
    }
  
}

