import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./paginas/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./paginas/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./paginas/home/home.module').then( m => m.HomePageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'dispositivo-list',
    loadChildren: () => import('./paginas/dispositivo-list/dispositivo-list.module').then( m => m.DispositivoListPageModule)
  },
  {
    path: 'suscripcion',
    loadChildren: () => import('./paginas/suscripcion/suscripcion.module').then( m => m.SuscripcionPageModule)
  },
  {
    path: 'configuracion',
    loadChildren: () => import('./paginas/configuracion/configuracion.module').then( m => m.ConfiguracionPageModule)
  },
  {
    path: 'tutorial',
    loadChildren: () => import('./paginas/tutorial/tutorial.module').then( m => m.TutorialPageModule)
  },
  {
    path: 'portada',
    loadChildren: () => import('./paginas/portada/portada.module').then( m => m.PortadaPageModule)
  },
  {
    path: 'pacientes-eliminados',
    loadChildren: () => import('./paginas/pacientes-eliminados/pacientes-eliminados.module').then( m => m.PacientesEliminadosPageModule)
  },
  {
    path: 'pacientes',
    loadChildren: () => import('./paginas/pacientes/pacientes.module').then( m => m.PacientesPageModule)
  },
  {
    path: 'reportes',
    loadChildren: () => import('./paginas/reportes/reportes.module').then( m => m.ReportesPageModule)
  },
  {
    path: 'paciente-detalle/:id',
    loadChildren: () => import('./paginas/paciente-detalle/paciente-detalle.module').then( m => m.PacienteDetallePageModule)
  },
  {
    path: 'paciente-nuevo',
    loadChildren: () => import('./paginas/paciente-nuevo/paciente-nuevo.module').then( m => m.PacienteNuevoPageModule)
  },
  {
    path: 'olvide-clave',
    loadChildren: () => import('./paginas/olvide-clave/olvide-clave.module').then( m => m.OlvideClavePageModule)
  },
  {
    path: 'dispositivo-detalle',
    loadChildren: () => import('./paginas/dispositivo-detalle/dispositivo-detalle.module').then( m => m.DispositivoDetallePageModule)
  },
  {
    path: 'upgo',
    loadChildren: () => import('./paginas/prueba/upgo/upgo.module').then( m => m.UpgoPageModule)
  },
  {
    path: 'diez-mtr',
    loadChildren: () => import('./paginas/prueba/diez-mtr/diez-mtr.module').then( m => m.DiezMtrPageModule)
  },
  {
    path: 'upgo-resultados',
    loadChildren: () => import('./paginas/prueba/upgo-resultados/upgo-resultados.module').then( m => m.UpgoResultadosPageModule)
  },
  {
    path: 'diez-mtr-resultados',
    loadChildren: () => import('./paginas/prueba/diez-mtr-resultados/diez-mtr-resultados.module').then( m => m.DiezMtrResultadosPageModule)
  },
  {
    path: 'paciente-historial/:id',
    loadChildren: () => import('./paginas/paciente-historial/paciente-historial.module').then( m => m.PacienteHistorialPageModule)
  },
  {
    path: 'diez-mtr-historial/:id',
    loadChildren: () => import('./paginas/prueba/diez-mtr-historial/diez-mtr-historial.module').then( m => m.DiezMtrHistorialPageModule)
  },
  {
    path: 'upgo-historial/:id',
    loadChildren: () => import('./paginas/prueba/upgo-historial/upgo-historial.module').then( m => m.UpgoHistorialPageModule)
  },
  {
    path: 'seleccion-paciente',
    loadChildren: () => import('./componentes/seleccion-paciente/seleccion-paciente.module').then( m => m.SeleccionPacientePageModule)
  },
  {
    path: 'register2',
    loadChildren: () => import('./paginas/register2/register2.module').then( m => m.Register2PageModule)
  },
  {
    path: 'perfil',
    loadChildren: () => import('./paginas/perfil/perfil.module').then( m => m.PerfilPageModule)
  },
  {
    path: '',
    redirectTo: 'portada',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
