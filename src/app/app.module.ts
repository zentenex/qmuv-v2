import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';
import { StatusBar } from '@awesome-cordova-plugins/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Ionic4DatepickerModule } from  '@logisticinfotech/ionic4-datepicker';
import { BLE } from '@awesome-cordova-plugins/ble/ngx';
import { IonicStorageModule } from '@ionic/storage-angular';
import { Clipboard } from '@awesome-cordova-plugins/clipboard/ngx';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './interceptores/AuthInterceptor';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { TranslateConfigService } from './services/translate-config.service';
import { NotificacionesComponent } from './componentes/notificaciones/notificaciones.component';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideAuth,getAuth } from '@angular/fire/auth';


export function LanguageLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
    
@NgModule({
  declarations: [AppComponent, NotificacionesComponent],
  entryComponents: [NotificacionesComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot({mode: 'ios'}), //forzamos a que todo se vea como ios, sin importar si es ios o android
    AppRoutingModule,
    Ionic4DatepickerModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (LanguageLoader),
        deps: [HttpClient]
      }
    }),
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideAuth(() => getAuth())
  ],
  providers: [
    StatusBar,
    SplashScreen,
    TranslateConfigService,
    AppVersion,
    AndroidPermissions,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    BLE,
    Clipboard,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
