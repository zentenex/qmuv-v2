import { Component, OnInit } from '@angular/core';
import { PacientesDAOService, PacienteDTO } from 'src/app/dao/pacientes-dao.service';
import { FormControl } from '@angular/forms';
import { debounceTime } from "rxjs/operators";
import { UtilsService } from 'src/app/services/utils.service';
import { PacienteService } from 'src/app/services/paciente.service';
import { TranslateConfigService } from 'src/app/services/translate-config.service';

@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.page.html',
  styleUrls: ['./pacientes.page.scss'],
})
export class PacientesPage implements OnInit {

  public pacientes : PacienteDTO[] = [];
  //public searchTerm: string = "";
  public searchControl: FormControl;
  searching = false;

  constructor(private pacientesDAO: PacientesDAOService, 
    private utilsService: UtilsService, 
    private pacienteService: PacienteService,
    private translateConfigService: TranslateConfigService) {
      this.searchControl = new FormControl();
  }

  onSearchInput(){
    this.searching = true;
  }

  ngOnInit() {
    console.log("En el on init de pacientes");
    this.setFilteredItems("");
    
    this.searchControl.valueChanges
      .pipe(debounceTime(700))
      .subscribe(search => {
        console.log("a buscar");
        this.setFilteredItems(search);
      });
  }

  ionViewWillEnter() {
    let valorActual: string = this.searchControl.value;
    if(valorActual == null) valorActual = "";

    console.log("En el view will enter de paciente : " + valorActual);
    this.searching = true;
    this.setFilteredItems(valorActual);
  }

  async setFilteredItems(searchTerm: string) {
    let pacientes = await this.pacienteService.pacientesActivos(searchTerm).catch(err => {
      console.error("Error al obtener pacientes : " + JSON.stringify(err));
      this.translateConfigService.getTextoNow("PACI.acciones.filtra_no").then(txt => {
        this.utilsService.presentaToast(txt);
      });
      //this.utilsService.presentaToast("No se pudo filtrar por paciente");
    });
    this.searching = false;
    if(pacientes) {
      this.pacientes = pacientes;
    }
  }

  async elimina(idx: number) {
    let t1 = await this.translateConfigService.getTextoNow("PACI.acciones.basura_tit"); //Elimina
    let t2 = await this.translateConfigService.getTextoNow("PACI.acciones.basura_conf"); //"¿Confirma que desea eliminar a este paciente?"
    this.utilsService.confirma(t1, t2, (confirma: boolean) => {
      if(confirma) {
        let paciente = this.pacientes[idx];
        if(this.pacientesDAO.pacienteBorra(paciente)) {
          //this.utilsService.presentaToast("Paciente eliminado");
          this.translateConfigService.getTextoNow("PACI.acciones.basura").then(txt => {
            this.utilsService.presentaToast(txt);
          })
          this.pacientes.splice(idx, 1);
        } else {
          //this.utilsService.presentaToast("Paciente no eliminado");
          this.translateConfigService.getTextoNow("PACI.acciones.basura_no").then(txt => {
            this.utilsService.presentaToast(txt);
          })
        }
      }
      
    })
  }

  

}
