import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OlvideClavePageRoutingModule } from './olvide-clave-routing.module';

import { OlvideClavePage } from './olvide-clave.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OlvideClavePageRoutingModule
  ],
  declarations: [OlvideClavePage]
})
export class OlvideClavePageModule {}
