import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OlvideClavePage } from './olvide-clave.page';

describe('OlvideClavePage', () => {
  let component: OlvideClavePage;
  let fixture: ComponentFixture<OlvideClavePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OlvideClavePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OlvideClavePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
