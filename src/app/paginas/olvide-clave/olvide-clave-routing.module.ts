import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OlvideClavePage } from './olvide-clave.page';

const routes: Routes = [
  {
    path: '',
    component: OlvideClavePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OlvideClavePageRoutingModule {}
