import { Component, OnInit } from '@angular/core';
import { LoadingController, AlertController, NavController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-olvide-clave',
  templateUrl: './olvide-clave.page.html',
  styleUrls: ['./olvide-clave.page.scss'],
})
export class OlvideClavePage implements OnInit {

  email: string;

  constructor(
    private loadingController: LoadingController,
    private authenticationService: AuthenticationService,
    private alert: AlertController,
    private navCtrl: NavController,
    private router: Router) {
      this.email = this.router.getCurrentNavigation().extras.state.correo;
      console.log(this.email);
    }

  ngOnInit() {
    
  }

  gotoLogin() {
    this.navCtrl.navigateBack('/login');
  }

  async olvido(email: string) {
    const loading = await this.loadingController.create({
      message: 'Enviando...'
    });
    await loading.present();

    this.authenticationService.olvidePassword(email).then(resp => {
      if(resp == 'OK') {
        //Correo de firebase enviado
        this.presentAlert("Envio exitoso de correo electrónico.");
      } else if(resp == 'NOTFOUND') {
        //no existo en firebase, usare el recuperar clave de UMS
        this.presentAlert("No existe usuario asociado al correo electrónico suministrado.");
      } else {
        this.presentAlert(resp);
      }
      loading.dismiss();
    }, err => {
      this.presentAlert("Error interno.");
      console.error("Error : " + JSON.stringify(err));
      loading.dismiss();
    })
  }

  async presentAlert(mensaje : string) {
    const alert = await this.alert.create({
      header: 'Reestablecer contraseña',
      message: mensaje,
      buttons: ['Aceptar']
    });
    await alert.present();
  }



}
