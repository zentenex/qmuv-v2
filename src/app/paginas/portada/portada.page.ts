import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-portada',
  templateUrl: './portada.page.html',
  styleUrls: ['./portada.page.scss'],
})
export class PortadaPage implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  //Debe ir el constructor de AuthenticationService para que se ejecute la validacion de login
  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }

  gotoCrearCuenta() {
    this.navCtrl.navigateForward('/register');
  }

  gotoLogin() {
    this.navCtrl.navigateForward('/login');
  }

}
