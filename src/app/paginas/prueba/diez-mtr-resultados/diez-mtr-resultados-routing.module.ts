import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiezMtrResultadosPage } from './diez-mtr-resultados.page';

const routes: Routes = [
  {
    path: '',
    component: DiezMtrResultadosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiezMtrResultadosPageRoutingModule {}
