import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiezMtrResultadosPage } from './diez-mtr-resultados.page';

describe('DiezMtrResultadosPage', () => {
  let component: DiezMtrResultadosPage;
  let fixture: ComponentFixture<DiezMtrResultadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiezMtrResultadosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiezMtrResultadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
