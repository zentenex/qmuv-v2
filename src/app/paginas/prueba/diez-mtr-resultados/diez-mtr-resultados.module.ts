import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiezMtrResultadosPageRoutingModule } from './diez-mtr-resultados-routing.module';

import { DiezMtrResultadosPage } from './diez-mtr-resultados.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiezMtrResultadosPageRoutingModule,
    MenuInferiorModule
  ],
  declarations: [DiezMtrResultadosPage]
})
export class DiezMtrResultadosPageModule {}
