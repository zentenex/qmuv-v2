import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Analisis10MTS } from 'src/app/dao/pruebas-dao.service';
import { PacienteDTO } from 'src/app/dao/pacientes-dao.service';
import { Chart, registerables } from 'chart.js';
import annotationPlugin from 'chartjs-plugin-annotation';
import { DiezMtrService, Sesion10MTS } from 'src/app/services/prueba/diez-mtr.service';
import { UtilsService } from 'src/app/services/utils.service';
import { PacienteService } from 'src/app/services/paciente.service';
import { LoadingController, NavController, PopoverController } from '@ionic/angular';
import { Location } from '@angular/common';
import { NotificacionesComponent } from 'src/app/componentes/notificaciones/notificaciones.component';
import { environment } from 'src/environments/environment';
import { TranslateConfigService } from 'src/app/services/translate-config.service';

@Component({
  selector: 'app-diez-mtr-resultados',
  templateUrl: './diez-mtr-resultados.page.html',
  styleUrls: ['./diez-mtr-resultados.page.scss'],
})
export class DiezMtrResultadosPage implements OnInit {
  @ViewChild("lineCanvas1", { static: false }) lineCanvas1: ElementRef;
  @ViewChild("lineCanvas2", { static: false }) lineCanvas2: ElementRef;
  @ViewChild("lineCanvas3", { static: false }) lineCanvas3: ElementRef;
  private lineChart: Chart;
  slideOpts = {
    spaceBetween: 0,
    initialSlide: 0,
    speed: 400
  };

  modoDebug = false;

  glosaResaltado = "";
  destacado1 = false;
  destacado2 = false;
  destacado3 = false;

  //Tamano de los puntos de la dimension Y para los 3 posibles graficos
  private radioPuntoYArr: any[][] = [];

  //Tamano de los puntos de la dimension Z para los 3 posibles graficos
  private radioPuntoZArr: any[][] = [];

  //Color de los puntos de la dimesion Y para los 3 posibles graficos
  private colorBordePuntoYArr: any[][] = [];

  //Color de los puntos de la dimesion Z para los 3 posibles graficos
  private colorBordePuntoZArr: any[][] = [];

  //Datos de aceleracion eje X para los 3 posibles graficos
  private dataAccXArr: any[][] = [];

  //Datos de aceleracion eje Y para los 3 posibles graficos
  private dataAccYArr: any[][] = [];

  //Datos de aceleracion eje Z para los 3 posibles graficos
  private dataAccZArr: any[][] = [];

  
  loadingSave: HTMLIonLoadingElement = null;
  analisis: Analisis10MTS;
  paciente: PacienteDTO;
  sesionActual: Sesion10MTS;
  guardado = false;

  maximo(a: number, b: number) {
    return Math.max(a, b);
  }

  volver() {
    console.log("y volver volver voolver");
    this.navCtrl.navigateBack("/home");
  }

  constructor(private navCtrl: NavController, 
    public popoverCtrl: PopoverController, 
    private location: Location, 
    private diezMtsService: DiezMtrService, 
    private loadingController: LoadingController,
    private utilsService: UtilsService, 
    private pacienteService: PacienteService,
    private translateConfigService: TranslateConfigService) { }

  async pintaGrafico() {
    let text_acel = await this.translateConfigService.getTextoNow("TEST.10MTS.gfx_acel")
    if(this.lineCanvas1) {
      this.lineChart = new Chart(this.lineCanvas1.nativeElement, {
        type: "line",
        data: {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [
            /*
            {
              label: "X",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(101, 108, 188,0.4)",
              borderColor: "rgba(101, 108, 188,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(101, 108, 188,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              //pointRadius: this.radioPunto,
              pointHitRadius: 10,
              data: this.dataAccXArr[0],
              spanGaps: false
            },
            {
              label: "Y",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(255, 191, 96,0.4)",
              borderColor: "rgba(255, 191, 96,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: this.colorBordePuntoYArr[0], //this.colorBordePunto, //"rgba(255, 191, 96,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              //pointRadius: 1,
              pointRadius: this.radioPuntoYArr[0],
              //pointHitRadius: 10,
              data: this.dataAccYArr[0],
              spanGaps: false
            },
            */
            {
              label: text_acel, //ex Z
              fill: false,
              //lineTension: 0.1,
              backgroundColor: "rgba(255, 50, 96,0.4)",
              borderColor: "rgba(255, 50, 96,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: this.colorBordePuntoZArr[0], //"rgba(255, 50, 96,1)",
              //pointBorderColor: "rgba(0, 255, 0,1)",
              pointBackgroundColor: this.colorBordePuntoZArr[0], //"#fff",
              borderWidth: 1,
              pointBorderWidth: 0,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(255, 50, 96,1)",
              pointHoverBorderColor: "rgba(220,120,220,1)",
              //pointHoverBorderWidth: 2,
              //pointRadius: 1,
              pointRadius: this.radioPuntoZArr[0],
              //pointHitRadius: 10,
              data: this.dataAccZArr[0],
              spanGaps: false
            }
          ]
        }, options: {
          elements: {
            line: {
                tension: 0.1
            }
          },
          scales: {
              xAxes: {
                  type: 'linear',
                  position: 'bottom'
              }
          },plugins:{ 
            tooltip: {
              callbacks: {
                  label: function(ctx) {
                      var label = ctx.dataset[ctx.datasetIndex].label || '';

                      if (label) {
                          label += ': ';
                      }
                      label += Math.round(+ctx.label * 100) / 100;
                      return label;
                  }
              }
          }
        }
      }
    });
    } else {
      console.error("El canvas no esta listo");
    }

    //this.lineChart.data.datasets[0].points[4].fillColor =  "#FF0000"; //['pointRadius'] = 30;

    if(this.lineCanvas2) {
      this.lineChart = new Chart(this.lineCanvas2.nativeElement, {
        type: "line",
        data: {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [
            /*
            {
              label: "X",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(101, 108, 188,0.4)",
              borderColor: "rgba(101, 108, 188,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(101, 108, 188,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.dataAccXArr[1],
              spanGaps: false
            },
            {
              label: "Y",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(255, 191, 96,0.4)",
              borderColor: "rgba(255, 191, 96,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: this.colorBordePuntoYArr[1], //this.colorBordePunto_2, //"rgba(255, 191, 96,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: this.radioPuntoYArr[1],
              pointHitRadius: 10,
              data: this.dataAccYArr[1],
              spanGaps: false
            },
            */
            {
              label: text_acel, //ex Z
              fill: false,
              //lineTension: 0.1,
              backgroundColor: "rgba(255, 50, 96,0.4)",
              borderColor: "rgba(255, 50, 96,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: this.colorBordePuntoZArr[1], //"rgba(255, 50, 96,1)",
              pointBackgroundColor: this.colorBordePuntoZArr[1], //"#fff",
              borderWidth: 0.5,
              pointBorderWidth: 0,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(255, 50, 96,1)",
              pointHoverBorderColor: "rgba(220,120,220,1)",
              //pointHoverBorderWidth: 2,
              pointRadius: this.radioPuntoZArr[1],
              pointHitRadius: 10,
              data: this.dataAccZArr[1],
              spanGaps: false
            }
          ]
        }, options: {
          elements: {
            line: {
                tension: 0.1
            }
          },
          scales: {
              xAxes: {
                  type: 'linear',
                  position: 'bottom'
              }
          }, plugins:{ 
            tooltip: {
              callbacks: {
                  label: function(ctx) {
                      var label = ctx.dataset[ctx.datasetIndex].label || '';

                      if (label) {
                          label += ': ';
                      }
                      label += Math.round(+ctx.label * 100) / 100;
                      return label;
                  }
              }
            }
          }
        }
      });
    } else {
      console.error("El canvas no esta listo");
    }

    if(this.lineCanvas3) {
      this.lineChart = new Chart(this.lineCanvas3.nativeElement, {
        type: "line",
        data: {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [
            /*
            {
              label: "X",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(101, 108, 188,0.4)",
              borderColor: "rgba(101, 108, 188,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(101, 108, 188,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.dataAccXArr[2],
              spanGaps: false
            },
            {
              label: "Y",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(255, 191, 96,0.4)",
              borderColor: "rgba(255, 191, 96,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: this.colorBordePuntoYArr[2], //this.colorBordePunto_3, //"rgba(255, 191, 96,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: this.radioPuntoYArr[2],
              pointHitRadius: 10,
              data: this.dataAccYArr[2],
              spanGaps: false
            },
            */
            {
              label: text_acel, //ex Z
              fill: false,
              //lineTension: 0.1,
              //backgroundColor: "rgba(0, 255, 0,0.4)",
              backgroundColor: "rgba(255, 50, 96,0.4)",
              borderColor: "rgba(255, 50, 96,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: this.colorBordePuntoZArr[2], //"rgba(255, 50, 96,1)",
              pointBackgroundColor: this.colorBordePuntoZArr[2], //"#fff",
              borderWidth: 1,
              pointBorderWidth: 0,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(255, 50, 96,1)",
              pointHoverBorderColor: "rgba(220,120,220,1)",
              //pointHoverBorderWidth: 2,
              pointRadius: this.radioPuntoZArr[2],
              pointHitRadius: 10,
              data: this.dataAccZArr[2],
              spanGaps: false
            }
          ]
        }, options: {
          elements: {
            line: {
                tension: 0.1
            }
          },
          scales: {
              xAxes: {
                  type: 'linear',
                  position: 'bottom'
              }
          }, plugins:{  
            tooltip: {
              callbacks: {
                  label: function(ctx) {
                      var label = ctx.dataset[ctx.datasetIndex].label || '';

                      if (label) {
                          label += ': ';
                      }
                      label += Math.round(+ctx.label * 100) / 100;
                      return label;
                  }
              }
            }
          }
        }
      });
    } else {
      console.error("El canvas no esta listo");
    }
  }

  /**
   * 
   * @param contacto Determina el color a usar, dependiendo del pie detectado y el tipo de contacto
   * @param arreglo 
   */
  private colorPie(contacto: string, arreglo: number[]) {
    if(contacto === "FC") {
      //CONTACTO FINAL
      if(arreglo.shift() === 1) {
        //Pie derecho para el caso de FC (azul)
        return "rgba(101, 108, 188, 1)";
      } else {
        //Pie izquierdo para el caso de FC (amarillo)
        return "rgba(255, 191, 96, 1)";
      }
    } else if(contacto === "IC") {
      //CONTACTO INICIAL
      if(arreglo.shift() === 1) {
        //Pie izquierdo para el caso de IC (amarillo)
        return "rgba(255, 191, 96, 1)";
      } else {
        //Pie derecho para el caso de IC (azul)
        return "rgba(101, 108, 188, 1)";
      }
    } else {
      return "rgba(255, 255, 255, 1)";
    }
  }

  /**
   * Pobla los arreglos de datos, tamano de los puntos y color de estos, para el eje X
   * @param graficoNro Numero del grafico [1, 2, 3]
   */
  private datosGraficoEjeX(graficoNro: number) {
      let i=0;
      if(this.analisis.ax_array.length > graficoNro) {
        this.dataAccXArr[graficoNro] = [];
        for(let elem of this.analisis.ax_array[graficoNro]) {
          let unpunto: any = {};
          unpunto.x = i / environment.BT_LTE.frecuencia;
          unpunto.y = elem;
          this.dataAccXArr[graficoNro].push(unpunto);
          i++;
        }
      }
  }

  /**
   * Pobla los arreglos de datos, tamano de los puntos y color de estos, para el eje Y
   * @param graficoNro Numero del grafico [1, 2, 3]
   */
  private datosGraficoEjeY(graficoNro: number) {
      let i=0;
      if(this.analisis.ay_array.length > graficoNro) {
        //const cloned_step_side_array  = [...this.analisis.step_side_array[graficoNro]]; //clonamos el arreglo
        this.radioPuntoYArr[graficoNro] = [];
        this.colorBordePuntoYArr[graficoNro] = [];
        this.dataAccYArr[graficoNro] = [];
        for(let elem of this.analisis.ay_array[graficoNro]) {
          let unpunto: any = {};
          unpunto.x = i / environment.BT_LTE.frecuencia;
          unpunto.y = elem;
          this.dataAccYArr[graficoNro].push(unpunto);

          let tamanoPunto = 1;
          let colorPunto = "rgba(255, 191, 96,1)";
          /*
          for(let unFC of this.analisis.FCpos_array[graficoNro]) {
            //console.log("Comparo ["+i+"] con ["+ unFC +"]");
            if(i === unFC) {
              console.log("punto encontrado");
              tamanoPunto = 10;
              colorPunto = this.colorPie("FC", cloned_step_side_array);
            }
          }
          */
          this.radioPuntoYArr[graficoNro].push(tamanoPunto);
          this.colorBordePuntoYArr[graficoNro].push(colorPunto); //"rgba(255, 191, 96,1)",
          i++;
        }
      }
  }

  /**
   * Pobla los arreglos de datos, tamano de los puntos y color de estos, para el eje Z
   * @param graficoNro Numero del grafico [1, 2, 3]
   */
  private datosGraficoEjeZ(graficoNro: number) {
      let i=0;
      if(this.analisis.az_array.length > graficoNro) {
        //const cloned_step_side_array  = [...this.analisis.step_side_array[graficoNro]]; //clonamos el arreglo
        const cloned_step_side_array  = [...this.analisis.ICside_array[graficoNro]]; //clonamos el arreglo
        this.radioPuntoZArr[graficoNro] = [];
        this.colorBordePuntoZArr[graficoNro] = [];
        this.dataAccZArr[graficoNro] = [];

        //Evaluamos cada punto del arreglo az_array
        for(let elem of this.analisis.az_array[graficoNro]) {

          //Creamos el punto para poner en el grafico
          let unpunto: any = {};
          unpunto.x = i / environment.BT_LTE.frecuencia;
          unpunto.y = elem;

          //ponemos el punto en el arreglo que se mostrara en el grafico
          this.dataAccZArr[graficoNro].push(unpunto);

          //Ahora definiremos las caracteristicas del punto (punto normal o grande)

          //caracteristicas de un punto normal
          let tamanoPunto = 0.1;
          let colorPunto = "rgba(255, 50, 96,1)";
          
          //revisamos cada elemento del arreglo con info de puntos especiales
          for(let unIC of this.analisis.ICpos_array[graficoNro]) {
            //console.log("Comparo ["+i+"] con ["+ unFC +"]");
            if(i === unIC) {
              console.log("punto encontrado");
              tamanoPunto = 3;
              colorPunto = this.colorPie("IC", cloned_step_side_array);
            }
          }

          //agregamos el tamaño y color del punto a los arreglos correspondientes
          this.radioPuntoZArr[graficoNro].push(tamanoPunto);
          this.colorBordePuntoZArr[graficoNro].push(colorPunto);

          i++;
        }
      }
  }

  /**
   * Determino el grafico que se va a resaltar y su glosa
   */
  private determinoResaltado() {
    //Tiempo que tomo el grafico ganador
    let tempoWin = -1;

    //Grafico ganador (o -1 si no hay ganador, por ejemplo cuando es promedio de las muestras)
    let winner = -1;

    //Grafico en evaluacion para ver si es ganador
    let participante = 0;

    if(this.analisis.analysisMode == 0) {
      this.translateConfigService.getTextoNow("TEST.10MTS.medi_best").then(txt => {
        this.glosaResaltado = "(" + txt +")";
      })
      
    } else if(this.analisis.analysisMode == 1) {
      //this.glosaResaltado = "(La peor medición)";
      this.translateConfigService.getTextoNow("TEST.10MTS.medi_worst").then(txt => {
        this.glosaResaltado = "(" + txt +")";
      });
    }

    for(let esteTiempo of this.analisis.tiemposMulti) {
      if(this.analisis.analysisMode == 0) {
        //El mejor tiempo, el menor
        if(tempoWin > esteTiempo || tempoWin == -1) {
          tempoWin = esteTiempo;
          winner = participante;
        }
      } else if(this.analisis.analysisMode == 1) {
        //El peor tiempo, el mas grande
        if(tempoWin < esteTiempo || tempoWin == -1) {
          tempoWin = esteTiempo;
          winner = participante;
        }
      }
      participante++;
    }
    if(winner == 0 || winner == -1) {
      this.destacado1 = true;
    }
    if(winner == 1 || winner == -1) {
      this.destacado2 = true;
    }
    if(winner == 2 || winner == -1) {
      this.destacado3 = true;
    }
  }

  ngOnInit() {
    this.modoDebug = environment.debug;
    Chart.register(annotationPlugin, ...registerables);

    if(this.diezMtsService.sesionActual != null) {
      this.analisis = this.diezMtsService.sesionActual.analisis;
      this.paciente = this.diezMtsService.sesionActual.paciente;
      this.sesionActual = this.diezMtsService.sesionActual;
     } else {
      console.warn("No hay sesion");
     }

     if(this.analisis) {

      this.datosGraficoEjeX(0);
      this.datosGraficoEjeX(1);
      this.datosGraficoEjeX(2);

      this.datosGraficoEjeY(0);
      this.datosGraficoEjeY(1);
      this.datosGraficoEjeY(2);

      this.datosGraficoEjeZ(0);
      this.datosGraficoEjeZ(1);
      this.datosGraficoEjeZ(2);
      
    }

    setTimeout(() => {
      this.pintaGrafico();
    }, environment.delayGfxMiliSeg);

    this.determinoResaltado();
  }


  eliminar() {
    if(confirm("¿Esta seguro que desea eliminar esta prueba?")) {
      this.diezMtsService.sesionNueva();
      this.location.back();
      //this.navCtrl.navigateBack('/home');
      //this.navCtrl.pop();
    }
  }

  async agregaHistorial() {
    let extra = "";
    let t1 = await this.translateConfigService.getTextoNow("COMUN.guardar");
    let t2 = await this.translateConfigService.getTextoNow("TEST.confim_save_hist");
    let t3 = await this.translateConfigService.getTextoNow("TEST.agregado");
    let t4 = await this.translateConfigService.getTextoNow("COMUN.guardando");

    //this.utilsService.confirma("Guardar","¿Confirma que desea guardar este resultado en el historial de " + this.paciente.nombre + "?" + extra, async (apruebo: boolean) => {
    this.utilsService.confirma(t1,t2 + " " + this.paciente.nombre + "?" + extra, async (apruebo: boolean) => {
      if(apruebo) {
        //Guardo la prueba en el historial del usuario
        this.analisis.idPaciente = this.paciente.id;
        this.analisis.fecha = new Date();

        //Muestro spinner
        this.loadingSave = await this.loadingController.create({
          //message: 'Guardando...'
          message: t4+'...'
        });
        await this.loadingSave.present();

        let resp = await this.pacienteService.agregaAlHistorialDePruebas(this.analisis, this.sesionActual);

        if(this.loadingSave) {
          this.loadingSave.dismiss();
        }

        if(resp.length > 0) {
          this.utilsService.alerta("Alerta",resp);
        } else {
          //this.utilsService.presentaToast("Agregado al historial");
          // Agregado al historial
          this.utilsService.presentaToast(t3);
          //this.diezMtsService.pruebas = [];
          this.diezMtsService.sesionNueva();
          this.guardado = true;
        }
      }
    })
  }

  async notifications(code: number) {  
    const popover = await this.popoverCtrl.create({  
        component: NotificacionesComponent,  
        cssClass: 'contact-popover',
        //event: ev,  
        animated: true,  
        showBackdrop: true,
        componentProps: {codigo: code}
    });  
    return await popover.present();  
  }  

}