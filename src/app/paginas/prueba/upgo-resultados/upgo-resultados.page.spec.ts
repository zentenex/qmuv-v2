import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpgoResultadosPage } from './upgo-resultados.page';

describe('UpgoResultadosPage', () => {
  let component: UpgoResultadosPage;
  let fixture: ComponentFixture<UpgoResultadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgoResultadosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpgoResultadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
