import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AlgoritmoTimedUpandGo, SesionUPGO, UpgoService } from 'src/app/services/prueba/upgo.service';
import { Chart, registerables } from 'chart.js';
import annotationPlugin from 'chartjs-plugin-annotation';
import { UtilsService } from 'src/app/services/utils.service';
import { PacienteDTO } from 'src/app/dao/pacientes-dao.service';
import { AnalisisUPGO } from 'src/app/dao/pruebas-dao.service';
import { PacienteService } from 'src/app/services/paciente.service';
import { environment } from 'src/environments/environment';
import { LoadingController, NavController, PopoverController } from '@ionic/angular';
import { Location } from '@angular/common';
import { NotificacionesComponent } from 'src/app/componentes/notificaciones/notificaciones.component';
import { TranslateConfigService } from 'src/app/services/translate-config.service';
import { Clipboard } from '@awesome-cordova-plugins/clipboard/ngx'

@Component({
  selector: 'app-upgo-resultados',
  templateUrl: './upgo-resultados.page.html',
  styleUrls: ['./upgo-resultados.page.scss'],
})
export class UpgoResultadosPage implements OnInit {
  @ViewChild("lineCanvas1", { static: false }) lineCanvas1: ElementRef;
  @ViewChild("lineCanvas2", { static: false }) lineCanvas2: ElementRef;
  @ViewChild("lineCanvas3", { static: false }) lineCanvas3: ElementRef;
  loadingSave: HTMLIonLoadingElement = null;

  slideOpts = {
    spaceBetween: 0,
    initialSlide: 0,
    speed: 400
  };

  modoDebug = false;

  elAlgoritmo: AlgoritmoTimedUpandGo;

  private lineChart: Chart;
  analisis: AnalisisUPGO;
  paciente: PacienteDTO;
  sesionActual: SesionUPGO;

  private dataRotacion1: any[] = [];
  private dataInclinacion1: any[] = [];
  private etapasGfx1: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  destacado1 = false;

  private dataRotacion2: any[] = [];
  private dataInclinacion2: any[] = [];
  private etapasGfx2: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  destacado2 = false;

  private dataRotacion3: any[] = [];
  private dataInclinacion3: any[] = [];
  private etapasGfx3: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  destacado3 = false;

  guardado = false;
  glosaResaltado = "";
  regiones: number[] = [];

  volver() {
    console.log("y volver volver voolver");
    this.navCtrl.navigateBack("/home");
  }

  constructor(private navCtrl: NavController,
    public popoverCtrl: PopoverController,
    private location: Location,
    public upgoService: UpgoService,
    private clipboard: Clipboard,
    private loadingController: LoadingController,
    private utilsService: UtilsService,
    private pacienteService: PacienteService,
    private translateConfigService: TranslateConfigService,) {
  }

  async pintaGrafico() {
    let text_rot = await this.translateConfigService.getTextoNow("TEST.UPGO.gfx_rot")
    let text_inc = await this.translateConfigService.getTextoNow("TEST.UPGO.gfx_inc")
    if (this.lineCanvas1) {
      this.lineChart = new Chart(this.lineCanvas1.nativeElement, {
        type: "line",
        data: {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [
            {
              label: text_rot,
              fill: false,
              //lineTension: 0.1,
              backgroundColor: "rgba(101, 108, 188,0.4)",
              borderColor: "rgba(101, 108, 188,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(101, 108, 188,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.dataRotacion1,
              spanGaps: false
            },
            {
              label: text_inc,
              fill: false,
              //lineTension: 0.1,
              backgroundColor: "rgba(255, 191, 96,0.4)",
              borderColor: "rgba(255, 191, 96,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(255, 191, 96,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.dataInclinacion1,
              spanGaps: false
            }
            /*,{
              label: 'Line Dataset',
              data: [50, 50, 50, 50],

              fill: true,
              // Changes this dataset to become a line
              type: 'line'
            }*/
          ]
        }, options: {
          elements: {
            line: {
                tension: 0.1
            }
          },
          scales: {
            xAxes: {
              type: 'linear',
              position: 'bottom'
            }
          }, plugins:{ 
            tooltip: {
              callbacks: {
                label: function (context) {
                  var label = context.dataset[context.datasetIndex].label || '';

                  if (label) {
                    label += ': ';
                  }
                  label += Math.round(+context.label * 100) / 100;
                  return label;
                }
              }
            }
            , annotation: {
              annotations: [
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[0],
                  "xMax": this.etapasGfx1[1],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(250,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                },
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[1],
                  "xMax": this.etapasGfx1[2],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(200,200,200,0.25)",
                  "borderColor": "rgba(200,200,200,0.25)"
                },
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[2],
                  "xMax": this.etapasGfx1[3],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(60,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                }, {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[3],
                  "xMax": this.etapasGfx1[4],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(200,200,200,0.25)",
                  "borderColor": "rgba(200,200,200,0.25)"
                },
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[4],
                  "xMax": this.etapasGfx1[5],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(60,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                }, {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[5],
                  "xMax": this.etapasGfx1[6],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(200,200,200,0.25)",
                  "borderColor": "rgba(200,200,200,0.25)"
                },
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[6],
                  "xMax": this.etapasGfx1[7],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(60,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                }, {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[7],
                  "xMax": this.etapasGfx1[8],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(200,200,200,0.25)",
                  "borderColor": "rgba(200,200,200,0.25)"
                },
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[8],
                  "xMax": this.etapasGfx1[9],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(250,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                }]
            }
          }
        },
      });
    } else {
      console.error("El canvas no esta listo");
    }

    if (this.lineCanvas2) {
      this.lineChart = new Chart(this.lineCanvas2.nativeElement, {
        type: "line",
        data: {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [
            {
              label: text_rot,
              fill: false,
              //lineTension: 0.1,
              backgroundColor: "rgba(101, 108, 188,0.4)",
              borderColor: "rgba(101, 108, 188,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(101, 108, 188,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.dataRotacion2,
              spanGaps: false
            },
            {
              label: text_inc,
              fill: false,
              //lineTension: 0.1,
              backgroundColor: "rgba(255, 191, 96,0.4)",
              borderColor: "rgba(255, 191, 96,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(255, 191, 96,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.dataInclinacion2,
              spanGaps: false
            }
            /*,{
              label: 'Line Dataset',
              data: [50, 50, 50, 50],

              fill: true,
              // Changes this dataset to become a line
              type: 'line'
            }*/
          ]
        }, options: {
          elements: {
            line: {
                tension: 0.1
            }
          },
          scales: {
            xAxes: {
              type: 'linear',
              position: 'bottom'
            }
          }, plugins:{ 
            tooltip: {
              callbacks: {
                label: function (context) {
                  var label = context.dataset[context.datasetIndex].label || '';

                  if (label) {
                    label += ': ';
                  }
                  label += Math.round(+context.label * 100) / 100;
                  return label;
                }
              }
            }, 
            annotation: {
              annotations: {
                a1: {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx2[0],
                  "xMax": this.etapasGfx2[1],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(250,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                },
                a2:
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx2[1],
                  "xMax": this.etapasGfx2[2],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(200,200,200,0.25)",
                  "borderColor": "rgba(200,200,200,0.25)"
                },
                a3:
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx2[2],
                  "xMax": this.etapasGfx2[3],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(60,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                },
                a4: {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx2[3],
                  "xMax": this.etapasGfx2[4],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(200,200,200,0.25)",
                  "borderColor": "rgba(200,200,200,0.25)"
                },
                a5: {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx2[4],
                  "xMax": this.etapasGfx2[5],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(60,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                }, 
                a6: {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx2[5],
                  "xMax": this.etapasGfx2[6],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(200,200,200,0.25)",
                  "borderColor": "rgba(200,200,200,0.25)"
                },
                a7: {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx2[6],
                  "xMax": this.etapasGfx2[7],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(60,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                }, 
                a8: {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx2[7],
                  "xMax": this.etapasGfx2[8],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(200,200,200,0.25)",
                  "borderColor": "rgba(200,200,200,0.25)"
                },
                a9: {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx2[8],
                  "xMax": this.etapasGfx2[9],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(250,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                }
              }
            },
          },
        }
      });
    } else {
      console.error("El canvas no esta listo");
    }

    if (this.lineCanvas3) {
      this.lineChart = new Chart(this.lineCanvas3.nativeElement, {
        type: "line",
        data: {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [
            {
              label: text_rot,
              fill: false,
              //lineTension: 0.1,
              backgroundColor: "rgba(101, 108, 188,0.4)",
              borderColor: "rgba(101, 108, 188,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(101, 108, 188,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.dataRotacion3,
              spanGaps: false
            },
            {
              label: text_inc,
              fill: false,
              //lineTension: 0.1,
              backgroundColor: "rgba(255, 191, 96,0.4)",
              borderColor: "rgba(255, 191, 96,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(255, 191, 96,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.dataInclinacion3,
              spanGaps: false
            }
            /*,{
              label: 'Line Dataset',
              data: [50, 50, 50, 50],

              fill: true,
              // Changes this dataset to become a line
              type: 'line'
            }*/
          ]
        }, options: {
          elements: {
            line: {
                tension: 0.1
            }
          },
          scales: {
            xAxes: {
              type: 'linear',
              position: 'bottom'
            }
          }, plugins:{ 
            tooltip: {
              callbacks: {
                label: function (context) {
                  var label = context.dataset[context.datasetIndex].label || '';

                  if (label) {
                    label += ': ';
                  }
                  label += Math.round(+context.label * 100) / 100;
                  return label;
                }
              }
            }, annotation: {
            annotations: [
              {
                "type": "box",
                "xScaleID": "x-axis-0",
                "yScaleID": "y-axis-0",
                "xMin": this.etapasGfx3[0],
                "xMax": this.etapasGfx3[1],
                "borderWidth": 1,
                "backgroundColor": "rgba(250,60,60,0.25)",
                "borderColor": "rgba(60,60,60,0.25)"
              },
              {
                "type": "box",
                "xScaleID": "x-axis-0",
                "yScaleID": "y-axis-0",
                "xMin": this.etapasGfx3[1],
                "xMax": this.etapasGfx3[2],
                "borderWidth": 1,
                "backgroundColor": "rgba(200,200,200,0.25)",
                "borderColor": "rgba(200,200,200,0.25)"
              },
              {
                "type": "box",
                "xScaleID": "x-axis-0",
                "yScaleID": "y-axis-0",
                "xMin": this.etapasGfx3[2],
                "xMax": this.etapasGfx3[3],
                "borderWidth": 1,
                "backgroundColor": "rgba(60,60,60,0.25)",
                "borderColor": "rgba(60,60,60,0.25)"
              }, {
                "type": "box",
                "xScaleID": "x-axis-0",
                "yScaleID": "y-axis-0",
                "xMin": this.etapasGfx3[3],
                "xMax": this.etapasGfx3[4],
                "borderWidth": 1,
                "backgroundColor": "rgba(200,200,200,0.25)",
                "borderColor": "rgba(200,200,200,0.25)"
              },
              {
                "type": "box",
                "xScaleID": "x-axis-0",
                "yScaleID": "y-axis-0",
                "xMin": this.etapasGfx3[4],
                "xMax": this.etapasGfx3[5],
                "borderWidth": 1,
                "backgroundColor": "rgba(60,60,60,0.25)",
                "borderColor": "rgba(60,60,60,0.25)"
              }, {
                "type": "box",
                "xScaleID": "x-axis-0",
                "yScaleID": "y-axis-0",
                "xMin": this.etapasGfx3[5],
                "xMax": this.etapasGfx3[6],
                "borderWidth": 1,
                "backgroundColor": "rgba(200,200,200,0.25)",
                "borderColor": "rgba(200,200,200,0.25)"
              },
              {
                "type": "box",
                "xScaleID": "x-axis-0",
                "yScaleID": "y-axis-0",
                "xMin": this.etapasGfx3[6],
                "xMax": this.etapasGfx3[7],
                "borderWidth": 1,
                "backgroundColor": "rgba(60,60,60,0.25)",
                "borderColor": "rgba(60,60,60,0.25)"
              }, {
                "type": "box",
                "xScaleID": "x-axis-0",
                "yScaleID": "y-axis-0",
                "xMin": this.etapasGfx3[7],
                "xMax": this.etapasGfx3[8],
                "borderWidth": 1,
                "backgroundColor": "rgba(200,200,200,0.25)",
                "borderColor": "rgba(200,200,200,0.25)"
              },
              {
                "type": "box",
                "xScaleID": "x-axis-0",
                "yScaleID": "y-axis-0",
                "xMin": this.etapasGfx3[8],
                "xMax": this.etapasGfx3[9],
                "borderWidth": 1,
                "backgroundColor": "rgba(250,60,60,0.25)",
                "borderColor": "rgba(60,60,60,0.25)"
              }]
            }
          }
        }
      });
    } else {
      console.error("El canvas no esta listo");
    }

  }

  eliminar() {
    if (confirm("¿Esta seguro que desea eliminar esta prueba?")) {
      this.upgoService.sesionNueva();
      this.location.back();
      //this.navCtrl.navigateBack('/home');
      //this.navCtrl.pop();
    }
  }

  async agregaHistorial() {
    let extra = "";
    let t1 = await this.translateConfigService.getTextoNow("COMUN.guardar");
    let t2 = await this.translateConfigService.getTextoNow("TEST.confim_save_hist");
    let t3 = await this.translateConfigService.getTextoNow("TEST.agregado");
    let t4 = await this.translateConfigService.getTextoNow("COMUN.guardando");

    this.utilsService.confirma(t1, t2 + " " + this.paciente.nombre + "?" + extra, async (apruebo: boolean) => {
      if (apruebo) {
        //Guardo la prueba en el historial del usuario
        this.analisis.idPaciente = this.paciente.id;
        this.analisis.fecha = new Date();

        //Muestro spinner
        this.loadingSave = await this.loadingController.create({
          //Guardando...
          message: t4 + "..."
        });
        await this.loadingSave.present();

        let resp = await this.pacienteService.agregaAlHistorialDePruebas(this.analisis, this.sesionActual);

        if (this.loadingSave) {
          this.loadingSave.dismiss();
        }

        if (resp.length > 0) {
          this.utilsService.alerta("Alerta", resp);
        } else {
          // Agregado al historial
          this.utilsService.presentaToast(t3);
          //this.upgoService.pruebas = [];
          this.upgoService.sesionNueva();
          this.guardado = true;
        }
      }
    })
  }

  ngOnInit() {

    this.modoDebug = environment.debug;
    Chart.register(annotationPlugin, ...registerables);

    if (this.upgoService.sesionActual != null) {
      this.analisis = this.upgoService.sesionActual.analisis;
      this.paciente = this.upgoService.sesionActual.paciente;

      this.elAlgoritmo = this.upgoService.sesionActual.algoritmo;

      this.sesionActual = this.upgoService.sesionActual;
    } else {
      console.warn("No hay sesion");
    }


    if (this.analisis) {
      if (this.analisis.analysisMode == 0) {
        //this.glosaResaltado = "(La mejor medición)";
        this.translateConfigService.getTextoNow("TEST.10MTS.medi_best").then(txt => {
          this.glosaResaltado = "(" + txt + ")";
        })
      } else if (this.analisis.analysisMode == 1) {
        //this.glosaResaltado = "(La peor medición)";
        this.translateConfigService.getTextoNow("TEST.10MTS.medi_worst").then(txt => {
          this.glosaResaltado = "(" + txt + ")";
        });
      }
      else if (this.analisis.analysisMode == 2) {
        //this.glosaResaltado = "(La peor medición)";
        this.translateConfigService.getTextoNow("TEST.10MTS.average").then(txt => {
          this.glosaResaltado = "(" + txt + ")";
        });
      }


      //En caso de modo debug, copiamos los valores al portapapeles

      if (environment.debug) {
        this.utilsService.presentaToastDebug("Debug al portapaleles el algoritmo");
        this.clipboard.copy(JSON.stringify(this.elAlgoritmo));
      }


      let i = 0;
      for (let elem of this.analisis.rotacionesMulti[0]) {
        let unpunto: any = {};
        unpunto.x = i / environment.BT_LTE.frecuencia;
        unpunto.y = elem;
        this.dataRotacion1.push(unpunto);
        i++;
      }

      i = 0;
      for (let elem of this.analisis.inclinacionesMulti[0]) {
        let unpunto: any = {};
        unpunto.x = i / environment.BT_LTE.frecuencia;
        unpunto.y = elem;
        this.dataInclinacion1.push(unpunto);
        i++;
      }

      let maxCantidadDePuntos = Math.max(this.analisis.rotacionesMulti[0].length, this.analisis.inclinacionesMulti[0].length);

      this.etapasGfx1[0] = 0;
      this.etapasGfx1[1] = this.analisis.eventosMulti[0][0];
      this.etapasGfx1[2] = this.analisis.eventosMulti[0][1];
      this.etapasGfx1[3] = this.analisis.eventosMulti[0][2];
      this.etapasGfx1[4] = this.analisis.eventosMulti[0][3];
      this.etapasGfx1[5] = this.analisis.eventosMulti[0][4];
      this.etapasGfx1[6] = this.analisis.eventosMulti[0][5];
      this.etapasGfx1[7] = this.analisis.eventosMulti[0][6];
      this.etapasGfx1[8] = this.analisis.eventosMulti[0][7];
      this.etapasGfx1[9] = maxCantidadDePuntos / environment.BT_LTE.frecuencia;
    }

    if (this.analisis && this.analisis.tiemposMulti.length > 1) {
      let i = 0;
      for (let elem of this.analisis.rotacionesMulti[1]) {
        let unpunto: any = {};
        unpunto.x = i / environment.BT_LTE.frecuencia;
        unpunto.y = elem;
        this.dataRotacion2.push(unpunto);
        i++;
      }

      i = 0;
      for (let elem of this.analisis.inclinacionesMulti[1]) {
        let unpunto: any = {};
        unpunto.x = i / environment.BT_LTE.frecuencia;
        unpunto.y = elem;
        this.dataInclinacion2.push(unpunto);
        i++;
      }

      let maxCantidadDePuntos = Math.max(this.analisis.rotacionesMulti[1].length, this.analisis.inclinacionesMulti[1].length);

      this.etapasGfx2[0] = 0;
      this.etapasGfx2[1] = this.analisis.eventosMulti[1][0];
      this.etapasGfx2[2] = this.analisis.eventosMulti[1][1];
      this.etapasGfx2[3] = this.analisis.eventosMulti[1][2];
      this.etapasGfx2[4] = this.analisis.eventosMulti[1][3];
      this.etapasGfx2[5] = this.analisis.eventosMulti[1][4];
      this.etapasGfx2[6] = this.analisis.eventosMulti[1][5];
      this.etapasGfx2[7] = this.analisis.eventosMulti[1][6];
      this.etapasGfx2[8] = this.analisis.eventosMulti[1][7];
      this.etapasGfx2[9] = maxCantidadDePuntos / environment.BT_LTE.frecuencia;
    }

    if (this.analisis && this.analisis.tiemposMulti.length > 2) {
      let i = 0;
      for (let elem of this.analisis.rotacionesMulti[2]) {
        let unpunto: any = {};
        unpunto.x = i / environment.BT_LTE.frecuencia;
        unpunto.y = elem;
        this.dataRotacion3.push(unpunto);
        i++;
      }

      i = 0;
      for (let elem of this.analisis.inclinacionesMulti[2]) {
        let unpunto: any = {};
        unpunto.x = i / environment.BT_LTE.frecuencia;
        unpunto.y = elem;
        this.dataInclinacion3.push(unpunto);
        i++;
      }

      let maxCantidadDePuntos = Math.max(this.analisis.rotacionesMulti[2].length, this.analisis.inclinacionesMulti[2].length);
      this.etapasGfx3[0] = 0;
      this.etapasGfx3[1] = this.analisis.eventosMulti[2][0];
      this.etapasGfx3[2] = this.analisis.eventosMulti[2][1];
      this.etapasGfx3[3] = this.analisis.eventosMulti[2][2];
      this.etapasGfx3[4] = this.analisis.eventosMulti[2][3];
      this.etapasGfx3[5] = this.analisis.eventosMulti[2][4];
      this.etapasGfx3[6] = this.analisis.eventosMulti[2][5];
      this.etapasGfx3[7] = this.analisis.eventosMulti[2][6];
      this.etapasGfx3[8] = this.analisis.eventosMulti[2][7];
      this.etapasGfx3[9] = maxCantidadDePuntos / environment.BT_LTE.frecuencia;
    }

    //Determino el grafico a resaltar
    let tempoWin = -1;
    let winner = -1;
    let participante = 0;
    for (let esteTiempo of this.analisis.tiemposMulti) {
      if (this.analisis.analysisMode == 0) {
        //El mejor tiempo, el menor
        if (tempoWin > esteTiempo || tempoWin == -1) {
          tempoWin = esteTiempo;
          winner = participante;
        }
      } else if (this.analisis.analysisMode == 1) {
        //El peor tiempo, el mas grande
        if (tempoWin < esteTiempo || tempoWin == -1) {
          tempoWin = esteTiempo;
          winner = participante;
        }
      }
      participante++;
    }
    if (winner == 0 || winner == -1) {
      this.destacado1 = true;
    }
    if (winner == 1 || winner == -1) {
      this.destacado2 = true;
    }
    if (winner == 2 || winner == -1) {
      this.destacado3 = true;
    }

    setTimeout(() => {
      this.pintaGrafico();
    }, environment.delayGfxMiliSeg);
  }

  async notifications(code: number) {
    const popover = await this.popoverCtrl.create({
      component: NotificacionesComponent,
      cssClass: 'contact-popover',
      //event: ev,  
      animated: true,
      showBackdrop: true,
      componentProps: { codigo: code }
    });
    return await popover.present();
  }

}
