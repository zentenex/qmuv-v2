import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpgoResultadosPage } from './upgo-resultados.page';

const routes: Routes = [
  {
    path: '',
    component: UpgoResultadosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpgoResultadosPageRoutingModule {}
