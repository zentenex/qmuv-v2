import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpgoResultadosPageRoutingModule } from './upgo-resultados-routing.module';

import { UpgoResultadosPage } from './upgo-resultados.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpgoResultadosPageRoutingModule,
    MenuInferiorModule
  ],
  declarations: [UpgoResultadosPage]
})
export class UpgoResultadosPageModule {}
