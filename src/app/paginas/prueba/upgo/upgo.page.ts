import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { BLE } from '@awesome-cordova-plugins/ble/ngx';
import { DispositivosDAOService, DispositivoDTO } from 'src/app/dao/dispositivos-dao.service';
import { UtilsService } from 'src/app/services/utils.service';
import { IonSlides, ModalController, NavController } from '@ionic/angular';
import { PacienteDTO } from 'src/app/dao/pacientes-dao.service';
import { Subscription } from 'rxjs';
import { PacienteService } from 'src/app/services/paciente.service';
import { UpgoService, MuestraUPGO } from 'src/app/services/prueba/upgo.service';
import { environment } from 'src/environments/environment';
import { SensorInnercialService } from 'src/app/services/sensor-innercial.service';
import { ConfigDaoService } from 'src/app/dao/config-dao.service';
import { TranslateConfigService } from 'src/app/services/translate-config.service';
import { SeleccionPacientePage } from 'src/app/componentes/seleccion-paciente/seleccion-paciente.page';
import { MuestreoService } from 'src/app/services/muestreo.service';

@Component({
  selector: 'app-upgo',
  templateUrl: './upgo.page.html',
  styleUrls: ['./upgo.page.scss'],
})
export class UpgoPage implements OnInit {

  @ViewChild(IonSlides) protected slider: IonSlides;

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  /**
   * Listado de los pacientes activos del usuario
   */
  pacientes: PacienteDTO[] = [];

  /**
   * Paciente al que se le asignara la prueba
   */
  pacienteSeleccionado: PacienteDTO = null;

  /**
   * Suscripcion de paciente nuevo creado
   */
  subscription1: Subscription;

  /**
   * Suscripcion de dispositivo cambiado
   */
  subscription2: Subscription;

  /**
   * El dispositivo QMUV con el que se va a trabajar
   */
  private dispositivo: DispositivoDTO;

  /**
   * Le indica al GUI que debe mostrar o no el landing page del test
   */
  muestroLanding = true;

  /**
   * Le indica al GUI si debe mostrar o no la tabla con los datos de debug
   */
  modoDebug = false;

  textoDEBUG: string = "";

  /**
   * Arreglo de enteros de los ultimos datos recibidos desde el QMUV.
   * Este arreglo lo muestro en pantalla para debuguear
   */
  public get debugRaw() {
    return this.muestreoService.debugRaw;
  }

  debugCount: any;

  debugAX: any;
  debugAY: any;
  debugAZ: any;

  debugGX: any;
  debugGY: any;
  debugGZ: any;

  debugQW: any;
  debugQX: any;
  debugQY: any;
  debugQZ: any;

  debugAX_2: any;
  debugAY_2: any;
  debugAZ_2: any;

  debugGX_2: any;
  debugGY_2: any;
  debugGZ_2: any;

  debugQW_2: any;
  debugQX_2: any;
  debugQY_2: any;
  debugQZ_2: any;

  public get segundos() {
    return this.muestreoService.segundos;
  }

  public get recibiendo() {
    return this.muestreoService.recibiendo;
  }

  //Deprecados
  dataGiroscopio: Uint8Array[] = [];
  dataACC: Uint8Array[] = [];
  dataQUAT: Uint8Array[] = [];

  /**
  * Indica si el examen esta siendo ejecutado.
  * Se usa para saber que boton mostrar y desactivar funcionalidades en la UI
  * */
  examenEnEjecucion = false;

  /**
   * Cantidad de mediciones que se deben realizar para que la sesion se de por completada
   */
  private cantidadMedicionesPorSesion = 0;

  /**
   * Flag que indica si el usuario presiono el boton para ir a analizar.
   * Se utiliza para que el GUARD pueda saber si debe dejar o no salir al usuario de la pantalla
   */
  voyAAnalizar = false;
  nombrePaciente = '';

  constructor(private ble: BLE,
    private utilsService: UtilsService,
    private configDAOService: ConfigDaoService,
    private _ngZone: NgZone,
    private pacienteService: PacienteService,
    public upgoService: UpgoService,
    private sensorInnercialService: SensorInnercialService,
    private dispositivosDAOService: DispositivosDAOService,
    public modalController: ModalController,
    private navCtrl: NavController,
    private muestreoService: MuestreoService,
    private translateConfigService: TranslateConfigService) {
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: SeleccionPacientePage,
      componentProps: {
        'pacientes': this.pacientes,
      },
      cssClass: 'modalSeleccionPaciente',
      backdropDismiss: false
    });

    modal.onDidDismiss()
      .then((data) => {
        if (data) {
          const idUsuario = data['data']; // Here's your selected user!
          if (!idUsuario.dismissed) {
            this.pacienteSeleccionado = idUsuario;
            this.nombrePaciente = idUsuario.nombre;
          }
        }

      });
    modal.dismiss({
      'dismissed': true
    });

    return await modal.present();
  }

  /**** GETTERS *******/

  /**
   * Retorna la cantidad de muestras guardadas en la sesion actual
   */
  get pruebasRealizadas() {
    return this.upgoService.sesionActual.muestras.length;
  }

  /**
   * Retorna un arreglo con la cantidad de muestras que faltan para completar la sesion actual
   */
  get pruebasFaltantes() {
    //Obtenemos las muestras ya guardadas
    let pruebasCompletadas = this.upgoService.sesionActual.muestras.length;
    //Obtenemos la cantidad que debo cumplir
    let pruebasNecesarias = this.cantidadMedicionesPorSesion;
    //Calculamos cuantas muestras faltan
    let resp = pruebasNecesarias - pruebasCompletadas;
    //Retornamos un arreglo del tamano de la cantidad de muestras faltantes
    return resp >= 0 ? Array(pruebasNecesarias - pruebasCompletadas) : Array(0);
  }

  /******** UI *********/

  /**
   * El usuario comienza la toma de muestra para la sesion actual
   * Recoje los datos desde el dispositivo y llena this.muestra
   */
  async btnComenzar() {

    //Reviso si es que tengo dispositivo QMUV para trabajar
    if (this.dispositivo) {

      //Reviso si estoy o no conectado al dispositivo
      this.ble.isConnected(this.dispositivo.id).then(
        _ => {
          //Si estoy conectado
          this.dispositivoConectado();
        }, async _ => {
          //No estoy conectado
          //Reviso si tengo activado el flag virtual (para simular sin tener un equipo fisico)
          if (this.sensorInnercialService.virtual) {
            //Continuo como si estuviese conectado, estoy en modo virtual
            this.dispositivoConectado();
          } else {
            //No estoy conectado a ningun QMUV, redirijo a la pantalla de conexion
            let errorCuerpoNoConexion = await this.translateConfigService.getTextoNow("ERRORES.errorCuerpoNoConexion");
            let errorCabeceraNoConexion = await this.translateConfigService.getTextoNow("ERRORES.errorCabeceraNoConexion");
            this.utilsService.alerta(errorCabeceraNoConexion, errorCuerpoNoConexion);
            this.navCtrl.navigateForward('/dispositivo-list');
          }
        }
      )
    } else {
      //No tengo dispositivo QMUV, lo reenvio a pantalla para conectarse a uno
      let cabecera = await this.translateConfigService.getTextoNow("ERRORES.errorCabecera");
      let cuerpo = await this.translateConfigService.getTextoNow("ERRORES.errorCuerpo");
      this.utilsService.alerta(cabecera, cuerpo);
      this.navCtrl.navigateForward('/dispositivo-list');
    }
  }

  /**
   * El usuario indica que ya quiere terminar, aunque le falten muestras por completar
   */
  async btnTerminar() {
    //Obtenemos el texto en el idioma correcto
    let txt = await this.translateConfigService.getTextoNow("TEST.terminar_med_antes")

    //pedimos confirmacion al usuario para terminar la prueba antes de tiempo
    if (confirm(txt)) {
      //El usuario confirma, entonces cambiamos el valor de la cantidad de mediciones necesarias
      //para terminar por la cantidad que ya hicimos, asi ya habremos llegado a la meta
      this.cantidadMedicionesPorSesion = this.upgoService.sesionActual.muestras.length;
    }
  }

  /**
   * El usuario termino de tomar muestras y quiere analizarlas
   */
  btnAnalizar() {
    //Marco la variable para que el GUARD me deje salir a analizar
    this.voyAAnalizar = true;
    //Comienza el analisis de UPGO para el paciente seleccionado
    this.upgoService.sesionAnaliza(this.pacienteSeleccionado);
    //Una vez terminado el analisis, nos vamos a la pantalla de resultados
    this.navCtrl.navigateForward('/upgo-resultados');
  }

  /**
   * Termina de tomar la muestra. termina de llenar this.muestra y ejecuta (en parte) el algoritmo
   */
  async btnFinalizar() {

    if (this.segundos && this.segundos > 0) {
      //Indicamos que ya no estamos muestreando
      this.examenEnEjecucion = false;

      //Reseteamos el cronometro
      //this.segundos = 0;

      //Evaluamos si estamos trabajando con un QMUV real o virtual
      if (!this.sensorInnercialService.virtual) {

        //Le indico al QMUV que termine de muestrear
        this.sensorInnercialService.envioDeDatos(this.dispositivo.id, false).then(async _ => {
          //Cuando el QMUV ya no me envie nada (buffer vacio), dejamos de escuchar datos
          //this.ble.stopNotification(this.dispositivo.id, environment.BT_LTE.servicio.IMU_SERVICE.UUID, environment.BT_LTE.servicio.IMU_SERVICE.caracteristica.DATA_COMP_CHAR);

          let errorBuffer = await this.translateConfigService.getTextoNow("ERRORES.errorBuffer");
          this.utilsService.presentaToastDebug(errorBuffer);
        }).catch(async err => {
          console.error("No se pudo enviar la orden para terminar las mediciones: " + err);
          let noCompletadoCuerpo = await this.translateConfigService.getTextoNow("ERRORES.noCompletadoCuerpo");
          let noCompletadoCabecera = await this.translateConfigService.getTextoNow("ERRORES.noCompletadoCabecera");
          this.utilsService.alerta(noCompletadoCabecera, noCompletadoCuerpo);
          this.sensorInnercialService.desconecta();
        });
        //}
        //Sin importar la version del FW, mostramos en pantalla que el muestreo termino
        this.translateConfigService.getTextoNow("TEST.prueba_fin").then(t1 => {
          this.utilsService.presentaToast(t1);
        })

        //});
      } else {
        //Es un QMUV virtual
        //Mostramos en pantalla que el muestreo termino
        this.translateConfigService.getTextoNow("TEST.prueba_fin").then(t1 => {
          this.utilsService.presentaToast(t1);
        })
      }

      //agrego muestra
      console.log("Comienza el analisis");
      try {
        this.upgoService.sesionAddMedicion(this.muestreoService.muestra);
      } catch (err) {
        //alert(err);
        let falloMedicionCabecera = await this.translateConfigService.getTextoNow("ERRORES.falloMedicionCabecera");
        let falloMedicionCuerpo = await this.translateConfigService.getTextoNow("ERRORES.falloMedicionCuerpo");
        this.utilsService.alerta(falloMedicionCabecera, falloMedicionCuerpo);
        console.error("Error al agregar la medicion : " + err);
      }
    } else {
      this.utilsService.presentaToast("Cargando...")
    }
  }

  /**
   * El usuario termino en onboarding
   */
  btnFinLanding() {
    this.muestroLanding = false;
    this.configDAOService.saltaElOnboarding(environment.PRUEBA_UPANDGO_CODE);
  }

  //Deprecated
  siguienteSlide() {
    this.slider.slideNext();
  }

  /************** GUARD *****************/

  /**
   * Metodo invocado por la guardia para saber si hay o no permiso para desactivar (salir de la pantalla) el componente actual
   * @returns verdadero si puedo salir libremente, falso si estoy atrapado
   */
  canDeactivate() {

    //Si el usuario presiono el boton "Analizar", entonces lo dejo salir de la pantalla
    if (this.voyAAnalizar) {
      return true;
    }

    //Si se estan tomando los datos de una medicion, entonces no puedo salir. Debo detenerla primero
    console.log("Examen en ejecucion : " + this.examenEnEjecucion);
    if (this.examenEnEjecucion) {
      alert("Debe detener la prueba antes de salir");
      return false;
    }

    //Si quiero salir y ya tome al menos una medicion, aviso que perdere los datos si salgo
    if (this.pruebasRealizadas > 0) {
      return confirm("¿Seguro que desea salir?. Se perderan los datos no guardados");
    }
    return true;
  }

  /********** EVENTOS IONIC ************/

  /**
   * Se inicia el componente
   */
  ngOnInit() {
    console.log("En el ngOnInit del test upgo");

    //Marcamos la variable como falsa para indicar que no ha sido presionado el boton de "analizar"
    this.voyAAnalizar = false;

    //Obtenemos el valor de modo debug
    this.modoDebug = environment.debug;

    //Obtengo el valor para ver si muestro o no el on boarding
    this.configDAOService.muestroOnboarding(environment.PRUEBA_UPANDGO_CODE).then(muestro => {
      console.log("Debo mostrar landing : " + muestro)
      this.muestroLanding = muestro;
    })

    //Obtengo la informacion del dispositivo actual con el que voy a trabajar
    this.dispositivosDAOService.getCurrentDevice().then(disp => {
      this.dispositivo = disp;
    });

    //Obtiene todos los pacientes activos del usuario
    this.pacienteService.pacientesActivos("").then(losPacientes => {
      this.pacientes = losPacientes;
    });

    //Crea una nueva sesion para trabajar
    this.upgoService.sesionNueva();

    //Nos suscribimos para enterarnos si se crea un nuevo paciente
    this.subscription1 = this.pacienteService.observadorPacientesNuevos.subscribe(async paciente => {
      //Actualizamos mis pacientes activos
      this.pacientes = await this.pacienteService.pacientesActivos("");

      for (let unPaciente of this.pacientes) {
        //Buscamos el paciente recien creado dentro de mis pacientes activos
        if (unPaciente.id === paciente.id) {
          //Si encontramos al paciente, este pasa a ser automaticamente el paciente seleccionado
          console.log("dejamos al paciente seleccionado : " + paciente.nombre)
          this._ngZone.run(() => {
            this.pacienteSeleccionado = unPaciente;
          });
        }
      }
    });



    this.subscription2 = this.sensorInnercialService.observadorDispositivo$().subscribe(disp => {
      console.log("ICN Cambio en el dispositivo " + disp.nombre);
      this._ngZone.run(() => {
        //this.bluetoothActivo = this.sensorInnercialService.conectado;

        //veamos si seguimos conectados
        this.sensorInnercialService.estoyConectado(disp.id).then(_ => {
          console.log("estoy conectado al dispositivo");
        }).catch(async _ => {
          console.log("No estoy conectado al dispositivo");
          if (this.examenEnEjecucion) {
            this.examenEnEjecucion = false;
            let falloMedicionCabecera = await this.translateConfigService.getTextoNow("ERRORES.falloMedicionCabecera");
            let pruebaabortada = await this.translateConfigService.getTextoNow("ERRORES.pruebaabortada");
            this.utilsService.alerta(falloMedicionCabecera, pruebaabortada);
          }
        })

      });
    });


  }

  /**
   * Se va a entrar a la vista
   */
  ionViewWillEnter() {
    console.log("En el ionViewWillEnter del test upgo");

    //Al entrar en la pantalla, obtenemos la cantidad de mediciones que deben ser ejecutadas para completar esta prueba
    this.configDAOService.cantidadMedicionesPorSesion(environment.PRUEBA_UPANDGO_CODE).then(resp => {
      this.cantidadMedicionesPorSesion = resp;
    })
  }

  /**
   * Al destruir el componente
   */
  ngOnDestroy() {
    console.log("En el ngOnDestroy del test upgo");
    //Nos desuscribimos de pacientes nuevos creados
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
  }

  /********* Metodos auxiliares ***************/

  /**
   * Si ya estoy conectado, entonces este metodo continua con el proceso de muestreo
   */
  private dispositivoConectado() {

    //Revisamos si el paciente ya fue seleccionado
    if (!this.pacienteSeleccionado) {

      //No hay paciente seleccionado, alerto con un mensaje que debe seleccionar uno para continuar
      let p1 = this.translateConfigService.getTextoNow("TEST.falta_paci_1");
      let p2 = this.translateConfigService.getTextoNow("TEST.falta_paci_2");
      Promise.all([p1, p2]).then(values => {
        this.utilsService.alerta(values[0], values[1]);
      })
    } else {
      //Si hay paciente seleccionado. Ya tengo todo lo necesario para comenzar

      //Indico que el examen esta en ejecucion
      this.examenEnEjecucion = true;

      this.muestreoService.resetMuestreo();

      //Mostramos un mensaje indicando que el muestreo ya comenzo
      this.translateConfigService.getTextoNow("TEST.prueba_progreso").then(t1 => {
        this.utilsService.presentaToast(t1);
      })

      //Vamos a trabajar con el firmware nuevo
      this.comenzamos();

    }
  }

  /**
   * Comienza el proceso de muestreo
   */
  private comenzamos() {


    //if (this.examenEnEjecucion) {
    if (!this.sensorInnercialService.virtual) {
      //Escribimos en la caracteristica para iniciar la medicion
      this.sensorInnercialService.envioDeDatos(this.dispositivo.id, true).then(_ => {
        console.log("Comenzamos...");
      }).catch(async err => {
        console.error("No se pudo enviar la orden para comenzar las mediciones: " + err);
        let noCompletadoCuerpo = await this.translateConfigService.getTextoNow("ERRORES.noCompletadoCuerpo");
        let noCompletadoCabecera = await this.translateConfigService.getTextoNow("ERRORES.noCompletadoCabecera");
        this.utilsService.alerta(noCompletadoCabecera, noCompletadoCuerpo + err);
      });
    } else {
      this.muestreoService.onDataDummy_UPGO();

    }
  }

}
