import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpgoPage } from './upgo.page';

describe('UpgoPage', () => {
  let component: UpgoPage;
  let fixture: ComponentFixture<UpgoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpgoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
