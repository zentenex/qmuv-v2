import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaveGuard } from 'src/app/interceptores/LeaveGuard';

import { UpgoPage } from './upgo.page';

const routes: Routes = [
  {
    path: '',
    canDeactivate: [LeaveGuard],
    component: UpgoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpgoPageRoutingModule {}
