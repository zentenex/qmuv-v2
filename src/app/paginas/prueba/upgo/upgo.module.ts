import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpgoPageRoutingModule } from './upgo-routing.module';

import { UpgoPage } from './upgo.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';
import { IconoBTModule } from 'src/app/componentes/icono-bt/icono-bt.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpgoPageRoutingModule,
    MenuInferiorModule,
    IconoBTModule
  ],
  declarations: [UpgoPage]
})
export class UpgoPageModule {}
