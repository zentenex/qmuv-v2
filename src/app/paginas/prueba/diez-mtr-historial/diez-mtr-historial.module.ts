import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiezMtrHistorialPageRoutingModule } from './diez-mtr-historial-routing.module';

import { DiezMtrHistorialPage } from './diez-mtr-historial.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiezMtrHistorialPageRoutingModule,
    MenuInferiorModule
  ],
  declarations: [DiezMtrHistorialPage]
})
export class DiezMtrHistorialPageModule {}
