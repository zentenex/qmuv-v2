import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiezMtrHistorialPage } from './diez-mtr-historial.page';

describe('DiezMtrHistorialPage', () => {
  let component: DiezMtrHistorialPage;
  let fixture: ComponentFixture<DiezMtrHistorialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiezMtrHistorialPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiezMtrHistorialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
