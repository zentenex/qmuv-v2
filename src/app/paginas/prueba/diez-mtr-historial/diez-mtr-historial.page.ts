import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PruebasDAOService, Analisis10MTS } from 'src/app/dao/pruebas-dao.service';
import { PacienteDTO, PacientesDAOService } from 'src/app/dao/pacientes-dao.service';
import { Chart } from 'chart.js';
import { environment } from 'src/environments/environment';
import { UtilsService } from 'src/app/services/utils.service';
import { PopoverController } from '@ionic/angular';
import { NotificacionesComponent } from 'src/app/componentes/notificaciones/notificaciones.component';
import { TranslateConfigService } from 'src/app/services/translate-config.service';

@Component({
  selector: 'app-diez-mtr-historial',
  templateUrl: './diez-mtr-historial.page.html',
  styleUrls: ['./diez-mtr-historial.page.scss'],
})
export class DiezMtrHistorialPage implements OnInit {

  slideOpts1 = {
    spaceBetween: 0,
    initialSlide: 0,
    speed: 400
  };

  glosaResaltado = "";

  //Grafico de pasos
  @ViewChild("lineCanvas1", { static: false }) lineCanvas1: ElementRef;
  @ViewChild("lineCanvas2", { static: false }) lineCanvas2: ElementRef;
  @ViewChild("lineCanvas3", { static: false }) lineCanvas3: ElementRef;
  private lineChart: Chart;

  //Graficos historico
  @ViewChild("barCanvasVelocidadMarcha", { static: false }) barCanvasVelocidadMarcha: ElementRef;

  @ViewChild("lineCanvasCadencia", { static: false }) lineCanvasCadencia: ElementRef;
  @ViewChild("lineCanvasPaso", { static: false }) lineCanvasPaso: ElementRef;
  @ViewChild("lineCanvasZancada", { static: false }) lineCanvasZancada: ElementRef;
  @ViewChild("lineCanvasApoyoSimple", { static: false }) lineCanvasApoyoSimple: ElementRef;
  @ViewChild("lineCanvasApoyoDoble", { static: false }) lineCanvasApoyoDoble: ElementRef;

  @ViewChild("lineCanvasApoyo", { static: false }) lineCanvasApoyo: ElementRef;
  @ViewChild("lineCanvasBalanceo", { static: false }) lineCanvasBalanceo: ElementRef;
  @ViewChild("lineCanvasLargoPaso", { static: false }) lineCanvasLargoPaso: ElementRef;

  slideOpts = {
    spaceBetween: 0,
    initialSlide: 0,
    speed: 400
  };
  analisis: Analisis10MTS;
  analisisUltimos10: Analisis10MTS[] = [];

  gfxDataTiempos: number[] = [];

  tend0: number[] = [0, 0];
  tend1: number[] = [0, 0];
  tend2: number[] = [0, 0];
  tend3: number[] = [0, 0];
  tend4: number[] = [0, 0];

  tend5: number[] = [0, 0];
  tend6: number[] = [0, 0];
  tend7: number[] = [0, 0];

  gfxDataTiempo0i: number[] = []; //l_cadence
  gfxDataTiempo0d: number[] = []; //r_cadence
  gfxDataTiempo1i: number[] = []; //l_steptime
  gfxDataTiempo1d: number[] = []; //r_steptime
  gfxDataTiempo2i: number[] = []; //l_stridetime
  gfxDataTiempo2d: number[] = []; //d_stridetime
  gfxDataTiempo3i: number[] = []; //l_ssupporttime
  gfxDataTiempo3d: number[] = []; //d_ssupporttime
  gfxDataTiempo4i: number[] = []; //l_dsupporttime
  gfxDataTiempo4d: number[] = []; //d_dsupporttime

  gfxDataTiempoStepI: number[] = []; //l_steplength
  gfxDataTiempoStepD: number[] = []; //r_steplength
  gfxDataTiempoBalanceoI: number[] = []; //l_swing
  gfxDataTiempoBalanceoD: number[] = []; //r_swing
  gfxDataTiempoApoyoI: number[] = []; //l_stance
  gfxDataTiempoApoyoD: number[] = []; //r_stance

  destacado1 = false;
  destacado2 = false;
  destacado3 = false;

  //Tamano de los puntos de la dimension Z para los 3 posibles graficos
  private radioPuntoZArr: any[][] = [];

  //Color de los puntos de la dimesion Z para los 3 posibles graficos
  private colorBordePuntoZArr: any[][] = [];

  //Datos de aceleracion eje Z para los 3 posibles graficos
  private dataAccZArr: any[][] = [];

  paciente: PacienteDTO;
  seleccion: number = 1;

  constructor(public popoverCtrl: PopoverController,
    private activatedRoute: ActivatedRoute,
    private pruebasDAOService: PruebasDAOService,
    private utilsService: UtilsService,
    private pacientesDAOService: PacientesDAOService,
    private translateConfigService: TranslateConfigService) { }

  /**
   * Pobla los arreglos de datos, tamano de los puntos y color de estos, para el eje Z
   * @param graficoNro Numero del grafico [1, 2, 3]
   */
  private datosGraficoEjeZ(graficoNro: number) {
    let i = 0;
    if (this.analisis.az_array.length > graficoNro) {
      //const cloned_step_side_array  = [...this.analisis.step_side_array[graficoNro]]; //clonamos el arreglo
      const cloned_step_side_array = [...this.analisis.ICside_array[graficoNro]]; //clonamos el arreglo
      this.radioPuntoZArr[graficoNro] = [];
      this.colorBordePuntoZArr[graficoNro] = [];
      this.dataAccZArr[graficoNro] = [];

      //Evaluamos cada punto del arreglo az_array
      for (let elem of this.analisis.az_array[graficoNro]) {

        //Creamos el punto para poner en el grafico
        let unpunto: any = {};
        unpunto.x = i / environment.BT_LTE.frecuencia;
        unpunto.y = elem;

        //ponemos el punto en el arreglo que se mostrara en el grafico
        this.dataAccZArr[graficoNro].push(unpunto);

        //Ahora definiremos las caracteristicas del punto (punto normal o grande)

        //caracteristicas de un punto normal
        let tamanoPunto = 0.1;
        let colorPunto = "rgba(255, 50, 96,1)";

        //revisamos cada elemento del arreglo con info de puntos especiales
        for (let unIC of this.analisis.ICpos_array[graficoNro]) {
          //console.log("Comparo ["+i+"] con ["+ unFC +"]");
          if (i === unIC) {
            console.log("punto encontrado");
            tamanoPunto = 3;
            colorPunto = this.colorPie("IC", cloned_step_side_array);
          }
        }

        //agregamos el tamaño y color del punto a los arreglos correspondientes
        this.radioPuntoZArr[graficoNro].push(tamanoPunto);
        this.colorBordePuntoZArr[graficoNro].push(colorPunto);

        i++;
      }
    }
  }

  /**
   * 
   * @param contacto Determina el color a usar, dependiendo del pie detectado y el tipo de contacto
   * @param arreglo 
   */
  private colorPie(contacto: string, arreglo: number[]) {
    if (contacto === "FC") {
      //CONTACTO FINAL
      if (arreglo.shift() === 1) {
        //Pie derecho para el caso de FC (azul)
        return "rgba(101, 108, 188, 1)";
      } else {
        //Pie izquierdo para el caso de FC (amarillo)
        return "rgba(255, 191, 96, 1)";
      }
    } else if (contacto === "IC") {
      //CONTACTO INICIAL
      if (arreglo.shift() === 1) {
        //Pie izquierdo para el caso de IC (amarillo)
        return "rgba(255, 191, 96, 1)";
      } else {
        //Pie derecho para el caso de IC (azul)
        return "rgba(101, 108, 188, 1)";
      }
    } else {
      return "rgba(255, 255, 255, 1)";
    }
  }

  /**
   * Determino el grafico que se va a resaltar y su glosa
   */
  private determinoResaltado() {
    //Tiempo que tomo el grafico ganador
    let tempoWin = -1;

    //Grafico ganador (o -1 si no hay ganador, por ejemplo cuando es promedio de las muestras)
    let winner = -1;

    //Grafico en evaluacion para ver si es ganador
    let participante = 0;


    console.log("El analisis MODE", this.analisis.analysisMode)

    if (this.analisis.analysisMode == 0) {
      //this.glosaResaltado = "(La mejor medición)";
      this.translateConfigService.getTextoNow("TEST.10MTS.medi_best").then(txt => {
        this.glosaResaltado = "(" + txt + ")";
      })
    } else if (this.analisis.analysisMode == 1) {
      //this.glosaResaltado = "(La peor medición)";
      this.translateConfigService.getTextoNow("TEST.10MTS.medi_worst").then(txt => {
        this.glosaResaltado = "(" + txt + ")";
      });
    }
    else if (this.analisis.analysisMode == 2) {
      //this.glosaResaltado = "(La peor medición)";
      this.translateConfigService.getTextoNow("TEST.10MTS.average").then(txt => {
        this.glosaResaltado = "(" + txt + ")";
      });
    }

    for (let esteTiempo of this.analisis.tiemposMulti) {
      console.log("Reviso al que tiene tiempo = [" + esteTiempo + "]");

      if (esteTiempo == 0) {
        continue;
      }

      if (this.analisis.analysisMode == 0) {
        //El mejor tiempo, el menor
        if (tempoWin > esteTiempo || tempoWin == -1) {
          tempoWin = esteTiempo;
          winner = participante;
        }
      } else if (this.analisis.analysisMode == 1) {
        //El peor tiempo, el mas grande
        if (tempoWin < esteTiempo || tempoWin == -1) {
          tempoWin = esteTiempo;
          winner = participante;
        }
      }
      participante++;
    }

    if (winner == 0 || winner == -1) {
      this.destacado1 = true;
    }
    if (winner == 1 || winner == -1) {
      this.destacado2 = true;
    }
    if (winner == 2 || winner == -1) {
      this.destacado3 = true;
    }
  }

  async pintaGraficoUltimaSesion() {
    let text_acel = await this.translateConfigService.getTextoNow("TEST.10MTS.gfx_acel")
    setTimeout(() => {

      if (this.lineCanvas1) {
        this.lineChart = new Chart(this.lineCanvas1.nativeElement, {
          type: "line",
          data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
              {
                label: text_acel, //ex Z
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 50, 96,0.4)",
                borderColor: "rgba(255, 50, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: this.colorBordePuntoZArr[0], //"rgba(255, 50, 96,1)",
                //pointBorderColor: "rgba(0, 255, 0,1)",
                pointBackgroundColor: this.colorBordePuntoZArr[0], //"#fff",
                borderWidth: 1,
                pointBorderWidth: 0,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 50, 96,1)",
                pointHoverBorderColor: "rgba(220,120,220,1)",
                //pointHoverBorderWidth: 2,
                //pointRadius: 1,
                pointRadius: this.radioPuntoZArr[0],
                //pointHitRadius: 10,
                data: this.dataAccZArr[0],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              xAxes: {
                type: 'linear',
                position: 'bottom'
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (ctx) {
                    var label = ctx.dataset[ctx.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+ctx.label * 100) / 100;
                    return label;
                  }
                }
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      //this.lineChart.data.datasets[0].points[4].fillColor =  "#FF0000"; //['pointRadius'] = 30;

      if (this.lineCanvas2) {
        this.lineChart = new Chart(this.lineCanvas2.nativeElement, {
          type: "line",
          data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
              {
                label: text_acel, //ex Z
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 50, 96,0.4)",
                borderColor: "rgba(255, 50, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: this.colorBordePuntoZArr[1], //"rgba(255, 50, 96,1)",
                pointBackgroundColor: this.colorBordePuntoZArr[1], //"#fff",
                borderWidth: 0.5,
                pointBorderWidth: 0,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 50, 96,1)",
                pointHoverBorderColor: "rgba(220,120,220,1)",
                //pointHoverBorderWidth: 2,
                pointRadius: this.radioPuntoZArr[1],
                pointHitRadius: 10,
                data: this.dataAccZArr[1],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              xAxes: {
                type: 'linear',
                position: 'bottom'
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (ctx) {
                    var label = ctx.dataset[ctx.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+ctx.label * 100) / 100;
                    return label;
                  }
                }
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvas3) {
        this.lineChart = new Chart(this.lineCanvas3.nativeElement, {
          type: "line",
          data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
              {
                label: text_acel, //ex Z
                fill: false,
                //lineTension: 0.1,
                //backgroundColor: "rgba(0, 255, 0,0.4)",
                backgroundColor: "rgba(255, 50, 96,0.4)",
                borderColor: "rgba(255, 50, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: this.colorBordePuntoZArr[2], //"rgba(255, 50, 96,1)",
                pointBackgroundColor: this.colorBordePuntoZArr[2], //"#fff",
                borderWidth: 1,
                pointBorderWidth: 0,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 50, 96,1)",
                pointHoverBorderColor: "rgba(220,120,220,1)",
                //pointHoverBorderWidth: 2,
                pointRadius: this.radioPuntoZArr[2],
                pointHitRadius: 10,
                data: this.dataAccZArr[2],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              xAxes: {
                type: 'linear',
                position: 'bottom'
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (ctx) {
                    var label = ctx.dataset[ctx.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+ctx.label * 100) / 100;
                    return label;
                  }
                }
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }
    }, environment.delayGfxMiliSeg);
  }

  maximo(a: number, b: number) {
    return Math.max(a, b);
  }

  get velocidadPromedio() {
    let suma = 0;
    let cantidad = this.analisisUltimos10.length;
    for (let i = 0; i < cantidad; i++) {
      suma = suma + this.analisisUltimos10[i].v_gait;
    }
    return (cantidad == 0) ? 0 : (suma / cantidad);
  }

  segmentChanged(ev: any) {
    if (this.seleccion == 1) {
      this.pintaGraficoUltimaSesion();
    } else if (this.seleccion == 2) {
      this.pintaGraficoHistorico();
    }
  }

  private generaValoresParaGraficos() {
    for (let hist of this.analisisUltimos10) {
      this.gfxDataTiempos.push(hist.v_gait);

      this.gfxDataTiempo0i.push(hist.l_cadence);
      this.gfxDataTiempo0d.push(hist.r_cadence);
      this.gfxDataTiempo1i.push(hist.l_steptime);
      this.gfxDataTiempo1d.push(hist.r_steptime);
      this.gfxDataTiempo2i.push(hist.l_stridetime);
      this.gfxDataTiempo2d.push(hist.r_stridetime);
      this.gfxDataTiempo3i.push(hist.l_ssupporttime);
      this.gfxDataTiempo3d.push(hist.r_ssupporttime);
      this.gfxDataTiempo4i.push(hist.l_dsupporttime);
      this.gfxDataTiempo4d.push(hist.r_dsupporttime);

      this.gfxDataTiempoStepI.push(hist.l_steplength);
      this.gfxDataTiempoStepD.push(hist.r_steplength);

      this.gfxDataTiempoBalanceoI.push(hist.l_swing);
      this.gfxDataTiempoBalanceoD.push(hist.r_swing);

      this.gfxDataTiempoApoyoI.push(hist.l_stance);
      this.gfxDataTiempoApoyoD.push(hist.r_stance);
    }

    this.tend0 = this.utilsService.tendence('cadencia', this.gfxDataTiempo0i);
    this.tend1 = this.utilsService.tendence('tpaso', this.gfxDataTiempo1i);
    this.tend2 = this.utilsService.tendence('tzancada', this.gfxDataTiempo2i);
    this.tend3 = this.utilsService.tendence('ssupport', this.gfxDataTiempo3i);
    this.tend4 = this.utilsService.tendence('dsupport', this.gfxDataTiempo4i);

    this.tend5 = this.utilsService.tendence('lpaso', this.gfxDataTiempoStepI);
    this.tend6 = this.utilsService.tendence('balanceo', this.gfxDataTiempoBalanceoI);
    this.tend7 = this.utilsService.tendence('apoyo', this.gfxDataTiempoApoyoI);
  }

  ngOnInit() {
    let idPaciente = this.activatedRoute.snapshot.paramMap.get('id');
    this.pacientesDAOService.pacienteBusca(+idPaciente).then(elPaciente => {
      this.paciente = elPaciente;
      this.analisis = this.pruebasDAOService.loadUltimoAnalisis10mts(elPaciente.id);
      this.analisisUltimos10 = this.pruebasDAOService.loadUltimos10Analisis10mts(elPaciente.id);

      this.generaValoresParaGraficos();

      //inicio - Obtiene data y pinta el grafico nuevo
      if (this.analisis) {
        this.datosGraficoEjeZ(0);
        this.datosGraficoEjeZ(1);
        this.datosGraficoEjeZ(2);
      }
      this.pintaGraficoUltimaSesion();

      this.determinoResaltado();

      //fin - Obtiene data y pinta el grafico nuevo


    });
  }

  async pintaGraficoHistorico() {

    let text_dur = await this.translateConfigService.getTextoNow("COMUN.gfx_dura")
    let text_izq = await this.translateConfigService.getTextoNow("COMUN.izq")
    let text_der = await this.translateConfigService.getTextoNow("COMUN.der")

    setTimeout(() => {

      if (this.barCanvasVelocidadMarcha) {
        let histRCChart = new Chart(this.barCanvasVelocidadMarcha.nativeElement, {
          type: "bar",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: text_dur, //"Duración de la prueba",
                data: this.gfxDataTiempos, // [12, 19, 3, 5, 2, 3, 12, 19, 3, 5],
                backgroundColor: [
                  "rgba(66, 77, 153, 0.2)", //"rgba(255, 99, 132, 0.2)",
                  "rgba(66, 77, 153, 0.2)",
                  "rgba(66, 77, 153, 0.2)", //"rgba(255, 206, 86, 0.2)",
                  "rgba(66, 77, 153, 0.2)", //"rgba(75, 192, 192, 0.2)",
                  "rgba(66, 77, 153, 0.2)", //"rgba(153, 102, 255, 0.2)",
                  "rgba(66, 77, 153, 0.2)", //"rgba(255, 159, 64, 0.2)",
                  "rgba(66, 77, 153, 0.2)", //"rgba(255, 99, 132, 0.2)",
                  "rgba(66, 77, 153, 0.2)", //"rgba(54, 162, 235, 0.2)",
                  "rgba(66, 77, 153, 0.2)", //"rgba(255, 206, 86, 0.2)",
                  "rgba(66, 77, 153, 0.2)", //"rgba(75, 192, 192, 0.2)"
                ],
                borderColor: [
                  "rgba(66, 77, 153, 1)", //"rgba(255,99,132,1)",
                  "rgba(66, 77, 153, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(255, 206, 86, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(75, 192, 192, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(153, 102, 255, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(255, 159, 64, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(255,99,132,1)",
                  "rgba(66, 77, 153, 1)", //"rgba(54, 162, 235, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(255, 206, 86, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(75, 192, 192, 1)"
                ],
                borderWidth: 1
              }
            ]
          },
          options: {
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                enabled: false, // <-- this option disables tooltips
                callbacks: {
                  label: function (ctx) {
                    var label = ctx.dataset[ctx.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+ctx.label * 100) / 100;
                    return label;
                  }
                }
              },
            
              legend: {
                display: false
              }, annotation: {
                annotations: [
                  {
                    type: 'line',
                    //mode: 'horizontal',
                    scaleID: 'y-axis-0',
                    value: 0.4,
                    borderColor: 'rgb(255, 0, 0)',
                    borderWidth: 2,
                    label: {
                      //enabled: false,
                      content: 'Leve'
                    }
                  }, {
                    type: 'line',
                    //mode: 'horizontal',
                    scaleID: 'y-axis-0',
                    value: 0.8,
                    borderColor: 'rgb(0, 0, 255)',
                    borderWidth: 2,
                    label: {
                      //enabled: false,
                      content: 'Alto'
                    }
                  }, {
                    type: 'line',
                    //mode: 'horizontal',
                    scaleID: 'y-axis-0',
                    value: 1.2,
                    borderColor: 'rgb(0, 255, 0)',
                    borderWidth: 2,
                    label: {
                      //enabled: false,
                      content: 'Alto'
                    }
                  }]
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasCadencia) {
        let grafico = new Chart(this.lineCanvasCadencia.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: text_izq, //"Izquierda",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(101, 108, 188,0.4)",
                borderColor: "rgba(101, 108, 188,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(101, 108, 188,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempo0i, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              },
              {
                label: text_der, //"Derecha",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 191, 96,0.4)",
                borderColor: "rgba(255, 191, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(255, 191, 96,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempo0d, //[2, 5, 12, 19, 3, 5, 2, 3, 12, 19],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (ctx) {
                    var label = ctx.dataset[ctx.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+ctx.label * 100) / 100;
                    return label;
                  }
                }
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasPaso) {
        let grafico = new Chart(this.lineCanvasPaso.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: text_izq, //"Izquierda",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(101, 108, 188,0.4)",
                borderColor: "rgba(101, 108, 188,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(101, 108, 188,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempo1i, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              },
              {
                label: text_der, //"Derecha",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 191, 96,0.4)",
                borderColor: "rgba(255, 191, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(255, 191, 96,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempo1d, //[2, 5, 12, 19, 3, 5, 2, 3, 12, 19],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (ctx) {
                    var label = ctx.dataset[ctx.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+ctx.label * 100) / 100;
                    return label;
                  }
                }
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasZancada) {
        let grafico = new Chart(this.lineCanvasZancada.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: text_izq, //"Izquierda",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(101, 108, 188,0.4)",
                borderColor: "rgba(101, 108, 188,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(101, 108, 188,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempo2i, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              },
              {
                label: text_der, //"Derecha",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 191, 96,0.4)",
                borderColor: "rgba(255, 191, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(255, 191, 96,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempo2d, //[2, 5, 12, 19, 3, 5, 2, 3, 12, 19],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (ctx) {
                    var label = ctx.dataset[ctx.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+ctx.label * 100) / 100;
                    return label;
                  }
                }
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasApoyoSimple) {
        let grafico = new Chart(this.lineCanvasApoyoSimple.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: text_izq, //"Izquierda",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(101, 108, 188,0.4)",
                borderColor: "rgba(101, 108, 188,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(101, 108, 188,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempo3i, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              },
              {
                label: text_der, //"Derecha",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 191, 96,0.4)",
                borderColor: "rgba(255, 191, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(255, 191, 96,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempo3d, //[2, 5, 12, 19, 3, 5, 2, 3, 12, 19],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (ctx) {
                    var label = ctx.dataset[ctx.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+ctx.label * 100) / 100;
                    return label;
                  }
                }
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasApoyoDoble) {
        let grafico = new Chart(this.lineCanvasApoyoDoble.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: text_izq, //"Izquierda",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(101, 108, 188,0.4)",
                borderColor: "rgba(101, 108, 188,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(101, 108, 188,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempo4i, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              },
              {
                label: text_der, //"Derecha",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 191, 96,0.4)",
                borderColor: "rgba(255, 191, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(255, 191, 96,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempo4d, //[2, 5, 12, 19, 3, 5, 2, 3, 12, 19],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (ctx) {
                    var label = ctx.dataset[ctx.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+ctx.label * 100) / 100;
                    return label;
                  }
                }
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasApoyo) {
        let grafico = new Chart(this.lineCanvasApoyo.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: text_izq, //"Izquierda",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(101, 108, 1880.4)",
                borderColor: "rgba(101, 108, 188,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(101, 108, 188,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempoApoyoI, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              },
              {
                label: text_der, //"Derecha",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 191, 96,0.4)",
                borderColor: "rgba(255, 191, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(255, 191, 96,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempoApoyoD, //[2, 5, 12, 19, 3, 5, 2, 3, 12, 19],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (ctx) {
                    var label = ctx.dataset[ctx.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+ctx.label * 100) / 100;
                    return label;
                  }
                }
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasBalanceo) {
        let grafico = new Chart(this.lineCanvasBalanceo.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: text_izq, //"Izquierda",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(101, 108, 1880.4)",
                borderColor: "rgba(101, 108, 188,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(101, 108, 188,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempoBalanceoI, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              },
              {
                label: text_der, //"Derecha",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 191, 96,0.4)",
                borderColor: "rgba(255, 191, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(255, 191, 96,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempoBalanceoD, //[2, 5, 12, 19, 3, 5, 2, 3, 12, 19],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (ctx) {
                    var label = ctx.dataset[ctx.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+ctx.label * 100) / 100;
                    return label;
                  }
                }
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasLargoPaso) {
        let grafico = new Chart(this.lineCanvasLargoPaso.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: text_izq, //"Izquierda",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(101, 108, 1880.4)",
                borderColor: "rgba(101, 108, 188,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(101, 108, 188,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempoStepI, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              },
              {
                label: text_der, //"Derecha",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 191, 96,0.4)",
                borderColor: "rgba(255, 191, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(255, 191, 96,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataTiempoStepD, //[2, 5, 12, 19, 3, 5, 2, 3, 12, 19],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (ctx) {
                    var label = ctx.dataset[ctx.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+ctx.label * 100) / 100;
                    return label;
                  }
                }
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

    }, environment.delayGfxMiliSeg);
  }

  async notifications(code: number) {
    const popover = await this.popoverCtrl.create({
      component: NotificacionesComponent,
      cssClass: 'contact-popover',
      //event: ev,  
      animated: true,
      showBackdrop: true,
      componentProps: { codigo: code }
    });
    return await popover.present();
  }

}
