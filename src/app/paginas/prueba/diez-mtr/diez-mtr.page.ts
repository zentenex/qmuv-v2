import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { IonSlides, ModalController, NavController } from '@ionic/angular';
import { DispositivoDTO, DispositivosDAOService } from 'src/app/dao/dispositivos-dao.service';
import { BLE } from '@awesome-cordova-plugins/ble/ngx';
import { UtilsService } from 'src/app/services/utils.service';
import { PacienteDTO } from 'src/app/dao/pacientes-dao.service';
import { environment } from 'src/environments/environment';
import { PacienteService } from 'src/app/services/paciente.service';
import { Observable, Subscription } from 'rxjs';
import { Muestra10MTS, DiezMtrService } from 'src/app/services/prueba/diez-mtr.service';
import { SensorInnercialService } from 'src/app/services/sensor-innercial.service';
import { ConfigDaoService } from 'src/app/dao/config-dao.service';
import { TranslateConfigService } from 'src/app/services/translate-config.service';
import { SeleccionPacientePage } from 'src/app/componentes/seleccion-paciente/seleccion-paciente.page';
import { MuestreoService } from 'src/app/services/muestreo.service';

@Component({
  selector: 'app-diez-mtr',
  templateUrl: './diez-mtr.page.html',
  styleUrls: ['./diez-mtr.page.scss'],
})
export class DiezMtrPage implements OnInit {

  @ViewChild(IonSlides) protected slider: IonSlides;

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  voyAAnalizar = false;

  pacientes: PacienteDTO[] = [];
  pacienteSeleccionado: PacienteDTO = null;


  /**
   * Suscripcion de paciente nuevo creado
   */
  subscription1: Subscription;

  /**
   * Suscripcion de dispositivo cambiado
   */
  subscription2: Subscription;

  private dispositivo: DispositivoDTO;
  muestroLanding = true;

  modoDebug = false;
  textoDEBUG: string = "";

  public get debugRaw() {
    return this.muestreoService.debugRaw;
  }

  debugCount: any;

  debugAX: any;
  debugAY: any;
  debugAZ: any;

  debugGX: any;
  debugGY: any;
  debugGZ: any;

  debugQW: any;
  debugQX: any;
  debugQY: any;
  debugQZ: any;

  debugAX_2: any;
  debugAY_2: any;
  debugAZ_2: any;

  debugGX_2: any;
  debugGY_2: any;
  debugGZ_2: any;

  debugQW_2: any;
  debugQX_2: any;
  debugQY_2: any;
  debugQZ_2: any;

  //private muestra: Muestra10MTS;

  //segundos = 0;
  public get segundos() {
    return this.muestreoService.segundos;
  }

  //tiempoStart: Date;
  //tiempoStop: Date;
  dataGiroscopio: Uint8Array[] = [];

  /**
   * Indica si comence o no a recibir datos
   * Se usa para mostrar o no el cronometro en la UI y para comenzar a contar el tiempo transcurrido
   */
  //recibiendo = false;
  public get recibiendo() {
    return this.muestreoService.recibiendo;
  }

  nombrePaciente = '';

  /**
  * Indica si el examen esta siendo ejecutado.
  * Se usa para saber que boton mostrar y desactivar funcionalidades en la UI
  * */
  examenEnEjecucion = false;

  private cantidadMedicionesPorSesion = 0;

  constructor(private ble: BLE,
    private utilsService: UtilsService,
    private configDAOService: ConfigDaoService,
    private _ngZone: NgZone,
    private pacienteService: PacienteService,
    public diezMtrService: DiezMtrService,
    private sensorInnercialService: SensorInnercialService,
    private dispositivosDAOService: DispositivosDAOService,
    public modalController: ModalController,
    private navCtrl: NavController,
    private muestreoService: MuestreoService,
    private translateConfigService: TranslateConfigService) {
    console.log("** CONSTRUCTOR de diezmts **");
    //setInterval(() => this.cronometro(), 1000);
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: SeleccionPacientePage,
      componentProps: {
        'pacientes': this.pacientes,
      },
      cssClass: 'modalSeleccionPaciente',
      backdropDismiss: false
    })

    modal.onDidDismiss()
      .then((data) => {
        if (data) {
          const idUsuario = data['data']; // Here's your selected user!
          if (!idUsuario.dismissed) {
            this.pacienteSeleccionado = idUsuario;
            this.nombrePaciente = idUsuario.nombre;
          }
        }
      });

    modal.dismiss({
      'dismissed': true
    });

    return await modal.present();
  }

  /*
  cronometro() {
    if (this.examenEnEjecucion && this.recibiendo) {
      this.tiempoStop = new Date();
      let milisegundos = this.tiempoStop.getTime() - this.tiempoStart.getTime();
      this.segundos = Math.round(milisegundos / 1000);
    }
  }
  */

  get pruebasRealizadas() {
    return this.diezMtrService.sesionActual.muestras.length;
  }

  get pruebasFaltantes() {
    let pruebasCompletadas = this.diezMtrService.sesionActual.muestras.length;
    let pruebasNecesarias = this.cantidadMedicionesPorSesion;
    let resp = pruebasNecesarias - pruebasCompletadas;
    return resp >= 0 ? Array(pruebasNecesarias - pruebasCompletadas) : Array(0);
  }

  async terminar() {
    let txt = await this.translateConfigService.getTextoNow("TEST.terminar_med_antes")
    if (confirm(txt)) {
      this.cantidadMedicionesPorSesion = this.diezMtrService.sesionActual.muestras.length;
    }
  }

  async canDeactivate(): Promise<Observable<boolean> | boolean> {
    if (this.voyAAnalizar) {
      return true;
    }
    console.log("Examen en ejecucion : " + this.examenEnEjecucion);
    if (this.examenEnEjecucion) {
      let confirm = await this.translateConfigService.getTextoNow("ERRORES.completarPrueba");
      alert(confirm);
      return false;
    }
    if (this.pruebasRealizadas > 0) {
      let confirm = await this.translateConfigService.getTextoNow("ERRORES.confirm");
      return confirm(confirm);
    }
    return true;
  }

  ngOnInit() {
    this.modoDebug = environment.debug;
    this.voyAAnalizar = false;

    this.configDAOService.muestroOnboarding(environment.PRUEBA_DIEZMTS_CODE).then(muestro => {
      this.muestroLanding = muestro;
    })

    this.dispositivosDAOService.getCurrentDevice().then(disp => {
      this.dispositivo = disp;
    });

    this.pacienteService.pacientesActivos("").then(losPacientes => {
      this.pacientes = losPacientes;
    });

    this.diezMtrService.sesionNueva();

    this.subscription1 = this.pacienteService.observadorPacientesNuevos.subscribe(async paciente => {
      this.pacientes = await this.pacienteService.pacientesActivos("");
      for (let unPaciente of this.pacientes) {
        if (unPaciente.id === paciente.id) {
          console.log("dejamos al paciente seleccionado : " + paciente.nombre)
          this._ngZone.run(() => {
            this.pacienteSeleccionado = unPaciente;
          });
        }
      }
    });


    this.subscription2 = this.sensorInnercialService.observadorDispositivo$().subscribe(disp => {
      console.log("ICN Cambio en el dispositivo " + disp.nombre);
      this._ngZone.run(() => {
        //this.bluetoothActivo = this.sensorInnercialService.conectado;

        //veamos si seguimos conectados
        this.sensorInnercialService.estoyConectado(disp.id).then(_ => {
          console.log("estoy conectado al dispositivo");
        }).catch(async _ => {
          console.log("No estoy conectado al dispositivo");
          if (this.examenEnEjecucion) {
            this.examenEnEjecucion = false;

            let falloMedicionCabecera = await this.translateConfigService.getTextoNow("ERRORES.falloMedicionCabecera");
            let pruebaabortada = await this.translateConfigService.getTextoNow("ERRORES.pruebaabortada");
            this.utilsService.alerta(falloMedicionCabecera, pruebaabortada);
          }
        })

      });
    });


  }

  ionViewWillEnter() {
    console.log("En el ionViewWillEnter del test upgo");

    this.configDAOService.cantidadMedicionesPorSesion(environment.PRUEBA_DIEZMTS_CODE).then(resp => {
      this.cantidadMedicionesPorSesion = resp;
    });
  }

  ngOnDestroy() {
    console.log("En el ngOnDestroy del test 10mts");
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
  }

  finLanding() {
    this.muestroLanding = false;
    //TODO marcarlo permanentemente
    this.configDAOService.saltaElOnboarding(environment.PRUEBA_DIEZMTS_CODE);
  }

  siguienteSlide() {
    this.slider.slideNext();
  }

  async comenzar() {
    //this.recibiendo = false;
    if (this.dispositivo) {
      this.ble.isConnected(this.dispositivo.id).then(
        _ => {
          this.dispositivoConectado();
        }, async _ => {
          if (this.sensorInnercialService.virtual) {
            this.dispositivoConectado();
          } else {

            let cabecera = await this.translateConfigService.getTextoNow("ERRORES.errorCabeceraNoConexion");
            let cuerpo = await this.translateConfigService.getTextoNow("ERRORES.NoConexion");

            this.utilsService.alerta(cabecera, cuerpo);
            this.navCtrl.navigateForward('/dispositivo-list');
          }
        }
      )
    } else {
      let cabecera = await this.translateConfigService.getTextoNow("ERRORES.errorCabecera");
      let cuerpo = await this.translateConfigService.getTextoNow("ERRORES.errorCuerpo");
      this.utilsService.alerta(cabecera, cuerpo);
      this.navCtrl.navigateForward('/dispositivo-list');
    }
  }

  private dispositivoConectado() {
    if (!this.pacienteSeleccionado) {
      //this.utilsService.alerta("Falta el paciente", "Debe seleccionar un paciente");
      let p1 = this.translateConfigService.getTextoNow("TEST.falta_paci_1");
      let p2 = this.translateConfigService.getTextoNow("TEST.falta_paci_2");
      Promise.all([p1, p2]).then(values => {
        this.utilsService.alerta(values[0], values[1]);
      })
    } else {
      this.examenEnEjecucion = true;

      this.muestreoService.resetMuestreo();

      this.translateConfigService.getTextoNow("TEST.prueba_progreso").then(t1 => {
        this.utilsService.presentaToast(t1);
      })



      //if (this.examenEnEjecucion) {
      if (!this.sensorInnercialService.virtual) {
        //Escribimos en la caracteristica para iniciar la medicion
        this.sensorInnercialService.envioDeDatos(this.dispositivo.id, true).then(_ => {
          console.log("Comenzamos...");
        }).catch(async err => {
          console.error("No se pudo enviar la orden para comenzar las mediciones: " + err);
          let noCompletadoCuerpo = await this.translateConfigService.getTextoNow("ERRORES.noCompletadoCuerpo");
          let noCompletadoCabecera = await this.translateConfigService.getTextoNow("ERRORES.noCompletadoCabecera");
          this.utilsService.alerta(noCompletadoCabecera, noCompletadoCuerpo);
          this.sensorInnercialService.desconecta();
        });
      } else {
        //this.muestreoService.recibiendo = true;
        this.muestreoService.onDataDummy_10MTS();
      }

      //}
      //})

    }
  }

  /**
   * Termina de ejecutar el algoritmo
   */
  analizar() {
    console.log("analizo");
    this.voyAAnalizar = true;
    this.diezMtrService.sesionAnaliza(this.pacienteSeleccionado);
    this.navCtrl.navigateForward('/diez-mtr-resultados');
  }

  async finalizar() {

    if (this.segundos && this.segundos > 0) {

      this.examenEnEjecucion = false;
      if (!this.sensorInnercialService.virtual) {
        //Escribimos en la caracteristica para terminar la medicion
        this.sensorInnercialService.envioDeDatos(this.dispositivo.id, false).then(async _ => {
          //Deja de escuchar datos
          //this.ble.stopNotification(this.dispositivo.id, environment.BT_LTE.servicio.IMU_SERVICE.UUID, environment.BT_LTE.servicio.IMU_SERVICE.caracteristica.DATA_COMP_CHAR);
          let errorBuffer = await this.translateConfigService.getTextoNow("ERRORES.errorBuffer");
          this.utilsService.presentaToastDebug(errorBuffer);
        }).catch(async err => {
          console.error("No se pudo enviar la orden para terminar las mediciones: " + err);
          let noCompletadoCuerpo = await this.translateConfigService.getTextoNow("ERRORES.noCompletadoCuerpo");
          let noCompletadoCabecera = await this.translateConfigService.getTextoNow("ERRORES.noCompletadoCabecera");
          this.utilsService.alerta(noCompletadoCabecera, noCompletadoCuerpo);
          this.sensorInnercialService.desconecta();
        });
        this.translateConfigService.getTextoNow("TEST.prueba_fin").then(t1 => {
          this.utilsService.presentaToast(t1);
        })
        //});
      } else {
        this.translateConfigService.getTextoNow("TEST.prueba_fin").then(t1 => {
          this.utilsService.presentaToast(t1);
        })
      }

      //comienza el analisis
      console.log("Comienza el analisis");
      try {
        //let algoritmo = this.diezMtrService.analizaDatos(this.muestra, this.pacienteSeleccionado);
        //console.log("ALGORITMO : " + JSON.stringify(algoritmo));
        this.diezMtrService.sesionAddMedicion(this.muestreoService.muestra, this.pacienteSeleccionado);
      } catch (err) {
        //alert(err);
        let falloMedicionCabecera = await this.translateConfigService.getTextoNow("ERRORES.falloMedicionCabecera");
        let falloMedicionCuerpo = await this.translateConfigService.getTextoNow("ERRORES.falloMedicionCuerpo");
        this.utilsService.alerta(falloMedicionCabecera, falloMedicionCuerpo);
        console.error("Error al agregar medicion : " + err)
      }
    } else {
      this.utilsService.presentaToast("Cargando...")
    }
  }



}
