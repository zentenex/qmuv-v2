import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiezMtrPageRoutingModule } from './diez-mtr-routing.module';

import { DiezMtrPage } from './diez-mtr.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';
import { IconoBTModule } from 'src/app/componentes/icono-bt/icono-bt.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiezMtrPageRoutingModule,
    MenuInferiorModule,
    IconoBTModule
  ],
  declarations: [DiezMtrPage]
})
export class DiezMtrPageModule {}
