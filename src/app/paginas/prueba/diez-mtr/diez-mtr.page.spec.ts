import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiezMtrPage } from './diez-mtr.page';

describe('DiezMtrPage', () => {
  let component: DiezMtrPage;
  let fixture: ComponentFixture<DiezMtrPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiezMtrPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiezMtrPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
