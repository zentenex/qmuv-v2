import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaveGuard } from 'src/app/interceptores/LeaveGuard';

import { DiezMtrPage } from './diez-mtr.page';

const routes: Routes = [
  {
    path: '',
    canDeactivate: [LeaveGuard],
    component: DiezMtrPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiezMtrPageRoutingModule {}
