import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpgoHistorialPage } from './upgo-historial.page';

const routes: Routes = [
  {
    path: '',
    component: UpgoHistorialPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpgoHistorialPageRoutingModule {}
