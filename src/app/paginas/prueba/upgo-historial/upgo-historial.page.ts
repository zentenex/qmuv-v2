import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AnalisisUPGO, PruebasDAOService } from 'src/app/dao/pruebas-dao.service';
import { PacienteDTO, PacientesDAOService } from 'src/app/dao/pacientes-dao.service';
import { ActivatedRoute } from '@angular/router';
import { UpgoService } from 'src/app/services/prueba/upgo.service';
import { UtilsService } from 'src/app/services/utils.service';
import { Chart } from 'chart.js';
import { environment } from 'src/environments/environment';
import { NotificacionesComponent } from 'src/app/componentes/notificaciones/notificaciones.component';
import { PopoverController } from '@ionic/angular';
import { TranslateConfigService } from 'src/app/services/translate-config.service';

@Component({
  selector: 'app-upgo-historial',
  templateUrl: './upgo-historial.page.html',
  styleUrls: ['./upgo-historial.page.scss'],
})
export class UpgoHistorialPage implements OnInit {

  slideOpts1 = {

    spaceBetween: 0,
    initialSlide: 0,
    speed: 400
  };

  slideOpts = {
    slidesPerView: 1,
    spaceBetween: 0,
    initialSlide: 0,
    speed: 400
  };
  glosaResaltado = "";
  //Grafico ultima sesion
  @ViewChild("lineCanvas1", { static: false }) lineCanvas1: ElementRef;
  @ViewChild("lineCanvas2", { static: false }) lineCanvas2: ElementRef;
  @ViewChild("lineCanvas3", { static: false }) lineCanvas3: ElementRef;

  //Graficos historico
  @ViewChild("barCanvasRiesgoCaida", { static: false }) barCanvasRiesgoCaida: ElementRef;
  @ViewChild("pointCanvasEvolEtapa", { static: false }) pointCanvasEvolEtapa: ElementRef;
  @ViewChild("lineCanvasIndice1", { static: false }) lineCanvasIndice1: ElementRef;
  @ViewChild("lineCanvasIndice2", { static: false }) lineCanvasIndice2: ElementRef;
  @ViewChild("lineCanvasIndice3", { static: false }) lineCanvasIndice3: ElementRef;
  @ViewChild("lineCanvasIndice4", { static: false }) lineCanvasIndice4: ElementRef;
  @ViewChild("lineCanvasIndice5", { static: false }) lineCanvasIndice5: ElementRef;
  @ViewChild("lineCanvasIndice6", { static: false }) lineCanvasIndice6: ElementRef;
  @ViewChild("lineCanvasIndice7", { static: false }) lineCanvasIndice7: ElementRef;
  @ViewChild("lineCanvasIndice8", { static: false }) lineCanvasIndice8: ElementRef;

  tend1: number[] = [0, 0];
  tend2: number[] = [0, 0];
  tend3: number[] = [0, 0];
  tend4: number[] = [0, 0];
  tend5: number[] = [0, 0];
  tend6: number[] = [0, 0];
  tend7: number[] = [0, 0];
  tend8: number[] = [0, 0];

  private lineChart: Chart;

  private histRCChart: Chart;
  private histEEChart: Chart;
  private histI1Chart: Chart;
  private histI2Chart: Chart;
  private histI3Chart: Chart;
  private histI4Chart: Chart;
  private histI5Chart: Chart;
  private histI6Chart: Chart;
  private histI7Chart: Chart;
  private histI8Chart: Chart;

  analisis: AnalisisUPGO;
  analisisUltimos10: AnalisisUPGO[] = [];


  menorDuracion = 0;

  gfxDataTiempos: number[] = [];
  gfxDataT1: number[] = [];
  gfxDataT2: number[] = [];
  gfxDataT3: number[] = [];
  gfxDataT4: number[] = [];
  gfxDataT5: number[] = [];
  gfxDataT6: number[] = [];
  gfxDataAcel1: number[] = [];
  gfxDataAcel2: number[] = [];
  gfxDataVel1: number[] = [];
  gfxDataVel2: number[] = [];
  gfxDataCad1: number[] = [];
  gfxDataCad2: number[] = [];
  gfxDataInc1: number[] = [];
  gfxDataInc2: number[] = [];

  paciente: PacienteDTO;

  private dataRotacion1: any[] = [];
  private dataInclinacion1: any[] = [];
  private etapasGfx1: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  destacado1 = false;

  private dataRotacion2: any[] = [];
  private dataInclinacion2: any[] = [];
  private etapasGfx2: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  destacado2 = false;

  private dataRotacion3: any[] = [];
  private dataInclinacion3: any[] = [];
  private etapasGfx3: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  destacado3 = false;

  seleccion: number = 1;

  regiones: number[] = [];

  constructor(public popoverCtrl: PopoverController,
    private activatedRoute: ActivatedRoute,
    public upgoService: UpgoService,
    private utilsService: UtilsService,
    private pruebasDAOService: PruebasDAOService,
    private pacientesDAOService: PacientesDAOService,
    private translateConfigService: TranslateConfigService) { }

  segmentChanged(ev: any) {
    if (this.seleccion == 1) {
      this.pintaGraficoUltimaSesion();
    } else if (this.seleccion == 2) {
      this.pintaGraficoHistorico();
    }
  }

  private generaValoresParaGraficos() {
    for (let hist of this.analisisUltimos10) {
      this.gfxDataTiempos.push(hist.t);
      this.gfxDataT1.push(hist.t1);
      this.gfxDataT2.push(hist.t2);
      this.gfxDataT3.push(hist.t3);
      this.gfxDataT4.push(hist.t4);
      this.gfxDataT5.push(hist.t5);
      this.gfxDataT6.push(hist.t6);

      this.gfxDataAcel1.push(hist.aceleracionAlLevantarse);
      this.gfxDataAcel2.push(hist.aceleracionAlSentarse);
      this.gfxDataVel1.push(hist.velocidadAngularGiro3m);
      this.gfxDataVel2.push(hist.velocidadAngularGiroSilla);
      this.gfxDataCad1.push(hist.cadenciaIda);
      this.gfxDataCad2.push(hist.cadenciaVuelta);
      this.gfxDataInc1.push(hist.maxStandInclination);
      this.gfxDataInc2.push(hist.maxSitInclination);
    }

    this.tend1 = this.utilsService.tendence('accel', this.gfxDataAcel1);
    this.tend2 = this.utilsService.tendence('accel', this.gfxDataAcel2);
    this.tend3 = this.utilsService.tendence('angvel', this.gfxDataVel1);
    this.tend4 = this.utilsService.tendence('angvel', this.gfxDataVel2);
    this.tend5 = this.utilsService.tendence('other', this.gfxDataCad1);
    this.tend6 = this.utilsService.tendence('other', this.gfxDataCad2);
    this.tend7 = this.utilsService.tendence('inclination', this.gfxDataInc1);
    this.tend8 = this.utilsService.tendence('inclination', this.gfxDataInc2);

  }

  ngOnInit() {

    let idPaciente = this.activatedRoute.snapshot.paramMap.get('id');
    this.pacientesDAOService.pacienteBusca(+idPaciente).then(elPaciente => {
      this.paciente = elPaciente;
      this.analisis = this.pruebasDAOService.loadUltimoAnalisisUPGO(elPaciente.id);
      this.analisisUltimos10 = this.pruebasDAOService.loadUltimos10AnalisisUpgo(elPaciente.id);

      this.generaValoresParaGraficos();

      if (this.analisis) {
        console.log("con analisis : " + JSON.stringify(this.analisis));
        let i = 0;
        for (let elem of this.analisis.rotacionesMulti[0]) {
          let unpunto: any = {};
          unpunto.x = i / environment.BT_LTE.frecuencia;
          unpunto.y = elem;
          this.dataRotacion1.push(unpunto);
          i++;
        }

        i = 0;
        for (let elem of this.analisis.inclinacionesMulti[0]) {
          let unpunto: any = {};
          unpunto.x = i / environment.BT_LTE.frecuencia;
          unpunto.y = elem;
          this.dataInclinacion1.push(unpunto);
          i++;
        }
        /*
                let tiempoTotalEtapas = this.analisis.t1 + this.analisis.t2 + this.analisis.t3 + this.analisis.t4 + this.analisis.t5 + this.analisis.t6;
              let maxCantidadDePuntos = Math.max(this.analisis.rotacionesMulti[0].length, this.analisis.inclinacionesMulti[0].length);
              this.etapasGfx1[0] = (this.analisis.t1) * maxCantidadDePuntos / tiempoTotalEtapas;
              this.etapasGfx1[1] = (this.analisis.t1 + this.analisis.t2) * maxCantidadDePuntos / tiempoTotalEtapas;
              this.etapasGfx1[2] = (this.analisis.t1 + this.analisis.t2 + this.analisis.t3) * maxCantidadDePuntos / tiempoTotalEtapas;
              this.etapasGfx1[3] = (this.analisis.t1 + this.analisis.t2 + this.analisis.t3 + this.analisis.t4) * maxCantidadDePuntos / tiempoTotalEtapas;
              this.etapasGfx1[4] = (this.analisis.t1 + this.analisis.t2 + this.analisis.t3 + this.analisis.t4 + this.analisis.t5) * maxCantidadDePuntos / tiempoTotalEtapas;
              this.etapasGfx1[5] = (this.analisis.t1 + this.analisis.t2 + this.analisis.t3 + this.analisis.t4 + this.analisis.t5 + this.analisis.t6) * maxCantidadDePuntos / tiempoTotalEtapas;
        */

        if (this.analisis.eventosMulti.length > 0) {
          let maxCantidadDePuntos = Math.max(this.analisis.rotacionesMulti[0].length, this.analisis.inclinacionesMulti[0].length);

          this.etapasGfx1[0] = 0;
          this.etapasGfx1[1] = this.analisis.eventosMulti[0][0];
          this.etapasGfx1[2] = this.analisis.eventosMulti[0][1];
          this.etapasGfx1[3] = this.analisis.eventosMulti[0][2];
          this.etapasGfx1[4] = this.analisis.eventosMulti[0][3];
          this.etapasGfx1[5] = this.analisis.eventosMulti[0][4];
          this.etapasGfx1[6] = this.analisis.eventosMulti[0][5];
          this.etapasGfx1[7] = this.analisis.eventosMulti[0][6];
          this.etapasGfx1[8] = this.analisis.eventosMulti[0][7];
          this.etapasGfx1[9] = maxCantidadDePuntos / environment.BT_LTE.frecuencia;
        } else {
          console.warn("No hay data para el grafico 1 de upgo");
        }


        //seteo el menor de los tiempos
        this.menorDuracion = Math.min(...this.gfxDataTiempos);

      } else {
        console.error("Sin analisis");
      }

      if (this.analisis && this.analisis.tiemposMulti.length > 1) {
        let i = 0;
        for (let elem of this.analisis.rotacionesMulti[1]) {
          let unpunto: any = {};
          unpunto.x = i / environment.BT_LTE.frecuencia;
          unpunto.y = elem;
          this.dataRotacion2.push(unpunto);
          i++;
        }

        i = 0;
        for (let elem of this.analisis.inclinacionesMulti[1]) {
          let unpunto: any = {};
          unpunto.x = i / environment.BT_LTE.frecuencia;
          unpunto.y = elem;
          this.dataInclinacion2.push(unpunto);
          i++;
        }
        /*
              let tiempoTotalEtapas = this.analisis.t1 + this.analisis.t2 + this.analisis.t3 + this.analisis.t4 + this.analisis.t5 + this.analisis.t6;
              let maxCantidadDePuntos = Math.max(this.analisis.rotacionesMulti[1].length, this.analisis.inclinacionesMulti[1].length);
              this.etapasGfx2[0] = (this.analisis.t1) * maxCantidadDePuntos / tiempoTotalEtapas;
              this.etapasGfx2[1] = (this.analisis.t1 + this.analisis.t2) * maxCantidadDePuntos / tiempoTotalEtapas;
              this.etapasGfx2[2] = (this.analisis.t1 + this.analisis.t2 + this.analisis.t3) * maxCantidadDePuntos / tiempoTotalEtapas;
              this.etapasGfx2[3] = (this.analisis.t1 + this.analisis.t2 + this.analisis.t3 + this.analisis.t4) * maxCantidadDePuntos / tiempoTotalEtapas;
              this.etapasGfx2[4] = (this.analisis.t1 + this.analisis.t2 + this.analisis.t3 + this.analisis.t4 + this.analisis.t5) * maxCantidadDePuntos / tiempoTotalEtapas;
              this.etapasGfx2[5] = (this.analisis.t1 + this.analisis.t2 + this.analisis.t3 + this.analisis.t4 + this.analisis.t5 + this.analisis.t6) * maxCantidadDePuntos / tiempoTotalEtapas;
      */

        if (this.analisis.eventosMulti.length > 1) {
          let maxCantidadDePuntos = Math.max(this.analisis.rotacionesMulti[1].length, this.analisis.inclinacionesMulti[1].length);

          this.etapasGfx2[0] = 0;
          this.etapasGfx2[1] = this.analisis.eventosMulti[1][0];
          this.etapasGfx2[2] = this.analisis.eventosMulti[1][1];
          this.etapasGfx2[3] = this.analisis.eventosMulti[1][2];
          this.etapasGfx2[4] = this.analisis.eventosMulti[1][3];
          this.etapasGfx2[5] = this.analisis.eventosMulti[1][4];
          this.etapasGfx2[6] = this.analisis.eventosMulti[1][5];
          this.etapasGfx2[7] = this.analisis.eventosMulti[1][6];
          this.etapasGfx2[8] = this.analisis.eventosMulti[1][7];
          this.etapasGfx2[9] = maxCantidadDePuntos / environment.BT_LTE.frecuencia;
        } else {
          console.warn("No hay data para el grafico 2 de upgo");
        }
      }

      if (this.analisis && this.analisis.tiemposMulti.length > 2) {
        let i = 0;
        for (let elem of this.analisis.rotacionesMulti[2]) {
          let unpunto: any = {};
          unpunto.x = i / environment.BT_LTE.frecuencia;
          unpunto.y = elem;
          this.dataRotacion3.push(unpunto);
          i++;
        }

        i = 0;
        for (let elem of this.analisis.inclinacionesMulti[2]) {
          let unpunto: any = {};
          unpunto.x = i / environment.BT_LTE.frecuencia;
          unpunto.y = elem;
          this.dataInclinacion3.push(unpunto);
          i++;
        }

        /*
        let tiempoTotalEtapas = this.analisis.t1 + this.analisis.t2 + this.analisis.t3 + this.analisis.t4 + this.analisis.t5 + this.analisis.t6;
        let maxCantidadDePuntos = Math.max(this.analisis.rotacionesMulti[2].length, this.analisis.inclinacionesMulti[2].length);
        this.etapasGfx3[0] = (this.analisis.t1) * maxCantidadDePuntos / tiempoTotalEtapas;
        this.etapasGfx3[1] = (this.analisis.t1 + this.analisis.t2) * maxCantidadDePuntos / tiempoTotalEtapas;
        this.etapasGfx3[2] = (this.analisis.t1 + this.analisis.t2 + this.analisis.t3) * maxCantidadDePuntos / tiempoTotalEtapas;
        this.etapasGfx3[3] = (this.analisis.t1 + this.analisis.t2 + this.analisis.t3 + this.analisis.t4) * maxCantidadDePuntos / tiempoTotalEtapas;
        this.etapasGfx3[4] = (this.analisis.t1 + this.analisis.t2 + this.analisis.t3 + this.analisis.t4 + this.analisis.t5) * maxCantidadDePuntos / tiempoTotalEtapas;
        this.etapasGfx3[5] = (this.analisis.t1 + this.analisis.t2 + this.analisis.t3 + this.analisis.t4 + this.analisis.t5 + this.analisis.t6) * maxCantidadDePuntos / tiempoTotalEtapas;
        */

        if (this.analisis.eventosMulti.length > 2) {
          let maxCantidadDePuntos = Math.max(this.analisis.rotacionesMulti[2].length, this.analisis.inclinacionesMulti[2].length);
          this.etapasGfx3[0] = 0;
          this.etapasGfx3[1] = this.analisis.eventosMulti[2][0];
          this.etapasGfx3[2] = this.analisis.eventosMulti[2][1];
          this.etapasGfx3[3] = this.analisis.eventosMulti[2][2];
          this.etapasGfx3[4] = this.analisis.eventosMulti[2][3];
          this.etapasGfx3[5] = this.analisis.eventosMulti[2][4];
          this.etapasGfx3[6] = this.analisis.eventosMulti[2][5];
          this.etapasGfx3[7] = this.analisis.eventosMulti[2][6];
          this.etapasGfx3[8] = this.analisis.eventosMulti[2][7];
          this.etapasGfx3[9] = maxCantidadDePuntos / environment.BT_LTE.frecuencia;
        } else {
          console.warn("No hay data para el grafico 3 de upgo");
        }
      }

      //Determino el grafico a resaltar
      let tempoWin = -1;
      let winner = -1;
      let participante = 0;
      for (let esteTiempo of this.analisis.tiemposMulti) {
        if (esteTiempo == 0) {
          continue;
        }

        if (this.analisis.analysisMode == 0) {
          //El mejor tiempo, el menor
          this.translateConfigService.getTextoNow("TEST.10MTS.medi_best").then(txt => {
            this.glosaResaltado = "(" + txt + ")";
          })
          if (tempoWin > esteTiempo || tempoWin == -1) {
            tempoWin = esteTiempo;
            winner = participante;
          }
        } else if (this.analisis.analysisMode == 1) {
          //El peor tiempo, el mas grande
          this.translateConfigService.getTextoNow("TEST.10MTS.medi_worst").then(txt => {
            this.glosaResaltado = "(" + txt + ")";
          });
          if (tempoWin < esteTiempo || tempoWin == -1) {
            tempoWin = esteTiempo;
            winner = participante;
          }
        }
        else if (this.analisis.analysisMode == 2) {
          //this.glosaResaltado = "(La peor medición)";
          this.translateConfigService.getTextoNow("TEST.10MTS.average").then(txt => {
            this.glosaResaltado = "(" + txt + ")";
          });
        }
        participante++;
      }
      if (winner == 0 || winner == -1) {
        this.destacado1 = true;
      }
      if (winner == 1 || winner == -1) {
        this.destacado2 = true;
      }
      if (winner == 2 || winner == -1) {
        this.destacado3 = true;
      }

      this.pintaGraficoUltimaSesion();
    });

  }

  async pintaGraficoUltimaSesion() {
    let text_rot = await this.translateConfigService.getTextoNow("TEST.UPGO.gfx_rot")
    let text_inc = await this.translateConfigService.getTextoNow("TEST.UPGO.gfx_inc")
    setTimeout(() => {

      if (this.lineCanvas1) {
        this.lineChart = new Chart(this.lineCanvas1.nativeElement, {
          type: "line",
          data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
              {
                label: text_rot, //"Rotación",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(101, 108, 188,0.4)",
                borderColor: "rgba(101, 108, 188,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(101, 108, 188,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.dataRotacion1,
                spanGaps: false
              },
              {
                label: text_inc, //"Inclinacion",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 191, 96,0.4)",
                borderColor: "rgba(255, 191, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(255, 191, 96,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.dataInclinacion1,
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              
              xAxes: {
                type: 'linear',
                position: 'bottom'
              }
            },
            plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (context) { //tooltipItem, data) {
                    let label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              },
            
            annotation: {
              annotations: [
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[0],
                  "xMax": this.etapasGfx1[1],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(250,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                },
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[1],
                  "xMax": this.etapasGfx1[2],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(200,200,200,0.25)",
                  "borderColor": "rgba(200,200,200,0.25)"
                },
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[2],
                  "xMax": this.etapasGfx1[3],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(60,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                }, {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[3],
                  "xMax": this.etapasGfx1[4],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(200,200,200,0.25)",
                  "borderColor": "rgba(200,200,200,0.25)"
                },
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[4],
                  "xMax": this.etapasGfx1[5],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(60,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                },
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[5],
                  "xMax": this.etapasGfx1[6],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(200,200,200,0.25)",
                  "borderColor": "rgba(200,200,200,0.25)"
                },
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[6],
                  "xMax": this.etapasGfx1[7],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(60,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                }, {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[7],
                  "xMax": this.etapasGfx1[8],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(200,200,200,0.25)",
                  "borderColor": "rgba(200,200,200,0.25)"
                },
                {
                  "type": "box",
                  "xScaleID": "x-axis-0",
                  "yScaleID": "y-axis-0",
                  "xMin": this.etapasGfx1[8],
                  "xMax": this.etapasGfx1[9],
                  "borderWidth": 1,
                  "backgroundColor": "rgba(250,60,60,0.25)",
                  "borderColor": "rgba(60,60,60,0.25)"
                }]
            }
          },
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvas2) {
        this.lineChart = new Chart(this.lineCanvas2.nativeElement, {
          type: "line",
          data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
              {
                label: text_rot, //"Rotación",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(101, 108, 188,0.4)",
                borderColor: "rgba(101, 108, 188,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(101, 108, 188,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.dataRotacion2,
                spanGaps: false
              },
              {
                label: text_inc, //"Inclinacion",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 191, 96,0.4)",
                borderColor: "rgba(255, 191, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(255, 191, 96,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.dataInclinacion2,
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              xAxes: {
                type: 'linear',
                position: 'bottom'
              }
            }, plugins:{ 
            
              tooltip: {
                callbacks: {
                  label: function (context) {
                    var label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              },
              annotation: {
                annotations: [
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx2[0],
                    "xMax": this.etapasGfx2[1],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(250,60,60,0.25)",
                    "borderColor": "rgba(60,60,60,0.25)"
                  },
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx2[1],
                    "xMax": this.etapasGfx2[2],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(200,200,200,0.25)",
                    "borderColor": "rgba(200,200,200,0.25)"
                  },
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx2[2],
                    "xMax": this.etapasGfx2[3],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(60,60,60,0.25)",
                    "borderColor": "rgba(60,60,60,0.25)"
                  }, {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx2[3],
                    "xMax": this.etapasGfx2[4],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(200,200,200,0.25)",
                    "borderColor": "rgba(200,200,200,0.25)"
                  },
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx2[4],
                    "xMax": this.etapasGfx2[5],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(60,60,60,0.25)",
                    "borderColor": "rgba(60,60,60,0.25)"
                  },
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx2[5],
                    "xMax": this.etapasGfx2[6],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(200,200,200,0.25)",
                    "borderColor": "rgba(200,200,200,0.25)"
                  },
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx2[6],
                    "xMax": this.etapasGfx2[7],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(60,60,60,0.25)",
                    "borderColor": "rgba(60,60,60,0.25)"
                  }, {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx2[7],
                    "xMax": this.etapasGfx2[8],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(200,200,200,0.25)",
                    "borderColor": "rgba(200,200,200,0.25)"
                  },
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx2[8],
                    "xMax": this.etapasGfx2[9],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(250,60,60,0.25)",
                    "borderColor": "rgba(60,60,60,0.25)"
                  }]
              }
            },
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvas3) {
        this.lineChart = new Chart(this.lineCanvas3.nativeElement, {
          type: "line",
          data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
              {
                label: text_rot, //"Rotación",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(101, 108, 188,0.4)",
                borderColor: "rgba(101, 108, 188,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(101, 108, 188,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.dataRotacion3,
                spanGaps: false
              },
              {
                label: text_inc, //"Inclinacion",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 191, 96,0.4)",
                borderColor: "rgba(255, 191, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(255, 191, 96,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.dataInclinacion3,
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              xAxes: {
                type: 'linear',
                position: 'bottom'
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (context) {
                    var label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              },
              annotation: {
                annotations: [
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx3[0],
                    "xMax": this.etapasGfx3[1],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(250,60,60,0.25)",
                    "borderColor": "rgba(60,60,60,0.25)"
                  },
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx3[1],
                    "xMax": this.etapasGfx3[2],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(200,200,200,0.25)",
                    "borderColor": "rgba(200,200,200,0.25)"
                  },
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx3[2],
                    "xMax": this.etapasGfx3[3],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(60,60,60,0.25)",
                    "borderColor": "rgba(60,60,60,0.25)"
                  }, {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx3[3],
                    "xMax": this.etapasGfx3[4],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(200,200,200,0.25)",
                    "borderColor": "rgba(200,200,200,0.25)"
                  },
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx3[4],
                    "xMax": this.etapasGfx3[5],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(60,60,60,0.25)",
                    "borderColor": "rgba(60,60,60,0.25)"
                  },
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx3[5],
                    "xMax": this.etapasGfx3[6],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(200,200,200,0.25)",
                    "borderColor": "rgba(200,200,200,0.25)"
                  },
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx3[6],
                    "xMax": this.etapasGfx3[7],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(60,60,60,0.25)",
                    "borderColor": "rgba(60,60,60,0.25)"
                  }, {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx3[7],
                    "xMax": this.etapasGfx3[8],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(200,200,200,0.25)",
                    "borderColor": "rgba(200,200,200,0.25)"
                  },
                  {
                    "type": "box",
                    "xScaleID": "x-axis-0",
                    "yScaleID": "y-axis-0",
                    "xMin": this.etapasGfx3[8],
                    "xMax": this.etapasGfx3[9],
                    "borderWidth": 1,
                    "backgroundColor": "rgba(250,60,60,0.25)",
                    "borderColor": "rgba(60,60,60,0.25)"
                  }]
              }
            },
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

    }, environment.delayGfxMiliSeg);
  }


  async pintaGraficoHistorico() {
    let text_dur = await this.translateConfigService.getTextoNow("COMUN.gfx_dura")

    let txt_e1 = await this.translateConfigService.getTextoNow("TEST.UPGO.etapas.e1")
    let txt_e2 = await this.translateConfigService.getTextoNow("TEST.UPGO.etapas.e2")
    let txt_e3 = await this.translateConfigService.getTextoNow("TEST.UPGO.etapas.e3")
    let txt_e4 = await this.translateConfigService.getTextoNow("TEST.UPGO.etapas.e4")
    let txt_e5 = await this.translateConfigService.getTextoNow("TEST.UPGO.etapas.e5")
    let txt_e6 = await this.translateConfigService.getTextoNow("TEST.UPGO.etapas.e6")

    let txt_movi1 = await this.translateConfigService.getTextoNow("TEST.UPGO.idx_movi.inc_par")
    let txt_movi2 = await this.translateConfigService.getTextoNow("TEST.UPGO.idx_movi.inc_sen")
    let txt_movi3 = await this.translateConfigService.getTextoNow("TEST.UPGO.idx_movi.ace_lev")
    let txt_movi4 = await this.translateConfigService.getTextoNow("TEST.UPGO.idx_movi.ace_sen")
    let txt_movi5 = await this.translateConfigService.getTextoNow("TEST.UPGO.idx_movi.vel_3m")
    let txt_movi6 = await this.translateConfigService.getTextoNow("TEST.UPGO.idx_movi.vel_sil")

    setTimeout(() => {

      if (this.barCanvasRiesgoCaida) {
        this.histRCChart = new Chart(this.barCanvasRiesgoCaida.nativeElement, {
          type: "bar",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: text_dur, //"Duración de la prueba",
                data: this.gfxDataTiempos, //[12, 19, 3, 5, 2, 3, 12, 19, 3, 5],
                backgroundColor: [
                  "rgba(101, 108, 188, 0.2)", //"rgba(255, 99, 132, 0.2)",
                  "rgba(101, 108, 188, 0.2)",
                  "rgba(101, 108, 188, 0.2)", //"rgba(255, 206, 86, 0.2)",
                  "rgba(101, 108, 188, 0.2)", //"rgba(75, 192, 192, 0.2)",
                  "rgba(101, 108, 188, 0.2)", //"rgba(153, 102, 255, 0.2)",
                  "rgba(101, 108, 188, 0.2)", //"rgba(255, 159, 64, 0.2)",
                  "rgba(101, 108, 188, 0.2)", //"rgba(255, 99, 132, 0.2)",
                  "rgba(101, 108, 188, 0.2)", //"rgba(54, 162, 235, 0.2)",
                  "rgba(101, 108, 188, 0.2)", //"rgba(255, 206, 86, 0.2)",
                  "rgba(101, 108, 188, 0.2)", //"rgba(75, 192, 192, 0.2)"
                ],
                borderColor: [
                  "rgba(66, 77, 153, 1)", //"rgba(255,99,132,1)",
                  "rgba(66, 77, 153, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(255, 206, 86, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(75, 192, 192, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(153, 102, 255, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(255, 159, 64, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(255,99,132,1)",
                  "rgba(66, 77, 153, 1)", //"rgba(54, 162, 235, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(255, 206, 86, 1)",
                  "rgba(66, 77, 153, 1)", //"rgba(75, 192, 192, 1)"
                ],
                borderWidth: 1
              }
            ]
          },
          options: {
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                enabled: false, // <-- this option disables tooltips
                callbacks: {
                  label: function (context) {
                    var label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              },
              legend: {
                display: false
              }, annotation: {
                annotations: [
                  {
                    type: 'line',
                    //mode: 'horizontal',
                    scaleID: 'y-axis-0',
                    value: 10,
                    borderColor: 'rgb(0, 255, 0)',
                    borderWidth: 2,
                    label: {
                      //enabled: false,
                      content: 'Leve'
                    }
                  }, {
                    type: 'line',
                    //mode: 'horizontal',
                    scaleID: 'y-axis-0',
                    value: 20,
                    borderColor: 'rgb(255, 0, 0)',
                    borderWidth: 2,
                    label: {
                      //enabled: false,
                      content: 'Alto'
                    }
                  }]
              }
            }
          },
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.pointCanvasEvolEtapa) {
        this.histEEChart = new Chart(this.pointCanvasEvolEtapa.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: txt_e1, //"Levantarse",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(101, 108, 188,0.4)",
                borderColor: "rgba(101, 108, 188,1)",
                borderCapStyle: "butt",
                borderWidth: 1,
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(101, 108, 188,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(101, 108, 188,1)",
                pointHoverBorderColor: "rgba(220,9,220,1)",
                pointHoverBorderWidth: 2,
                //pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataT1, //[12, 19, 3, 5, 2, 3, 12, 19, 3, 5],
                spanGaps: false
              },
              {
                label: txt_e2, //"Ida",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(255, 191, 96,0.4)",
                borderColor: "rgba(255, 191, 96,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderWidth: 1,
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(255, 191, 96,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(255, 191, 96,1)",
                pointHoverBorderColor: "rgba(120,220,220,1)",
                pointHoverBorderWidth: 2,
                pointHitRadius: 10,
                data: this.gfxDataT2, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              },
              {
                label: txt_e3, //"Giro 3m",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(66, 77, 153,0.4)",
                borderColor: "rgba(66, 77, 153,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderWidth: 1,
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(66, 77, 153,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(66, 77, 153,1)",
                pointHoverBorderColor: "rgba(120,120,220,1)",
                pointHoverBorderWidth: 2,
                pointHitRadius: 10,
                data: this.gfxDataT3, //[50, 20, 90, 30, 50, 20, 30, 20, 90, 30],
                spanGaps: false
              },
              {
                label: txt_e4, //"Vuelta",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(217, 138, 20,0.4)",
                borderColor: "rgba(217, 138, 20,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderWidth: 1,
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(217, 138, 20,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(217, 138, 20,1)",
                pointHoverBorderColor: "rgba(120,120,120,1)",
                pointHoverBorderWidth: 2,
                pointHitRadius: 10,
                data: this.gfxDataT4, //[15, 12, 19, 13, 15, 12, 13, 12, 19, 13],
                spanGaps: false
              },
              {
                label: txt_e5, //"Giro silla",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(183, 77, 92,0.4)",
                borderColor: "rgba(183, 77, 92,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderWidth: 1,
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(183, 77, 92,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(183, 77, 92,1)",
                pointHoverBorderColor: "rgba(0,120,120,1)",
                pointHoverBorderWidth: 2,
                pointHitRadius: 10,
                data: this.gfxDataT5, //[25, 22, 29, 23, 25, 22, 23, 22, 29, 23],
                spanGaps: false
              },
              {
                label: txt_e6, //"Sentarse",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(122,0,55,0.4)",
                borderColor: "rgba(122,0,55,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderWidth: 1,
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(122,0,55,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(122,0,55,1)",
                pointHoverBorderColor: "rgba(200,120,120,1)",
                pointHoverBorderWidth: 2,
                pointHitRadius: 10,
                data: this.gfxDataT6, //[11, 25, 22, 29, 23, 25, 22, 23, 22, 29],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
              
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (context) {
                    var label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasIndice1) {
        this.histI1Chart = new Chart(this.lineCanvasIndice1.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: txt_movi3, //"Aceleración al levantarse",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataAcel1, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (context) {
                    var label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              },
              legend: {
                display: false
              }
            },
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasIndice2) {
        this.histI2Chart = new Chart(this.lineCanvasIndice2.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: txt_movi4, //"Aceleración al sentarse",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataAcel2, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (context) {
                    var label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              },
              legend: {
                display: false
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasIndice3) {
        this.histI3Chart = new Chart(this.lineCanvasIndice3.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: txt_movi5, //"Vel 1",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataVel1, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (context) {
                    var label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              },
              legend: {
                display: false
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasIndice4) {
        this.histI4Chart = new Chart(this.lineCanvasIndice4.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: txt_movi6, //"Vel 2",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataVel2, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (context) {
                    var label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              },
              legend: {
                display: false
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasIndice5) {
        this.histI5Chart = new Chart(this.lineCanvasIndice5.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: "Cad 1",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataCad1, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (context) {
                    var label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              },
              legend: {
                display: false
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo (deprecado)");
      }

      if (this.lineCanvasIndice6) {
        this.histI6Chart = new Chart(this.lineCanvasIndice6.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: "Cad 2",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataCad2, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (context) {
                    var label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              },
              legend: {
                display: false
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo (deprecado)");
      }

      if (this.lineCanvasIndice7) {
        this.histI7Chart = new Chart(this.lineCanvasIndice7.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: txt_movi1, //"Inc 1",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataInc1, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (context) {
                    var label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              },
              legend: {
                display: false
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

      if (this.lineCanvasIndice8) {
        this.histI8Chart = new Chart(this.lineCanvasIndice8.nativeElement, {
          type: "line",
          data: {
            labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
            datasets: [
              {
                label: txt_movi2, //"Inc 2",
                fill: false,
                //lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.gfxDataInc2, //[5, 12, 19, 3, 5, 2, 3, 12, 19, 3],
                spanGaps: false
              }
            ]
          }, options: {
            elements: {
              line: {
                  tension: 0.1
              }
            },
            scales: {
              y: {
                min: 0
              }
            }, plugins:{ 
              tooltip: {
                callbacks: {
                  label: function (context) {
                    var label = context.dataset[context.datasetIndex].label || '';

                    if (label) {
                      label += ': ';
                    }
                    label += Math.round(+context.label * 100) / 100;
                    return label;
                  }
                }
              },
              legend: {
                display: false
              }
            }
          }
        });
      } else {
        console.error("El canvas no esta listo");
      }

    }, environment.delayGfxMiliSeg);
  }

  async notifications(code: number) {
    const popover = await this.popoverCtrl.create({
      component: NotificacionesComponent,
      cssClass: 'contact-popover',
      //event: ev,  
      animated: true,
      showBackdrop: true,
      componentProps: { codigo: code }
    });
    return await popover.present();
  }
}
