import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpgoHistorialPage } from './upgo-historial.page';

describe('UpgoHistorialPage', () => {
  let component: UpgoHistorialPage;
  let fixture: ComponentFixture<UpgoHistorialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgoHistorialPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpgoHistorialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
