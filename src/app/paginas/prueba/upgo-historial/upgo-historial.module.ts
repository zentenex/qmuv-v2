import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpgoHistorialPageRoutingModule } from './upgo-historial-routing.module';

import { UpgoHistorialPage } from './upgo-historial.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpgoHistorialPageRoutingModule,
    MenuInferiorModule
  ],
  declarations: [UpgoHistorialPage]
})
export class UpgoHistorialPageModule {}
