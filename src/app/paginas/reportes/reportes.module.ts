import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportesPageRoutingModule } from './reportes-routing.module';

import { ReportesPage } from './reportes.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';
import { IconoBTModule } from 'src/app/componentes/icono-bt/icono-bt.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ReportesPageRoutingModule,
    MenuInferiorModule,
    IconoBTModule
  ],
  declarations: [ReportesPage]
})
export class ReportesPageModule {}
