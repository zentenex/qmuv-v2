import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PacienteService } from 'src/app/services/paciente.service';
import { PacienteDTO, PacientesDAOService, ReporteDTO } from 'src/app/dao/pacientes-dao.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UtilsService } from 'src/app/services/utils.service';
import { Chart } from 'chart.js';
import annotationPlugin from 'chartjs-plugin-annotation';
import { Analisis10MTS, AnalisisUPGO, PruebasDAOService } from 'src/app/dao/pruebas-dao.service';
import { LoadingController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { CuentaDAOService } from 'src/app/dao/cuenta-dao.service';
import { TranslateConfigService } from 'src/app/services/translate-config.service';
import { ModalController } from '@ionic/angular';
import { SeleccionPacientePage } from 'src/app/componentes/seleccion-paciente/seleccion-paciente.page';


@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.page.html',
  styleUrls: ['./reportes.page.scss'],
})
export class ReportesPage implements OnInit {
  loadingSave: HTMLIonLoadingElement = null;

  existeDataPrueba1 = false;
  existeDataPrueba2 = false;

  //Graficos historico 1
  @ViewChild("barCanvasRiesgoCaida", { static: false }) barCanvasRiesgoCaida: ElementRef;
  //Graficos historico 2
  @ViewChild("barCanvasVelocidadMarcha", { static: false }) barCanvasVelocidadMarcha: ElementRef;
  private histRCChart: Chart;

  pacientes: PacienteDTO[] = [];
  pacienteSeleccionado: PacienteDTO = null;
  seleccionaUPGO: boolean = false;
  selecciona30mts: boolean = false;

  muestraDatosDest = false;
  gfxDataTiempos: number[] = [];
  analisisUltimos10: AnalisisUPGO[] = [];

  gfxDataTiempos2: number[] = [];
  analisisUltimos10_2: Analisis10MTS[] = [];

  repoCompara = false;

  emailKine = "";
  nombreKine = "";
  nombrePaciente = "";

  userForm: FormGroup;
  /*
  validation_messages = {
    'nombre': [
      { type: 'required', message: 'El nombre es requerido.' }
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'paciente': [
      { type: 'required', message: 'El paciente es requerido.' }
    ]
  };*/
  validation_messages = {
    'nombre': [],
    'email': [],
    'paciente': []
  };
  pacienteSelect: any;

  private async setMensajes() {
    this.validation_messages.nombre.push({ type: 'required', message: await this.translateConfigService.getTextoNow("REPO.requ.nombre") });
    this.validation_messages.email.push({ type: 'required', message: await this.translateConfigService.getTextoNow("REPO.requ.email_1") });
    this.validation_messages.email.push({ type: 'pattern', message: await this.translateConfigService.getTextoNow("REPO.requ.email_2") });
    this.validation_messages.paciente.push({ type: 'required', message: await this.translateConfigService.getTextoNow("REPO.requ.paciente") });
  }

  constructor(private cuentaDAO: CuentaDAOService,
    private pacientesDAOService: PacientesDAOService,
    private loadingController: LoadingController,
    private pruebasDAOService: PruebasDAOService,
    private utilsService: UtilsService,
    private pacienteService: PacienteService,
    public modalController: ModalController,
    private formBuilder: FormBuilder,
    private translateConfigService: TranslateConfigService) {
    console.log("En el constructor de reportes");
  }

  ngOnInit() {
    console.log("En el ngOnInit de reportes");

    Chart.register(annotationPlugin);

    this.setMensajes();

    this.cuentaDAO.obtieneDetalleUsuario().then(u => {
      if (u.code >= 0) {
        console.log("Se obtuvo usuario desde el servidor : " + u.user.name)
        this.emailKine = u.user.email;
        this.nombreKine = u.user.name;
      }
    }).catch(_ => {
      this.emailKine = "usuario@local";
      this.nombreKine = "usuario kine";
    });

    this.pacienteService.pacientesActivos("").then(losPacientes => {
      this.pacientes = losPacientes;
    });

    let elPlan = environment.planes.find(x => x.CODE === this.utilsService.planCODE);
    if (elPlan != null && elPlan.REPORTE_DE_COMPARATIVA) {
      this.repoCompara = true;
    } else {
      this.repoCompara = false;
    }

    this.userForm = this.formBuilder.group({
      nombre: new FormControl('', Validators.compose([
        Validators.required
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      paciente: new FormControl('', Validators.compose([
        Validators.required
      ])),
      destinatario: new FormControl('0'),
      testUPGO: new FormControl([]),
      test30mts: new FormControl([])
    });
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: SeleccionPacientePage,
      componentProps: {
        'pacientes': this.pacientes,
      },
      cssClass: 'modalSeleccionPaciente',
      backdropDismiss:false
    });

    modal.onDidDismiss()
      .then((data) => {
        if (data) {
          const idUsuario = data['data']; // Here's your selected user!
          if (!idUsuario.dismissed) {
            this.pacienteChange(idUsuario);
          }
        }
      });

    modal.dismiss({
      'dismissed': true
    });

    return await modal.present();

  }

  onDestChange($event) {
    console.log("no se que es esto", $event);
    console.log("no se que es esto", this.pacienteSelect);

    if ("0" === $event.target.value) {
      this.muestraDatosDest = false;
      if (this.userForm.value["paciente"] != null) {
        this.userForm.patchValue({ "nombre": this.pacienteSelect.nombre });
        this.userForm.patchValue({ "email": this.pacienteSelect.correo });
      }
    } else if ("2" === $event.target.value) {
      this.muestraDatosDest = false;
      this.userForm.patchValue({ "nombre": this.nombreKine });
      this.userForm.patchValue({ "email": this.emailKine });

    } else {
      this.userForm.patchValue({ "nombre": "" });
      this.userForm.patchValue({ "email": "" });
      this.muestraDatosDest = true;
    }
    console.log(this.userForm);

  }

  async pacienteChange($event: any) {

    this.pacienteSelect = $event;
    //alert(JSON.stringify(paciente));
    this.nombrePaciente = this.pacienteSelect.nombre;

    //Obtenemos datos para el grafico historico
    //this.analisisUltimos10 = this.pruebasDAOService.loadUltimos10AnalisisUpgo(value.paciente.id);

    let t1 = await this.translateConfigService.getTextoNow("REPO.buscando");
    //Muestro spinner
    this.loadingSave = await this.loadingController.create({
      //'Obteniendo información...'
      message: t1
    });
    await this.loadingSave.present();

    //Este metodo no solo cuenta, sino que tambien trae la data de los ultimos 10 para poder graficar
    let cantidad1 = await this.pruebasDAOService.cantidadTestUPGOPorPaciente(this.pacienteSelect["id"]);
    if (cantidad1 > 0) {
      this.analisisUltimos10 = this.pruebasDAOService.loadUltimos10AnalisisUpgo(this.pacienteSelect["id"]);
      console.log("Obtenemos 10 ultimos para paciente [" + this.pacienteSelect["id"] + "] : " + this.analisisUltimos10.length)

      //poblamos los datos para el grafico
      for (let hist of this.analisisUltimos10) {
        console.log("tiempos upgo paciente [" + this.pacienteSelect["id"] + "]: " + hist.t)
        this.gfxDataTiempos.push(hist.t);
      }

      //Generar grafico de barra
      await this.dibujaGraficoUPGO();
      this.existeDataPrueba1 = true;
    } else {
      //No hay data para generar historico TODO: Deshabilitar opcion
      this.existeDataPrueba1 = false;
    }



    //------------------
    //Este metodo no solo cuenta, sino que tambien trae la data de los ultimos 10 para poder graficar
    let cantidad2 = await this.pruebasDAOService.cantidadTest10mtsPorPaciente(this.pacienteSelect["id"]);
    if (cantidad2 > 0) {
      this.analisisUltimos10_2 = this.pruebasDAOService.loadUltimos10Analisis10mts(this.pacienteSelect["id"]);

      //poblamos los datos para el grafico
      for (let hist of this.analisisUltimos10_2) {
        console.log("tiempos 10mts paciente [" + this.pacienteSelect["id"] + "]: " + hist.v_gait)
        this.gfxDataTiempos2.push(hist.v_gait);
      }

      //Generar grafico de barra
      await this.dibujaGrafico10mts();
      this.existeDataPrueba2 = true;
    } else {
      //No hay data para generar historico TODO: Deshabilitar opcion
      this.existeDataPrueba2 = false;
    }


    if (this.userForm.value["destinatario"] == 0) {
      console.log("completo los campos");
      this.userForm.patchValue({ "nombre": this.pacienteSelect["nombre"] });
      this.userForm.patchValue({ "email": this.pacienteSelect["correo"] });
    } else {
      console.log("el reporte es para otra persona");
    }

    this.userForm.patchValue({ "testUPGO": [] });
    this.userForm.patchValue({ "test30mts": [] });

    if (this.loadingSave) {
      this.loadingSave.dismiss();
    }

  }

  async tryEnvia(value) {
    value.language = this.translateConfigService.getCurrentLanguage();
    console.log("Envia");
    console.log(JSON.stringify(value));
    console.log("Nombre : " + value.nombre);
    console.log("email : " + value.email);
    console.log("paciente : " + this.pacienteSelect.nombre);
    console.log("paciente id : " + this.pacienteSelect.id);
    console.log("testUPGO : " + JSON.stringify(value.testUPGO));
    console.log("test30mts : " + value.test30mts);
    console.log("lenguaje : " + value.language);

    let t1 = await this.translateConfigService.getTextoNow("REPO.no_data_1");
    let t2 = await this.translateConfigService.getTextoNow("REPO.no_data_2");

    if (value.testUPGO.length + value.test30mts.length == 0) {
      //"Faltan datos", "Debe especificar que reportes desea generar"
      this.utilsService.alerta(t1, t2);
    } else {

      let repo = new ReporteDTO();
      repo.patient_id = this.pacienteSelect.id;
      repo.recipient_name = this.pacienteSelect.nombre;
      repo.recipient_email = value.email;
      repo.reports_upgo = [];
      repo.reports_10mts = [];
      repo.language = this.translateConfigService.getCurrentLanguage();

      console.log("reporte", repo);

      for (let unTest of value.testUPGO) {

        repo.reports_upgo.push(unTest);

        if (unTest == "ULT") {
          console.log("Ultima sesion del upgo");
        } else if (unTest == "HIS") {
          console.log("Historico del upgo");


          var canvas = document.getElementById('canvas') as HTMLCanvasElement;
          var dataURL = canvas.toDataURL();
          console.log(dataURL);
          repo.gfx_his_upgo = dataURL;

        }
      }
      for (let unTest of value.test30mts) {

        repo.reports_10mts.push(unTest);

        if (unTest == "ULT") {
          console.log("Ultima sesion del 30 mts");
        } else if (unTest == "HIS") {
          console.log("Historico del 30 mts");
          //Generar grafica de barra

          var canvas = document.getElementById('canvas2') as HTMLCanvasElement;
          var dataURL = canvas.toDataURL();
          console.log(dataURL);
          repo.gfx_his_10mts = dataURL;
        }
      }

      let t3 = await this.translateConfigService.getTextoNow("REPO.gen");
      this.loadingSave = await this.loadingController.create({
        //'Generando reporte...'
        message: t3
      });
      await this.loadingSave.present();

      try {
        let resp = await this.pacientesDAOService.pacienteReporta(repo);
        if (this.loadingSave) {
          this.loadingSave.dismiss();
        }
        console.log("El servidor respondio: " + resp)
        if (resp === 0) {
          let txt1 = await this.translateConfigService.getTextoNow("REPO.resultados.repo");
          let txt2 = await this.translateConfigService.getTextoNow("REPO.resultados.repo_msg");
          //"Reporte", "Reporte generado exitosamente."
          this.utilsService.alerta(txt1, txt2);
        } else if (resp === 1) {
          let txt1 = await this.translateConfigService.getTextoNow("REPO.resultados.plan");
          let txt2 = await this.translateConfigService.getTextoNow("REPO.resultados.plan_msg");
          //"Mejora tu plan", "Alcanzaste la máxima cantidad de reportes que puedes enviar con tu plan."
          this.utilsService.alerta(txt1, txt2);
        } else {
          let txt1 = await this.translateConfigService.getTextoNow("REPO.resultados.err");
          let txt2 = await this.translateConfigService.getTextoNow("REPO.resultados.err_msg");
          //"Error", "Ocurrió un error al generar el reporte, por favor intente mas tarde."
          this.utilsService.alerta(txt1, txt2);
        }
      } catch (err) {
        if (this.loadingSave) {
          this.loadingSave.dismiss();
        }
        this.utilsService.alerta("Error", "Error interno, por favor intente mas tarde.");
        console.error(JSON.stringify(err));
      }
    }
  }

  doneDibujandoUPGO() {
    console.log("Termine de dibujar upgo")
  }

  doneDibujando10mts() {
    console.log("Termine de dibujar 10 mts")
  }

  async dibujaGrafico10mts() {
    let txt_dura = await this.translateConfigService.getTextoNow("REPO.gfx_dura");

    if (this.barCanvasVelocidadMarcha) {
      let histRCChart = new Chart(this.barCanvasVelocidadMarcha.nativeElement, {
        type: "bar",
        data: {
          labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
          datasets: [
            {
              label: txt_dura, //"Duración de la prueba",
              data: this.gfxDataTiempos2, //[12, 19, 3, 5, 2, 3, 12, 19, 3, 5],
              backgroundColor: [
                "rgba(54, 162, 235, 0.2)", //"rgba(255, 99, 132, 0.2)",
                "rgba(54, 162, 235, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(255, 206, 86, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(75, 192, 192, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(153, 102, 255, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(255, 159, 64, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(255, 99, 132, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(54, 162, 235, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(255, 206, 86, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(75, 192, 192, 0.2)"
              ],
              borderColor: [
                "rgba(54, 162, 235, 1)", //"rgba(255,99,132,1)",
                "rgba(54, 162, 235, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(255, 206, 86, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(75, 192, 192, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(153, 102, 255, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(255, 159, 64, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(255,99,132,1)",
                "rgba(54, 162, 235, 1)", //"rgba(54, 162, 235, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(255, 206, 86, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(75, 192, 192, 1)"
              ],
              borderWidth: 1
            }
          ]
        },
        options: {
          animation: {
            onComplete: this.doneDibujando10mts
          },
          scales: {
            y: {
              min: 0
            }
          },
          plugins:{ 
            legend: {
              display: false
            },
            annotation: {
              annotations: {
                line1: {
                  type: 'line',
                  //mode: 'horizontal',
                  scaleID: 'y-axis-0',
                  value: 0.4,
                  borderColor: 'rgb(255, 0, 0)',
                  borderWidth: 2,
                  label: {
                    //enabled: false,
                    content: 'Leve'
                  }
                }, line2: {
                  type: 'line',
                  //mode: 'horizontal',
                  scaleID: 'y-axis-0',
                  value: 0.8,
                  borderColor: 'rgb(0, 0, 255)',
                  borderWidth: 2,
                  label: {
                    //enabled: false,
                    content: 'Alto'
                  }
                }, line3: {
                  type: 'line',
                  //mode: 'horizontal',
                  scaleID: 'y-axis-0',
                  value: 1.2,
                  borderColor: 'rgb(0, 255, 0)',
                  borderWidth: 2,
                  label: {
                    //enabled: false,
                    content: 'Alto'
                  }
                }
              },
              
            },
          }
        }
      });
    } else {
      console.error("El canvas no esta listo");
    }
  }

  async dibujaGraficoUPGO() {
    let txt_dura = await this.translateConfigService.getTextoNow("REPO.gfx_dura");

    if (this.barCanvasRiesgoCaida) {
      this.histRCChart = new Chart(this.barCanvasRiesgoCaida.nativeElement, {
        type: "bar",
        data: {
          labels: ["S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10"],
          datasets: [
            {
              label: txt_dura, //"Duración de la prueba"
              data: this.gfxDataTiempos, //[12, 19, 3, 5, 2, 3, 12, 19, 3, 5], 
              backgroundColor: [
                "rgba(54, 162, 235, 0.2)", //"rgba(255, 99, 132, 0.2)",
                "rgba(54, 162, 235, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(255, 206, 86, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(75, 192, 192, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(153, 102, 255, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(255, 159, 64, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(255, 99, 132, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(54, 162, 235, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(255, 206, 86, 0.2)",
                "rgba(54, 162, 235, 0.2)", //"rgba(75, 192, 192, 0.2)"
              ],
              borderColor: [
                "rgba(54, 162, 235, 1)", //"rgba(255,99,132,1)",
                "rgba(54, 162, 235, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(255, 206, 86, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(75, 192, 192, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(153, 102, 255, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(255, 159, 64, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(255,99,132,1)",
                "rgba(54, 162, 235, 1)", //"rgba(54, 162, 235, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(255, 206, 86, 1)",
                "rgba(54, 162, 235, 1)", //"rgba(75, 192, 192, 1)"
              ],
              borderWidth: 1
            }
          ]
        },
        options: {
          animation: {
            onComplete: this.doneDibujandoUPGO
          },
          scales: {
            y: {
              min: 0
            }
          },
          plugins:{ 
            legend: {
              display: false
            }, annotation: {
              annotations: [
              {
                type: 'line',
                //mode: 'horizontal',
                scaleID: 'y-axis-0',
                value: 10,
                borderColor: 'rgb(0, 255, 0)',
                borderWidth: 2,
                label: {
                  //enabled: false,
                  content: 'Leve'
                }
              }, {
                type: 'line',
                //mode: 'horizontal',
                scaleID: 'y-axis-0',
                value: 20,
                borderColor: 'rgb(255, 0, 0)',
                borderWidth: 2,
                label: {
                  //enabled: false,
                  content: 'Alto'
                }
              }]
            }
          }
        }
      });
    } else {
      console.error("El canvas no esta listo");
    }
  }

}
