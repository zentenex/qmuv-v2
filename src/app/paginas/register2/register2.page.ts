import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { CuentaDAOService, UsuarioCreaRequest } from 'src/app/dao/cuenta-dao.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-register2',
  templateUrl: './register2.page.html',
  styleUrls: ['./register2.page.scss'],
})
export class Register2Page implements OnInit {

  validations_form: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';
  hide1 = true;
  hide2 = true;

  validation_messages = {
    'nombre': [
      { type: 'required', message: 'El nombre es requerido.' }
    ]
  };

  constructor(
    private navCtrl: NavController,
    private cuentaDAO: CuentaDAOService,
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private utilsService: UtilsService
  ) { }

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      nombre: new FormControl('', Validators.compose([
        Validators.required
      ])),
      legal: new FormControl(false, Validators.compose([
        Validators.requiredTrue
      ])),
    });
  }

  async tryRegister(value) {

    let usr = new UsuarioCreaRequest();
    usr.name = value.nombre;
    usr.email = this.authService.logedInUserDetails().email;

    let resp = await this.cuentaDAO.registraUsuario(usr);
    if(resp.code >= 0) {
        this.errorMessage = "";
        this.successMessage = "Tu cuenta ha sido completada exitosamente";
        this.navCtrl.navigateForward('/home');
      } else {
        //alert("No se pudo completar la cuenta de usuario")
        this.utilsService.alerta("Error", "No se pudo completar la cuenta de usuario");
        console.error("Error al agregar la medicion : " + resp.message);
      }
  }

}
