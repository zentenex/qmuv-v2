import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PacientesEliminadosPage } from './pacientes-eliminados.page';

describe('PacientesEliminadosPage', () => {
  let component: PacientesEliminadosPage;
  let fixture: ComponentFixture<PacientesEliminadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacientesEliminadosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PacientesEliminadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
