import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacientesEliminadosPage } from './pacientes-eliminados.page';

const routes: Routes = [
  {
    path: '',
    component: PacientesEliminadosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PacientesEliminadosPageRoutingModule {}
