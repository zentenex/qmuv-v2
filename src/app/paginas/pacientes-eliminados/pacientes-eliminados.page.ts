import { Component, OnInit } from '@angular/core';
import { PacienteDTO, PacientesDAOService } from 'src/app/dao/pacientes-dao.service';
import { FormControl } from '@angular/forms';
import { debounceTime } from "rxjs/operators";
import { UtilsService } from 'src/app/services/utils.service';
import { TranslateConfigService } from 'src/app/services/translate-config.service';

@Component({
  selector: 'app-pacientes-eliminados',
  templateUrl: './pacientes-eliminados.page.html',
  styleUrls: ['./pacientes-eliminados.page.scss'],
})
export class PacientesEliminadosPage implements OnInit {

  public pacientes : PacienteDTO[] = [];
  //public searchTerm: string = "";
  public searchControl: FormControl;
  searching = false;

  onSearchInput(){
    this.searching = true;
}

  constructor(
    private pacientesDAO: PacientesDAOService, 
    private utilsService: UtilsService,
    private translateConfigService: TranslateConfigService) {
      this.searchControl = new FormControl();
  }

  ngOnInit() {
    this.setFilteredItems("");
    this.searchControl.valueChanges
      .pipe(debounceTime(700))
      .subscribe(search => {
        this.setFilteredItems(search);
        this.searching = false;
      });
  }

  setFilteredItems(searchTerm: string) {
    this.pacientesDAO.pacientesEliminados(searchTerm).then(pacientes => {
      this.pacientes = pacientes;
    });
  }

  async elimina(idx: number) {
    let t1 = await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.acciones.elimina_tit"); //"Elimina"
    let t2 = await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.acciones.elimina_conf"); //"¿Confirma que desea eliminar permanentemente a este paciente?"
    this.utilsService.confirma(t1, t2, (confirma: boolean) => {
      if(confirma) {
        let paciente = this.pacientes[idx];
        if(this.pacientesDAO.pacientePurga(paciente)) {
          //this.utilsService.presentaToast("Paciente eliminado");
          this.translateConfigService.getTextoNow("PACI_NEW_EDIT.acciones.eliminado").then(txt => {
            this.utilsService.presentaToast(txt);
          })
          this.pacientes.splice(idx, 1);
        } else {
          //this.utilsService.presentaToast("Paciente no eliminado");
          this.translateConfigService.getTextoNow("PACI_NEW_EDIT.acciones.eliminado_no").then(txt => {
            this.utilsService.presentaToast(txt);
          })
        }
      }
    })
  }

  restaura(idx: number) {
        let paciente = this.pacientes[idx];
        if(this.pacientesDAO.pacienteRestaura(paciente)) {
          //this.utilsService.presentaToast("Paciente restaurado");
          this.translateConfigService.getTextoNow("PACI_NEW_EDIT.acciones.restaurado").then(txt => {
            this.utilsService.presentaToast(txt);
          })
          this.pacientes.splice(idx, 1);
        } else {
          //this.utilsService.presentaToast("Paciente no restaurado");
          this.translateConfigService.getTextoNow("PACI_NEW_EDIT.acciones.restaurado_no").then(txt => {
            this.utilsService.presentaToast(txt);
          })
        }
  }

}
