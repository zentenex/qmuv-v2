import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PacientesEliminadosPageRoutingModule } from './pacientes-eliminados-routing.module';

import { PacientesEliminadosPage } from './pacientes-eliminados.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';
import { IconoBTModule } from 'src/app/componentes/icono-bt/icono-bt.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    //FormsModule,
    ReactiveFormsModule,
    IonicModule,
    PacientesEliminadosPageRoutingModule,
    MenuInferiorModule,
    IconoBTModule,
    TranslateModule.forChild()
  ],
  declarations: [PacientesEliminadosPage]
})
export class PacientesEliminadosPageModule {}
