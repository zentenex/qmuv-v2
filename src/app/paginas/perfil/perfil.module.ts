import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PerfilPageRoutingModule } from './perfil-routing.module';

import { PerfilPage } from './perfil.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';
import { IconoBTModule } from 'src/app/componentes/icono-bt/icono-bt.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PerfilPageRoutingModule,
    MenuInferiorModule,
    IconoBTModule,
    TranslateModule.forChild()
  ],
  declarations: [PerfilPage]
})
export class PerfilPageModule {}
