import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Ionic4DatepickerModalComponent } from '@logisticinfotech/ionic4-datepicker';
import { CuentaDAOService, UsuarioActualizaRequest } from 'src/app/dao/cuenta-dao.service';
import { OpenDaoService } from 'src/app/dao/open-dao.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import * as moment from 'moment';
import { TranslateConfigService } from 'src/app/services/translate-config.service';


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  selectedDate = '1985-11-05';
  datePickerObj: any = {
    mondayFirst: true, // default false
    fromDate: new Date('1900-01-02'), // default null
    /*
    inputDate: new Date('2018-08-10'), // default new Date()
    
    toDate: new Date('2018-12-28'), // default null
    showTodayButton: false, // default true
    closeOnSelect: true, // default false
    disableWeekDays: [4], // default []
    
    
    //disabledDates: disabledDates, // default []
    titleLabel: 'Select a Date', // default null
    */
    setLabel: 'Aceptar',  // default 'Set'
    todayLabel: 'Hoy', // default 'Today'
    closeLabel: 'Cerrar', // default 'Close'
    monthsList: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
    weeksList: ["do", "lu", "ma", "mi", "ju", "vi", "sa"],
    dateFormat: 'YYYY-MM-DD', // default DD MMM YYYY
    /*
    clearButton : false , // default true
    momentLocale: 'pt-BR', // Default 'en-US'
    yearInAscending: true, // Default false
    btnCloseSetInReverse: true, // Default false
    btnProperties: {
      expand: 'block', // Default 'block'
      fill: '', // Default 'solid'
      size: '', // Default 'default'
      disabled: '', // Default false
      strong: '', // Default false
      color: '' // Default ''
    },
    arrowNextPrev: {
      nextArrowSrc: 'assets/images/arrow_right.svg',
      prevArrowSrc: 'assets/images/arrow_left.svg'
    }, // This object supports only SVG files.
    highlightedDates: [
     { date: new Date('2019-09-10'), color: '#ee88bf', fontColor: '#fff' },
     { date: new Date('2019-09-12'), color: '#50f2b1', fontColor: '#fff' }
    ], // Default [],
    
    */
    isSundayHighlighted: {
      fontColor: '#ee88bf' // Default null
    } // Default {}
  };

  mostrar = false;
  email: string;
  nombre: string;
  genero: number;
  state: number;
  region: string;
  fechaNacimiento: any;
  profesion: string;
  tipoSub: string;
  zoneId: number;

  listaPaises: any;
  fono: string;
  direccion: string;

  constructor(public openControl: OpenDaoService,
    public modalCtrl: ModalController,
    private cuentaDAO: CuentaDAOService,
    private authenticationService: AuthenticationService,
    private translateConfigService: TranslateConfigService,
    private alert: AlertController) { }

  ngOnInit() {

    this.openControl.regiones().then(listaPaises => {
      if (listaPaises) {
        this.listaPaises = listaPaises;
        console.log(this.listaPaises);
      }
      this.datosUsurario();
    }).catch(_ => {
      console.log("ErrocargarRegiones")
    });



  }

  datosUsurario() {
    this.cuentaDAO.obtieneDetalleUsuario().then(u => {

      //Obtenemos el lenguaje actual
    var selectedLanguage = this.translateConfigService.getCurrentLanguage();

      if (u.code >= 0) {

        this.email = u.user.email;
        this.nombre = u.user.name;
        this.genero = u.user.gender;
        this.tipoSub = u.user.plan_name;
        this.profesion = u.user.profession;
        this.fechaNacimiento = this.functionParseaFecha(u.user.birthday);
        this.region = u.user.region;
        this.zoneId = u.user.zoneId;
        this.fono = u.user.telephone;
        this.direccion = u.user.address;

        if(u.user.plan_name == 'Trial' && selectedLanguage == 'es') {
          this.tipoSub = 'Version de prueba'
        } else if(u.user.plan_name == 'Trial' && selectedLanguage == 'en') {
          this.tipoSub = 'Trial version'
        }

        console.log("Se obtuvo usuario desde el servidor : ", u.user)
        this.mostrar = true;
      }
    }).catch(_ => {
      this.email = "usuario@local";
    });
  }

  async openDatePicker() {
    const datePickerModal = await this.modalCtrl.create({
      component: Ionic4DatepickerModalComponent,
      cssClass: 'li-ionic4-datePicker',
      componentProps: {
        'objConfig': this.datePickerObj,
        'selectedDate': this.selectedDate
      }
    });
    await datePickerModal.present();

    datePickerModal.onDidDismiss()
      .then((data) => {
        console.log(data);
        if (data.data.date != 'Invalid date') {
          this.selectedDate = data.data.date;
          this.fechaNacimiento = this.selectedDate;
        }
      });
  }

  public updateUser() {
    let uar = new UsuarioActualizaRequest();
    uar.name = this.nombre;
    uar.gender = parseInt(this.genero.toString());
    uar.birthday = this.fechaNacimiento;
    uar.profession = this.profesion;
    uar.region = this.region;
    uar.zoneId = parseInt(this.zoneId.toString());
    uar.telephone = this.fono;
    uar.address = this.direccion;
    console.log("el USUARIO Q MANDARE", uar);

    this.cuentaDAO.actualizaUsuario(uar).then(resp => {
      console.log(JSON.stringify(resp));
      this.presentAlert('Actualiza datos', "Datos actualizados.");
    }, err => {
      this.presentAlert('Actualiza datos', "Error interno.");
      console.error("Error : " + JSON.stringify(err));
    })
  }

  public resetPassword() {
    this.authenticationService.olvidePassword(this.email).then(resp => {
      if (resp == 'OK') {
        //Correo de firebase enviado
        this.presentAlert('Reestablecer contraseña', "Envio exitoso de correo electrónico.");
      } else if (resp == 'NOTFOUND') {
        //no existo en firebase, usare el recuperar clave de UMS
        this.presentAlert('Reestablecer contraseña', "No existe usuario asociado al correo electrónico suministrado.");
      } else {
        this.presentAlert('Reestablecer contraseña', resp);
      }
      //loading.dismiss();
    }, err => {
      this.presentAlert('Reestablecer contraseña', "Error interno.");
      console.error("Error : " + JSON.stringify(err));
      //loading.dismiss();
    })
  }

  async presentAlert(titulo: string, mensaje: string) {
    const alert = await this.alert.create({
      header: titulo,
      message: mensaje,
      buttons: ['Aceptar']
    });
    await alert.present();
  }


  functionParseaFecha(date) {
    let a = moment(date).add(1, 'days').calendar();
    return moment(a).format('YYYY-MM-DD');

    
  }

}
