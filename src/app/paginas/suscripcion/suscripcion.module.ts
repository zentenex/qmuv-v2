import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuscripcionPageRoutingModule } from './suscripcion-routing.module';

import { SuscripcionPage } from './suscripcion.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';
import { IconoBTModule } from 'src/app/componentes/icono-bt/icono-bt.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuscripcionPageRoutingModule,
    MenuInferiorModule,
    IconoBTModule
  ],
  declarations: [SuscripcionPage]
})
export class SuscripcionPageModule {}
