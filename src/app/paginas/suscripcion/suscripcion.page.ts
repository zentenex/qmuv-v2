import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-suscripcion',
  templateUrl: './suscripcion.page.html',
  styleUrls: ['./suscripcion.page.scss'],
})
export class SuscripcionPage implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  public poseePlan: boolean;
  public planNombre: string;

  constructor(private utilsService: UtilsService) { }

  ngOnInit() {
    let planId = this.utilsService.planDelUsuarioId;
    if(planId > 0) {
      this.poseePlan = true;
      this.planNombre = this.utilsService.planDelUsuarioNombre;
      this.utilsService.presentaToastDebug("Plan : "+ this.planNombre);
    } else {
      this.poseePlan = false;
      this.utilsService.presentaToastDebug("No tengo plan");
    }
  }

}
