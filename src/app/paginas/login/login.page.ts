import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UtilsService } from 'src/app/services/utils.service';
import { NavigationExtras } from '@angular/router';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  validations_form: FormGroup;
  hide1 = true;
  version = "0.0.0";

  constructor(
    private appVersion: AppVersion,
    private navCtrl: NavController,
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private utilsService: UtilsService
  ) { }

  ngOnInit() {

    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });

    this.appVersion.getVersionNumber().then(v => this.version = v)
  }


  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };


  loginUser(value) {
    this.authService.loginUser(value)
      .then(res => {
        console.log(res);
        //this.errorMessage = "";
        //this.navCtrl.navigateForward('/home');
      }, err => {
        //this.errorMessage = err.message;
        console.warn(JSON.stringify(err));
        if(err.code == 'auth/user-not-found') {
          this.utilsService.alerta("Error", "No existe cuenta para el correo electrónico suministrado.");
        } else if(err.code == 'auth/wrong-password') {
          this.utilsService.alerta("Error", "Contraseña incorrecta.");
        } else if(err.code == 'auth/too-many-requests') {
          this.utilsService.alerta("Error", "Demasiados intentos incorrectos de auteticarse. Por favor intente mas tarde.");
        } else {
          this.utilsService.alerta("Error", err.message);
        }
        
      })
  }

  goToOlvideClave() {
    const email = this.validations_form.get('email').value

    let navigationExtras: NavigationExtras = {
      state: {
        correo: email
      } 
    };
    this.navCtrl.navigateForward('/olvide-clave', navigationExtras);
  }

}
