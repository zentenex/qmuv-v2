import { Component, NgZone, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CuentaDAOService, FirstUserOutput, UsuarioCreaRequest } from 'src/app/dao/cuenta-dao.service';
import { NavController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import * as Chart from 'chart.js';
import { SensorInnercialService } from 'src/app/services/sensor-innercial.service';
import { Subscription } from 'rxjs';
import { ConfigDaoService } from 'src/app/dao/config-dao.service';
import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  userEmail: string;
  userNombre: string;

  testTUGO: boolean = true;
  test10MT: boolean = true;
  testSUTE: boolean = false;

  cargaBateria: number;

  private subscription1: Subscription;

  constructor(
    private navCtrl: NavController,
    private cuentaDAO: CuentaDAOService,
    private authService: AuthenticationService,
    private utilsService: UtilsService,
    private _ngZone: NgZone,
    private splashScreen: SplashScreen,
    private sensorInnercialService: SensorInnercialService,
    private configDAO: ConfigDaoService
  ) {
    //Configuracion por defecto para todos los graficos Chart.js
    //Elimina el cuadriculado de todos los graficos
    //Chart.defaults.scale.gridLines.display = false;
    Chart.defaults.scale.grid.display = false;
    //Graficos con tooltip
    //TODO buscar reemplazo
    //Chart.defaults.global.tooltips.enabled = true;
    console.log("En el constructor del home");
  }

  ngOnInit() {
    console.log("En el on-init home");

    this.splashScreen.hide();

      this.cuentaDAO.obtieneDetalleUsuario().then(u => {
        if(u.code >= 0) {
          console.log("Se obtuvo usuario desde el servidor : " + u.user.name + ", ID: " + u.user.id + ", PLAN code: " + u.user.plan_code);
          this.tenemosUsuario(u);
        } else {
          console.error("No se pudo obtener usuario desde el servidor")

          //Se debe crear la cuenta del usuario
          this.iniciaProcesoCreacionCuenta();
        }
      }, err => {
        console.error("Error al obtener info del usuario : " + JSON.stringify(err))
      });

      this.cargaBateria = this.sensorInnercialService.ultimaLecturaBateria;
      //this.cargaBateria = 50;
      this.subscription1 = this.sensorInnercialService.observadorBateriaDispositivo$().subscribe(carga => {
        this._ngZone.run(() => {
          this.cargaBateria = carga;
        });
      });
      
  }

  ngOnDestroy() {
    this.subscription1.unsubscribe();
  }

  get codigoPlan() {
    return this.utilsService.planCODE;
  }
  private tenemosUsuario(u: FirstUserOutput) {
    this.userEmail = u.user.email;
    this.userNombre = u.user.name;

    //Guardamos los datos del plan y el estado de la cuenta
    this.utilsService.planDelUsuarioId = u.user.plan_id;
    this.utilsService.planDelUsuarioNombre = u.user.plan_name;
    this.utilsService.cuentaEstado = u.user.account_status;
    
    this.utilsService.cuentaAlt = false;
    this.utilsService.planAltNombre = "";

    if(this.utilsService.cuentaEstado > 0) {
      //La cuenta esta deshabilitada. Revisamos si el usuario esta añadido en otra cuenta
      console.log("La cuenta esta desabilitada (estado>0) : " + this.utilsService.cuentaEstado)
      if(u.account_others.length > 0) {
        this.utilsService.cuentaAlt = true;
        for(let ctaAlt of u.account_others) {
          console.log("La otra cuenta : " + JSON.stringify(ctaAlt))
          this.utilsService.planAltNombre = this.utilsService.planAltNombre + "[" + ctaAlt.Plan.name + "]";
          this.utilsService.planAltId = ctaAlt.PlanID;
          this.utilsService.planCODE = ctaAlt.Plan.code;
        }
      } else {
        //alert("Esta cuenta se encuentra deshabilitada");
        this.utilsService.alerta("Atención", "Esta cuenta se encuentra deshabilitada");
        console.log("Se saca el codigo de plan [" + this.utilsService.planCODE + "] del util services.");
        this.utilsService.planCODE = "";
      }
    } else {
      this.utilsService.planCODE = u.user.plan_code;
    }

    console.log("PLAN CODE : " + this.utilsService.planCODE);
    console.log("PLAN ID : " + this.utilsService.planDelUsuarioId);
    console.log("PLAN NOMBRE : " + this.utilsService.planDelUsuarioNombre);
    console.log("CTA ESTADO : " + this.utilsService.cuentaEstado);
  }

  async iniciaProcesoCreacionCuenta() {
    //Obtenemos los datos previamente ingresados en el registro

    let elNombre = await this.configDAO.nombreUsuarioRegistrado();

    let usr = new UsuarioCreaRequest();
    usr.name = elNombre;
    usr.email = this.authService.logedInUserDetails().email;
    let resp = await this.cuentaDAO.registraUsuario(usr);
    if(resp.code >= 0) {
      let u = await this.cuentaDAO.obtieneDetalleUsuario();
      if(u.code >= 0) {
        console.log("Se obtuvo usuario desde el servidor : " + u.user.name)
        this.tenemosUsuario(u);
      } else {
        console.error("No se pudo obtener usuario desde el servidor y ya se supone que lo habia creado")
      }
    } else {
      this.utilsService.alerta("Error", "Ocurrio un problema al finalizar el proceso de creacion de cuenta");
      console.error("No se pudo registrar usuario con nombre [" + usr.name + "] y email [" + usr.email + "] : " + resp.message);
    }
/*
    if(this.authService.registroNombre != null && this.authService.registroEmail != null) {
      //Nos estamos recien registrando, por lo que ya tengo los datos faltantes para la creacion de la cta
      console.log("Nos estamos recien registrando. Tenemos nombre ["+this.authService.registroNombre+"] y email ["+this.authService.registroEmail+"]")
      let usr = new UsuarioCreaRequest();
      usr.name = this.authService.registroNombre;
      usr.email = this.authService.registroEmail;
      let resp = await this.cuentaDAO.registraUsuario(usr);
      if(resp.code >= 0) {
        let u = await this.cuentaDAO.obtieneDetalleUsuario();
        if(u.code >= 0) {
          console.log("Se obtuvo usuario desde el servidor : " + u.user.name)
          this.tenemosUsuario(u);
        } else {
          console.error("No se pudo obtener usuario desde el servidor y ya se supone que lo habia creado")
        }
      }
    } else {
      //No tengo datos en el registro, me faltan datos, los pedire
      console.log("No tengo datos en el registro, me faltan datos, los pedire")
      this.navCtrl.navigateForward('/register2');
    }
    */
  }
}
