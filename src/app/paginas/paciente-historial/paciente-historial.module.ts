import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PacienteHistorialPageRoutingModule } from './paciente-historial-routing.module';

import { PacienteHistorialPage } from './paciente-historial.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PacienteHistorialPageRoutingModule,
    MenuInferiorModule
  ],
  declarations: [PacienteHistorialPage]
})
export class PacienteHistorialPageModule {}
