import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { PacientesDAOService, PacienteDTO } from 'src/app/dao/pacientes-dao.service';
import { PruebasDAOService } from 'src/app/dao/pruebas-dao.service';
import { UtilsService } from 'src/app/services/utils.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-paciente-historial',
  templateUrl: './paciente-historial.page.html',
  styleUrls: ['./paciente-historial.page.scss'],
})
export class PacienteHistorialPage implements OnInit {
  loadingLoad: HTMLIonLoadingElement = null;

  constructor(private utilsService: UtilsService, private navCtrl: NavController, private activatedRoute: ActivatedRoute, private loadingController: LoadingController, private pacientesDAOService: PacientesDAOService, private pruebasDAOService: PruebasDAOService) { }

  paciente: PacienteDTO = null;
  cantidadUPGO = 0;
  cantidad10mts = 0;

  gotoHistorialUPGO() {
    let elPlan = environment.planes.find(x => x.CODE === this.utilsService.planCODE);
    if(elPlan != null && elPlan.HISTORIAL_DE_PACIENTE) {
      this.navCtrl.navigateForward('/upgo-historial/' + this.paciente.id);
    } else {
      this.utilsService.alerta("Mejora tu plan", "Tu plan actual no permite acceder al historial de tus pacientes.");
    }
    
  }

  gotoHistorial10MTS() {
    console.log("buscamos el plan que tenga como codigo : " + this.utilsService.planCODE);
    let elPlan = environment.planes.find(x => x.CODE === this.utilsService.planCODE);
    if(elPlan != null && elPlan.HISTORIAL_DE_PACIENTE) {
      this.navCtrl.navigateForward('/diez-mtr-historial/' + this.paciente.id);
    } else {
      this.utilsService.alerta("Mejora tu plan", "Tu plan actual no permite acceder al historial de tus pacientes.");
    }
  }

  ngOnInit() {
    let idPaciente = this.activatedRoute.snapshot.paramMap.get('id');
    this.pacientesDAOService.pacienteBusca(+idPaciente).then(async elPaciente => {
      this.paciente = elPaciente;

      //Muestro spinner
      this.loadingLoad = await this.loadingController.create({
        message: 'Cargando...'
      });
      await this.loadingLoad.present();

      this.cantidadUPGO = await this.pruebasDAOService.cantidadTestUPGOPorPaciente(+idPaciente); //.then(cant => {
      //  this.cantidadUPGO = cant;
      //})
      this.cantidad10mts = await this.pruebasDAOService.cantidadTest10mtsPorPaciente(+idPaciente); //.then(cant => {
        //this.cantidad10mts = cant;
      //})
      if(this.loadingLoad) {
        this.loadingLoad.dismiss();
      }
    })
  }

}
