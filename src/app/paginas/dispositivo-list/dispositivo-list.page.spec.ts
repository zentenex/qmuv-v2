import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DispositivoListPage } from './dispositivo-list.page';

describe('DispositivoListPage', () => {
  let component: DispositivoListPage;
  let fixture: ComponentFixture<DispositivoListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispositivoListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DispositivoListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
