import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DispositivoListPage } from './dispositivo-list.page';

const routes: Routes = [
  {
    path: '',
    component: DispositivoListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DispositivoListPageRoutingModule {}
