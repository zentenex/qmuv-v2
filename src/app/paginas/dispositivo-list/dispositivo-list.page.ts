import { Component, OnInit, NgZone } from '@angular/core';
import { DispositivosDAOService, DispositivoDTO } from 'src/app/dao/dispositivos-dao.service';
import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { SensorInnercialService } from 'src/app/services/sensor-innercial.service';
import { UtilsService } from 'src/app/services/utils.service';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TranslateConfigService } from 'src/app/services/translate-config.service';

@Component({
  selector: 'app-dispositivo-list',
  templateUrl: './dispositivo-list.page.html',
  styleUrls: ['./dispositivo-list.page.scss'],
})
export class DispositivoListPage implements OnInit {

  subscription1: Subscription;
  subscription2: Subscription;
  loadingScan: HTMLIonLoadingElement = null;

  pidUpdateRSSI: any;

  public dispositivosDescubiertos: DispositivoDTO[] = [];
  public dispositivoElegido: DispositivoDTO;
  public escaneando = false;

  //statusMessage: string;
  conectado = false;
  visible = false;
  debug = false;

  borraTextoLog() {
    this.sensorInnercialService.textoLog = [];
  }

  constructor(
    private alertController: AlertController,
    private dispositivosDAOService: DispositivosDAOService,
    public sensorInnercialService: SensorInnercialService,
    private navCtrl: NavController,
    private utilsService: UtilsService,
    private translateConfigService: TranslateConfigService,
    private _ngZone: NgZone) { }

  ngOnInit() {
    this.dispositivosDAOService.getCurrentDevice().then(disp => {
      if(disp) {
        console.log("Existe dispositivo por defecto");

        //veamos si esta conectado
        this.sensorInnercialService.estoyConectado(disp.id).then(_ => {
          console.log("estoy conectado al dispositivo");
          this.conectado = true;
        }). catch(_ => {
          console.log("No estoy conectado al dispositivo");
          this.conectado = false;
        })
        

        this.dispositivoElegido = disp;
        //this.conectado = this.sensorInnercialService.conectado;
        //this.visible = this.sensorInnercialService.visible;
      } else {
        //alert("No existe dispositivo por defecto");
        console.log("No existe dispositivo por defecto");
      }
    });

    this.subscription1 = this.sensorInnercialService.observadorDispositivo$().subscribe(disp => {
      console.log("MEN INF Cambio en el dispositivo " + disp.nombre);
      this._ngZone.run(() => {
        this.dispositivoElegido = disp;

        //veamos si seguimos conectados
        this.sensorInnercialService.estoyConectado(disp.id).then(_ => {
          console.log("estoy conectado al dispositivo");
          this.conectado = true;
          this.corrigoListado();
        }).catch(_ => {
          console.log("No estoy conectado al dispositivo");
          this.conectado = false;
          this.corrigoListado();

          //Si no estoy conectado al disp elegido y no le puedo leer su rssi
          //entonces no lo muestro, no esta visible
          if(disp.rssi > -9999) {
            this.visible = true;
          } else {
            this.visible = false;
          }
        });

        //this.conectado = this.sensorInnercialService.conectado;
        //this.visible = this.sensorInnercialService.visible;
      });
    });

    //Nos suscribimos cada vez que se descubre un dispositivo
    this.subscription2 = this.sensorInnercialService.observadorNuevosDispositivos$().subscribe(disp => {
      if(disp) {
        console.log("Descubierto " + disp.nombre);
        if(this.dispositivoElegido && disp.id === this.dispositivoElegido.id) {
          console.log("no lo agrego al listado, es el mismo elegido");

          //QUE HAGO: Actualizo el current device
          //PARA: Para que tenga el atributo 'visto' actualizado y asi pueda saber cuando lo vi por ultima vez
          this.dispositivosDAOService.setCurrentDevice(disp);

          this._ngZone.run(() => {
            this.dispositivoElegido = disp; //para reflejar el nuevo RSSI
            this.visible = true;
          });
        } else {
          //reviso primero si ya lo tengo
          let teTengo = false;
          for (var unD of this.dispositivosDescubiertos) {
            if(unD.id === disp.id) {
              console.log("a ti ya te conocia, pero tomare tu RSSI")
              this._ngZone.run(() => {
                unD.rssi = disp.rssi;
              });
              teTengo = true;
              break;
            }
          }
          if(!teTengo) {
            this._ngZone.run(() => {
              this.dispositivosDescubiertos.push(disp);
              this.dispositivosDescubiertos.sort(this.compara);
            });
          }
          
        }
      } else {
        this._ngZone.run(() => {
          //this.utilsService.alerta("Error", "No se pudieron escanear los dispositivos");
          this.translateConfigService.getTextoNow("SCAN.no_se_pudo").then(txt => {
            this.utilsService.alerta("Error", txt);
          })
          
          if(this.loadingScan) {
            this.loadingScan.dismiss();
          }
        });
      }
    });
  }

  /**
   * Reviso si tengo duplicado un dispositivo arriba y en el listado. De ser asi, saco el del listado
   */
  private corrigoListado() {
    
    if(this.dispositivoElegido && (this.visible || this.conectado)) {
      console.log("Me muestro arriba, por lo tanto me saco del listado si existo");
      let i = 0;
      let index = -1;
      for(let unDisp of this.dispositivosDescubiertos) {
        
        if(this.dispositivoElegido.id === unDisp.id) {
          //el dispositivo esta en arriba y en el listado, lo saco de la lista (esto pasa cuando selecciono un elemento para conectarme)
          index = i;
          console.log("encontrado en la posicion [" + index + "]");
          break;
        }
        i++;
      }
      if(index>=0) {
        this.dispositivosDescubiertos.splice(index, 1);
      }
    } else {
      console.log("No me muestro arriba, no saco a nadie del listado");
    }
    
  }

  ngOnDestroy() {
    //Me desuscribo de todo al destruir la instancia
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
  }

  ionViewDidEnter() {
    this.debug = environment.debug;
    //Comienzo a escanear dispositivos al entrar en la pantalla
    console.log("Comienzo a escanear dispositivos al entrar en la pantalla")
    this.scan();

    console.log("Inicio proceso que se ejecuta cada 5 segundos")
    this.pidUpdateRSSI = setInterval(() => { 
      //Actualizo RSSI del dispositivo conectado
      console.log("Actualizo RSSI del dispositivo conectado")
      this.sensorInnercialService.actualizoRSSI();

      let dispositivosYaNoDescubiertos: DispositivoDTO[] = []

      //marco el RSSI en -9999 a los que ya no he visto ultimamente (hace 15 segundos o mas)
      console.log("marco el RSSI en -9999 a los que ya no he visto ultimamente (hace 20 segundos o mas)")

      for(var unDis of this.dispositivosDescubiertos) {
        console.log("Para ["+unDis.nombre+"] pregunto si ["+Date.now()+"] - ["+unDis.visto+"] es mayor que ["+(1000 * environment.btScanSeg)+"]. Si es asi, entonces hace rato no te veo")
        if(Date.now() - unDis.visto > (1000 * environment.btScanSeg)) {
          //Como no veo al dispositivo hace rato, lo saco del listado de descubiertos
          unDis.rssi = -9999
          dispositivosYaNoDescubiertos.push(unDis);
        }
      }

      for(var unDispAElim of dispositivosYaNoDescubiertos) {
        let index = -1;
        let i = 0;
        for(let unDisp of this.dispositivosDescubiertos) {
          if(unDispAElim.id === unDisp.id) {
            index = i;
            console.log("encontrado en la posicion [" + index + "]");
            break;
          }
          i++;
        }
        if(index>=0) {
          this.dispositivosDescubiertos.splice(index, 1);
        }
      }

      if(!this.debug) {
        //limpio el texto log para no usar memoria
        this.borraTextoLog();
      }

    }, 5000);
  }

  ionViewDidLeave() {
    //Dejo de escanear dispositivos al salir de la pantalla (para ahorrar bateria en el cel)
    this.scanEnds();  

    //Dejo de actualizar el RSSI
    clearInterval(this.pidUpdateRSSI);
  }

  async scan() {
    this.dispositivosDescubiertos = [];  // clear list
    this.visible = false;
    //const segundos = environment.btScanSeg;
    //const milisegundos = segundos * 1000;

    //Muestro spinner
    /*
    this.loadingScan = await this.loadingController.create({
      message: 'Escaneando...',
      duration: milisegundos
    });
    await this.loadingScan.present();
    */

    

    //this.setStatus('Buscando dispositivos QMUV vía Bluetooth');
    this.sensorInnercialService.escaneaStart();

    //setTimeout(() => {this.scanEnds()}, milisegundos);
  }

  private scanEnds() {
    this.sensorInnercialService.escaneaStop();
    //this.setStatus('Escaneo completo');
  }

  shiftConexion(dev: DispositivoDTO) {
    if(dev) {
      //if(this.sensorInnercialService.conectado) {
        if(this.conectado) {
        this.desconectaConfirma(dev);
      } else {
        this.conectaConfirma(dev);
      }
    }
  }

  async desconectaConfirma(disp: DispositivoDTO) {
    let t1 = await this.translateConfigService.getTextoNow("SCAN.desconexion");
    let t2 = await this.translateConfigService.getTextoNow("SCAN.desconexion_glosa");
    let t_acept = await this.translateConfigService.getTextoNow("COMUN.aceptar");
    let t_cancel = await this.translateConfigService.getTextoNow("COMUN.cancelar");

    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: t1,
      message: '<strong>' + t2 + ' ' + disp.nombre + '?</strong>',
      buttons: [
        {
          text: t_cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: t_acept,
          handler: () => {
            console.log('Confirm Okay');
            //this.dispositivoElegido = null;
            this.sensorInnercialService.desconecta();
          }
        }
      ]
    });
    await alert.present();
  }

  async conectaConfirma(disp: DispositivoDTO) {

    let t1 = await this.translateConfigService.getTextoNow("SCAN.conexion");
    let t2 = await this.translateConfigService.getTextoNow("SCAN.conexion_glosa");
    let t_acept = await this.translateConfigService.getTextoNow("COMUN.aceptar");
    let t_cancel = await this.translateConfigService.getTextoNow("COMUN.cancelar");

    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: t1,
      message: '<strong>' + t2 + ' ' + disp.nombre + '?</strong>',
      buttons: [
        {
          text: t_cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: t_acept,
          handler: () => {
            console.log('Confirm Okay');
            this.dispositivoElegido = disp;
            //this.conectado = this.sensorInnercialService.conectado;
            this.sensorInnercialService.conectaNuevo(disp);
          }
        }
      ]
    });
    await alert.present();
  }
/*
  setStatus(message) {
    console.log(message);
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }
  */

  private compara(a: DispositivoDTO, b: DispositivoDTO) {
    // Use toUpperCase() to ignore character casing
    const bandA = a.rssi;
    const bandB = b.rssi;
  
    let comparison = 0;
    if (bandA > bandB) {
      comparison = -1;
    } else if (bandA < bandB) {
      comparison = 1;
    }
    return comparison;
  }

  //DEBUG
  deviceDetalle(device) {
    console.log(JSON.stringify(device) + ' seleccionado');
    let navigationExtras: NavigationExtras = {
      state: {
        device: device
      } 
    };
    this.navCtrl.navigateForward('/dispositivo-detalle', navigationExtras);
  }

}
