import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DispositivoListPageRoutingModule } from './dispositivo-list-routing.module';

import { DispositivoListPage } from './dispositivo-list.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';
import { IconoBTModule } from 'src/app/componentes/icono-bt/icono-bt.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DispositivoListPageRoutingModule,
    MenuInferiorModule,
    IconoBTModule,
    TranslateModule.forChild()
  ],
  declarations: [DispositivoListPage]
})
export class DispositivoListPageModule {}
