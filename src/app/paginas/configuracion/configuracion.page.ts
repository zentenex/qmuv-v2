import { Component, OnInit } from '@angular/core';
import { SensorInnercialService } from 'src/app/services/sensor-innercial.service';
import { environment } from 'src/environments/environment';
import { UtilsService } from 'src/app/services/utils.service';
import { ConfigDaoService } from 'src/app/dao/config-dao.service';
import { TranslateConfigService } from 'src/app/services/translate-config.service';
import { CuentaDAOService } from 'src/app/dao/cuenta-dao.service';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.page.html',
  styleUrls: ['./configuracion.page.scss'],
})
export class ConfiguracionPage implements OnInit {

  //email: string;
  virtualDevice: string;
  debugApp: string;
  servidor: string;
  scanFull: string;
  upgoAnalisis: string;
  diezAnalisis: string;
  upgoMediciones: string;
  diezMediciones: string;
  firmware: string;

  public countdev=0;

  selectedLanguage: string;

  constructor(
    private translateConfigService: TranslateConfigService, 
    private sensorInnercialService: SensorInnercialService, 
    private configDAO: ConfigDaoService, 
    private utilsService: UtilsService
    //private cuentaDAO: CuentaDAOService
    ) {
    //this.selectedLanguage = this.translateConfigService.getDefaultLanguage();
  }

  languageChanged(){
    this.translateConfigService.setLanguage(this.selectedLanguage);
    this.configDAO.seteaIdioma(this.selectedLanguage);
  }

  clickTitulo(){
    this.countdev = this.countdev + 1;
  }

  public status = "desconocido"

  checkConnection() {
    this.sensorInnercialService.estoyConectadoUltimo().then(resp => {
      if(resp) {
        this.status = "conectado"
      } else {
        this.status = "desconectado"
      }
    }).catch(() => {
      this.status = "indefinido"
    })
  }

  ngOnInit() {

    setInterval(() => this.checkConnection(), 1000);

    this.sensorInnercialService.elUltimoDispositivo().then(disp => {
      if(disp) {
        this.sensorInnercialService.leeBateria(disp);
      } else {
        this.utilsService.presentaToastDebug("No hay dispositivo conocido");
      }
    })

    //Obtenemos el lenguaje actual
    this.selectedLanguage = this.translateConfigService.getCurrentLanguage();

    if(this.sensorInnercialService.virtual) {
      this.virtualDevice = "SI";
    } else {
      this.virtualDevice = "NO";
    }

    if(environment.debug) {
      this.debugApp = "SI";
    } else {
      this.debugApp = "NO";
    }

    if(environment.server) {
      this.servidor = "SI";
    } else {
      this.servidor = "NO";
    }

    this.configDAO.scanAllBT().then(resp => {
      if(resp) {
        console.log("escaneo todo");
        this.scanFull = "SI";
      } else {
        console.log("escaneo solo qmuv");
        this.scanFull = "NO";
      }
    });

    this.configDAO.cantidadMedicionesPorSesion(environment.PRUEBA_UPANDGO_CODE).then(cant => {
      this.upgoMediciones = cant + "";
    });

    this.configDAO.cantidadMedicionesPorSesion(environment.PRUEBA_DIEZMTS_CODE).then(cant => {
      this.diezMediciones = cant + "";
    });

    this.configDAO.modoDeAnalisis(environment.PRUEBA_UPANDGO_CODE).then(modo => {
      this.upgoAnalisis = modo + "";
    });

    this.configDAO.modoDeAnalisis(environment.PRUEBA_DIEZMTS_CODE).then(modo => {
      this.diezAnalisis = modo + "";
    });

    this.configDAO.versionFirmware().then(v => {
      this.firmware = v;
    });
/*
    this.cuentaDAO.obtieneDetalleUsuario().then(u => {
      if(u.code >= 0) {
        console.log("Se obtuvo usuario desde el servidor : " + u.user.name)
        this.email = u.user.email;
      }
    }).catch(_ => {
      this.email = "usuario@local";
    });
    */
  }

  resetOnboarding() {
    this.configDAO.reestableceOnboarding();
    this.utilsService.presentaToast("Onboarding restaurados")
  }

  upgoMedChanged(e) {
    //alert("cambia mediciones 0 : " + this.upgoMediciones);
    this.configDAO.seteaMedicionesPorSesion(environment.PRUEBA_UPANDGO_CODE, +this.upgoMediciones);
  }

  diezMedChanged(e) {
    //alert("cambia mediciones 1 : " + this.diezMediciones);
    this.configDAO.seteaMedicionesPorSesion(environment.PRUEBA_DIEZMTS_CODE, +this.diezMediciones);
  }

  upgoAnaChanged(e) {
    this.configDAO.seteaModoAnalisis(environment.PRUEBA_UPANDGO_CODE, +this.upgoAnalisis);
  }

  diezAnaChanged(e) {
    this.configDAO.seteaModoAnalisis(environment.PRUEBA_DIEZMTS_CODE, +this.diezAnalisis);
  }

  virtualChanged(e) {
    if(this.virtualDevice === "SI") {
      this.sensorInnercialService.virtual = true;
    } else {
      this.sensorInnercialService.virtual = false;
    }
  }

  debugChanged(e) {
    if(this.debugApp === "SI") {
      environment.debug = true;
    } else {
      environment.debug = false;
    }
  }

  firmwareChanged(e) {
    this.configDAO.seteaVersionFirmware(this.firmware);
  }

  scanChanged(e) {
    if(this.scanFull === "SI") {
      this.configDAO.seteaScanAllBT(true);
    } else {
      this.configDAO.seteaScanAllBT(false);
    }
  }

  serverChanged(e) {
    if(this.servidor === "SI") {
      environment.server = true;
    } else {
      environment.server = false;
    }
  }

}
