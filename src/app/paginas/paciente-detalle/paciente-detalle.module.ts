import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PacienteDetallePageRoutingModule } from './paciente-detalle-routing.module';

import { PacienteDetallePage } from './paciente-detalle.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    PacienteDetallePageRoutingModule,
    MenuInferiorModule
  ],
  declarations: [PacienteDetallePage]
})
export class PacienteDetallePageModule {}
