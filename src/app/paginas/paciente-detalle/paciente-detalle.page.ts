import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PacientesDAOService, PacienteDTO } from 'src/app/dao/pacientes-dao.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UtilsService } from 'src/app/services/utils.service';
import { ModalController } from '@ionic/angular';
import { Ionic4DatepickerModalComponent } from '@logisticinfotech/ionic4-datepicker';
import { RegionDTO } from 'src/app/dao/open-dao.service';
import { TranslateConfigService } from 'src/app/services/translate-config.service';

@Component({
  selector: 'app-paciente-detalle',
  templateUrl: './paciente-detalle.page.html',
  styleUrls: ['./paciente-detalle.page.scss'],
})
export class PacienteDetallePage implements OnInit {

  max_obs = 100;
  actual = 0;

  public paciente: PacienteDTO = null;
  userForm: FormGroup;
  public regiones: RegionDTO[] = [];

  selectedDate = '1985-11-05';
  datePickerObj: any = {
    mondayFirst: true, // default false
    fromDate: new Date('1900-01-02'), // default null
    /*
    inputDate: new Date('2018-08-10'), // default new Date()
    
    toDate: new Date('2018-12-28'), // default null
    showTodayButton: false, // default true
    closeOnSelect: true, // default false
    disableWeekDays: [4], // default []
    
    
    //disabledDates: disabledDates, // default []
    titleLabel: 'Select a Date', // default null
    */
    setLabel: 'Aceptar',  // default 'Set'

    todayLabel: 'Hoy', // default 'Today'
    closeLabel: 'Cerrar', // default 'Close'
    monthsList: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
    weeksList: ["do", "lu", "ma", "mi", "ju", "vi", "sa"],
    dateFormat: 'YYYY-MM-DD', // default DD MMM YYYY
    /*
    clearButton : false , // default true
    momentLocale: 'pt-BR', // Default 'en-US'
    yearInAscending: true, // Default false
    btnCloseSetInReverse: true, // Default false
    btnProperties: {
      expand: 'block', // Default 'block'
      fill: '', // Default 'solid'
      size: '', // Default 'default'
      disabled: '', // Default false
      strong: '', // Default false
      color: '' // Default ''
    },
    arrowNextPrev: {
      nextArrowSrc: 'assets/images/arrow_right.svg',
      prevArrowSrc: 'assets/images/arrow_left.svg'
    }, // This object supports only SVG files.
    highlightedDates: [
     { date: new Date('2019-09-10'), color: '#ee88bf', fontColor: '#fff' },
     { date: new Date('2019-09-12'), color: '#50f2b1', fontColor: '#fff' }
    ], // Default [],
    
    */
    isSundayHighlighted: {
      fontColor: '#ee88bf' // Default null
    } // Default {}
  };


  validation_messages = {
    'nombre': [],
    'email': [],
    'fechaNacimiento': [],
    'genero': [],
    'altura': [],
    'peso': [],
    'direccion': [],
    'pais': [],
    'region': [],
    'telefono': [],
    'obs': []
  };

  cambioObs() {
    //Actualizamos la cuenta de caracteres
    this.actual = this.userForm.get('obs').value.length
  }

  private async setMensajes() {
    this.validation_messages.nombre.push({ type: 'required', message: await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.validaciones.nombre_req") });
    this.validation_messages.email.push({ type: 'required', message: await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.validaciones.email_req") });
    this.validation_messages.email.push({ type: 'pattern', message: await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.validaciones.email_pat") });
    this.validation_messages.fechaNacimiento.push({ type: 'required', message: await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.validaciones.fechaNacimiento_req") });
    this.validation_messages.genero.push({ type: 'required', message: await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.validaciones.genero_req") });
    this.validation_messages.altura.push({ type: 'required', message: await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.validaciones.altura_req") });
    this.validation_messages.peso.push({ type: 'required', message: await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.validaciones.peso_req") });
    this.validation_messages.direccion.push({ type: 'required', message: await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.validaciones.direccion_req") });
    this.validation_messages.pais.push({ type: 'required', message: await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.validaciones.pais_req") });
    this.validation_messages.region.push({ type: 'required', message: await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.validaciones.region_req") });
    this.validation_messages.telefono.push({ type: 'required', message: await this.translateConfigService.getTextoNow("PACI_NEW_EDIT.validaciones.telefono_req") });
  }

  private async setCalendario() {
    this.datePickerObj = {
      fromDate: new Date('1900-01-02'),
      dateFormat: 'YYYY-MM-DD',
      isSundayHighlighted: {
        fontColor: '#ee88bf' // Default null
      },
      mondayFirst: await this.translateConfigService.getTextoNow("COMUN.mondayFirst"),
      setLabel: await this.translateConfigService.getTextoNow("COMUN.aceptar"),
      todayLabel: await this.translateConfigService.getTextoNow("COMUN.hoy"),
      closeLabel: await this.translateConfigService.getTextoNow("COMUN.cerrar"),
      monthsList: await this.translateConfigService.getTextoNow("COMUN.meses"),
      weeksList: await this.translateConfigService.getTextoNow("COMUN.dias"),
    }
  }

  constructor(public modalCtrl: ModalController,
    private activatedRoute: ActivatedRoute,
    private pacientesDAO: PacientesDAOService,
    private formBuilder: FormBuilder,
    private utilsService: UtilsService,
    private translateConfigService: TranslateConfigService) { }

  ngOnInit() {
    this.setMensajes();
    this.setCalendario();
    let myId = this.activatedRoute.snapshot.paramMap.get('id');
    this.regiones = this.utilsService.regiones;

    this.userForm = this.formBuilder.group({
      nombre: new FormControl('', Validators.compose([
        Validators.required
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      fechaNacimiento: new FormControl('', Validators.compose([
        Validators.required
      ])),
      region: new FormControl('', Validators.compose([
        Validators.required
      ])),
      pais: new FormControl('', Validators.compose([
        Validators.required
      ])),
      direccion: new FormControl('', Validators.compose([
        Validators.required
      ])),
      telefono: new FormControl('', Validators.compose([
        Validators.required
      ])),
      genero: new FormControl('', Validators.compose([
        Validators.required
      ])),
      altura: new FormControl('', Validators.compose([
        Validators.required
      ])),
      peso: new FormControl('', Validators.compose([
        Validators.required
      ])),
      obs: new FormControl('')
    });

    this.pacientesDAO.pacienteBusca(+myId).then(elP => {
      console.log("EL PACIENTE ", elP)
      this.userForm.setValue({
        'nombre': elP.nombre,
        'email': elP.correo,
        'fechaNacimiento': elP.fechaNacimiento,
        'pais': elP.zoneId,
        'region': elP.region,
        'direccion': elP.direccion,
        'telefono': elP.telefono,
        'genero': elP.genero + "",
        'altura': elP.altura,
        'peso': elP.peso,
        'obs': elP.obs
      })
      this.selectedDate = elP.fechaNacimiento;
      this.paciente = elP;
    });
  }

  async openDatePicker() {
    const datePickerModal = await this.modalCtrl.create({
      component: Ionic4DatepickerModalComponent,
      cssClass: 'li-ionic4-datePicker',
      componentProps: {
        'objConfig': this.datePickerObj,
        'selectedDate': this.selectedDate
      }
    });
    await datePickerModal.present();

    datePickerModal.onDidDismiss()
      .then((data) => {
        console.log(data);
        if (data.data.date != 'Invalid date') {
          this.selectedDate = data.data.date;
          this.userForm.patchValue({
            'fechaNacimiento': this.selectedDate
          })
        }

      });
  }

  tryActualiza(value) {
    let pac = new PacienteDTO();
    pac.id = this.paciente.id;
    pac.nombre = value.nombre;
    pac.zoneId = +value.pais;
    pac.region = value.region;
    pac.telefono = value.telefono;
    pac.correo = value.email;
    pac.direccion = value.direccion;
    pac.fechaNacimiento = value.fechaNacimiento;
    pac.altura = +value.altura;
    pac.genero = +value.genero;
    pac.peso = +value.peso;
    pac.obs = value.obs;
    pac.eliminado = false;

    this.pacientesDAO.pacienteActualiza(pac).then(exito => {
      if (exito) {
        this.translateConfigService.getTextoNow("PACI_NEW_EDIT.acciones.actualizado").then(txt => {
          this.utilsService.presentaToast(txt);
        })
      } else {
        this.translateConfigService.getTextoNow("PACI_NEW_EDIT.acciones.actualizado_no").then(txt => {
          this.utilsService.presentaToast(txt);
        });
      }
    }, err => {
      this.utilsService.presentaToast("Error interno");
      console.error("Error : " + JSON.stringify(err));
    })
  }

}
