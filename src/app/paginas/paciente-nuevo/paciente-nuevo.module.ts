import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PacienteNuevoPageRoutingModule } from './paciente-nuevo-routing.module';

import { PacienteNuevoPage } from './paciente-nuevo.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    PacienteNuevoPageRoutingModule,
    MenuInferiorModule
  ],
  declarations: [PacienteNuevoPage]
})
export class PacienteNuevoPageModule {}
