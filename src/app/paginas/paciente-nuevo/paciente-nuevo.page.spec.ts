import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PacienteNuevoPage } from './paciente-nuevo.page';

describe('PacienteNuevoPage', () => {
  let component: PacienteNuevoPage;
  let fixture: ComponentFixture<PacienteNuevoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacienteNuevoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PacienteNuevoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
