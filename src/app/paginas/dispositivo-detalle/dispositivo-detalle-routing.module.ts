import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DispositivoDetallePage } from './dispositivo-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: DispositivoDetallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DispositivoDetallePageRoutingModule {}
