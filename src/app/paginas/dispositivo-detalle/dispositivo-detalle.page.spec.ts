import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DispositivoDetallePage } from './dispositivo-detalle.page';

describe('DispositivoDetallePage', () => {
  let component: DispositivoDetallePage;
  let fixture: ComponentFixture<DispositivoDetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispositivoDetallePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DispositivoDetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
