import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { BLE } from '@awesome-cordova-plugins/ble/ngx';
import { DispositivoDTO } from 'src/app/dao/dispositivos-dao.service';

@Component({
  selector: 'app-dispositivo-detalle',
  templateUrl: './dispositivo-detalle.page.html',
  styleUrls: ['./dispositivo-detalle.page.scss'],
})
export class DispositivoDetallePage implements OnInit {

  statusMessage: string;
  periferico: any;
  raw: string;

  constructor(
    private router: Router, 
    private ngZone: NgZone,
    private ble: BLE
  ) {
    let device: DispositivoDTO = this.router.getCurrentNavigation().extras.state.device;
    this.setStatus("Conectando a " + device.nombre || device.id);
    this.ble.connect(device.id).subscribe(
      periferico => this.onConnected(periferico),
      periferico => this.onDeviceDisconnected(periferico)
    );
  }

  onConnected(periferico) {
    this.ngZone.run(() => {
      this.setStatus('Conectado');
      this.periferico = periferico;
      this.raw = JSON.stringify(this.periferico);
      //alert("conectado");
      console.log('PERIFERICO : ' + this.raw);
    })
  }

  onDeviceDisconnected(periferico) {
    this.ngZone.run(() => {
      this.setStatus('Desconectado');
      this.periferico = periferico;
      //this.raw = JSON.stringify(this.periferico);
      //alert("desconectado");
      console.log('Desconectado del periferico');
    })
  }

  ngOnInit() {
  }

  setStatus(message) {
    console.log(message);
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }

}
