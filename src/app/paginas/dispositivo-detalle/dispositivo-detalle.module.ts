import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DispositivoDetallePageRoutingModule } from './dispositivo-detalle-routing.module';

import { DispositivoDetallePage } from './dispositivo-detalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DispositivoDetallePageRoutingModule
  ],
  declarations: [DispositivoDetallePage]
})
export class DispositivoDetallePageModule {}
