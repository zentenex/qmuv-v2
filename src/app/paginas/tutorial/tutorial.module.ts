import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TutorialPageRoutingModule } from './tutorial-routing.module';

import { TutorialPage } from './tutorial.page';
import { MenuInferiorModule } from 'src/app/componentes/menu-inferior/menu-inferior.module';
import { IconoBTModule } from 'src/app/componentes/icono-bt/icono-bt.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TutorialPageRoutingModule,
    MenuInferiorModule,
    IconoBTModule
  ],
  declarations: [TutorialPage]
})
export class TutorialPageModule {}
