import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage implements OnInit {

  public videoUrl: SafeResourceUrl;
  public videoUrl2: SafeResourceUrl;

  constructor(private domSan: DomSanitizer) { }

  ngOnInit() {
    var url = "https://youtube.com/embed/ROBfQWSLhWU";
    this.videoUrl = this.domSan.bypassSecurityTrustResourceUrl(url);

    var url2 = "https://youtube.com/embed/O78RReuTXf4";
    this.videoUrl2 = this.domSan.bypassSecurityTrustResourceUrl(url2);
  }

}
