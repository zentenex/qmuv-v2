// JWT interceptor
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { from } from 'rxjs'
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return from(this.handle(request, next));
    }

    async handle(request: HttpRequest<any>, next: HttpHandler) {
        // if your getAuthToken() function declared as "async getAuthToken() {}"
        let token = await this.authService.getAccessToken();

        if (token) {
            console.log("Agrego el token de firebase : " + token);
            const headers = {
                'Authorization': 'Bearer ' + token,
            };
            if (request.responseType === 'json') {
                headers['Content-Type'] = 'application/json';
            }
            request = request.clone({
                setHeaders: headers
            });
        } else {
            console.log("No hay access token de firebase disponible para poner en el Header");
        }

        // Important: Note the .toPromise()
        return next.handle(request).toPromise()
      }
}
