import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IconoBTComponent } from './icono-bt.component';

describe('IconoBTComponent', () => {
  let component: IconoBTComponent;
  let fixture: ComponentFixture<IconoBTComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconoBTComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IconoBTComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
