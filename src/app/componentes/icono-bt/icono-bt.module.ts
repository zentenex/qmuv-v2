import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { IconoBTComponent } from './icono-bt.component';

@NgModule({
    declarations: [
        IconoBTComponent
    ],
   imports: [RouterModule,
    CommonModule,
    IonicModule,
    TranslateModule.forChild()],
   exports: [
    IconoBTComponent
   ]
})
   export class IconoBTModule {}