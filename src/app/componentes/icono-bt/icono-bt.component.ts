import { Component, NgZone, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { SensorInnercialService } from 'src/app/services/sensor-innercial.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-icono-bt',
  templateUrl: './icono-bt.component.html',
  styleUrls: ['./icono-bt.component.scss'],
})
export class IconoBTComponent implements OnInit {

  public bluetoothActivo = false;
  public imgActive = 'assets/icon/bluetooth-active.svg';
  public imgInactive = 'assets/icon/bluetooth-inactive.svg';

  private subscription1: Subscription;

  constructor(private utilsService: UtilsService, private sensorInnercialService: SensorInnercialService, private _ngZone: NgZone, private platform: Platform) { }

  //este metodo siempre se llama al ser un componente incrustado dentro de otra pagina
  ngOnInit() {
    console.log("En el on-init icono-bt");
    this.subscription1 = this.sensorInnercialService.observadorDispositivo$().subscribe(disp => {
      console.log("ICN Cambio en el dispositivo " + disp.nombre);
      this._ngZone.run(() => {
        //this.bluetoothActivo = this.sensorInnercialService.conectado;

        //veamos si seguimos conectados
        this.sensorInnercialService.estoyConectado(disp.id).then(_ => {
          console.log("estoy conectado al dispositivo");
          this.bluetoothActivo = true;
        }). catch(_ => {
          console.log("No estoy conectado al dispositivo");
          this.bluetoothActivo = false;
        })

      });
    });

    this.revisaConexion();
  }

  revisaConexion() {
    console.log("reviso cone boton-bt");
    //this.bluetoothActivo = this.sensorInnercialService.conectado;



    this.sensorInnercialService.elUltimoDispositivo().then(disp => {

      //console.log("reviso el ult disp");
      this._ngZone.run(() => {
        if(disp) {
          //console.log("rhay ultimo disp");

          //veamos si seguimos conectados
          this.sensorInnercialService.estoyConectado(disp.id).then(_ => {
            //console.log("si conect");
            this.bluetoothActivo = true;
          }). catch(_ => {
            //console.log("no conect");
            this.bluetoothActivo = false;
          })

          /*
          if(disp.conectado) {
            console.log("si conect");
            this.bluetoothActivo = true;
          } else {
            console.log("no conect");
          }
          */
        }
      });
    });
    
  }

  //este metodo siempre se llama al ser un componente incrustado dentro de otra pagina
  ngOnDestroy() {
    console.log("En el on-destroy boton-bt");
    this.subscription1.unsubscribe();
  }

  //este metodo nunca se llama al ser un componente incrustado dentro de otra pagina
  ionViewWillEnter() {
    console.log("En el view will enter boton-bt");
  }

  bluetooth() {
    if(this.bluetoothActivo) {
      this.utilsService.confirma("Bluetooth", "¿Está seguro de desactivar?", (resp: boolean) => {
        if(resp) {
          this.sensorInnercialService.desconecta();
        } else {
          console.log("Se arrepintio de desactivar");
        }
      });
    } else {
      console.log("Nos conectamos al device por defecto")
      this.sensorInnercialService.conectaUltimoRetry3(true);
    }
  }

}
