import { Component, OnInit, Input, NgZone } from '@angular/core';
import { UtilsService } from 'src/app/services/utils.service';
import { SensorInnercialService } from 'src/app/services/sensor-innercial.service';
import { Subscription } from 'rxjs';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-menu-inferior',
  templateUrl: './menu-inferior.component.html',
  styleUrls: ['./menu-inferior.component.scss'],
})
export class MenuInferiorComponent implements OnInit {

  @Input() pagina: string;

  public bluetoothActivo = false;
  public tecladoAbierto = false;
  public imgActive = 'assets/icon/bluetooth-active.svg';
  public imgInactive = 'assets/icon/bluetooth-inactive.svg';

  private subscription1: Subscription;

  constructor(private utilsService: UtilsService, private sensorInnercialService: SensorInnercialService, private _ngZone: NgZone, private platform: Platform) {
    console.log("En el constructor del menu inferior");
    /*
    this.platform.keyboardDidShow.subscribe(() => {
      this.tecladoAbierto = true;
    });
  
    this.platform.keyboardDidHide.subscribe(() => {
      this.tecladoAbierto = false;
    });
    */
  }

  //este metodo siempre se llama al ser un componente incrustado dentro de otra pagina
  ngOnInit() {
    /*
    console.log("En el on-init menu-inferior");
    this.subscription1 = this.sensorInnercialService.observadorDispositivo$().subscribe(disp => {
      console.log("MEN INF Cambio en el dispositivo " + disp.nombre);
      this._ngZone.run(() => {
        //this.bluetoothActivo = this.sensorInnercialService.conectado;

        //veamos si seguimos conectados
        this.sensorInnercialService.estoyConectado(disp.id).then(_ => {
          console.log("estoy conectado al dispositivo");
          this.bluetoothActivo = true;
        }). catch(_ => {
          console.log("No estoy conectado al dispositivo");
          this.bluetoothActivo = false;
        })

      });
    });
    */

    //this.revisaConexion();
  }

  revisaConexion() {
    console.log("reviso cone");
    //this.bluetoothActivo = this.sensorInnercialService.conectado;



    this.sensorInnercialService.elUltimoDispositivo().then(disp => {

      console.log("reviso el ult disp");
      this._ngZone.run(() => {
        if(disp) {
          console.log("rhay ultimo disp");

          //veamos si seguimos conectados
          this.sensorInnercialService.estoyConectado(disp.id).then(_ => {
            console.log("si conect");
            this.bluetoothActivo = true;
          }). catch(_ => {
            console.log("no conect");
            this.bluetoothActivo = false;
          })

          /*
          if(disp.conectado) {
            console.log("si conect");
            this.bluetoothActivo = true;
          } else {
            console.log("no conect");
          }
          */
        }
      });
    });
    
  }

  //este metodo siempre se llama al ser un componente incrustado dentro de otra pagina
  ngOnDestroy() {
    console.log("En el on-destroy menu-inferior");
    if(this.subscription1) {
      this.subscription1.unsubscribe();
    }
    
  }

  //este metodo nunca se llama al ser un componente incrustado dentro de otra pagina
  ionViewWillEnter() {
    console.log("En el view will enter menu-inferior");
  }

  bluetooth() {
    if(this.bluetoothActivo) {
      this.utilsService.confirma("Bluetooth", "¿Está seguro de desactivar?", (resp: boolean) => {
        if(resp) {
          this.sensorInnercialService.desconecta();
        } else {
          console.log("Se arrepintio de desactivar");
        }
      });
    } else {
      console.log("Nos conectamos al device por defecto")
      this.sensorInnercialService.conectaUltimoRetry3(true);
    }
  }

}
