import { NgModule } from '@angular/core';
import { MenuInferiorComponent } from './menu-inferior.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
    declarations: [
        MenuInferiorComponent
    ],
   imports: [RouterModule,
    CommonModule,
    IonicModule,
    TranslateModule.forChild()],
   exports: [
    MenuInferiorComponent,
    CommonModule,
    TranslateModule
   ]
})
   export class MenuInferiorModule {}