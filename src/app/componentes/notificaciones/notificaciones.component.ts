import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.component.html',
  styleUrls: ['./notificaciones.component.scss'],
})
export class NotificacionesComponent implements OnInit {

  codigo: string;

  constructor(navParams: NavParams) {
    this.codigo = navParams.get('codigo');
  }

  ngOnInit() {}

}
