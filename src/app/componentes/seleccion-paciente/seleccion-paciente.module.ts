import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeleccionPacientePageRoutingModule } from './seleccion-paciente-routing.module';

import { SeleccionPacientePage } from './seleccion-paciente.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    SeleccionPacientePageRoutingModule
  ],
  declarations: [SeleccionPacientePage]
})
export class SeleccionPacientePageModule { }
