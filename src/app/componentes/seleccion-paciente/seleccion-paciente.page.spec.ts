import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SeleccionPacientePage } from './seleccion-paciente.page';

describe('SeleccionPacientePage', () => {
  let component: SeleccionPacientePage;
  let fixture: ComponentFixture<SeleccionPacientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeleccionPacientePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SeleccionPacientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
