import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { debounceTime } from 'rxjs/operators';
import { TranslateConfigService } from 'src/app/services/translate-config.service';

@Component({
  selector: 'app-seleccion-paciente',
  templateUrl: './seleccion-paciente.page.html',
  styleUrls: ['./seleccion-paciente.page.scss'],
})
export class SeleccionPacientePage implements OnInit {

  @Input() pacientes: any;
  searchControl: any;
  pacientesFiltro: any;
  filtro = '';
  idUsuarioSelect: string;
  constructor(private translateConfigService: TranslateConfigService, private modalController: ModalController) {
    this.searchControl = new FormControl();
  }

  ngOnInit() {
    console.log("pacientes en modal", this.pacientes);

    this.pacientesFiltro = this.pacientes;

  }

  borrarFiltro($event) {
    this.filtro = '';
    this.pacientesFiltro = this.pacientes;
  }

  filtrarBusqueda($event) {

    this.filtro = ($event.target as HTMLInputElement).value;
    console.log(this.filtro);
    this.resultadoFitro();

  }

  resultadoFitro() {

    this.pacientesFiltro = this.pacientes.filter((item: any) => {
      return item.nombre.toLowerCase().includes(this.filtro.toLowerCase());
    });
  }

  cerrar() {
    this.modalController.dismiss({
      'dismissed': true
    });
    this.modalController.dismiss();
  }

  enviarPaciente(): void {

    for (let i = 0; i < this.pacientes.length; i++) {
      if (this.pacientes[i].id.toString() === this.idUsuarioSelect) {
        this.modalController.dismiss(this.pacientes[i]);
      }
    }
  }

}
