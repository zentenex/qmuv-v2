import { Component, OnInit } from '@angular/core';

import { Platform, MenuController, LoadingController } from '@ionic/angular';
import { AuthenticationService } from './services/authentication.service';
import { Observable, Subscription } from 'rxjs';
import { SensorInnercialService } from './services/sensor-innercial.service';
import { environment } from 'src/environments/environment';
import { UtilsService } from './services/utils.service';
import { TranslateConfigService } from './services/translate-config.service';
import { LangChangeEvent } from '@ngx-translate/core';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { StatusBar } from '@awesome-cordova-plugins/status-bar/ngx';
import { Auth } from '@angular/fire/auth';
import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  
  public usuario$: Observable<any>;

  public version = "0.0.0";

  public selectedIndex = 0;
  public appPages = [
    {
      title: '',//'Inicio',
      langCode: 'MENU.home',
      url: '/home',
      icon: 'home'
    },
    {
      title: '',//'Dispositivos',
      langCode: 'MENU.devices',
      url: '/dispositivo-list',
      icon: 'bluetooth'
    },

    {
      title: '',//'Tutorial',
      langCode: 'MENU.profile',
      url: '/perfil',
      icon: 'person'
    },

    {
      title: '',//'Tutorial',
      langCode: 'MENU.tutorial',
      url: '/tutorial',
      icon: 'videocam'
    },

    {
      title: '',//'Pacientes eliminados',
      langCode: 'MENU.deleted',
      url: '/pacientes-eliminados',
      icon: 'person-remove'
    },

    {
      title: '',//'Configuración',
      langCode: 'MENU.config',
      url: '/configuracion',
      icon: 'cog'
    },

    {
      title: '',//'Suscripción',
      langCode: 'MENU.subscription',
      url: '/suscripcion',
      icon: 'card'
    }
  ];
  public labels = ['pacientes', 'reportes'];
  public muestraMenu = false;
  private escaneando = false;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authenticationService: AuthenticationService, //No sacar, gatilla el redireccionamiento en el constructor
    private menuCtrl: MenuController,
    private menu: MenuController,
    private sensorInnercialService: SensorInnercialService,
    private utilsService: UtilsService,
    //private loadingController: LoadingController,
    private translateConf: TranslateConfigService,
    private appVersion: AppVersion,
    private androidPermissions: AndroidPermissions,
    private afAuth: Auth,
    private storage: Storage
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      //this.statusBar.styleDefault();
      this.statusBar.styleLightContent();
      // set status bar to moradou

      this.storage.create();

      this.statusBar.backgroundColorByHexString('#424D99');
      //Seteamos el idioma por defecto el del navegador

      this.statusBar.overlaysWebView(false)

      this.translateConf.inicializaLanguage().then(_ => {
        this.translateConf.getLangObs().subscribe((event: LangChangeEvent) => {
          console.log("Cambio en el idioma :" + event.lang)
          for (let entradaMenu of this.appPages) {
            this.translateConf.getTexto(entradaMenu.langCode).subscribe(trans => {
              entradaMenu.title = trans;
            })
          }
        });

        if (this.platform.is('cordova')) {
          this.sensorInnercialService.virtual = false;
  
          this.appVersion.getVersionNumber().then(v => this.version = v)
  
          //reviso si el bt esta disponible
          this.sensorInnercialService.bluetoothCorrecto();

          if (this.platform.is('android')) {
            console.log("SOY ANDROIDE");
            this.permisosAndroid();
          }
        } else {
          this.sensorInnercialService.virtual = true;
        }
  
        //Nos conectamos al device por defecto
        if (environment.autoconnect) {
          this.conectaQMUV();
        }
  
        this.sensorInnercialService.muestraEstado();

      })
      //this.splashScreen.hide();

    });
  }

  /**
   * Nos conectamos al ultimo dispositivo siempre que ya no estemos previamente conectados
   *
  private async conectaUltim() {
    let ultimoDisp = await this.sensorInnercialService.elUltimoDispositivo();
    if(ultimoDisp) {
      this.sensorInnercialService.estoyConectado(ultimoDisp.id).then(() => {
        console.log("Ya estoy conectado, no insistire otra vez");
      }).catch(() => {
        this.sensorInnercialService.conectaUltimo(false);
      })
    }
  }
  */

  private permisosAndroid() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
      result => {
        console.log('Has fine location permission?',result.hasPermission);
        if(!result.hasPermission) {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION);
        }
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
    );
  }

  private async conectaQMUV() {
    console.log("Nos conectamos al ultimo dispositivo conocido");

    let ultimoDisp = await this.sensorInnercialService.elUltimoDispositivo();
    if (ultimoDisp) {

      //Muestro spinner
      /*
      const loading = await this.loadingController.create({
        message: 'Buscando último dispositivo conocido...',
        duration: environment.btScanSeg * 1000
      });
      await loading.present();
      */

      let intentandoConectar = false;
      let subscripcion = this.sensorInnercialService.observadorNuevosDispositivos$().subscribe(async dev => {
        if (dev.id === ultimoDisp.id && !intentandoConectar) {
          console.log("EUREKA");
          intentandoConectar = true;
          let terminoDebug = await this.translateConf.getTextoNow("terminoDebug");
          this.utilsService.presentaToastDebug(terminoDebug);

          this.escaneando = false;
          this.sensorInnercialService.escaneaStop();
          subscripcion.unsubscribe();


          //loading.dismiss();

          //this.sensorInnercialService.conectaUltimo(false);

          /*
          this.conectaUltim();
          setTimeout(() => this.conectaUltim(), 3000);
          setTimeout(() => this.conectaUltim(), 6000);
          */
          this.sensorInnercialService.conectaUltimoRetry3(false);
        }
      })

      //Primero debemos escanear. Debe ser descubierto sino no nos deja conectar
      const scanSegundos = environment.btScanSeg;
      const scanMilisegundos = scanSegundos * 1000;
      this.sensorInnercialService.escaneaStart();
      this.escaneando = true;

      setTimeout(() => this.finScan(subscripcion), scanMilisegundos);
    } else {
      console.log("No existe ultimo dispositivo");
    }

  }

  private async finScan(subscripcion: Subscription) {
    if (this.escaneando) {
      let errorCabecera = await this.translateConf.getTextoNow("ERRORES.errorCabecera");
      this.utilsService.presentaToastDebug(errorCabecera);
      console.log("Fin escaneo");
      this.sensorInnercialService.escaneaStop();
      subscripcion.unsubscribe();
    } else {
      let terminoEscaner = await this.translateConf.getTextoNow("ERRORES.terminoEscaner");
      this.utilsService.presentaToastDebug(terminoEscaner);
    }
  }

  cierraMenu() {
    this.menu.close('ppal');
  }

  ngOnInit() {
    console.log("En el on-init app.component");
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }

    console.log("EL APP COMPONENT SE SUSCRIBE")
    //this.usuario$ = this.authenticationService.observaUserAuth();
    this.menuCtrl.enable(false);

    this.afAuth.onAuthStateChanged((usr) => {
      if (usr) {
        this.menuCtrl.enable(true);
      } else {
        this.menuCtrl.enable(false);
      }
    })
/*
    this.usuario$.subscribe(usr => {
      if (usr) {
        this.menuCtrl.enable(true);
      } else {
        this.menuCtrl.enable(false);
      }
    })
    */

    /*
    this.router.events.subscribe((event: RouterEvent) => {
      if (event instanceof NavigationEnd && event.url === '/login') {
        this.menuCtrl.enable(false);
      } else if (event instanceof NavigationEnd && event.url === '/') {
        this.menuCtrl.enable(false);
      } else if (event instanceof NavigationEnd && event.url === '/register') {
        this.menuCtrl.enable(false);
      }
    });
    */
  }

  logout() {
    this.authenticationService.logoutUser()
      .then(res => {
        console.log(res);
        //this.navCtrl.navigateBack('');
      })
      .catch(error => {
        console.log(error);
      })
  }
}
