import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AlgoritmoTimedUpandGo, SesionUPGO } from '../services/prueba/upgo.service';
import { UtilsService } from '../services/utils.service';
import { ControlDTO } from './cuenta-dao.service';

@Injectable({
  providedIn: 'root'
})
export class PruebasDAOService {
  private baseURI = '/api/app/test';

  private todosLosAnalisisUPGO: Array<AnalisisUPGO> = [];
  private todosLosAnalisis10mts: Array<Analisis10MTS> = [];

  constructor(private http: HttpClient, private utilSrv: UtilsService) { }

  public puebasHabilitadas(): number[] {
    let idPruebas: number[] = [1, 2, 3];
    return idPruebas;
  }

  /**
   * Guarda la prueba UPGO en el backend
   * @param ana Resultado del analisis, tal como se ve en pantalla
   * @param sesi Los datos de mediciones y algoritmo usados en el analisis (usado para guardar el RAW)
   */
  public async savePruebaUPGO(ana: AnalisisUPGO, sesi: SesionUPGO) {
    if(environment.server) {

      let modelo = new TestUpgoSesionModel();
      modelo.analysisMode = ana.analysisMode;

      modelo.standAccMax = ana.aceleracionAlLevantarse;
      modelo.sitAccMax = ana.aceleracionAlSentarse;
      modelo.cadenceGo = ana.cadenciaIda;
      modelo.cadenceReturn = ana.cadenciaVuelta;
      modelo.maxStandInclination = ana.maxStandInclination;
      modelo.maxSitInclination = ana.maxSitInclination;
      
      modelo.test = ana.t;
      modelo.stand = ana.t1;
      modelo.go = ana.t2;
      modelo.turn1 = ana.t3;
      modelo.back = ana.t4;
      modelo.turn2 = ana.t5;
      modelo.sit = ana.t6;
      modelo.turn1Vel = ana.velocidadAngularGiro3m;
      modelo.turn2Vel = ana.velocidadAngularGiroSilla;

      modelo.rof = ana.rof;

      modelo.rofGo = ana.rofGo;
      modelo.rofReturn = ana.rofReturn;
      modelo.rofSit = ana.rofSit;
      modelo.rofStand = ana.rofStand;
      modelo.rofTurn1 = ana.rofTurn1;
      modelo.rofTurn2 = ana.rofTurn2;

      modelo.fecha = ana.fecha.toISOString();

      modelo.events = [];
      for(let valor of ana.eventosMulti[0]) {
        let vm = new Value2Model();
        vm.medicion = 1;
        vm.value = valor;
        modelo.events.push(vm)
      }
      if(ana.eventosMulti.length>1) {
        for(let valor of ana.eventosMulti[1]) {
          let vm = new Value2Model();
          vm.medicion = 2;
          vm.value = valor;
          modelo.events.push(vm)
        }
      }
      if(ana.eventosMulti.length>2) {
        for(let valor of ana.eventosMulti[2]) {
          let vm = new Value2Model();
          vm.medicion = 3;
          vm.value = valor;
          modelo.events.push(vm)
        }
      }

      modelo.inclinations = [];
      modelo.rotations = [];

      if(ana.tiemposMulti.length>0) {
        modelo.tiempo1 = ana.tiemposMulti[0];
      }
      if(ana.tiemposMulti.length>1) {
        modelo.tiempo2 = ana.tiemposMulti[1];
      }
      if(ana.tiemposMulti.length>2) {
        modelo.tiempo3 = ana.tiemposMulti[2];
      }

      //Agrego datos del 1er grafico
      for(let valor of ana.inclinacionesMulti[0]) {
        let vm = new Value2Model();
        vm.medicion = 1;
        vm.value = valor;
        modelo.inclinations.push(vm)
      }

      for(let valor of ana.rotacionesMulti[0]) {
        let vm = new Value2Model();
        vm.medicion = 1;
        vm.value = valor;
        modelo.rotations.push(vm)
      }

      //Agrego datos del 2do grafico
      if(ana.tiemposMulti.length>1) {
        for(let valor of ana.inclinacionesMulti[1]) {
          let vm = new Value2Model();
          vm.medicion = 2;
          vm.value = valor;
          modelo.inclinations.push(vm)
        }
  
        for(let valor of ana.rotacionesMulti[1]) {
          let vm = new Value2Model();
          vm.medicion = 2;
          vm.value = valor;
          modelo.rotations.push(vm)
        }
      }

      //Agrego datos del 3er grafico
      if(ana.tiemposMulti.length>2) {
        for(let valor of ana.inclinacionesMulti[2]) {
          let vm = new Value2Model();
          vm.medicion = 3;
          vm.value = valor;
          modelo.inclinations.push(vm)
        }
  
        for(let valor of ana.rotacionesMulti[2]) {
          let vm = new Value2Model();
          vm.medicion = 3;
          vm.value = valor;
          modelo.rotations.push(vm)
        }
      }


      //ses.recomendaciones = ana.recomendaciones;
      

      const recurso = this.baseURI + '/upgo/paciente/' + ana.idPaciente + '/sesion';
      console.log('REQUEST [POST]: ' + environment.endpointApi + recurso + " con: " + JSON.stringify(modelo));
      let resp = await this.http.post<ControlDTO>(environment.endpointApi + recurso, modelo).toPromise(); //.subscribe(resp => {
        if(resp.code >= 0) {
          let idSesion = resp.code;
          console.log("Sesion [" + idSesion + "] creada con exito")
          
          if(this.utilSrv.sendRawData) {
            //Guardamos la data RAW
            await this.savePruebaUPGOraw(ana.idPaciente, idSesion, sesi.rawData, sesi.algoritmo);
          }

        } else {
          console.error(resp.message)
        }
        /*
      }, err => {
        console.error("Error al guardar sesion upgo: " + JSON.stringify(err))
      })
      */

    }
    this.todosLosAnalisisUPGO.push(ana);
  }

  //Guarda la data RAW
  private async savePruebaUPGOraw(idPaciente: number, idSesion: number, m: AlgoritmoTimedUpandGo[], a: AlgoritmoTimedUpandGo) {

    let rawData = new UPGORawInput(); // TestUpgoRawModel

    let an = this.copiaAlgoritmoEnRaw(a);
    //an.sesionID = idSesion;
    rawData.raw_analisis = an;

    rawData.raw_samples = [];
    for(let unaM of m) {
      let sam = this.copiaAlgoritmoEnRaw(unaM);
      //sam.sesionID = idSesion;
      rawData.raw_samples.push(sam);
    }

    const recurso = this.baseURI + '/upgo/paciente/' + idPaciente + '/sesion/' + idSesion + '/raw';
    console.log('REQUEST [POST]: ' + environment.endpointApi + recurso + " con: " + JSON.stringify(rawData));
    let resp = await this.http.post<ControlDTO>(environment.endpointApi + recurso, rawData).toPromise(); //.subscribe(resp => {
      if(resp.code >= 0) {
        console.log("DATA RAW ENVIADA")
      } else {
        console.error(resp.message);
      }
      /*
    }, err => {
      console.error(JSON.stringify(err));
    });
    */
  }

  /**
   * Guarda la prueba 10 mts en el backend
   * @param ana Resultado del analisis, tal como se ve en pantalla
   */
  public async savePrueba10mts(ana: Analisis10MTS) {
    //Sesion10mtsInDTO
    if(environment.server) {

      let ses = new TestDiezSesionModel(); //Sesion10mtsInDTO();

      ses.analysisMode = ana.analysisMode;

      ses.dvLDsupporttime = ana.dv_l_dsupporttime;
      ses.dvLSsupporttime = ana.dv_l_ssupporttime;
      //ses.dvLSteplength = ana.dv_l_steplength;
      ses.dvLSteptime = ana.dv_l_steptime;
      ses.dvLStridetime = ana.dv_l_stridetime;
      ses.dvRDsupporttime = ana.dv_r_dsupporttime;
      ses.dvRSsupporttime = ana.dv_r_ssupporttime;
      //ses.dvRSteplength = ana.dv_r_steplength;
      ses.dvRSteptime = ana.dv_r_steptime;
      ses.dvRStridetime = ana.dv_r_stridetime;

        ses.lSteplength = ana.l_steplength;
        ses.dvLSteplength = ana.dv_l_steplength;
        ses.rSteplength = ana.r_steplength;
        ses.dvRSteplength = ana.dv_r_steplength;

        ses.lStance = ana.l_stance;
        ses.dvLStance = ana.dv_l_stance;
        ses.rStance = ana.r_stance;
        ses.dvRStance = ana.dv_r_stance;

        ses.lSwing = ana.l_swing;
        ses.dvLSwing = ana.dv_l_swing;
        ses.rSwing = ana.r_swing;
        ses.dvRSwing = ana.dv_r_swing;

      ses.lCadence = ana.l_cadence;
      ses.dvLCadence = ana.dv_l_cadence;
      ses.rCadence = ana.r_cadence;
      ses.dvRCadence = ana.dv_r_cadence;

      ses.gaitResult = ana.gait_result;
      ses.gaitSimetry = ana.gait_simetry;
      ses.lDsupporttime = ana.l_dsupporttime;
      ses.lSsupporttime = ana.l_ssupporttime;
      //ses.lSteplength = ana.l_steplength;
      ses.lSteptime = ana.l_steptime;
      ses.lStridetime = ana.l_stridetime;
      ses.rDsupporttime = ana.r_dsupporttime;
      ses.rSsupporttime = ana.r_ssupporttime;
      //ses.rSteplength = ana.r_ssupporttime;
      ses.rSteptime = ana.r_steptime;
      ses.rStridetime = ana.r_stridetime;
      ses.stanceSimetry = ana.stance_simetry;
      ses.swingSimetry = ana.swing_simetry;
      ses.vGait = ana.v_gait;

      if(ana.tiemposMulti.length>0) {
        ses.tiempo1 = ana.tiemposMulti[0];
      }
      if(ana.tiemposMulti.length>1) {
        ses.tiempo2 = ana.tiemposMulti[1];
      }
      if(ana.tiemposMulti.length>2) {
        ses.tiempo3 = ana.tiemposMulti[2];
      }

      ses.eje_z_array = [];

      //Agrego datos del 1er grafico
      for(let valor of ana.az_array[0]) {
        let vm = new Value2Model();
        vm.medicion = 1;
        vm.value = valor;
        ses.eje_z_array.push(vm)
      }

      for(let valor of ana.ICpos_array[0]) {
        let vm = new Value2Model();
        vm.medicion = 1;
        vm.value = valor;
        ses.pos_array.push(vm)
      }

      for(let valor of ana.ICside_array[0]) {
        let vm = new Value2Model();
        vm.medicion = 1;
        vm.value = valor;
        ses.side_array.push(vm)
      }

      //Agrego datos del 2do grafico
      if(ana.az_array.length>1) {
        for(let valor of ana.az_array[1]) {
          let vm = new Value2Model();
          vm.medicion = 2;
          vm.value = valor;
          ses.eje_z_array.push(vm)
        }
  
        for(let valor of ana.ICpos_array[1]) {
          let vm = new Value2Model();
          vm.medicion = 2;
          vm.value = valor;
          ses.pos_array.push(vm)
        }
  
        for(let valor of ana.ICside_array[1]) {
          let vm = new Value2Model();
          vm.medicion = 2;
          vm.value = valor;
          ses.side_array.push(vm)
        }
      }

      //Agrego datos del 3er grafico
      if(ana.az_array.length>2) {
        for(let valor of ana.az_array[2]) {
          let vm = new Value2Model();
          vm.medicion = 3;
          vm.value = valor;
          ses.eje_z_array.push(vm)
        }
  
        for(let valor of ana.ICpos_array[2]) {
          let vm = new Value2Model();
          vm.medicion = 3;
          vm.value = valor;
          ses.pos_array.push(vm)
        }
  
        for(let valor of ana.ICside_array[2]) {
          let vm = new Value2Model();
          vm.medicion = 3;
          vm.value = valor;
          ses.side_array.push(vm)
        }
      }

      ses.fecha = ana.fecha.toISOString();

      const recurso = this.baseURI + '/diez/paciente/' + ana.idPaciente + '/sesion';
      console.log('REQUEST [POST]: ' + environment.endpointApi + recurso + " con: " + JSON.stringify(ses));
      let resp = await this.http.post<ControlDTO>(environment.endpointApi + recurso, ses).toPromise(); //.subscribe(resp => {
        if(resp.code >= 0) {
          console.log("exito")
        } else {
          console.error(resp.message)
        }
        /*
      }, err => {
        console.error("Error al guardar sesion diez: " + JSON.stringify(err))
      })
*/
    }
    this.todosLosAnalisis10mts.push(ana);
  }

  /**
   * Obtenemos todas las pruebas upgo de un paciente
   * @param idPaciente ID del paciente
   */
  public async cargaTodosLosTestUPGODelPaciente(idPaciente: number) {
    if(environment.server) {
      this.todosLosAnalisisUPGO = [];
      const recurso = this.baseURI + '/upgo/paciente/' + idPaciente + '/sesiones';
      console.log('REQUEST [GET]: ' + environment.endpointApi + recurso);
      let resp = await this.http.get<TestUpgoSesionModel[]>(environment.endpointApi + recurso).toPromise();
      for(let sesMod of resp) {
        let ana = new AnalisisUPGO();
        ana.aceleracionAlLevantarse = sesMod.standAccMax;
        ana.aceleracionAlSentarse = sesMod.sitAccMax;
        ana.cadenciaIda = sesMod.cadenceGo
        ana.cadenciaVuelta = sesMod.cadenceReturn;
        ana.maxStandInclination = sesMod.maxStandInclination;
        ana.maxSitInclination = sesMod.maxSitInclination;
        ana.fecha = new Date(sesMod.fecha)
        ana.idPaciente = idPaciente;
        
        ana.t = +sesMod.test;
        ana.t1 = sesMod.stand;
        ana.t2 = sesMod.go;
        ana.t3 = sesMod.turn1;
        ana.t4 = sesMod.back;
        ana.t5 = sesMod.turn2;
        ana.t6 = sesMod.sit;
        ana.velocidadAngularGiro3m = sesMod.turn1Vel;
        ana.velocidadAngularGiroSilla = sesMod.turn2Vel;

        ana.eventosMulti = [];
        ana.eventosMulti[0] = [];
        ana.eventosMulti[1] = [];
        ana.eventosMulti[2] = [];

        for(let eve of sesMod.events) {
          if(eve.medicion > 0) {
            ana.eventosMulti[eve.medicion - 1].push(eve.value);
          }
        }

        ana.inclinacionesMulti = [];
        ana.rotacionesMulti = [];

        ana.inclinacionesMulti[0] = [];
        ana.rotacionesMulti[0] = [];
        ana.inclinacionesMulti[1] = [];
        ana.rotacionesMulti[1] = [];
        ana.inclinacionesMulti[2] = [];
        ana.rotacionesMulti[2] = [];

        if(sesMod.inclinations != null) {
          for(let inc of sesMod.inclinations) {
            //console.log("inc medicion : " + inc.medicion);
            if(inc.medicion > 0) {
              ana.inclinacionesMulti[inc.medicion - 1].push(inc.value);
            }
          }
        } else {
          console.warn("No se encontraron inclinaciones")
        }
        
        if(sesMod.rotations != null) {
          for(let rot of sesMod.rotations) {
            //console.log("rot medicion : " + rot.medicion);
            if(rot.medicion > 0) {
              ana.rotacionesMulti[rot.medicion - 1].push(rot.value);
            }
          }
        } else {
          console.warn("No se encontraron rotaciones")
        }

        ana.tiemposMulti = [];
        ana.tiemposMulti[0] = sesMod.tiempo1;
        ana.tiemposMulti[1] = sesMod.tiempo2;
        ana.tiemposMulti[2] = sesMod.tiempo3;

        ana.rof = sesMod.rof;
        ana.rofGo = sesMod.rofGo;
        ana.rofReturn = sesMod.rofReturn;
        ana.rofSit = sesMod.rofSit;
        ana.rofStand = sesMod.rofStand;
        ana.rofTurn1 = sesMod.rofTurn1;
        ana.rofTurn2 = sesMod.rofTurn2;

        ana.analysisMode = sesMod.analysisMode;

        console.log("Se agrega el analisis UPGO : " + JSON.stringify(ana));
        this.todosLosAnalisisUPGO.push(ana);
      }
    }
  }

  /**
   * Obtenemos todas las pruebas 10mts de un paciente
   * @param idPaciente ID del paciente
   */
  public async cargaTodosLosTestDIEZDelPaciente(idPaciente: number) {
    if(environment.server) {
      this.todosLosAnalisis10mts = [];
      const recurso = this.baseURI + '/diez/paciente/' + idPaciente + '/sesiones';
      console.log('REQUEST [GET]: ' + environment.endpointApi + recurso);
      let resp = await this.http.get<TestDiezSesionModel[]>(environment.endpointApi + recurso).toPromise();
      for(let ses of resp) {
        let ana = new Analisis10MTS();
        ana.fecha = new Date(ses.fecha)

        ana.l_cadence = ses.lCadence;
        ana.dv_l_cadence = ses.dvLCadence;
        ana.r_cadence = ses.rCadence;
        ana.dv_r_cadence = ses.dvRCadence;

        ana.l_steplength = ses.lSteplength;
        ana.dv_l_steplength = ses.dvLSteplength;
        ana.r_steplength = ses.rSteplength;
        ana.dv_r_steplength = ses.dvRSteplength;

        ana.l_stance = ses.lStance;
        ana.dv_l_stance = ses.dvLStance;
        ana.r_stance = ses.rStance;
        ana.dv_r_stance = ses.dvRStance;

        ana.l_swing = ses.lSwing;
        ana.dv_l_swing = ses.dvLSwing;
        ana.r_swing = ses.rSwing;
        ana.dv_r_swing = ses.dvRSwing;

        ana.dv_l_dsupporttime = ses.dvLDsupporttime;
        ana.dv_l_ssupporttime = ses.dvLSsupporttime;
        ana.dv_l_steptime = ses.dvLSteptime;
        ana.dv_l_stridetime = ses.dvLStridetime;
        ana.dv_r_dsupporttime = ses.dvRDsupporttime;
        ana.dv_r_ssupporttime = ses.dvRSsupporttime;
        ana.dv_r_steptime = ses.dvRSteptime;
        ana.dv_r_stridetime = ses.dvRStridetime;
        ana.gait_result = ses.gaitResult;
        ana.gait_simetry = ses.gaitSimetry;
        ana.idPaciente = idPaciente;
        ana.l_dsupporttime = ses.lDsupporttime;
        ana.l_ssupporttime = ses.lSsupporttime;
        ana.l_steptime = ses.lSteptime;
        ana.l_stridetime = ses.lStridetime;
        ana.r_dsupporttime = ses.rDsupporttime;
        ana.r_ssupporttime = ses.rSsupporttime;
        ana.r_steptime = ses.rSteptime;
        ana.r_stridetime = ses.rStridetime;
        ana.stance_simetry = ses.stanceSimetry;
        ana.swing_simetry = ses.swingSimetry;
        ana.v_gait = ses.vGait;

        ana.analysisMode = ses.analysisMode;

        ana.az_array = [];
        ana.az_array[0] = [];
        ana.az_array[1] = [];
        ana.az_array[2] = [];
        if(ses.eje_z_array != null) {
          for(let z of ses.eje_z_array) {
            //console.log("z medicion : " + z.medicion);
            if(z.medicion > 0) {
              ana.az_array[z.medicion - 1].push(z.value);
            }
          }
        } else {
          console.warn("No se encontraron datos para el eje Z")
        }

        ana.ICpos_array = [];
        ana.ICpos_array[0] = [];
        ana.ICpos_array[1] = [];
        ana.ICpos_array[2] = [];
        if(ses.pos_array != null) {
          for(let z of ses.pos_array) {
            //console.log("z medicion : " + z.medicion);
            if(z.medicion > 0) {
              ana.ICpos_array[z.medicion - 1].push(z.value);
            }
          }
        } else {
          console.warn("No se encontraron datos para IC Pos")
        }

        ana.ICside_array = [];
        ana.ICside_array[0] = [];
        ana.ICside_array[1] = [];
        ana.ICside_array[2] = [];
        if(ses.side_array != null) {
          for(let z of ses.side_array) {
            //console.log("z medicion : " + z.medicion);
            if(z.medicion > 0) {
              ana.ICside_array[z.medicion - 1].push(z.value);
            }
          }
        } else {
          console.warn("No se encontraron datos para IC Side")
        }

        ana.tiemposMulti = [];
        ana.tiemposMulti[0] = ses.tiempo1;
        ana.tiemposMulti[1] = ses.tiempo2;
        ana.tiemposMulti[2] = ses.tiempo3;

        this.todosLosAnalisis10mts.push(ana);
      }
    }
  }

  public async cantidadTestUPGOPorPaciente(idPaciente: number) {
    let total = 0;
    await this.cargaTodosLosTestUPGODelPaciente(idPaciente);
    for(let analisis of this.todosLosAnalisisUPGO) {
      if(analisis.idPaciente === idPaciente) {
        total = total + 1;
      }
    }
    return total;
  }

  public async cantidadTest10mtsPorPaciente(idPaciente: number) {
    let total = 0;
    await this.cargaTodosLosTestDIEZDelPaciente(idPaciente);
    for(let analisis of this.todosLosAnalisis10mts) {
      if(analisis.idPaciente === idPaciente) {
        total = total + 1;
      }
    }
    return total;
  }

  public loadUltimoAnalisisUPGO(idPaciente: number) {
    let ultimoAnalisis: AnalisisUPGO;
    for(let analisis of this.todosLosAnalisisUPGO) {
      if(analisis.idPaciente === idPaciente) {
        if(ultimoAnalisis == null) {
          ultimoAnalisis = analisis;
        } else {
          if(analisis.fecha > ultimoAnalisis.fecha) {
            ultimoAnalisis = analisis;
          }
        }
      }
    }
    return ultimoAnalisis;
  }

  public loadUltimoAnalisis10mts(idPaciente: number) {
    let ultimoAnalisis: Analisis10MTS;
    for(let analisis of this.todosLosAnalisis10mts) {
      if(analisis.idPaciente === idPaciente) {
        if(ultimoAnalisis == null) {
          ultimoAnalisis = analisis;
        } else {
          if(analisis.fecha > ultimoAnalisis.fecha) {
            ultimoAnalisis = analisis;
          }
        }
      }
    }
    return ultimoAnalisis;
  }

  public loadUltimos10Analisis10mts(idPaciente: number) {
    let ultimosAnalisis: Analisis10MTS[] = [];
    let los10: Analisis10MTS[] = [];

    for(let analisis of this.todosLosAnalisis10mts) {
      if(analisis.idPaciente === idPaciente) {
        ultimosAnalisis.push(analisis);
      }
    }
    let ordenados = ultimosAnalisis.sort(this.compara10mts);
    if(ordenados.length>10) {
      los10.push(ordenados[0]);
      for(let i=ordenados.length-(10-1); i<ordenados.length; i++) {
        los10.push(ordenados[i]);
      }
      //los10.push(ordenados[ordenados.length-1]);
    } else {
      los10 = ordenados;
    }
    return los10;
    //return ultimosAnalisis.sort(this.compara10mts);
  }

  public loadUltimos10AnalisisUpgo(idPaciente: number) {
    let ultimosAnalisis: AnalisisUPGO[] = [];
    let los10: AnalisisUPGO[] = [];

    for(let analisis of this.todosLosAnalisisUPGO) {
      if(analisis.idPaciente === idPaciente) {
        ultimosAnalisis.push(analisis);
      }
    }
    let ordenados = ultimosAnalisis.sort(this.comparaUpgo);
    if(ordenados.length>10) {
      los10.push(ordenados[0]);
      for(let i=ordenados.length-(10-1); i<ordenados.length; i++) {
        los10.push(ordenados[i]);
      }
      //los10.push(ordenados[ordenados.length-1]);
    } else {
      los10 = ordenados;
    }
    return los10;
  }

  private compara10mts(a: Analisis10MTS, b: Analisis10MTS) {
    const bandA = a.fecha;
    const bandB = b.fecha;
  
    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison;
  }

  private comparaUpgo(a: AnalisisUPGO, b: AnalisisUPGO) {
    const bandA = a.fecha;
    const bandB = b.fecha;
  
    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison;
  }

  private copiaAlgoritmoEnRaw(a: AlgoritmoTimedUpandGo) {
    let an = new TestUpgoRawModel();

    an.analysis_mode = a.analysis_mode;
    an.back = a.d_back;
    an.cadenceGo = a.cadence_go;
    an.cadenceReturn = a.cadence_return;
    an.eval_ok = a.eval_ok;
    an.go = a.d_go;
    an.maxSitInclination = a.max_sit_inclination;
    an.maxStandInclination = a.max_stand_inclination;
    an.rof = a.rof;
    an.rofGo = a.rof_go;
    an.rofReturn = a.rof_return;
    an.rofSit = a.rof_sit;
    an.rofStand = a.rof_stand;
    an.rofTurn1 = a.rof_turn1;
    an.rofTurn2 = a.rof_turn2;
    an.sit = a.d_sit;
    an.sitAccMax = a.sit_acc_max;
    an.stand = a.d_stand;
    an.standAccMax = a.stand_acc_max;
    an.test = a.d_test;
    an.test_number = a.test_number;
    an.turn1 = a.d_turn1;
    an.turn1Vel = a.turn1_vel;
    an.turn2 = a.d_turn2;
    an.turn2Vel = a.turn2_vel;

    //Arreglos
    an.arreglos = [];

    for(let valor of a.ax) {
      let aRaw = new ArregloRAW();
      aRaw.name = "AX";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.ay) {
      let aRaw = new ArregloRAW();
      aRaw.name = "AY";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.az) {
      let aRaw = new ArregloRAW();
      aRaw.name = "AZ";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.cadence_go_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "CADENCE_GO";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.cadence_return_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "CADENCE_RETURN";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.d_back_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "BACK";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.d_go_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "GO";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.d_sit_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "SIT";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.d_stand_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "STAND";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.d_test_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "TEST";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.d_turn1_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "TURN1";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.d_turn2_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "TURN2";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.events) {
      let aRaw = new ArregloRAW();
      aRaw.name = "EVENT";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }
    
    for(let valor of a.filter_coef) {
      let aRaw = new ArregloRAW();
      aRaw.name = "FILTER_COEF";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.gx) {
      let aRaw = new ArregloRAW();
      aRaw.name = "GX";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.gy) {
      let aRaw = new ArregloRAW();
      aRaw.name = "GY";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.gz) {
      let aRaw = new ArregloRAW();
      aRaw.name = "GZ";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.inclination_) {
      let aRaw = new ArregloRAW();
      aRaw.name = "INCLINATION";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.max_inclination) {
      let aRaw = new ArregloRAW();
      aRaw.name = "MAX_INCLINATION";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.max_rotation) {
      let aRaw = new ArregloRAW();
      aRaw.name = "MAX_ROTATION";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.max_sit_inclination_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "MAX_SIT_INCLINATION";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.max_stand_inclination_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "MAX_STAND_INCLINATION";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.qw) {
      let aRaw = new ArregloRAW();
      aRaw.name = "QW";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.qx) {
      let aRaw = new ArregloRAW();
      aRaw.name = "QX";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.qy) {
      let aRaw = new ArregloRAW();
      aRaw.name = "QY";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.qz) {
      let aRaw = new ArregloRAW();
      aRaw.name = "QZ";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.rotation_) {
      let aRaw = new ArregloRAW();
      aRaw.name = "ROTATION";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.sit_acc_max_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "SIT_ACC_MAX";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.stand_acc_max_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "STAND_ACC_MAX";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.turn1_vel_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "TURN1_VEL";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }

    for(let valor of a.turn2_vel_array) {
      let aRaw = new ArregloRAW();
      aRaw.name = "TURN2_VEL";
      aRaw.value = valor;
      an.arreglos.push(aRaw);
    }
    return an;
  }
  
}


/**
 * El resultado del algoritmo ejecutado con los datos de la muestra
 * Estos datos son los que se usaran para generar el reporte.
 * */
export class AnalisisUPGO {
  analysisMode: number;

  idPaciente: number;
  fecha: Date;

  t: number = 0;
  rof: string = "";

  //Datos con respecto al tiempo para el gráfico
  //rotaciones: number[] = [];
  //inclinaciones: number[] = [];
  //tiempos: number[] = [];

  rotacionesMulti: number[][] = [];
  inclinacionesMulti: number[][] = [];
  tiemposMulti: number[] = [];

  //eventos: number[] = [];
  eventosMulti: number[][] = [];

  //Duracion por etapas
  t1: number = 0;
  t2: number = 0;
  t3: number = 0;
  t4: number = 0;
  t5: number = 0;
  t6: number = 0;

  //Indices de movilidad
  aceleracionAlLevantarse: number = 0;
  aceleracionAlSentarse: number = 0;
  velocidadAngularGiro3m: number = 0;
  velocidadAngularGiroSilla: number = 0;
  cadenciaIda: number = 0;
  cadenciaVuelta: number = 0;
  maxStandInclination: number = 0;
  maxSitInclination: number = 0;

  //Las recomendaciones
  //recomendaciones: string[] = [];
  rofGo: number = 0;
  rofReturn: number = 0;
  rofSit: number = 0;
  rofStand: number = 0;
  rofTurn1: number = 0;
  rofTurn2: number = 0;
}

/**
 * El resultado del algoritmo ejecutado con los datos de la muestra.
 * Estos datos son los que se usaran para generar el reporte.
 * */
export class Analisis10MTS {
  idPaciente: number;
  fecha: Date;

  analysisMode: number;

  FCpos_array: number[][] = [];
  ICpos_array: number[][] = [];
  //step_side_array: number[][] = [];
  FCside_array: number[][] = [];
  ICside_array: number[][] = [];

  //Solo para debug en grafico
  ax: number[] = [];
  ay: number[] = [];
  az: number[] = [];

  //Para los 3 graficos
  ax_array: number[][] = [];
  ay_array: number[][] = [];
  az_array: number[][] = [];

  tiemposMulti: number[] = [];

  v_gait: number = 0; //velocidad de marcha medido en metros por segundo
  gait_result: string;  //tipo de marcha

  //Indices generales
  swing_simetry: number; //balanceo
  stance_simetry: number; //apoyo
  gait_simetry: number; //marcha


  //Indices temporales
  l_steptime: number; dv_l_steptime: number; r_steptime: number; dv_r_steptime: number; //Paso
  l_cadence: number; dv_l_cadence: number; r_cadence: number; dv_r_cadence: number; //Cadencia
  l_stridetime: number; dv_l_stridetime: number; r_stridetime: number; dv_r_stridetime: number; //Zancada
  l_ssupporttime: number; dv_l_ssupporttime: number; r_ssupporttime: number; dv_r_ssupporttime: number; //Apoyo simple
  l_dsupporttime: number; dv_l_dsupporttime: number; r_dsupporttime: number; dv_r_dsupporttime: number; //Apoyo doble

  //Indices espaciales
  l_steplength: number; dv_l_steplength: number; r_steplength: number; dv_r_steplength: number; //largo
  l_stance: number; dv_l_stance: number; r_stance: number; dv_r_stance: number; //apoyo
  l_swing: number; dv_l_swing: number; r_swing: number; dv_r_swing: number; //balanceo

}

/**
 * Representacion de una prueba upgo desde el backend
 */
export class TestUpgoSesionModel {
  fecha: string;
  
  standAccMax:	number;
  sitAccMax:	number;
  cadenceGo:	number;
  cadenceReturn:	number;
  turn1Vel:	number;
  turn2Vel: number;
  maxStandInclination: number;
  maxSitInclination: number;

  events: Value2Model[] = [];

  analysisMode: number;
  inclinations: Value2Model[] = [];
  rotations: Value2Model[] = [];
  tiempo1: number;
  tiempo2: number;
  tiempo3: number;

  test:	number;
  stand:	number;
  go:	number;
  turn1:	number;
  back:	number;
  turn2:	number;
  sit:	number;
  rofStand:	number;
	rofGo:	number;
	rofReturn:	number;
	rofTurn1:	number;
	rofTurn2:	number;
  rofSit:	number;
  rof: string;
}

export class ValueModel {
  value: number
}

export class Value2Model {
  value: number;
  medicion: number;
}

export class UPGORawInput {
  raw_analisis: TestUpgoRawModel;
  raw_samples: TestUpgoRawModel[] = []
}

export class ArregloRAW {
  name: string;
  value: number;
}

export class TestUpgoRawModel {
  //sesionID: number; //ID de la sesion

  analysis_mode: number;
  arreglos: ArregloRAW[];
  back:	number;
  cadenceGo:	number;
  cadenceReturn:	number;
  eval_ok: number;
  go:	number;
  maxSitInclination:	number;
  maxStandInclination:	number;
  rof:	string;
  rofGo:	number;
  rofReturn:	number;
  rofSit:	number;
  rofStand:	number;
  rofTurn1:	number;
  rofTurn2:	number;
  sit:	number;
  sitAccMax:	number;
  stand:	number;
  standAccMax:	number;
  test:	number;
  test_number:	number;
  turn1:	number;
  turn2:	number;
  turn1Vel:	number;
  turn2Vel:	number;
}

/**
 * Estructura que representa el input del servicio REST usado para guardar los resultados de una prueba
 */
export class TestDiezSesionModel {
  analysisMode: number;

  fecha: string;
  dvLDsupporttime:	number;
  dvLSsupporttime:	number;
  //dvLSteplength:	number;
  dvLSteptime:	number;
  dvLStridetime:	number;
  dvRDsupporttime:	number;
  dvRSsupporttime:	number;
  //dvRSteplength:	number;
  dvRSteptime:	number;
  dvRStridetime:	number;
  gaitResult:	string;
  gaitSimetry:	number;
  lDsupporttime:	number;
  lSsupporttime:	number;
  //lSteplength:	number;
  lSteptime:	number;
  lStridetime:	number;

  lSteplength:	number;
  dvLSteplength:	number;
  rSteplength:	number;
  dvRSteplength:	number;

  lStance:	number;
  dvLStance:	number;
  rStance:	number;
  dvRStance:	number;

  lSwing:	number;
  dvLSwing:	number;
  rSwing:	number;
  dvRSwing:	number;

  lCadence:	number;
  dvLCadence:	number;
  rCadence:	number;
  dvRCadence:	number;

  tiempo1: number;
  tiempo2: number;
  tiempo3: number;

  //raw_analisis:	DIEZRawInput;
  //raw_samples:	DIEZRawInput[];

  eje_z_array: Value2Model[] = [];
  pos_array: Value2Model[] = [];
  side_array: Value2Model[] = [];

  rDsupporttime:	number;
  rSsupporttime:	number;
  //rSteplength:	number;
  rSteptime:	number;
  rStridetime:	number;
  stanceSimetry:	number;
  swingSimetry:	number;
  vGait:	number;
}

export class DIEZRawInput {
  dvLCadence:	number
  dvLDsupporttime:	number
  dvLSsupporttime:	number
  dvLStance:	number
  dvLSteplength:	number
  dvLSteptime:	number
  dvLStridetime:	number
  dvLSwing:	number
  dvRCadence:	number
  dvRDsupporttime:	number
  dvRSsupporttime:	number
  dvRStance:	number
  dvRSteplength:	number
  dvRSteptime:	number
  dvRStridetime:	number
  dvRSwing:	number
  gaitResult:	number
  gaitSimetry:	number
  lcadence:	number
  ldsupporttime:	number
  lssupporttime:	number
  lstance:	number
  lsteplength:	number
  lsteptime:	number
  lstridetime:	number
  lswing:	number
  rcadence:	number
  rdsupporttime:	number
  rssupporttime:	number
  rstance:	number
  rsteplength:	number
  rsteptime:	number
  rstridetime:	number
  rswing:	number
  stanceSimetry:	number
  swingSimetry:	number
  vgait:	number
}