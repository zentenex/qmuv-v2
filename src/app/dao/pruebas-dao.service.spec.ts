import { TestBed } from '@angular/core/testing';

import { PruebasDAOService } from './pruebas-dao.service';

describe('PruebasDAOService', () => {
  let service: PruebasDAOService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PruebasDAOService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
