import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigDaoService {

  constructor(private storage: Storage) { }

  /**
   * Define que la prueba ya paso el onboarding y no debe ser mostrado denuevo
   * @param nombrePrueba Nombre de la prueba que se salta el onboarding
   */
  saltaElOnboarding(idPrueba: number) {
    let codigoPrueba = environment.pruebas[idPrueba].codigo;
    console.log("Codigo de la prueba ["+codigoPrueba+"]")
    this.storage.set("ONBOARD_" + codigoPrueba, false);
  }

  /**
   * Resetea los onboarding
   */
  reestableceOnboarding() {
    for(let p of environment.pruebas) {
      this.storage.remove("ONBOARD_" + p.codigo);
    }
  }

  /**
   * Pregunta si una prueba tiene que mostrar el landing
   * @param nombrePrueba Nombre de la prueba que se salta el landing
   */
  async muestroOnboarding(idPrueba: number) {
    let codigoPrueba = environment.pruebas[idPrueba].codigo;
    console.log("Codigo de la prueba ["+codigoPrueba+"]");
    let val : boolean = await this.storage.get("ONBOARD_" + codigoPrueba);
    return (val == null) ? true : val;
  }

  async idioma() {
    let val : string = await this.storage.get("IDIOMA");
    return val;
  }

  /**
   * Retorna la cantidad de mediciones por sesion que se deben realizar para los test de UPGO
   */
  async cantidadMedicionesPorSesion(idPrueba: number) {
    let codigoPrueba = environment.pruebas[idPrueba].codigo;
    let cantidad: number = await this.storage.get("MEDICIONES_X_SESION_" + codigoPrueba);
    if(cantidad == null) {
      //Valores de fabrica
      cantidad = environment.pruebas[idPrueba].med_x_ses;
    }
    return cantidad;
  }

  async modoDeAnalisis(idPrueba: number) {
    let codigoPrueba = environment.pruebas[idPrueba].codigo;
    let modo: number = await this.storage.get("ANALISIS_MODO_" + codigoPrueba);
    if(modo == null) {
      //Valores de fabrica
      modo = environment.pruebas[idPrueba].analysis_mode;
    }
    return modo;
  }

  /**
   * Retorna la versión del firmware que el usuario selecciono en la pantalla de configuracion.
   * Si no hay nada, entonces se retorna el valor por defecto
   * @returns texto con la version del firmware
   */
  async versionFirmware() {
    let fw: string = await this.storage.get("FIRMWARE");
    if(fw == null) {
      //Valores de fabrica
      fw = environment.firmware;
    }
    return fw;
  }

    /**
   * Retorna la versión del firmware que el usuario selecciono en la pantalla de configuracion.
   * Si no hay nada, entonces se retorna el valor por defecto
   * @returns texto con la version del firmware
   */
     async nombreUsuarioRegistrado() {
      let fw: string = await this.storage.get("NOMBRE_USUARIO_REG");
      if(fw == null) {
        //Valores de fabrica
        fw = "Sin nombre";
      }
      return fw;
    }

  async scanAllBT() {
    let resp = true;
    let fw: string = await this.storage.get("SCAN_ALL_BT");
    if(fw == null) {
      console.log("SCAN_ALL_BT: valores de fabrica: " + environment.scanAll);
      //Valores de fabrica
      resp = environment.scanAll;
    } else if(fw === "NO") {
      resp = false;
    }
    console.log("SCAN_ALL_BT: "+ resp);
    return resp;
  }



  seteaModoAnalisis(idPrueba: number, modo: number) {
    let codigoPrueba = environment.pruebas[idPrueba].codigo;
    this.storage.set("ANALISIS_MODO_" + codigoPrueba, modo);
  }

  seteaMedicionesPorSesion(idPrueba: number, mediciones: number) {
    let codigoPrueba = environment.pruebas[idPrueba].codigo;
    this.storage.set("MEDICIONES_X_SESION_" + codigoPrueba, mediciones);
  }

  seteaVersionFirmware(version: string) {
    this.storage.set("FIRMWARE", version);
  }

  seteaNombreUsuarioRegistrado(nombre: string) {
    this.storage.set("NOMBRE_USUARIO_REG", nombre);
  }

  seteaScanAllBT(valor: boolean) {
    console.log("Guardo SCAN_ALL_BT " + valor);
    if(valor) {
      this.storage.set("SCAN_ALL_BT", "SI");
    } else {
      this.storage.set("SCAN_ALL_BT", "NO");
    }
  }

  seteaIdioma(valor: string) {
    console.log("Guardo IDIOMA " + valor);
    this.storage.set("IDIOMA", valor);
  }
}
