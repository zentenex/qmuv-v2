import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OpenDaoService {

  private baseURI = '/api/open';

  constructor(private http: HttpClient) { }

  public async regiones() {
    const recurso = this.baseURI + '/zonas';
    console.log('REQUEST [GET]: ' + environment.endpointApi + recurso);
      let pacientes = await this.http.get<ZonaOutDTO[]>(environment.endpointApi + recurso, {}).toPromise();

      let regs: RegionDTO[] = [];

      for(let p of pacientes) {
        let p1 = new RegionDTO();
        p1.id = p.id;
        p1.nombre = p.name;
        regs.push(p1);
      }
      return regs;
  }

  public async constantes() {
    const recurso = this.baseURI + '/constantes';
    console.log('REQUEST [GET]: ' + environment.endpointApi + recurso);
      let lasConstantes = await this.http.get<ConstanteDTO[]>(environment.endpointApi + recurso, {}).toPromise();

      return lasConstantes;
  }
}

export class RegionDTO {
  public id: number;
  public nombre: string;
}

export class ZonaOutDTO {
  public id: number;
  public name: string;
}

export class ConstanteDTO {
  public id: string;
  public value: string;
}