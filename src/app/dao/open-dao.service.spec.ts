import { TestBed } from '@angular/core/testing';

import { OpenDaoService } from './open-dao.service';

describe('OpenDaoService', () => {
  let service: OpenDaoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OpenDaoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
