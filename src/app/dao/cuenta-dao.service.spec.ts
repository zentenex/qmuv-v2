import { TestBed } from '@angular/core/testing';

import { CuentaDAOService } from './cuenta-dao.service';

describe('CuentaDAOService', () => {
  let service: CuentaDAOService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CuentaDAOService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
