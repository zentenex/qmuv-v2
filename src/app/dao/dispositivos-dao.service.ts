import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class DispositivosDAOService {

  private _currentDevice: DispositivoDTO;

  constructor(private storage: Storage) { }

  /**
   * Guarda en el almacenamiento local del celular, el dispositivo bluetooth actual
   * @param dispositivo Dispositivo a recordar
   */
  setCurrentDevice (dispositivo: DispositivoDTO) {
    this._currentDevice = dispositivo;
    this.storage.set('dispositivo-default', dispositivo);
  }

  /**
   * Recupero el dispositivo bluetooth actual, desde el almacenamiento local del celular
   * @returns El dispositivo bluetooth
   */
  async getCurrentDevice() : Promise<DispositivoDTO>{
    if(this._currentDevice) {
      return this._currentDevice;
    } else {
      let resp: DispositivoDTO = await this.storage.get('dispositivo-default');
      this._currentDevice = resp;
      return resp;
    }
  }
}

/**
 * Representa un Dispositivo Bluetooth.
 * 
 * Incluye su UUID, nombre, RSSI, timestamp de la ultima vez visto y la fuerza de la senal
 */
export class DispositivoDTO {
  public id: string;
  public nombre: string;
  public rssi: number;
  public visto: number; //milisegundo ultima vez visto

  constructor(readonly device: any) {
    this.id = device.id;
    this.nombre = device.name;
    this.rssi = device.rssi;
    this.visto = Date.now();
  }

  get fuerza(): number {
    //return (this.rssi + 80) / 80;
    let min = -80;
    let max = -50
    return (this.rssi - min) / (max - min);
  }
}
