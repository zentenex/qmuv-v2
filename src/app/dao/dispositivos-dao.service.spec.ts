import { TestBed } from '@angular/core/testing';

import { DispositivosDAOService } from './dispositivos-dao.service';

describe('DispositivosDAOService', () => {
  let service: DispositivosDAOService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DispositivosDAOService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
