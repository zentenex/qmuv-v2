import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CuentaDAOService {

  private baseURI = '/api/app/usuario';
  constructor(private http: HttpClient) { }

  async registraUsuario(nuevoUsuario: UsuarioCreaRequest) {
    if (environment.server) {
      const recurso = this.baseURI + '/crea';
      console.log('REQUEST [POST]: ' + environment.endpointApi + recurso + " con: " + JSON.stringify(nuevoUsuario));
      return await this.http.post<ControlDTO>(environment.endpointApi + recurso, nuevoUsuario).toPromise();
    } else {
      let ctrl = new ControlDTO();
      ctrl.code = 1;
      ctrl.message = "servidor dummy"
      return ctrl;
    }

  }

  async actualizaUsuario(nuevoUsuario: UsuarioActualizaRequest) {
    if (environment.server) {
      const recurso = this.baseURI + '/actualiza';
      console.log('REQUEST [PATCH]: ' + environment.endpointApi + recurso + " con: " + JSON.stringify(nuevoUsuario));
      return await this.http.patch<ControlDTO>(environment.endpointApi + recurso, nuevoUsuario).toPromise();
    } else {
      let ctrl = new ControlDTO();
      ctrl.code = 1;
      ctrl.message = "servidor dummy"
      return ctrl;
    }

  }

  async obtieneDetalleUsuario() {
    if (environment.server) {
      const recurso = this.baseURI + '/encuentra';
      console.log('REQUEST [GET]: ' + environment.endpointApi + recurso);

      console.log("llegue a los datos en linea")
      return await this.http.get<FirstUserOutput>(environment.endpointApi + recurso).toPromise();


    } else {
      console.log("llegue a los datos en linea")
      let fuo = new FirstUserOutput();
      fuo.code = 1;
      fuo.message = "dummy"
      fuo.user = new UsuarioDTO;
      fuo.user.id = 1;
      fuo.user.name = "Juan Pedro";
      fuo.user.firebase_id = "fb1";
      fuo.user.email = "juan@dummy.cl";
      fuo.user.plan_id = 1;
      fuo.user.plan_name = "Plan dummy"
      fuo.user.plan_code = "B"
      return fuo;
    }

    /*
        let usr = new UsuarioDTO();
        usr.ID = "acbdef123456";
        usr.nombre = "Juan Paco Pedro";
        usr.correo = "juan@delamar.cl";
        return usr;
        */
  }
}

export class UsuarioDTO {
  public id: number;
  public name: string;
  public email: string;
  public firebase_id: string; //ID Firebase
  public plan_id: number;
  public plan_code: string;
  public plan_name: string;
  public admin: boolean;
  public account_status: number;
  public account_id: number;
  public birthday: string;
  public gender: number;
  public profession: string;
  public region: string;
  public zoneId: number;
  public address: string;
  public telephone: string;
}

export class UsuarioActualizaRequest {
  public name: string;
  public birthday: string;
  public gender: number;
  public profession: string;
  public region: string;
  public zoneId: number;
  public address: string;
  public telephone: string;
}

export class UsuarioCreaRequest {
  public name: string;
  public email: string;
}

export class ControlDTO {
  public code: number;
  public message: string;
}

export class PlanDTO {
  public name: string;
  public code: string;
  public description: string;
}

export class CuentaDTO {
  public plan: PlanDTO;
  public planID: number;
}

export class FirstUserOutput {
  public code: number;
  public message: string;
  public user: UsuarioDTO;
  public account_others: CuentaOthDTO[];
}

export class CuentaOthDTO {
  public ID: number
  public Plan: PlanDTO;
  public PlanID: number;
}