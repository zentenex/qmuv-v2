import { TestBed } from '@angular/core/testing';

import { PacientesDAOService } from './pacientes-dao.service';

describe('PacientesDAOService', () => {
  let service: PacientesDAOService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PacientesDAOService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
