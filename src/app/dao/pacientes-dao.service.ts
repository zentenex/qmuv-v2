import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TranslateConfigService } from '../services/translate-config.service';
import { ControlDTO } from './cuenta-dao.service';

@Injectable({
  providedIn: 'root'
})
export class PacientesDAOService {

  private baseURI = '/api/app/paciente';

  /**
   * Todos los pacientes asociados al usuario
   */
  private todosMisPacientes: Array<PacienteDTO> = [];

  constructor(private http: HttpClient, private translateConfigService: TranslateConfigService) {
  }

  /**
   * Obtiene todos los pacientes eliminados que cumplen con el filtro de nombre pasado por parametro
   * @param searchTerm Nombre del paciente
   * @returns Listado de pacientes eliminados que cumplen el filtro del nombre
   */
  public async pacientesEliminados(searchTerm: string) {
    //Obtenemos los pacientes desde el servidor
    await this.obtieneTodosLosPacientes();

    //Retornamos todos los que cumplan el filtro de nombre y esten marcados como eliminados
    return this.todosMisPacientes.filter(item => {
      return (item.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) && item.eliminado;
    });

  }

  /**
   * Obtiene todos los pacientes activos que cumplen con el filtro de nombre pasado por parametro
   * @param searchTerm Nombre del paciente
   * @returns Listado de pacientes activos que cumplen el filtro del nombre
   */
  public async pacientesActivos(searchTerm: string) {
    //Obtenemos los pacientes desde el servidor
    await this.obtieneTodosLosPacientes();

    //Retornamos todos los que cumplan el filtro de nombre y no esten marcados como eliminados
    return this.todosMisPacientes.filter(item => {
      return (item.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) && !item.eliminado;
    });
  }

  public async pacienteBusca(id: number) {
    //if(environment.server) {
    //TODO Ir al servidor a buscar al paciente
    //return null;
    //} else {
    for (let p of this.todosMisPacientes) {
      if (p.id == id) {
        return p;
      }
    }
    //}
    return null;
  }

  /**
   * Crea un nuevo paciente en el servidor asociado al usuario logueado
   * @param nuevo El nuevo paciente a crear
   * @returns verdadero en caso de exito
   */
  public async pacienteCrea(nuevo: PacienteDTO) {
    if (environment.server) {

      let pacIn = new PacienteInDTO();
      pacIn.address = nuevo.direccion;
      pacIn.birthday = nuevo.fechaNacimiento;
      pacIn.email = nuevo.correo;
      pacIn.garbage = false;
      pacIn.gender = nuevo.genero;
      pacIn.height = nuevo.altura;
      pacIn.name = nuevo.nombre;
      pacIn.telephone = nuevo.telefono;
      pacIn.weight = nuevo.peso;
      pacIn.region = nuevo.region;
      pacIn.zoneId = +nuevo.zoneId;
      pacIn.obs = nuevo.obs;

      const recurso = this.baseURI + '/crea';
      console.log('REQUEST [POST]: ' + environment.endpointApi + recurso + " con: " + JSON.stringify(pacIn));
      let ctrl = await this.http.post<ControlDTO>(environment.endpointApi + recurso, pacIn).toPromise();
      if (ctrl.code >= 0) {
        nuevo.id = ctrl.code;
        this.todosMisPacientes.push(nuevo);
        return true;
      }
      return false;
    } else {
      nuevo.id = this.todosMisPacientes.length + 1;
      this.todosMisPacientes.push(nuevo);
      return true;
    }
  }

  public async pacienteReporta(repo: ReporteDTO) {


    repo.language = this.translateConfigService.getCurrentLanguage();
    console.log("Elrepo", repo);

    if (environment.server) {


      const recurso = this.baseURI + '/reporte';
      console.log('REQUEST [POST]: ' + environment.endpointApi + recurso + " con: " + JSON.stringify(repo));
      let ctrl = await this.http.post<ControlDTO>(environment.endpointApi + recurso, repo).toPromise();
      return ctrl.code;
    } else {
      return 0;
    }
  }

  public async pacienteBorra(p: PacienteDTO) {
    if (environment.server) {

      const recurso = this.baseURI + '/' + p.id + '/papelera';
      console.log('REQUEST [PATCH]: ' + environment.endpointApi + recurso);
      let ctrl = await this.http.patch<ControlDTO>(environment.endpointApi + recurso, {}).toPromise();
      if (ctrl.code >= 0) {
        let idx = this.todosMisPacientes.indexOf(p);
        if (idx >= 0) {
          this.todosMisPacientes[idx].eliminado = true;
          return true;
        }
      }
      return false;
    } else {
      let idx = this.todosMisPacientes.indexOf(p);
      if (idx >= 0) {
        this.todosMisPacientes[idx].eliminado = true;
        return true;
      }
    }
    return false;
  }

  public async pacienteRestaura(p: PacienteDTO) {
    if (environment.server) {
      const recurso = this.baseURI + '/' + p.id + '/papelera';
      console.log('REQUEST [PATCH]: ' + environment.endpointApi + recurso);
      let ctrl = await this.http.patch<ControlDTO>(environment.endpointApi + recurso, {}).toPromise();
      if (ctrl.code >= 0) {
        let idx = this.todosMisPacientes.indexOf(p);
        if (idx >= 0) {
          this.todosMisPacientes[idx].eliminado = false;
          return true;
        }
      }
      return false;
    } else {
      let idx = this.todosMisPacientes.indexOf(p);
      if (idx >= 0) {
        this.todosMisPacientes[idx].eliminado = false;
        return true;
      }
    }
    return false;
  }

  public async pacientePurga(p: PacienteDTO) {
    if (environment.server) {
      const recurso = this.baseURI + '/' + p.id + '/purga';
      console.log('REQUEST [DELETE]: ' + environment.endpointApi + recurso);
      let ctrl = await this.http.delete<ControlDTO>(environment.endpointApi + recurso).toPromise();
      if (ctrl.code >= 0) {
        let idx = this.todosMisPacientes.indexOf(p);
        if (idx >= 0) {
          this.todosMisPacientes.splice(idx, 1);
          return true;
        }
      }
      return false;
    } else {
      let idx = this.todosMisPacientes.indexOf(p);
      if (idx >= 0) {
        this.todosMisPacientes.splice(idx, 1);
        return true;
      }
    }
    return false;
  }

  public async pacienteActualiza(pac: PacienteDTO) {
    if (environment.server) {
      let pacIn = new PacienteInDTO();
      pacIn.address = pac.direccion;
      pacIn.birthday = pac.fechaNacimiento;
      pacIn.email = pac.correo;
      pacIn.garbage = pac.eliminado;
      pacIn.gender = pac.genero;
      pacIn.height = pac.altura;
      pacIn.name = pac.nombre;
      pacIn.telephone = pac.telefono;
      pacIn.weight = pac.peso;
      pacIn.region = pac.region;
      pacIn.zoneId = +pac.zoneId;
      pacIn.obs = pac.obs;

      const recurso = this.baseURI + '/' + pac.id + '/actualiza';
      console.log('REQUEST [PUT]: ' + environment.endpointApi + recurso + " con: " + JSON.stringify(pacIn));
      let ctrl = await this.http.put<ControlDTO>(environment.endpointApi + recurso, pacIn).toPromise();
      if (ctrl.code >= 0) {
        for (let p of this.todosMisPacientes) {
          if (p.id == pac.id) {
            p.nombre = pac.nombre;
            p.zoneId = pac.zoneId;
            p.region = pac.region;
            p.telefono = pac.telefono;
            p.correo = pac.correo;
            p.direccion = pac.direccion;
            p.fechaNacimiento = pac.fechaNacimiento;
            p.altura = pac.altura;
            p.genero = pac.genero;
            p.peso = pac.peso;
          }
        }
        return true;
      }
    } else {
      for (let p of this.todosMisPacientes) {
        if (p.id == pac.id) {
          p.nombre = pac.nombre;
          p.zoneId = pac.zoneId;
          p.region = pac.region;
          p.telefono = pac.telefono;
          p.correo = pac.correo;
          p.direccion = pac.direccion;
          p.fechaNacimiento = pac.fechaNacimiento;
          p.altura = pac.altura;
          p.genero = pac.genero;
          p.peso = pac.peso;
        }
      }
      return true;
    }
  }

  /**
   * Obtiene todos los pacientes del usuario logueado desde el servidor
   */
  private async obtieneTodosLosPacientes() {
    if (environment.server) {
      const recurso = this.baseURI + '/lista';
      console.log('REQUEST [GET]: ' + environment.endpointApi + recurso);
      let pacientes = await this.http.get<PacienteOutDTO[]>(environment.endpointApi + recurso, {}).toPromise();
      this.todosMisPacientes = [];
      for (let p of pacientes) {
        let p1 = new PacienteDTO();
        p1.id = p.id;
        p1.nombre = p.name;
        p1.fechaNacimiento = p.birthday;
        p1.direccion = p.address;
        p1.zoneId = p.zoneId;
        p1.region = p.region;
        p1.telefono = p.telephone;
        p1.correo = p.email;
        p1.eliminado = p.garbage;
        p1.altura = p.height;
        p1.genero = p.gender;
        p1.peso = p.weight;
        p1.obs = p.obs;
        this.todosMisPacientes.push(p1);
      }
    } else {
      this.todosMisPacientes = [];
      {
        let p1 = new PacienteDTO();
        p1.id = 1;
        p1.nombre = "Amy Winehouse";
        p1.fechaNacimiento = "1990-01-02";
        p1.direccion = "Avenida siempre viva 123";
        p1.zoneId = 1;
        p1.telefono = "(555) 55-5555"
        p1.correo = "amy@qmuv.cl";
        p1.eliminado = true;
        p1.altura = 165;
        p1.genero = 1;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 2;
        p1.nombre = "André Rieu";
        p1.fechaNacimiento = "1950-03-04";
        p1.direccion = "Avenida Strauss 123";
        p1.zoneId = 1;
        p1.telefono = "(555) 66-6666"
        p1.correo = "andre@qmuv.cl";
        p1.eliminado = false;
        p1.altura = 175;
        p1.genero = 0;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 3;
        p1.nombre = "Anneke Van Giersbergen";
        p1.fechaNacimiento = "1980-05-06";
        p1.direccion = "Avenida Gathering 123";
        p1.zoneId = 2;
        p1.telefono = "(555) 77-7777"
        p1.correo = "anneke@qmuv.cl";
        p1.eliminado = false;
        p1.altura = 165;
        p1.genero = 1;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 13;
        p1.nombre = "Annie Lennox";
        p1.fechaNacimiento = "1960-07-08";
        p1.direccion = "Avenida No more 123";
        p1.zoneId = 2;
        p1.telefono = "(555) 88-8888"
        p1.correo = "annie@qmuv.cl";
        p1.eliminado = false;
        p1.altura = 165;
        p1.genero = 1;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 4;
        p1.nombre = "Björk Guðmundsdóttir";
        p1.fechaNacimiento = "1970-07-08";
        p1.direccion = "Avenida Army 123";
        p1.zoneId = 3;
        p1.telefono = "(555) 99-9999"
        p1.correo = "bjork@qmuv.cl";
        p1.eliminado = false;
        p1.altura = 165;
        p1.genero = 1;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 14;
        p1.nombre = "Bob Marley";
        p1.fechaNacimiento = "1940-09-10";
        p1.direccion = "Avenida Sheriff 123";
        p1.zoneId = 4;
        p1.telefono = "(555) 11-1111"
        p1.correo = "bob@qmuv.cl";
        p1.eliminado = true;
        p1.altura = 175;
        p1.genero = 0;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 5;
        p1.nombre = "Carole King";
        p1.fechaNacimiento = "1930-09-10";
        p1.direccion = "Avenida Yonder 123";
        p1.zoneId = 5;
        p1.telefono = "(555) 22-2222"
        p1.correo = "carole@qmuv.cl";
        p1.eliminado = true;
        p1.altura = 165;
        p1.genero = 1;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 6;
        p1.nombre = "David Bowie";
        p1.fechaNacimiento = "1940-10-11";
        p1.direccion = "Avenida Starman 123";
        p1.zoneId = 6;
        p1.telefono = "(555) 33-3333"
        p1.correo = "david@qmuv.cl";
        p1.eliminado = true;
        p1.altura = 175;
        p1.genero = 0;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 7;
        p1.nombre = "Lionel Richie";
        p1.fechaNacimiento = "1940-04-11";
        p1.direccion = "Avenida Hello 123";
        p1.zoneId = 6;
        p1.telefono = "(555) 45-6789"
        p1.correo = "lionel@qmuv.cl";
        p1.eliminado = true;
        p1.altura = 175;
        p1.genero = 0;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 8;
        p1.nombre = "Elton John";
        p1.fechaNacimiento = "1930-11-12";
        p1.direccion = "Avenida Nikkita 123";
        p1.zoneId = 7;
        p1.telefono = "(555) 44-4444"
        p1.correo = "elton@qmuv.cl";
        p1.eliminado = false;
        p1.altura = 175;
        p1.genero = 0;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 9;
        p1.nombre = "Eric Clapton";
        p1.fechaNacimiento = "1940-10-14";
        p1.direccion = "Avenida Layla 123";
        p1.zoneId = 7;
        p1.telefono = "(555) 23-4567"
        p1.correo = "eric@qmuv.cl";
        p1.eliminado = false;
        p1.altura = 175;
        p1.genero = 0;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 10;
        p1.nombre = "Emilie Autumn";
        p1.fechaNacimiento = "1990-12-13";
        p1.direccion = "Avenida Girl 123";
        p1.zoneId = 8;
        p1.telefono = "(555) 12-3456"
        p1.correo = "emilie@qmuv.cl";
        p1.eliminado = false;
        p1.altura = 165;
        p1.genero = 1;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 11;
        p1.nombre = "Freddie Mercury";
        p1.fechaNacimiento = "1940-10-15";
        p1.direccion = "Avenida Bohemian 123";
        p1.zoneId = 8;
        p1.telefono = "(555) 98-7654"
        p1.correo = "freddie@qmuv.cl";
        p1.eliminado = true;
        p1.altura = 175;
        p1.genero = 0;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 12;
        p1.nombre = "King Diamond";
        p1.fechaNacimiento = "1930-8-15";
        p1.direccion = "Avenida Abigail 123";
        p1.zoneId = 9;
        p1.telefono = "(555) 76-6665"
        p1.correo = "king@qmuv.cl";
        p1.eliminado = false;
        p1.altura = 175;
        p1.genero = 0;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 15;
        p1.nombre = "John Lennon";
        p1.fechaNacimiento = "1920-8-15";
        p1.direccion = "Avenida Yesterday 123";
        p1.zoneId = 9;
        p1.telefono = "(555) 12-1212"
        p1.correo = "john@qmuv.cl";
        p1.eliminado = true;
        p1.altura = 175;
        p1.genero = 0;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }

      {
        let p1 = new PacienteDTO();
        p1.id = 16;
        p1.nombre = "Neil Diamond";
        p1.fechaNacimiento = "1910-4-19";
        p1.direccion = "Avenida Wine 123";
        p1.zoneId = 9;
        p1.telefono = "(555) 21-2112"
        p1.correo = "neil@qmuv.cl";
        p1.eliminado = true;
        p1.altura = 175;
        p1.genero = 0;
        p1.peso = 60;
        p1.obs = "";
        this.todosMisPacientes.push(p1);
      }
    }
  }
}

export class PacienteDTO {
  public id: number;
  public nombre: string;
  public fechaNacimiento: string;
  public direccion: string;
  public region: string;
  public zoneId: number;
  public telefono: string;
  public correo: string;
  public eliminado: boolean;
  public genero: number; //0 = hombre, 1 = mujer
  public altura: number; //altura en centimetros
  public peso: number;
  public obs: string;
}

export class PacienteInDTO {
  public address: string;
  public birthday: string;
  public email: string;
  public garbage: boolean;
  public gender: number;
  public height: number;
  public name: string;
  public telephone: string;
  public weight: number;
  public zoneId: number;
  public region : string;
  public obs: string;
}

export class PacienteOutDTO extends PacienteInDTO {
  public id: number;
}

export class ReporteDTO {
  public patient_id: number;
  public recipient_name: string;
  public recipient_email: string;
  public reports_upgo: string[];
  public reports_10mts: string[];
  public gfx_his_upgo: string;
  public gfx_his_10mts: string;
  public language: string;
}