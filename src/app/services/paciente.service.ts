import { Injectable } from '@angular/core';
import { PacienteDTO, PacientesDAOService } from '../dao/pacientes-dao.service';
import { Subject, Observable } from 'rxjs';
import { PruebasDAOService, AnalisisUPGO, Analisis10MTS } from '../dao/pruebas-dao.service';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  //Observador de pacientes nuevos
  private _pacienteNuevo$ = new Subject<PacienteDTO>();

  /**
   * Retorna el observador de paciente creado
   * */
  get observadorPacientesNuevos(): Observable<PacienteDTO> {
    return this._pacienteNuevo$.asObservable();
  }

  constructor(private pacientesDAO: PacientesDAOService, private pruebasDAOService: PruebasDAOService) { }

  /**
   * Envia a guardar un nuevo paciente. Si es exitoso, notifica que se ha creado un nuevo paciente
   * @param pac Paciente
   * @returns 
   */
  async guarda(pac: PacienteDTO) {
    let exito = await this.pacientesDAO.pacienteCrea(pac);
    if(exito) {
      this._pacienteNuevo$.next(pac);
    }
    return exito;
  }

  /**
   * Retorna los pacientes activos, que cumplen con el filtro de nombre, ordenados por nombre
   * @param searchTerm nombre del paciente
   * @returns Listado de pacientes
   */
  public async pacientesActivos(searchTerm: string) {
    let pacientes = await this.pacientesDAO.pacientesActivos(searchTerm);
    if(pacientes) {
      pacientes.sort(this.compara);
      return pacientes;
    } else {
      return [];
    }
  }

  public async agregaAlHistorialDePruebas(ana: any, ses: any) {
    if(ana instanceof AnalisisUPGO) {
      //Lo guardo en el historial de pruebas upgo del usuario
      await this.pruebasDAOService.savePruebaUPGO(ana, ses);
      return "";
    } else if(ana instanceof Analisis10MTS) {
      //Lo guardo en el historial de pruebas 10mts del usuario
      await this.pruebasDAOService.savePrueba10mts(ana);
      return "";
    } else {
      let msg = "No se pudo identificar el tipo de prueba";
      console.error(msg);
      return msg;
    }
  }

  private compara(a: PacienteDTO, b: PacienteDTO) {
    // Use toUpperCase() to ignore character casing
    const bandA = a.nombre.toUpperCase();
    const bandB = b.nombre.toUpperCase();
  
    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison;
  }
}
