import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AlertController, NavController } from '@ionic/angular';
import { UtilsService } from './utils.service';
import { TranslateConfigService } from './translate-config.service';
import { ConfigDaoService } from '../dao/config-dao.service';
import { Auth, createUserWithEmailAndPassword, sendEmailVerification, sendPasswordResetEmail, signInWithEmailAndPassword } from '@angular/fire/auth';
import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  //private userAuthObs: Observable<any>;
  private userDetails = null;

  //Email y nombre del usuario recien ingresado en el registro
  //public registroEmail: string;
  //public registroNombre: string;

  constructor(
    private afAuth: Auth,
    private navCtrl: NavController,
    private splashScreen: SplashScreen,
    private alertCtrl: AlertController,
    private translateConfigService: TranslateConfigService,
    private configDAO: ConfigDaoService) {

    //Observable del estado de logueo del usuario
    //this.userAuthObs = afAuth.authState;
    this.atentoALogueos();
  }

  /**
   * Retorno el token del usuario o nulo si no hay usuario logueado.
   * No sirve al cargar recien la app, para eso hay que usar el onAuthStateChanged
   */
  async getAccessToken() {
    let yo = this.afAuth.currentUser;
    if(yo) {
      return yo.getIdToken();
    } else {
      return null;
    }
    
  }


  private atentoALogueos() {

    //Nos subscribimos a cambios de estado en la autenticacion del usuario
    this.afAuth.onAuthStateChanged((user) => {
      if (user) {
        //hay informacion de usuario, por ende me acabo de loguear
        if(user.emailVerified) {
          this.userDetails = user;
          console.log("Usuarius Logueadus (mail validated) : " + JSON.stringify(this.userDetails));
          this.navCtrl.navigateRoot('/home');
          //this.router.navigate(['/home']);
        } else {
          console.log("Usuarius Logueadus pero sin validar mail, lo Deslogeamus");
          this.alerta("Valide su cuenta", "Un link de activación llegará a su casilla de correo " + user.email + ". Debe validar su cuenta antes de poder ingresar");
          sendEmailVerification(user).finally(() => {
            this.afAuth.signOut();
          })
          
        }
      } else {
        //no hay informacion de usuario, por ende me acabo de desloguear
        this.userDetails = null;
        console.log("Usuarius Deslogueadus");
        this.splashScreen.hide();
        this.navCtrl.navigateRoot('/');
        //this.router.navigate(['/login']);
      }
    } )

  }

  registerUser(value) {
    return new Promise<any>((resolve, reject) => {

      //Guardamos el nombre y el email para la creacion de la cuenta mas tarde
      //this.registroEmail = value.email;
      //this.registroNombre = value.nombre;

      this.configDAO.seteaNombreUsuarioRegistrado(value.nombre);

      createUserWithEmailAndPassword(this.afAuth, value.email, value.password)
        .then(
          res => resolve(res),
          err => reject(err))
    })
  }
/*
  observaUserAuth() {
    return this.userAuthObs;
  }
*/
  loginUser(value) {
    return new Promise<any>((resolve, reject) => {
      signInWithEmailAndPassword(this.afAuth, value.email, value.password)
        .then(
          res => resolve(res),
          err => reject(err))
    })
  }

  logoutUser() {
    return new Promise<void>((resolve, reject) => {
      if (this.afAuth.currentUser) {
        this.afAuth.signOut()
          .then(() => {
            console.log("LOG Out");
            resolve();
          }).catch((error) => {
            reject();
          });
      }
    })
  }

  isLoggedIn() {
    if (this.userDetails == null ) {
      console.log("Estoy logueado... nop");
        return false;
      } else {
        console.log("Estoy logueado... yep");
        return true;
      }
    }

  logedInUserDetails() {
    //return this.afAuth.user
    return this.userDetails;
  }

  async olvidePassword(email: string): Promise<string> {
    //const urlReset = environment.endpointApi + "/reset/" + email;
    //var actionCodeSettings = {url: urlReset};
    try {
      await sendPasswordResetEmail(this.afAuth, email);//, actionCodeSettings);
      return "OK";
    } catch(error) {
      console.error(JSON.stringify(error));
      if(error.code == 'auth/user-not-found') {
        //No existe el usuario en firebase
        return "NOTFOUND";
      } else if(error.code == 'auth/invalid-email') {
        return "El correo electrónico es inválido";
      } else if(error.code == 'auth/argument-error') {
        return "Debe ingresar el correo electrónico con el que creó su cuenta";
      }
      console.error("["+error.code+"] ["+error.message+"]");
      return error.message;
    }
  }

  async alerta(headerR: string, messageR: string) {
    let ace = await this.translateConfigService.getTextoNow("COMUN.aceptar");
    const alert = await this.alertCtrl.create({
        header: headerR,
        message: messageR,
        buttons: [ace]
    });
    await alert.present();
  }

}
