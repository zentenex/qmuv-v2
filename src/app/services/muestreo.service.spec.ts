import { TestBed } from '@angular/core/testing';

import { MuestreoService } from './muestreo.service';

describe('MuestreoService', () => {
  let service: MuestreoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MuestreoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
