import { Injectable } from '@angular/core';
import { PacienteDTO } from 'src/app/dao/pacientes-dao.service';
import { Analisis10MTS } from 'src/app/dao/pruebas-dao.service';
import { SensorInnercialService } from '../sensor-innercial.service';
import { ConfigDaoService } from 'src/app/dao/config-dao.service';
import { environment } from 'src/environments/environment';
import { UtilsService } from '../utils.service';
import { GaitAnalysis } from 'src/app/business/T10M';
import { Muestreo } from '../muestreo.service';
import { Clipboard } from '@awesome-cordova-plugins/clipboard/ngx';

@Injectable({
  providedIn: 'root'
})
export class DiezMtrService {

    //public pruebas: Prueba10MTS[] = [];
    public sesionActual: Sesion10MTS;

  constructor(private sensorInnercialService: SensorInnercialService, private configDAO: ConfigDaoService, private clipboard: Clipboard, private utilsService: UtilsService) { }

  async sesionNueva() {
    console.log("Se crea nueva sesion");
    this.sesionActual = new Sesion10MTS();
    this.sesionActual.dirt = false;

    let a = new Algoritmo10MTS();
    a.analysis_mode = await this.configDAO.modoDeAnalisis(environment.PRUEBA_DIEZMTS_CODE);

    this.sesionActual.algoritmo = a;
  }

  sesionAddMedicion(muestra: Muestreo, paciente: PacienteDTO) {
    console.log("Agrega muestra a sesion");
    let t10m = this.sesionActual.algoritmo;
    if(this.sesionActual.dirt) {
      //No es la primera medicion
      t10m.other_measurement();
    }
    this.sesionActual.dirt = true;

    t10m.contador = muestra.c;

    t10m.ax = muestra.ax;
    t10m.ay = muestra.ay;
    t10m.az = muestra.az;

    t10m.gx = muestra.gx;
    t10m.gy = muestra.gy;
    t10m.gz = muestra.gz;

    t10m.qw = muestra.qw;
    t10m.qx = muestra.qx;
    t10m.qy = muestra.qy;
    t10m.qz = muestra.qz;

    t10m.gender = paciente.genero;
    t10m.height = paciente.altura / 100; //En metros

    console.log("acondicionamiento de las señales");
    t10m.conditioning(); //acondicionamiento de las señales

    console.log("búsqueda de los eventos de la marcha");
    t10m.step_search(); //búsqueda de los eventos de la marcha

    t10m.side_leg(); //pie derecho o izquierdo

    console.log("revisión de la prueba");
    t10m.test_ok(); //Revisión de la prueba

    if(t10m.eval_ok === 0) {
        
        //Repetir la prueba, muestra no puede ser usada para generar el reporte
        console.warn("Repetir la prueba, muestra no puede ser usada para generar el reporte");
        if(environment.debug) {
          this.utilsService.presentaToastDebug("Debug al portapaleles");
          this.clipboard.copy(JSON.stringify(t10m));
        }

        if(this.sensorInnercialService.virtual) {
          t10m = this.dummyT10m(t10m);
        } else {
          throw new Error("No se puede generar el reporte, Por favor, repita la medición");
        }
      } else {

        //Se calculan los indices
        t10m.gait_index();
        t10m.step_length();

        if(environment.debug) {
          this.utilsService.presentaToastDebug("Debug al portapaleles");
          this.clipboard.copy(JSON.stringify(t10m));
        }
        
      }

    this.sesionActual.muestras.push(muestra);
  }

  dummyT10m(t10m: Algoritmo10MTS): Algoritmo10MTS {
    t10m.v_gait = 0.8;
    t10m.gait_result = "Marcha dummy";

    t10m.swing_simetry = 66.1;
    t10m.stance_simetry = 33.1;
    t10m.gait_simetry = 45;

    t10m.l_steptime = 0.5;
    t10m.dv_l_steptime = 0.05;
    t10m.r_steptime = 0.56;
    t10m.dv_r_steptime = 0.08;
    t10m.l_stridetime = 33.1;
    t10m.dv_l_stridetime = 1.2;
    t10m.r_stridetime = 32.2;
    t10m.dv_r_stridetime = 1.5;
    t10m.l_ssupporttime = 45;
    t10m.dv_l_ssupporttime = 3;
    t10m.r_ssupporttime = 42;
    t10m.dv_r_ssupporttime = 5;
    t10m.l_dsupporttime = 45;
    t10m.dv_l_dsupporttime = 3;
    t10m.r_dsupporttime = 42;
    t10m.dv_r_dsupporttime = 5;

    t10m.l_steplength = 66.1;
    t10m.dv_l_steplength = 1.2;
    t10m.r_steplength = 65.2;
    t10m.dv_r_steplength = 1.5;
    
    return t10m;
  }

  sesionAnaliza(paciente: PacienteDTO) {
    console.log("Se analiza la sesion")
    this.sesionActual.paciente = paciente;

    let t10m = this.sesionActual.algoritmo;
    t10m.test_results(); //Resultado de sesion (cuando ya no quiero tomar mas muestras)

    if(environment.debug) {
      this.utilsService.presentaToastDebug("Debug copiado al portapaleles");
      this.clipboard.copy(JSON.stringify(t10m));
    }

    let analisis = new Analisis10MTS();
    this.sesionActual.analisis = analisis;

    analisis.analysisMode = t10m.analysis_mode;
    analisis.FCpos_array = t10m.FCpos_array;
    analisis.ICpos_array = t10m.ICpos_array;
    //analisis.step_side_array = t10m.step_side_array;
    analisis.FCside_array = t10m.FCside_array;
    analisis.ICside_array = t10m.ICside_array;

    console.log(t10m.v_gait); 
    console.log('\n'); 
    console.log(t10m.l_cadence);
    console.log(t10m.dv_l_cadence); 
    console.log('\n'); 
    console.log(t10m.r_cadence);
    console.log(t10m.dv_r_cadence); 
    console.log('\n'); 
    console.log(t10m.l_stridetime); 
    console.log(t10m.dv_l_stridetime); 
    console.log('\n'); 
    console.log(t10m.r_stridetime); 
    console.log(t10m.dv_r_stridetime); 
    console.log('\n'); 
    console.log(t10m.l_steptime); 
    console.log(t10m.dv_l_steptime); 
    console.log('\n');
    console.log(t10m.r_steptime); 
    console.log(t10m.dv_r_steptime); 
    console.log('\n');
    console.log(t10m.l_ssupporttime); 
    console.log(t10m.dv_l_ssupporttime); 
    console.log('\n'); 
    console.log(t10m.r_ssupporttime); 
    console.log(t10m.dv_r_ssupporttime); 
    console.log('\n'); 
    console.log(t10m.l_dsupporttime); 
    console.log(t10m.dv_l_dsupporttime);
    console.log('\n'); 
    console.log(t10m.r_dsupporttime);
    console.log(t10m.dv_r_dsupporttime); 
    console.log('\n');
    console.log(t10m.l_steplength); 
    console.log(t10m.dv_l_steplength); 
    console.log('\n'); 
    console.log(t10m.r_steplength); 
    console.log(t10m.dv_r_steplength); 
    console.log('\n'); 
    console.log(t10m.l_stance); 
    console.log(t10m.dv_l_stance); 
    console.log('\n'); 
    console.log(t10m.r_stance);
    console.log(t10m.dv_r_stance); 
    console.log('\n'); 
    console.log(t10m.l_swing);
    console.log(t10m.dv_l_swing); 
    console.log('\n'); 
    console.log(t10m.r_swing);
    console.log(t10m.dv_r_swing); 
    console.log('\n'); 
    console.log(t10m.stance_simetry);
    console.log(t10m.swing_simetry); 
    console.log(t10m.gait_simetry); 
    console.log('\n'); 
    console.log(t10m.gait_result); 
    console.log('\n');

    //Solo para debug en grafico
    analisis.ax = t10m.ax;
    analisis.ay = t10m.ay;
    analisis.az = t10m.az;

    analisis.ax_array = t10m.ax_array;
    analisis.ay_array = t10m.ay_array;
    analisis.az_array = t10m.az_array;

    //Tiempos de cada test
    analisis.tiemposMulti = t10m.v_gait_array;

    analisis.analysisMode = t10m.analysis_mode;

    analisis.v_gait = t10m.v_gait;
    analisis.gait_result = t10m.gait_result;

    analisis.swing_simetry = t10m.swing_simetry;
    analisis.stance_simetry = t10m.stance_simetry;
    analisis.gait_simetry = t10m.gait_simetry;

    analisis.l_cadence = t10m.l_cadence;
    analisis.dv_l_cadence = t10m.dv_l_cadence;
    analisis.r_cadence = t10m.r_cadence;
    analisis.dv_r_cadence = t10m.dv_r_cadence;
    analisis.l_steptime = t10m.l_steptime;
    analisis.dv_l_steptime = t10m.dv_l_steptime;
    analisis.r_steptime = t10m.r_steptime;
    analisis.dv_r_steptime = t10m.dv_r_steptime;
    analisis.l_stridetime = t10m.l_stridetime;
    analisis.dv_l_stridetime = t10m.dv_l_stridetime;
    analisis.r_stridetime = t10m.r_stridetime;
    analisis.dv_r_stridetime = t10m.dv_r_stridetime;
    analisis.l_ssupporttime = t10m.l_ssupporttime;
    analisis.dv_l_ssupporttime = t10m.dv_l_ssupporttime;
    analisis.r_ssupporttime = t10m.r_ssupporttime;
    analisis.dv_r_ssupporttime = t10m.dv_r_ssupporttime;
    analisis.l_dsupporttime = t10m.l_dsupporttime;
    analisis.dv_l_dsupporttime = t10m.dv_l_dsupporttime;
    analisis.r_dsupporttime = t10m.r_dsupporttime;
    analisis.dv_r_dsupporttime = t10m.dv_r_dsupporttime;

    analisis.l_steplength = t10m.l_steplength;
    analisis.dv_l_steplength = t10m.dv_l_steplength;
    analisis.r_steplength = t10m.r_steplength;
    analisis.dv_r_steplength = t10m.dv_r_steplength;

    analisis.l_stance = t10m.l_stance;
    analisis.dv_l_stance = t10m.dv_l_stance;
    analisis.r_stance = t10m.r_stance;
    analisis.dv_r_stance = t10m.dv_r_stance;

    analisis.l_swing = t10m.l_swing;
    analisis.dv_l_swing = t10m.dv_l_swing;
    analisis.r_swing = t10m.r_swing;
    analisis.dv_r_swing = t10m.dv_r_swing;

    //tug.new_session();
  }
}

export class Sesion10MTS {
    public paciente: PacienteDTO;
    public muestras: Muestreo[] = [];
    public analisis: Analisis10MTS;
    public algoritmo: Algoritmo10MTS;
    public dirt: boolean;
  }

//Datos de la muestra para ejecutar el algoritmo, obtenidos desde el sensor QMUV
export class Muestra10MTS {
  //Cuenta
  c: number[] = [];
  //Aceleraciones
  ax: number[] = [];
  ay: number[] = [];
  az: number[] = [];
  //Velocidades angulares
  gx: number[] = [];
  gy: number[] = [];
  gz: number[] = [];
  //Quaternions
  qw: number[] = [];
  qx: number[] = [];
  qy: number[] = [];
  qz: number[] = [];
}

class Algoritmo10MTS extends GaitAnalysis {
  contador = [];
}

