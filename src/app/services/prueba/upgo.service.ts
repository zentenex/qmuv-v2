import { Injectable } from '@angular/core';
import { PacienteDTO } from 'src/app/dao/pacientes-dao.service';
import { AnalisisUPGO } from 'src/app/dao/pruebas-dao.service';
import { ConfigDaoService } from 'src/app/dao/config-dao.service';
import { environment } from 'src/environments/environment';
import { Clipboard } from '@awesome-cordova-plugins/clipboard/ngx'
import { UtilsService } from '../utils.service';
import { TimedUpandGo } from 'src/app/business/TimedUpandGo';
import { Muestreo } from '../muestreo.service';

@Injectable({
  providedIn: 'root'
})
export class UpgoService {

  /**
   * Instancia de la sesion donde se almacena todo los datos de la prueba en proceso
   */
  public sesionActual: SesionUPGO;

  constructor(private configDAO: ConfigDaoService, private clipboard: Clipboard, private utilsService: UtilsService) { }

  /**
   * Partimos desde cero.
   * Creamos una nueva instancia del algoritmo de UPGO limpia
   */
  async sesionNueva() {
    console.log("Se crea nueva sesion");

    //Instancia de una nueva macro sesion
    this.sesionActual = new SesionUPGO();

    //La marcamos como limpia, esto es para saber si estoy poniendo el primer muestreo o los siguientes
    this.sesionActual.dirt = false;

    //Creo instancia del algoritmo de Neurotech
    let tug = new AlgoritmoTimedUpandGo();

    //Se setea el analysis_mode
    tug.analysis_mode = await this.configDAO.modoDeAnalisis(environment.PRUEBA_UPANDGO_CODE);

    //Se agrega el algoritmo de neurotech a la macro sesion
    this.sesionActual.algoritmo = tug;
  }

  /**
   * Agregamos los datos de un muestreo a la sesion actual del algoritmo qmuv
   * @param muestra Datos recopilados desde el QMUV ya formateados
   */
  sesionAddMedicion(muestra: Muestreo) {
    console.log("Agrega muestra a sesion");

    //Obtenemos la instancia del algoritmo para la prueba actual
    let tug = this.sesionActual.algoritmo;
    if(this.sesionActual.dirt) {
      //No es la primera medicion
      tug.other_measurement();
    }
    this.sesionActual.dirt = true;

    //Se poblan los arreglos con lo que se recibio del muestreo recien completado
    tug.contador = muestra.c;

    tug.ax = muestra.ax;
    tug.ay = muestra.ay;
    tug.az = muestra.az;

    tug.gx = muestra.gx;
    tug.gy = muestra.gy;
    tug.gz = muestra.gz;

    tug.qw = muestra.qw;
    tug.qx = muestra.qx;
    tug.qy = muestra.qy;
    tug.qz = muestra.qz;

    console.log("primero se acondiciona los arreglos para obtener las rotaciones");
    tug.conditioning(); //primero se acondiciona los arreglos para obtener las rotaciones

    console.log("primera parte del algoritmo de busqueda");
    tug.subtask_points(); //primera parte del algoritmo de busqueda

    console.log("segmentación automática");
    tug.subtask_events(); //segmentación automática

    console.log("revisión de la prueba");
    tug.test_ok(); //Revisión de la prueba

    //Evaluamos si la muestra es valida
    if(tug.eval_ok) {
      tug.subtask_duration(); //Duración subtareas 
      tug.subtask_movility(); //Indices de movilidad 

      //En caso de modo debug, copiamos los valores al portapapeles
      if(environment.debug) {
        this.utilsService.presentaToastDebug("Debug al portapaleles");
        this.clipboard.copy(JSON.stringify(tug));
      }
      
      //Agregamos RAW data de medicion
      this.sesionActual.rawData.push(JSON.parse(JSON.stringify(tug)))

      //tug.test_results(); //Resultado de sesion (cuando ya no quiero tomar mas muestras)
      //tug.other_measurement(); //Si quiero comenzar una nueva toma de muestra
    } else {
      //Repetir la prueba, muestra no puede ser usada para generar el reporte
      console.warn("Repetir la prueba, muestra no puede ser usada para generar el reporte");

      //En caso de modo debug, copiamos los valores al portapapeles
      if(environment.debug) {
        this.utilsService.presentaToastDebug("Debug al portapaleles");
        this.clipboard.copy(JSON.stringify(tug));
      }
      
      throw new Error("No se puede generar el reporte, Por favor, repita la medición");
    }

    //En caso de prueba valida, agregamos la muestra al listado de la macro sesion
    this.sesionActual.muestras.push(muestra);
  }

  /**
   * Comenza el proceso de analisis de las muestras
   * @param paciente Paciente al que se le asigana el resultado del analisis
   */
  sesionAnaliza(paciente: PacienteDTO) {
    console.log("Se analiza la sesion")
    this.sesionActual.paciente = paciente;

    let tug = this.sesionActual.algoritmo;
    tug.test_results(); //Resultado de sesion (cuando ya no quiero tomar mas muestras)

    //En modo debug, copiamos al portapapeles el estado en que quedo el algoritmo tras realizar el analisis final
    if(environment.debug) {
      this.utilsService.presentaToastDebug("Debug copiado al portapaleles");
      this.clipboard.copy(JSON.stringify(tug));
    }

    //Instancia donde guardaremos el resultado del algoritmo de NeuroTech
    let analisis = new AnalisisUPGO();
    this.sesionActual.analisis = analisis;

    //Registramos el modo de analisis que se uso en el algoritmo
    //Esto nos servira cuando tengamos que pintar que muestreo
    //fue el que finalmente se selecciono
    analisis.analysisMode = tug.analysis_mode;

    //Tiempo total de la prueba
    analisis.t = tug.d_test;

    //Multiples datos con respecto al tiempo para el gráfico
    analisis.rotacionesMulti = tug.rotation_test;
    analisis.inclinacionesMulti = tug.inclination_test;
    analisis.tiemposMulti = tug.d_test_array;

    //Todo el resto de los datos de la prueba

    //analisis.eventos = tug.events;
    analisis.eventosMulti = tug.events_array;

    analisis.t1 = tug.d_stand;
    analisis.t2 = tug.d_go;
    analisis.t3 = tug.d_turn1;
    analisis.t4 = tug.d_back;
    analisis.t5 = tug.d_turn2;
    analisis.t6 = tug.d_sit;

    analisis.aceleracionAlLevantarse = tug.stand_acc_max; //tug.stand_av_max; // o tug.stand_ap_max
    analisis.aceleracionAlSentarse = tug.sit_acc_max; //sit_av_max; //o tug.sit_ap_max
    analisis.velocidadAngularGiro3m = tug.turn1_vel; //turn1_vel_max;
    analisis.velocidadAngularGiroSilla = tug.turn2_vel; //turn2_vel_max;
    analisis.cadenciaIda = tug.cadence_go;
    analisis.cadenciaVuelta = tug.cadence_return;
    analisis.maxStandInclination = tug.max_stand_inclination;
    analisis.maxSitInclination = tug.max_sit_inclination;

    analisis.rofStand = tug.rof_stand;
    analisis.rofGo = tug.rof_go;
    analisis.rofReturn = tug.rof_return;
    analisis.rofTurn1 = tug.rof_turn1;
    analisis.rofTurn2 = tug.rof_turn2;
    analisis.rofSit = tug.rof_sit;

    analisis.rof = tug.rof;

    console.log(tug.d_test);
    console.log(tug.d_stand);
    console.log(tug.d_go);
    console.log(tug.d_turn1);
    console.log(tug.d_back);
    console.log(tug.d_turn2);
    console.log(tug.d_sit);
    console.log('\n');
    console.log(tug.rof);
    console.log('\n');
    console.log(tug.max_stand_inclination);
    console.log(tug.max_sit_inclination);
    console.log(tug.cadence_go);
    console.log(tug.cadence_return);
    console.log(tug.stand_acc_max);
    console.log(tug.sit_acc_max);
    console.log(tug.turn1_vel);
    console.log(tug.turn2_vel);
    console.log('\n');
    if(tug.rof_stand){
      console.log('Reforzar Transferencia Sedente a Bípeda.');
    }
    if(tug.rof_go || tug.rof_return){
      console.log('Reforzar Marcha.');
    }
    if(tug.rof_turn1){
      console.log('Reforzar Marcha con Giro.');
    }
    if(tug.rof_turn2){
      console.log('Reforzar Giro Pre-Transferencia Bípeda a Sedente.');
    }
    if(tug.rof_sit){
      console.log('Reforzar Transferencia Bípeda a Sedente.');
    }

    //tug.new_session();
  }

}

/**
 * Datos de una muestra para ejecutar el algoritmo, obtenidos desde el sensor QMUV
 * */
export class MuestraUPGO {
  //Cuenta
  c: number[] = [];
  //Aceleraciones
  ax: number[] = [];
  ay: number[] = [];
  az: number[] = [];
  //Velocidades angulares
  gx: number[] = [];
  gy: number[] = [];
  gz: number[] = [];
  //Quaternions
  qw: number[] = [];
  qx: number[] = [];
  qy: number[] = [];
  qz: number[] = [];
}

/**
 * Clase que posee todo lo necesario para una sesion de UPGO
 */
export class SesionUPGO {
  /**
   * Paciente al que se le asigna la prueba
   */
  public paciente: PacienteDTO;
  /**
   * Todas las muestras que se tomaron
   */
  public muestras: Muestreo[] = [];
  /**
   * Resultado del analisis
   */
  public analisis: AnalisisUPGO;
  /**
   * Instancia del algoritmo de NeuroTech
   */
  public algoritmo: AlgoritmoTimedUpandGo;
  /**
   * Todas las instancias del algoritmo (foto) tras cada muestreo
   */
  public rawData: AlgoritmoTimedUpandGo[] = [];
  /**
   * Indica si una prueba es la primera en agregarse o no 
   */
  public dirt: boolean;
}

/**
 * Clase que hereda del algoritmo de Neurotech con un contador adicional
 */
export class AlgoritmoTimedUpandGo extends TimedUpandGo {
  /**
   * Arreglo de numeros correlativos enviados por el QMUV. USado para verificar el orden de los paquetes enviados y si existen perdidas
   * */
  contador = [];
}
