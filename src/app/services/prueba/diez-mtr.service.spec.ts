import { TestBed } from '@angular/core/testing';

import { DiezMtrService } from './diez-mtr.service';

describe('DiezMtrService', () => {
  let service: DiezMtrService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiezMtrService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
