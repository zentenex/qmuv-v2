import { TestBed } from '@angular/core/testing';

import { UpgoService } from './upgo.service';

describe('UpgoService', () => {
  let service: UpgoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UpgoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
