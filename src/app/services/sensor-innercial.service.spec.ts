import { TestBed } from '@angular/core/testing';

import { SensorInnercialService } from './sensor-innercial.service';

describe('SensorInnercialService', () => {
  let service: SensorInnercialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SensorInnercialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
