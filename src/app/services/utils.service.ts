import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { FormGroup } from '@angular/forms';
import { ConstanteDTO, OpenDaoService, RegionDTO } from '../dao/open-dao.service';
import { environment } from 'src/environments/environment';
import { TranslateConfigService } from './translate-config.service';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  public regiones: RegionDTO[] = [];
  public constantes: ConstanteDTO[];
  public sendRawData = false;
  public planDelUsuarioId = 0;
  public planDelUsuarioNombre = "";
  public cuentaEstado = 0; //0 = Activo, 1 = Suspendido

  public cuentaAlt = false; //Indica si esta vinculado a una cta alterna
  public planAltId = 0;
  public planAltNombre = ""; //Indica el plan de la cuenta alterna

  public planCODE = ""; //Codigo de plan que debe ser considerado dentro de la APP para habilitar o deshabilitar funcionalidades

  constructor(private alertCtrl: AlertController, 
    public toastController: ToastController, 
    public openDaoService: OpenDaoService,
    private translateConfigService: TranslateConfigService) {
    if(environment.server) {
      this.openDaoService.regiones().then(resp => {
        this.regiones = resp;
      })

      this.openDaoService.constantes().then(ct => {
        this.constantes = ct;
        console.log("Constantes obtenidas ["+ ct.length +"]")
        for(let unaC of ct) {
          console.log("Contante ["+unaC.id+"]["+unaC.value+"]")
          if(unaC.id == "RAW" && (unaC.value == "YES" || unaC.value == "SI")) {
            this.sendRawData = true;
            this.presentaToast("Envio de datos RAW activado")
            console.log("RAW activado")
          }
          if(unaC.id == "ENV" && unaC.value != "PROD") {
              alert("App en modo de demostración");
          }
        }
        if(!this.sendRawData) {
          console.log("RAW desactivado")
        }
      })
    } else {
      {
        let unaReg = new RegionDTO();
        unaReg.id = 1;
        unaReg.nombre = "Arica";
        this.regiones.push(unaReg);
      }
      {
        let unaReg = new RegionDTO();
        unaReg.id = 2;
        unaReg.nombre = "Region 2";
        this.regiones.push(unaReg);
      }
      {
        let unaReg = new RegionDTO();
        unaReg.id = 3;
        unaReg.nombre = "Region 3";
        this.regiones.push(unaReg);
      }
      {
        let unaReg = new RegionDTO();
        unaReg.id = 4;
        unaReg.nombre = "Region 4";
        this.regiones.push(unaReg);
      }
      {
        let unaReg = new RegionDTO();
        unaReg.id = 5;
        unaReg.nombre = "Region 5";
        this.regiones.push(unaReg);
      }
      {
        let unaReg = new RegionDTO();
        unaReg.id = 6;
        unaReg.nombre = "Region 6";
        this.regiones.push(unaReg);
      }
      {
        let unaReg = new RegionDTO();
        unaReg.id = 7;
        unaReg.nombre = "Region 7";
        this.regiones.push(unaReg);
      }
      {
        let unaReg = new RegionDTO();
        unaReg.id = 8;
        unaReg.nombre = "Region 8";
        this.regiones.push(unaReg);
      }
      {
        let unaReg = new RegionDTO();
        unaReg.id = 9;
        unaReg.nombre = "Region RM";
        this.regiones.push(unaReg);
      }
      {
        let unaReg = new RegionDTO();
        unaReg.id = 10;
        unaReg.nombre = "Region 10";
        this.regiones.push(unaReg);
      }
    }
    
  }

  async alerta(headerR: string, messageR: string) {
    let ace = await this.translateConfigService.getTextoNow("COMUN.aceptar");
    const alert = await this.alertCtrl.create({
        header: headerR,
        message: messageR,
        buttons: [ace]
    });
    await alert.present();
  }

  async presentaToast(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      position: 'top',
      color: "warning",
      duration: 2000
    });
    toast.present();
  }

  async presentaToastDebug(mensaje: string) {
    if(environment.debug) {
      const toast = await this.toastController.create({
        message: mensaje,
        position: 'bottom',
        color: "secondary",
        duration: 2000
      });
      toast.present();
    }
    
  }

  /**
   * 
   * @param headerR Titulo del popup
   * @param messageR Mensaje del popup
   * @param callback funcion (callback) que se llama al responder el usuario, pasa como parametro un boleano
   */
  async confirma(headerR: string, messageR: string, callback: any) {
      let si = await this.translateConfigService.getTextoNow("COMUN.si")
      let no = await this.translateConfigService.getTextoNow("COMUN.no")
    const alert = await this.alertCtrl.create({
        header: headerR,
        message: messageR,
        buttons: [{
          text: no,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            callback(false)
          }
        }, {
          text: si,
          handler: () => {
            callback(true)
          }
        }]
    });
    await alert.present();
  }

  MatchPassword(password: string, confirmPassword: string) {
    return (formGroup: FormGroup) => {
      const passwordControl = formGroup.controls[password];
      const confirmPasswordControl = formGroup.controls[confirmPassword];

      if (!passwordControl || !confirmPasswordControl) {
        return null;
      }

      if (confirmPasswordControl.errors && !confirmPasswordControl.errors.passwordMismatch) {
        return null;
      }

      if (passwordControl.value !== confirmPasswordControl.value) {
        confirmPasswordControl.setErrors({ passwordMismatch: true });
      } else {
        confirmPasswordControl.setErrors(null);
      }
    }
  }

  duerme(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  /**
   * Función que toma una serie de datos de un arreglo numérico y calcula su tendencia retornando la tendencia y su significado en un arreglo de dos elementos [tend, color].
   * @param index Define el tipo de parámetro al que se le calcula la tendencia [“inclination”,“other”]
   * @param values Arreglo numerico a los que se le calcula la tendencia
   */
  tendence(index: string, values: number[]) {
    let session = [];
    let tend = 0;
    let color = 0;
    if(values.length>1){
        for(var i = 0; i<values.length; i++){
            session.push(i+1);
        }
        var n = values.length;
        var m = 0;
        var a = 0;
        var b = 0;
        var c = 0;
        var d = 0; 
        let b_data = 0;
        let b_session = 0;
        for (i = 0; i < n; i++) {
            a += (values[i]*session[i]);
            b_data += values[i];
            b_session += session[i];
            c += (session[i]*session[i]);
        }
        a = n*a;
        b = b_data*b_session;
        c = n*c;
        d = b_session*b_session;
        m = (a - b)/(c - d);

        if(!index.localeCompare('inclination')){
            if(m > 0){
                if(values[0] < 10 && values[-1] <= 40){
                    tend = 1;
                    color = 1;
                }
                else if(values[0] > 40){
                    tend = 1;
                    color = -1;
                }
                else if(values[0] >= 10 && values[0] <= 40 && values[-1] > 40){
                    tend = 1;
                    color = -1;
                }
            }

            else if(m < 0){
                if(values[0] > 40 && values[-1] >= 10){
                    tend = -1;
                    color = 1;
                }
                else if(values[0] < 10){
                    tend = -1;
                    color = -1;
                }
                else if(values[0] >= 10 && values[0] <= 40 && values[-1] < 10){
                    tend = -1;
                    color = -1;
                }
            }

            else{
                if(values[0] < 10 || values[0] > 40){
                    tend = 0;
                    color = -1;
                }
                else if(values[0] >= 10 && values[0] <= 40){
                    tend = 0;
                    color = 0;
                }
            }
        }

        else if(!index.localeCompare('angvel')){
            if(m > 0){
                if(values[0] < 140){
                    tend = 1;
                    color = 1;
                }
                else if(values[0] > 140){
                    tend = 1;
                    color = 1;
                }
            }

            else if(m < 0){
                if(values[0] > 140 && values[-1] < 140){
                    tend = -1;
                    color = -1;
                }
                else if(values[0] < 140){
                    tend = -1;
                    color = -1;
                }
            }

            else{
                if(values[0] < 140){
                    tend = 0;
                    color = -1;
                }
                else if(values[0] >= 140 && values[0] <= 150){
                    tend = 0;
                    color = 0;
                }
            }
        }

        else if(!index.localeCompare('accel')){
            if(m > 0){
                if(values[0] < 0.7){
                    tend = 1;
                    color = 1;
                }
                else if(values[0] > 0.7){
                    tend = 1;
                    color = 1;
                }
            }

            else if(m < 0){
                if(values[0] > 0.7 && values[-1] < 0.7){
                    tend = -1;
                    color = -1;
                }
                else if(values[0] < 0.7){
                    tend = -1;
                    color = -1;
                }
            }

            else{
                if(values[0] < 0.7){
                    tend = 0;
                    color = -1;
                }
                else if(values[0] >= 0.7 && values[0] <= 0.8){
                    tend = 0;
                    color = 0;
                }
            }
        }

        else if(!index.localeCompare('apoyo')){
            if(m > 0){
                if(values[0] < 59 && values[-1] <= 62){
                    tend = 1;
                    color = 1;
                }
                else if(values[0] > 62){
                    tend = 1;
                    color = -1;
                }
                else if(values[0] >= 59 && values[0] <= 62 && values[-1] > 62){
                    tend = 1;
                    color = -1;
                }
            }

            else if(m < 0){
                if(values[0] > 62 && values[-1] >= 59){
                    tend = -1;
                    color = 1;
                }
                else if(values[0] < 59){
                    tend = -1;
                    color = -1;
                }
                else if(values[0] >= 59 && values[0] <= 62 && values[-1] < 59){
                    tend = -1;
                    color = -1;
                }
            }

            else{
                if(values[0] < 59 || values[0] > 62){
                    tend = 0;
                    color = -1;
                }
                else if(values[0] >= 59 && values[0] <= 62){
                    tend = 0;
                    color = 0;
                }
            }
        }

        else if(!index.localeCompare('balanceo')){
            if(m > 0){
                if(values[0] < 38 && values[-1] <= 41){
                    tend = 1;
                    color = 1;
                }
                else if(values[0] > 41){
                    tend = 1;
                    color = -1;
                }
                else if(values[0] >= 38 && values[0] <= 41 && values[-1] > 41){
                    tend = 1;
                    color = -1;
                }
            }

            else if(m < 0){
                if(values[0] > 41 && values[-1] >= 38){
                    tend = -1;
                    color = 1;
                }
                else if(values[0] < 38){
                    tend = -1;
                    color = -1;
                }
                else if(values[0] >= 38 && values[0] <= 41 && values[-1] < 38){
                    tend = -1;
                    color = -1;
                }
            }

            else{
                if(values[0] < 38 || values[0] > 41){
                    tend = 0;
                    color = -1;
                }
                else if(values[0] >= 38 && values[0] <= 41){
                    tend = 0;
                    color = 0;
                }
            }
        }

        else if(!index.localeCompare('lpaso')){
            if(m > 0){
                tend = 1;
                color = 1;
            }

            else if(m < 0){
                if(Math.abs(values[0]-values[-1])>0.1){
                    tend = -1;
                    color = -1;
                }
            }

            else{
                tend = 0;
                color = 0;
            }
        }

        else if(!index.localeCompare('cadencia')){
            if(m > 0){
                if(values[0] < 90){
                    tend = 1;
                    color = 1;
                }
                else if(values[0] > 90){
                    tend = 1;
                    color = 1;
                }
            }

            else if(m < 0){
                if(values[0] > 90 && values[-1] < 90){
                    tend = -1;
                    color = -1;
                }
                else if(values[0] < 90){
                    tend = -1;
                    color = -1;
                }
            }

            else{
                if(values[0] < 90){
                    tend = 0;
                    color = -1;
                }
                else if(values[0] >= 90 && values[0] <= 110){
                    tend = 0;
                    color = 0;
                }
            }
        }

        else if(!index.localeCompare('tpaso')){
            if(m > 0){
                if(values[0] < 0.45){
                    tend = 1;
                    color = 1;
                }
                else if(values[0] > 0.45){
                    tend = 1;
                    color = 1;
                }
            }

            else if(m < 0){
                if(values[0] > 0.45 && values[-1] < 0.45){
                    tend = -1;
                    color = -1;
                }
                else if(values[0] < 0.45){
                    tend = -1;
                    color = -1;
                }
            }

            else{
                if(values[0] < 0.45){
                    tend = 0;
                    color = -1;
                }
                else if(values[0] >= 0.45 && values[0] <= 0.7){
                    tend = 0;
                    color = 0;
                }
            }
        }

        else if(!index.localeCompare('tzancada')){
            if(m > 0){
                if(values[0] < 1){
                    tend = 1;
                    color = 1;
                }
                else if(values[0] > 1){
                    tend = 1;
                    color = 1;
                }
            }

            else if(m < 0){
                if(values[0] > 1 && values[-1] < 1){
                    tend = -1;
                    color = -1;
                }
                else if(values[0] < 1){
                    tend = -1;
                    color = -1;
                }
            }

            else{
                if(values[0] < 1){
                    tend = 0;
                    color = -1;
                }
                else if(values[0] >= 1 && values[0] <= 1.3){
                    tend = 0;
                    color = 0;
                }
            }
        }

        if(!index.localeCompare('ssupport')){
            if(m > 0){
                if(values[0] < 0.4 && values[-1] <= 0.6){
                    tend = 1;
                    color = 1;
                }
                else if(values[0] > 0.6){
                    tend = 1;
                    color = -1;
                }
                else if(values[0] >= 0.4 && values[0] <= 0.6 && values[-1] > 0.6){
                    tend = 1;
                    color = -1;
                }
            }

            else if(m < 0){
                if(values[0] > 0.6 && values[-1] >= 0.4){
                    tend = -1;
                    color = 1;
                }
                else if(values[0] < 0.4){
                    tend = -1;
                    color = -1;
                }
                else if(values[0] >= 0.4 && values[0] <= 0.6 && values[-1] < 0.4){
                    tend = -1;
                    color = -1;
                }
            }

            else{
                if(values[0] < 0.4 || values[0] > 0.6){
                    tend = 0;
                    color = -1;
                }
                else if(values[0] >= 0.4 && values[0] <= 0.6){
                    tend = 0;
                    color = 0;
                }
            }
        }

        if(!index.localeCompare('dsupport')){
            if(m > 0){
                if(values[0] < 0.1 && values[-1] <= 0.2){
                    tend = 1;
                    color = 1;
                }
                else if(values[0] > 0.2){
                    tend = 1;
                    color = -1;
                }
                else if(values[0] >= 0.1 && values[0] <= 0.2 && values[-1] > 0.2){
                    tend = 1;
                    color = -1;
                }
            }

            else if(m < 0){
                if(values[0] > 0.2 && values[-1] >= 0.1){
                    tend = -1;
                    color = 1;
                }
                else if(values[0] < 0.1){
                    tend = -1;
                    color = -1;
                }
                else if(values[0] >= 0.1 && values[0] <= 0.2 && values[-1] < 0.1){
                    tend = -1;
                    color = -1;
                }
            }

            else{
                if(values[0] < 0.1 || values[0] > 0.2){
                    tend = 0;
                    color = -1;
                }
                else if(values[0] >= 0.1 && values[0] <= 0.2){
                    tend = 0;
                    color = 0;
                }
            }
        }



    }    

    return [tend, color];
  }

}
