import { Injectable } from '@angular/core';
import { DispositivosDAOService, DispositivoDTO } from '../dao/dispositivos-dao.service';
import { UtilsService } from './utils.service';
import { Subject, Observable } from 'rxjs';
import { LoadingController, Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { ConfigDaoService } from '../dao/config-dao.service';
import { MuestreoService } from './muestreo.service';
import { TranslateConfigService } from './translate-config.service';
import { BLE } from '@awesome-cordova-plugins/ble/ngx';

@Injectable({
  providedIn: 'root'
})
export class SensorInnercialService {

  //Indica como (se supone) que esta mi estado de conexion con el dispositivo
  private __mementoConnect = false;

  private setMementoConnect(valor: boolean) {
    console.log("MEMENTO CONNECT : " + valor);
    this.__mementoConnect = valor;
  }

  private getMementoConnect(): boolean {
    return this.__mementoConnect;
  }

  private quieroEstarConectado = false;

  public textoLog: string[] = []

  private _loadingConectando: HTMLIonLoadingElement = null;
  public virtual: boolean = false;
  public ultimaLecturaBateria = -1;

  //Observador de dispositivo QMUV actual
  private _dispositivoDefault$ = new Subject<DispositivoDTO>();

  //Observador de dispositivos QMUV descubiertos
  private _dispositivoDescubierto$ = new Subject<DispositivoDTO>();

  //Observador de % de carga de la bateria del dispositivo
  private _dispositivoBateria$ = new Subject<number>();

  /**
   * Retorna el observador de dispositivo QMUV actual
   * */
  observadorDispositivo$(): Observable<DispositivoDTO> {
    return this._dispositivoDefault$.asObservable();
  }

  //Retorna el observador de dispositivos QMUV descubiertos
  observadorNuevosDispositivos$(): Observable<DispositivoDTO> {
    return this._dispositivoDescubierto$.asObservable();
  }

  //Retorna el observador de la bateria del dispositivo QMUV conectado
  observadorBateriaDispositivo$(): Observable<number> {
    return this._dispositivoBateria$.asObservable();
  }

  //Constructor
  constructor(
    private dispositivosDAOService: DispositivosDAOService,
    private ble: BLE,
    private platform: Platform,
    private loadingController: LoadingController,
    private configDaoService: ConfigDaoService,
    private muestreoService: MuestreoService,
    private translateConfigService: TranslateConfigService,
    private utilsService: UtilsService) {
    //setInterval(() => this.corrigeConexion(), 1000);
  };

  /************  INICIO BLUETOOTH  *********************/

  /**
  Si el qmuv esta conectado pero no paso por el proceso de inicio de la app, entonces reconecta
  */
  corrigeConexion() {
    this.dispositivosDAOService.getCurrentDevice().then(device => {
      if (device) {
        this.estoyConectado(device.id).then(() => {
          //qmuv conectado
          if (this.quieroEstarConectado) {
            //Todo en orden, solo me queda ver si paso el proceso de inicio
            if (this.getMementoConnect()) {
              //La app dice que si paso el proceso de inicio
            } else {
              //No ha pasado el proceso de inicio
            }
          } else {
            //
          }
        }).catch(() => {
          //qmuv desconectado
          if (this.quieroEstarConectado) {
            //me tengo que conectar
          } else {
            //todo en orden
          }
        })
      }
    })

    /*
    this.estoyConectadoUltimo().then(resp => {
      if(resp) {
        //El qmuv esta conectado a la app
        if(!this.getMementoConnect()) {
          //La app no esta conectada al qmuv
          //Corregiremos desconectando el qmuv e intentando una nueva conexion

          this.dispositivosDAOService.getCurrentDevice().then(device => {
            this.ble.disconnect(device.id).then(() => {
              console.log('Desconectado de forma manual');
    
              this.ultimaLecturaBateria = -3;
              this._dispositivoBateria$.next(this.ultimaLecturaBateria);
    
              this.utilsService.presentaToast("Reconectando...");
              this._dispositivoDefault$.next(device);

              //conectamos denuevo
              this.conectaUltimoRetry3(true);

            });
          })

          

        }
      } else {
        console.log("El qmuv no esta conectado a la app")
      }
    }).catch(() => {
      console.log("no se si el qmuv esta o no conectado");
    })
    */
  }


  //Conecta al dispositivo QMUV pasado como parametro
  conectaNuevo(disp: DispositivoDTO) {
    //revisamos si ya estoy conectado al ultimo disp registrado y me desconecto
    this.dispositivosDAOService.getCurrentDevice().then(device => {
      if (device) {

        this.ble.isConnected(device.id).then(
          () => {
            this.ble.disconnect(device.id).then(() => {
              console.log('Desconectados');
              this.utilsService.presentaToast("Desconectado de otro dispositivo");
              this._dispositivoDefault$.next(device);
            });
          }
        )

      }

      console.log("dejamos al dispositivo seleccionado como current device");
      this.dispositivosDAOService.setCurrentDevice(disp);
      this.conectaUltimoRetry3(true);

    });


  }

  bluetoothCorrecto() {
    this.ble.isEnabled().then(
      () => {
        console.log("Bluetooth is enabled");
      },
      () => {
        //alert("Debe activar bluetooth en su dispositivo");
        this.utilsService.alerta("Advertencia", "Debe activar bluetooth en su dispositivo");
        return;
      }
    )
  }

  abortaIntentoDeConexion() {
    this._loadingConectando.dismiss().then(exito => {
      if (exito) {
        this.utilsService.alerta("Dispositivo no encontrado", "Se conectara automáticamente al dispositivo cuando esté disponible");
      }
    })
  }

  async habilita() {
    return await this.ble.enable();
  }

  public meQuieroConectar() {
    this.quieroEstarConectado = true;
  }

  async conectaUltimoRetry3(conSpinner: boolean) {

    let reintentos = environment.btRetryConn;

    //Muestro spinner
    this._loadingConectando = await this.loadingController.create({
      message: 'Conectando...',
      backdropDismiss: true
    });

    if (conSpinner) {
      await this._loadingConectando.present();
    }

    this.utilsService.presentaToastDebug("Cmienza proceso de conexion");
    for (let i = 0; i < reintentos; i++) {
      setTimeout(() => this.conectaUltimoValidaConexion(i + 1), i * 3000);
    }
  }

  private async conectaUltimoValidaConexion(intento: number) {
    let ultimoDisp = await this.elUltimoDispositivo();
    if (ultimoDisp) {
      this.estoyConectado(ultimoDisp.id).then(() => {
        console.log("Try " + intento + ". Ya estoy conectado, no insistire");
      }).catch(() => {
        this.conectaUltimo(intento);
      })
    }
  }

  //Conecta al ultimo dispositivo QMUV conectado
  private conectaUltimo(intento: number) {

    console.log("Nos Intentamos (" + intento + ") conectar al current dispositivo...");
    this.dispositivosDAOService.getCurrentDevice().then(async device => {
      if (device) {

        //device.rssi = -80;
        //this.dispositivosDAOService.setCurrentDevice(device);
        console.log("Intentando el connect al dispositivo : " + device.nombre + " con ID : " + device.id);

        if (!this.virtual) {

          console.log("comienza el connect...")

          try {
            this.ble.connect(device.id).subscribe(
              peripheral => this.onConnected(peripheral, intento),
              peripheral => this.onDisconnected(peripheral, intento),
              () => this.utilsService.presentaToastDebug("Connect Complete")
            ) //, this.onConnected.bind(this), this.onDisconnected.bind(this));
          } catch (err) {
            alert("Error al intentar conectar BT");
            if (this._loadingConectando) {
              this._loadingConectando.dismiss();
            }
          }
          this.utilsService.presentaToastDebug("Conexion solicitada...");

        }

      } else {
        let noDefaultDevice = await this.translateConfigService.getTextoNow("ERRORES.noDefaultDevice");
        let noDevice = await this.translateConfigService.getTextoNow("ERRORES.noDevice");
        this.textoLog.push(noDefaultDevice);
        this.utilsService.presentaToast(noDevice);
      }
    }).catch(async _ => {
      this.textoLog.push("No se pudo obtener el current device");
      let noDefaultDevice = await this.translateConfigService.getTextoNow("ERRORES.noDefaultDevice");
      this.utilsService.presentaToast(noDefaultDevice);
    })
  }

  muestraEstado() {
    this.ble.startStateNotifications().subscribe(state => {
      console.log("Bluetooth is " + state);
      this.utilsService.presentaToastDebug("Bluetooth is " + state);
    });
  }



  /**
   * exito = conectado, error = desconectado
   * @param id id del dispositivo bluetooth
   */
  async estoyConectado(id: string) {
    return this.ble.isConnected(id);
  }

  async estoyConectadoUltimo() {
    let dev = await this.dispositivosDAOService.getCurrentDevice();
    if (dev) {
      try {
        await this.estoyConectado(dev.id);
        return true;
      } catch (err) {
        return false;
      }
    }
    throw Error("No tengo ultimo dispositivo");
  }

  //Actualizo el RSSI del dispositivo por defecto
  actualizoRSSI() {
    this.dispositivosDAOService.getCurrentDevice().then(device => {
      if (device) {
        this.ble.readRSSI(device.id).then(rssi => {
          console.log("Si pude leer el rssi del dispositivo por defecto")
          device.rssi = rssi;
          device.visto = Date.now();
          this.dispositivosDAOService.setCurrentDevice(device);
          this._dispositivoDefault$.next(device);
        }, _ => {
          console.log("No pude leer el rssi del dispositivo por defecto")
          if (Date.now() - device.visto > (1000 * environment.btScanSeg)) {
            device.rssi = -9999
            this.dispositivosDAOService.setCurrentDevice(device);
            this._dispositivoDefault$.next(device);
          }
        })
      } else {
        console.log("No hay disp por defecto");
      }
    });
  }

  //Cuando estamos listos con el mtu (cambiado en android o sin tocar en ios)
  private mtuListo(disp: DispositivoDTO) {
    this.bateriaYSuscripciones(disp);
    console.log("Se marca memento connect como verdadero");
    this.setMementoConnect(true);
  }

  //Se solicita cambiar el MTU y se indica el nro de intento
  private cambiaMTU(disp: DispositivoDTO, intento: number) {

    //Indica si el cambio de MTU fue exitoso y asi salir de aqui
    let mtuCambiado = false;

    this.ble.requestMtu(disp.id, environment.mtu).then(() => {
      console.log('MTU Size Accepted');
      this.utilsService.presentaToastDebug("MTU Size Accepted");
      mtuCambiado = true;
    }, _ => {
      console.log('MTU Size Failed');
      this.utilsService.presentaToastDebug("MTU Size Failed");

    }).finally(async () => {
      if (mtuCambiado) {
        //this.bateria(disp);

        //Estamos listos con el MTU
        this.mtuListo(disp);
      } else {

        if (intento < 3) {
          //Intentamos otra vez, incrementamos en numero de intentos
          intento++
          console.log("Se hara un nuevo intento de cambiar el MTU. Intento " + intento);
          this.cambiaMTU(disp, intento);
        } else {
          //No hay caso, no se puede cambiar el MTU. Nos desconectamos
          //dejamos de reintentar
          //ya no llamaremos a la bateria
          let noMTU = await this.translateConfigService.getTextoNow("ERRORES.noMTU");
          alert(noMTU)
          this.ble.disconnect(disp.id)
        }

      }
    })
  }

  //Invocado al establecer una conexion con un dispositivo
  private async onConnected(periferico: any, intento: number) {
    console.log("CONECTADO en el intento (" + intento + ")");

    let conectado = await this.translateConfigService.getTextoNow("ERRORES.conectado");
    this.utilsService.presentaToast(conectado);

    let disp = new DispositivoDTO(periferico);

    if (environment.mtu > 0 && this.platform.is('android')) {
      console.log("Se modifica el MTU");


      //Intentamos cambiar al MTU (3 intentos)
      //De tener exito, llamaremos a metodo mtuListo()
      this.cambiaMTU(disp, 1)

    } else {
      console.log("No se modifica el MTU");
      //this.bateria(disp);
      this.mtuListo(disp);
    }
    //console.log("Se marca memento connect como verdadero");
    //this.setMementoConnect(true);
  }

  public leeBateria(disp: DispositivoDTO) {
    //Leemos directamente el valor de la bateria
    console.log("Obtenemos carga de la bateria del dispositivo");
    this.ble.read(disp.id, environment.BT_LTE.servicio.BATTERY_SERVICE.UUID, environment.BT_LTE.servicio.BATTERY_SERVICE.caracteristica.BATTERY_LEVEL_CHAR).then(async buffer => {
      console.log("Carga inicial leida");
      const data = new Int8Array(buffer);
      this.ultimaLecturaBateria = data[0];
      console.log("Carga de la bateria primera lectura: " + this.ultimaLecturaBateria);
      this._dispositivoBateria$.next(this.ultimaLecturaBateria);

      let bateria = await this.translateConfigService.getTextoNow("ERRORES.bateria");
      this.utilsService.presentaToastDebug(bateria + this.ultimaLecturaBateria);
    }).catch(async err => {
      console.error("No se pudo leer la carga inicial : " + JSON.stringify(err));
      let nobateria = await this.translateConfigService.getTextoNow("ERRORES.nobateria");
      this.utilsService.presentaToastDebug(nobateria + ": " + err);
    })
  }

  //Obtenemos la carga de la bateria del dispositivo
  private bateriaYSuscripciones(disp: DispositivoDTO) {

    this.leeBateria(disp);

    //Nos suscribimos a la bateria
    //Cada vez que llega una publicacion, se publica para quien desee escuchar en (this._dispositivoBateria$)
    this.utilsService.presentaToastDebug("Nos suscribimos a BATERIA");
    console.log("Obtenemos carga de la bateria del dispositivo (notificamos los cambios)");
    this.ble.startNotification(disp.id, environment.BT_LTE.servicio.BATTERY_SERVICE.UUID, environment.BT_LTE.servicio.BATTERY_SERVICE.caracteristica.BATTERY_LEVEL_CHAR).subscribe(buffer => {
      console.log("Carga SUSC leida");
      const data = new Int8Array(buffer);
      this.ultimaLecturaBateria = data[0];
      console.log("Carga de la bateria : " + this.ultimaLecturaBateria);
      this._dispositivoBateria$.next(this.ultimaLecturaBateria);
    }, err => {
      console.log('Error al suscribir a bateria : ' + JSON.stringify(err));
    })

    //Nos suscribimos a la caracteristica que nos entrega los datos de todos los sensores
    //Cada vez que llegan datos, los ponemos en los arreglos
    this.utilsService.presentaToastDebug("Nos suscribimos a DATA");
    this.ble.startNotification(disp.id, environment.BT_LTE.servicio.IMU_SERVICE.UUID, environment.BT_LTE.servicio.IMU_SERVICE.caracteristica.DATA_COMP_CHAR).subscribe(data => {
      //Cuando nos llega informacion del QMUV, llamamos al metodo onData y le entregamos el buffer
      this.muestreoService.onData(data);
    }, async err => {
      if (!this.virtual) {
        console.error(JSON.stringify(err));
        //this.examenEnEjecucion = false;
        //this.segundos = 0;
        //this.utilsService.presentaToast("Prueba abortada");

        //Se comenta ya que al parecer en ios salta alerta al desconectarse
        //let noCompletadoCuerpo = await this.translateConfigService.getTextoNow("ERRORES.noCompletadoCuerpo");
        //let noCompletadoCabecera = await this.translateConfigService.getTextoNow("ERRORES.noCompletadoCabecera");
        //this.utilsService.alerta(noCompletadoCabecera, noCompletadoCuerpo);

        this.desconecta();
      } else {
        //Es virtual, simulo que recibo datos
        this.muestreoService.recibiendo = true;
      }
    });


    this.dispositivosDAOService.setCurrentDevice(disp);
    this._dispositivoDefault$.next(disp);
    console.log('Conectado al periferico : ' + disp.nombre);
    if (this._loadingConectando) {
      this._loadingConectando.dismiss();
    }
  }

  //Se invoca al no poder o desconectarse de un dispositivo
  private onDisconnected(periferico: any, intento: number) {
    console.log("TRY (" + intento + ") DESCONECTADO / CONN FALLIDA");

    let disp = new DispositivoDTO(periferico);
    this.estoyConectado(disp.id).then(() => {
      console.log("Mentira, sigo conectado");
      this.utilsService.presentaToastDebug("Exito el isConnected")
    }).catch(() => {
      this.utilsService.presentaToastDebug("Exception el isConnected")
      console.log("No pude determinar si sigo conectado");
      if (this.getMementoConnect()) {
        console.log("memento conect es verdadero")
        this.utilsService.presentaToast("Desconectado");

        console.log("Cambia ultima lectura bateria")
        this.ultimaLecturaBateria = -2;
        this._dispositivoBateria$.next(this.ultimaLecturaBateria);

        this.dispositivosDAOService.getCurrentDevice().then(dev => {
          console.log("Obtenemos el dispositivo actual")
          this.dispositivosDAOService.setCurrentDevice(dev);

          console.log("Informamos al observador del dispositivo actual")
          this._dispositivoDefault$.next(dev);
        })

        console.log('Desconectado del periferico qmuv');
        this.setMementoConnect(false);
      } else {
        console.log("memento conect es falso")
        this.utilsService.presentaToast("Conexión fallida");
      }

      console.log('Si estaba monstrando la rueda de conectando, la saco');
      if (this._loadingConectando) {
        this._loadingConectando.dismiss();
      }

    })

    console.log("Terminado el onDisconnected")

  }

  /**
   * Desconecta del dispositivo QMUV de forma manual
   */
  desconecta() {
    console.log("desconecta : Desconecta del dispositivo QMUV de forma manual")
    this.dispositivosDAOService.getCurrentDevice().then(device => {
      if (device) {
        console.log("Nos intentamos desconectar de forma manual");
        this.ble.disconnect(device.id).then(() => {
          console.log('Desconectado de forma manual');

          this.ultimaLecturaBateria = -3;
          this._dispositivoBateria$.next(this.ultimaLecturaBateria);

          this.utilsService.presentaToast("Desconectado manual");
          this._dispositivoDefault$.next(device);
        });
      } else {
        console.log("No hay dispositivo default para desconectarme");
        this.utilsService.presentaToast("No hay dispositivo default para desconectarme");
      }
    }).catch(_ => {
      console.log("No se pudo obtener el current device para desconectar");
      this.utilsService.presentaToast("Error: no se pudo obtener el dispositivo por defecto para desconectar");
    });
    this.setMementoConnect(false);
  }

  //Retorna el ultimo dispositivo QMUV conectado
  async elUltimoDispositivo() {
    return await this.dispositivosDAOService.getCurrentDevice();
  }

  private agregaDeviceDummy(id: string, nombre: string, rssi: number) {
    let deviceDummy: any = {};
    deviceDummy.id = id;
    deviceDummy.name = nombre;
    deviceDummy.rssi = rssi;
    let disp = new DispositivoDTO(deviceDummy);
    this._dispositivoDescubierto$.next(disp);
  }

  //Busca dispositivos QMUV cercanos en modo Discovery
  async escaneaStart() {
    if (!this.virtual) {
      this.textoLog.push("comienza escaneo");
      let dis = await this.elUltimoDispositivo();
      let idLast = "0";
      if (dis) {
        idLast = dis.id;
      }
      let escaneaTodo = await this.configDaoService.scanAllBT();

      this.ble.startScanWithOptions([], { reportDuplicates: true }).subscribe(
        device => this.onDeviceDiscovered(device, escaneaTodo),
        error => this.scanError(error)
      );
    } else {
      this.agregaDeviceDummy('4255093C-3F30-4F3C-8D0A-F1B968FCAD34', "Dispositivo virtual 1", -30);
      this.agregaDeviceDummy('3255093C-3F30-4F3C-8D0A-F1B968FCAD33', "Dispositivo virtual 2", -20);
    }
  }

  escaneaStop() {
    if (!this.virtual) {
      this.ble.stopScan();
    }
  }

  //Invocado al descubrir un dispositivo QMUV en modo discovery
  private onDeviceDiscovered(device, escaneaTodo: boolean) {
    this.textoLog.push('Descubierto ' + device.name);

    let nombre: string = "sin nombre";
    if (device.name) {
      nombre = device.name.toLowerCase();
    }

    if ((nombre.includes('qmuv') || nombre.includes('bluenrg')) || escaneaTodo) {
      this.textoLog.push("Agregado dispositivo : " + nombre);
      let disp = new DispositivoDTO(device);
      this._dispositivoDescubierto$.next(disp);
    } else {
      this.textoLog.push("Descartado dispositivo : " + nombre + " con rssi : " + device.rssi);
    }
  }

  // If location permission is denied, you'll end up here
  private scanError(error) {
    this.textoLog.push("Error al escanear : " + error);
    this._dispositivoDescubierto$.next(null);
  }

  /**
   * Notifica al QMUV que quiero iniciar o terminar el muestreo para que me notifique o deje de enviarme las notificaciones de los sensores
   * 
   * @param serviceID UUID del equipo bluetooth
   * @param activar Indica si quiero que me mande datos o no
   */
  public async envioDeDatos(serviceID: string, activar: boolean) {
    console.log("Escribimos en la caracteristica para iniciar la medicion : " + activar)

    //Creo el paquete a enviar al QMUV
    var data = new Uint8Array(1);
    if (activar) {
      //Quiero recibir datos
      data[0] = 1;
    } else {
      //No quiero recibir datos
      data[0] = 0;
    }

    //Escribo en el QMUV
    this.utilsService.presentaToastDebug("Se envia flag ADQ a QMUV : " + serviceID);
    await this.ble.writeWithoutResponse(serviceID, environment.BT_LTE.servicio.IMU_CTRL_SERVICE.UUID, environment.BT_LTE.servicio.IMU_CTRL_SERVICE.caracteristica.IMU_DATA_ADQ_CHAR, data.buffer);

    //Si no quiero recibir mas datos debo esperar a que el buffer el QMUV se vacie
    if (!activar) {
      while (true) {
        //Leo la caracteristica que me indica si quedan o no datos en el buffer
        let buff = await this.ble.read(serviceID, environment.BT_LTE.servicio.IMU_CTRL_SERVICE.UUID, environment.BT_LTE.servicio.IMU_CTRL_SERVICE.caracteristica.IMU_BUFFER_EMPTY_CHAR)
        //Convierto lo leido en un 0 o un 1
        let dataDatos = new Int8Array(buff);
        let quedanDatos = dataDatos[0];
        //Evaluo la respuesta del QMUV
        if (quedanDatos == 1) {
          console.log("Ya no queda nada nadina en el buffer")
          break;
        } else {
          console.log("Todavia queda algo, vuelvo a preguntar en 1 seg")
        }
        //Si no hice break, duerme 1 segundo y vuelve a intentarlo
        this.utilsService.duerme(1000);
      }
    }
  }

  /************  FIN BLUETOOTH  *********************/

  /************  INICIO TRANSFORMACION DE DATOS  *********************/

  /**
   * Función que transforma datos de uint8 o char a int16. Recibe el byte menos significativo (lsb) y después el más significativo (msb). 
   * Retorna el valor transformado a entero de 16 bits (2 bytes) con signo
   * @param lsb 
   * @param msb 
   */
  format_byte(lsb, msb) {
    return new Int16Array([(msb << 8) | lsb])
  }

  /**
   * Función que transforma datos de acelerómetro uint8 o char a int16. Recibe todo el vector no formateado de la forma 
   * [ax_lsb,ax_msb,ay_lsb,ay_msb,az_lsb,az_msb]. 
   * Retorna el vector transformado a entero de 16 bits (2 bytes) con signo de la forma [ax,ay,az].
   * 
   * @param accel
   */
  format_accel_data(accel) {
    return new Int16Array([
      (accel[1] << 8) | accel[0],
      (accel[3] << 8) | accel[2],
      (accel[5] << 8) | accel[4]
    ])
  }

  /**
   * Función que transforma datos de giroscopio uint8 o char a int16. Recibe todo el vector no formateado de la forma 
   * [gx_lsb,gx_msb,gy_lsb,gy_msb,gz_lsb,gz_msb]. 
   * Retorna el vector transformado a entero de 16 bits (2 bytes) con signo de la forma [gx,gy,gz]
   * @param gyro 
   */
  format_gyro_data(gyro) {
    return new Int16Array([
      (gyro[1] << 8) | gyro[0],
      (gyro[3] << 8) | gyro[2],
      (gyro[5] << 8) | gyro[4]
    ])
  }

  /**
   * Función que transforma datos de orientación cuaternaria uint8 o char a int16. Recibe todo el vector no formateado de la forma 
   * [qw_lsb,qw_msb,qx_lsb,qx_msb,qy_lsb,qy_msb,qz_lsb,qz_msb].
   * Retorna el vector transformado a entero de 16 bits (2 bytes) con signo de la forma [qw,qx,qy,qz]
   * @param quat 
   */
  format_quat_data(quat) {
    return new Int16Array([
      (quat[1] << 8) | quat[0],
      (quat[3] << 8) | quat[2],
      (quat[5] << 8) | quat[4],
      (quat[7] << 8) | quat[6]
    ])
  }

  /************  FIN TRANSFORMACION DE DATOS  *********************/


}

