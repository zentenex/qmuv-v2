import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ConfigDaoService } from '../dao/config-dao.service';

@Injectable({
  providedIn: 'root'
})
export class TranslateConfigService {

  constructor(
    private translate: TranslateService,
    private configDAO: ConfigDaoService, 
  ) { }

  async inicializaLanguage() {

    console.log("Busco idioma guardado")
    let language = await this.configDAO.idioma();
    console.log("El idioma guardado es : " + language)
    if(language == null) {
      //Obtengo el idioma del navegador
      language = this.translate.getBrowserLang();
      console.log("Obtengo el idioma del navegador : " + language);
    } 

    //Obtengo el idioma del navegador
    //let language = this.translate.getBrowserLang();

    if(language == null) {
      language = "en";
    }

    //Uso el lenguaje del navegador
    await this.translate.use(language);

    //En caso de ser desconocido, uso el lenguaje ingles
    this.translate.setDefaultLang(language);
  }

  getLangObs() {
    return this.translate.onLangChange
  }

  getCurrentLanguage() {
    let lengActual = this.translate.currentLang;
    console.log("Actualmente uso el lang: " + lengActual)
    return lengActual
  }

  setLanguage(setLang: string) {
    this.translate.use(setLang);
  }

  getTexto(texto: string) {
    return this.translate.get(texto)
  }

  getTextoNow(texto: string) {
    return this.translate.get(texto).toPromise();
  }
}
