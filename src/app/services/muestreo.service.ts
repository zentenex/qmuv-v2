import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UtilsService } from './utils.service';

@Injectable({
  providedIn: 'root'
})
export class MuestreoService {

  //Indica si ya se han recibido datos
  public recibiendo = false;

  //arreglo de integer para debug
  private _debugRaw: any;
  public get debugRaw() {
    return this._debugRaw;
  }

  //Las muestras de datos que se van recibiendo
  private _muestra: Muestreo;
  public get muestra() {
    return this._muestra;
  }

  /**
   * Segundos transcurridos desde que llego el primer dato del muestreo actual
   * Se utiliza para el cronometro en pantalla
   */
   private _segundos = 0;
   public get segundos() {
     return this._segundos;
   }

   /**
    * Indica el tiempo en el que comenzo una toma de muestra
    */
   private tiempoStart: Date;
 
   /**
    * Indica el tiempo en el que comenzo una toma de muestra
    */
   private tiempoStop: Date;

  constructor(private utilsService: UtilsService) {
    //Inicializamos el cronometro para que corra cada 1 segundo
    setInterval(() => this.cronometro(), 1000);
  }

  resetMuestreo() {
    //Indico que no he recibido ningun dato. Esto se usa para saber desde cuando empezar a contar el tiempo del cronometro
    this.recibiendo = false;

    //Creamos una nueva instancia para una nueva muestra
    this._muestra = new Muestreo();

    //Registramos el tiempo de partida de la muestra
    //this.tiempoStart = new Date();

    //Cuenta en cero
    this._segundos = 0;
  }

  cronometro() {
    if (this.recibiendo) {
      this.tiempoStop = new Date();
      let milisegundos = this.tiempoStop.getTime() - this.tiempoStart.getTime();
      this._segundos = Math.round(milisegundos / 1000);
    }
  }

  /**
   * Metodo que toma un paquete recibido desde el QMUV y lo convierte en los datos que se agregan a la muestra
   * El paquete es de forma [tmp0,ax_lsb0,ax_msb0,ay_lsb0,ay_msb0,az_lsb0,az_msb0,gx_lsb0,gx_msb0,gy_lsb0,gy_msb0,gz_lsb0,gz_msb0,qw_lsb0,qw_msb0,qx_lsb0,qx_msb0,qy_lsb0,qy_msb0,qz_lsb0,qz_msb0, ... ,tmp4,ax_lsb4,ax_msb4,ay_lsb4,ay_msb4,az_lsb4,az_msb4,gx_lsb4,gx_msb4,gy_lsb4,gy_msb4,gz_lsb4,gz_msb4,qw_lsb4,qw_msb4,qx_lsb4,qx_msb4,qy_lsb4,qy_msb4,qz_lsb4,qz_msb4]
   * @param buffer Paquete con toda la informacion de los sensores correspondiente a 5 muestras
   */
   public onData(buffer: ArrayBuffer) {

    //this.utilsService.presentaToastDebug("Se envia flag ADQ a QMUV");

    //Indico que he recibido datos
    if(!this.recibiendo) {
      this.tiempoStart = new Date();
      this.recibiendo = true;
    }

    //Convierto el buffer recibido desde el QMUV en un arreglo de integer
    const data = new Uint8Array(buffer);

    //Guardo un respaldo de arreglo de integer por si quiero hacer debug
    this._debugRaw = data;

    //Formateamos el arreglo de numeros de 8 bits en numeros de 16 bits
    let data_all = this.format_data(data);

    //Agregamos a los arreglos de la muestra, los valores transformados a las unidades fisicas usadas por el algoritmo
    
    this._muestra.c.push(data_all[0], data_all[11], data_all[22], data_all[33], data_all[44]);
    this._muestra.ax.push(data_all[1] / 1000.0, data_all[12] / 1000.0, data_all[23] / 1000.0, data_all[34] / 1000.0, data_all[45] / 1000.0);
    this._muestra.ay.push(data_all[2] / 1000.0, data_all[13] / 1000.0, data_all[24] / 1000.0, data_all[35] / 1000.0, data_all[46] / 1000.0);
    this._muestra.az.push(data_all[3] / 1000.0, data_all[14] / 1000.0, data_all[25] / 1000.0, data_all[36] / 1000.0, data_all[47] / 1000.0);
    this._muestra.gx.push(data_all[4] / 16.0, data_all[15] / 16.0, data_all[26] / 16.0, data_all[37] / 16.0, data_all[48] / 16.0);
    this._muestra.gy.push(data_all[5] / 16.0, data_all[16] / 16.0, data_all[27] / 16.0, data_all[38] / 16.0, data_all[49] / 16.0);
    this._muestra.gz.push(data_all[6] / 16.0, data_all[17] / 16.0, data_all[28] / 16.0, data_all[39] / 16.0, data_all[50] / 16.0);
    this._muestra.qw.push(data_all[7] / 16384.0, data_all[18] / 16384.0, data_all[29] / 16384.0, data_all[40] / 16384.0, data_all[51] / 16384.0);
    this._muestra.qx.push(data_all[8] / 16384.0, data_all[19] / 16384.0, data_all[30] / 16384.0, data_all[41] / 16384.0, data_all[52] / 16384.0);
    this._muestra.qy.push(data_all[9] / 16384.0, data_all[20] / 16384.0, data_all[31] / 16384.0, data_all[42] / 16384.0, data_all[53] / 16384.0);
    this._muestra.qz.push(data_all[10] / 16384.0, data_all[21] / 16384.0, data_all[32] / 16384.0, data_all[43] / 16384.0, data_all[54] / 16384.0);
    
  }

  /**
   * Función que transforma todos los datos medidos int8 o char a int16. Recibe todo el vector no formateado de la forma [tmp0,ax_lsb0,ax_msb0,ay_lsb0,ay_msb0,az_lsb0,az_msb0,gx_lsb0,gx_msb0,gy_lsb0,gy_msb0,gz_lsb0,gz_msb0,qw_lsb0,qw_msb0,qx_lsb0,qx_msb0,qy_lsb0,qy_msb0,qz_lsb0,qz_msb0, ... ,tmp4,ax_lsb4,ax_msb4,ay_lsb4,ay_msb4,az_lsb4,az_msb4,gx_lsb4,gx_msb4,gy_lsb4,gy_msb4,gz_lsb4,gz_msb4,qw_lsb4,qw_msb4,qx_lsb4,qx_msb4,qy_lsb4,qy_msb4,qz_lsb4,qz_msb4]
   * 
   * Retorna el vector transformado a entero de 16 bits (2 bytes) con signo de la forma [tmp0,ax0,ay0,az0,gx0,gy0,gz0,qw0,qx0,qy0,qz0, ... , tmp4,ax4,ay4,az4,gx4,gy4,gz4,qw4,qx4,qy4,qz4] 
   * */
   format_data(data){
    return new Int16Array([data[0],(data[2]<<8)|data[1],(data[4]<<8)|data[3],(data[6]<<8)|data[5],(data[8]<<8)|data[7],(data[10]<<8)|data[9],(data[12]<<8)|data[11],(data[14]<<8)|data[13],(data[16]<<8)|data[15],(data[18]<<8)|data[17],(data[20]<<8)|data[19],data[21],(data[23]<<8)|data[22],(data[25]<<8)|data[24],(data[27]<<8)|data[26],(data[29]<<8)|data[28],(data[31]<<8)|data[30],(data[33]<<8)|data[32],(data[35]<<8)|data[34],(data[37]<<8)|data[36],(data[39]<<8)|data[38],(data[41]<<8)|data[40],data[42],(data[44]<<8)|data[43],(data[46]<<8)|data[45],(data[48]<<8)|data[47],(data[50]<<8)|data[49],(data[52]<<8)|data[51],(data[54]<<8)|data[53],(data[56]<<8)|data[55],(data[58]<<8)|data[57],(data[60]<<8)|data[59],(data[62]<<8)|data[61],data[63],(data[65]<<8)|data[64],(data[67]<<8)|data[66],(data[69]<<8)|data[68],(data[71]<<8)|data[70],(data[73]<<8)|data[72],(data[75]<<8)|data[74],(data[77]<<8)|data[76],(data[79]<<8)|data[78],(data[81]<<8)|data[80],(data[83]<<8)|data[82],data[84],(data[86]<<8)|data[85],(data[88]<<8)|data[87],(data[90]<<8)|data[89],(data[92]<<8)|data[91],(data[94]<<8)|data[93],(data[96]<<8)|data[95],(data[98]<<8)|data[97],(data[100]<<8)|data[99],(data[102]<<8)|data[101],(data[104]<<8)|data[103]])
  }

  public onDataDummy_UPGO() {
    if(!this.recibiendo) {
      this.tiempoStart = new Date();
      this.recibiendo = true;
    }
      this._muestra.ax = environment.pruebas[environment.PRUEBA_UPANDGO_CODE].sensor_data_dummy.ax;
      this._muestra.ay = environment.pruebas[environment.PRUEBA_UPANDGO_CODE].sensor_data_dummy.ay;
      this._muestra.az = environment.pruebas[environment.PRUEBA_UPANDGO_CODE].sensor_data_dummy.az;

      this._muestra.gx = environment.pruebas[environment.PRUEBA_UPANDGO_CODE].sensor_data_dummy.gx;
      this._muestra.gy = environment.pruebas[environment.PRUEBA_UPANDGO_CODE].sensor_data_dummy.gy;
      this._muestra.gz = environment.pruebas[environment.PRUEBA_UPANDGO_CODE].sensor_data_dummy.gz;

      this._muestra.qw = environment.pruebas[environment.PRUEBA_UPANDGO_CODE].sensor_data_dummy.qw;
      this._muestra.qx = environment.pruebas[environment.PRUEBA_UPANDGO_CODE].sensor_data_dummy.qx;
      this._muestra.qy = environment.pruebas[environment.PRUEBA_UPANDGO_CODE].sensor_data_dummy.qy;
      this._muestra.qz = environment.pruebas[environment.PRUEBA_UPANDGO_CODE].sensor_data_dummy.qz;
  }

  public onDataDummy_10MTS() {
    if(!this.recibiendo) {
      this.tiempoStart = new Date();
      this.recibiendo = true;
    }
    this._muestra.ax = environment.pruebas[environment.PRUEBA_DIEZMTS_CODE].sensor_data_dummy.ax;
      this._muestra.ay = environment.pruebas[environment.PRUEBA_DIEZMTS_CODE].sensor_data_dummy.ay;
      this._muestra.az = environment.pruebas[environment.PRUEBA_DIEZMTS_CODE].sensor_data_dummy.az;

      this._muestra.gx = environment.pruebas[environment.PRUEBA_DIEZMTS_CODE].sensor_data_dummy.gx;
      this._muestra.gy = environment.pruebas[environment.PRUEBA_DIEZMTS_CODE].sensor_data_dummy.gy;
      this._muestra.gz = environment.pruebas[environment.PRUEBA_DIEZMTS_CODE].sensor_data_dummy.gz;

      this._muestra.qw = environment.pruebas[environment.PRUEBA_DIEZMTS_CODE].sensor_data_dummy.qw;
      this._muestra.qx = environment.pruebas[environment.PRUEBA_DIEZMTS_CODE].sensor_data_dummy.qx;
      this._muestra.qy = environment.pruebas[environment.PRUEBA_DIEZMTS_CODE].sensor_data_dummy.qy;
      this._muestra.qz = environment.pruebas[environment.PRUEBA_DIEZMTS_CODE].sensor_data_dummy.qz;
  }


}

/**
 * Datos de una muestra para ejecutar el algoritmo, obtenidos desde el sensor QMUV
 * */
 export class Muestreo {
  //Contador
  c: number[] = [];
  //Aceleraciones
  ax: number[] = [];
  ay: number[] = [];
  az: number[] = [];
  //Velocidades angulares
  gx: number[] = [];
  gy: number[] = [];
  gz: number[] = [];
  //Quaternions
  qw: number[] = [];
  qx: number[] = [];
  qy: number[] = [];
  qz: number[] = [];
}